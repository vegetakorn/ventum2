<?php

namespace App\Exports;

use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
Use \Maatwebsite\Excel\Sheet;

class DesempenoExport implements  FromView
{
    /**
     * @return \Illuminate\Support\Collection
     */

    public function __construct($inputs)
    {
        $this->inputs = $inputs;
    }


    public function view(): View
    {

        return view('exports.desempeno', [
            'info' => $this->inputs
        ]);
    }


}
