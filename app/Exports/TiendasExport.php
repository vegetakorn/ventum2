<?php

namespace App\Exports;

use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
Use \Maatwebsite\Excel\Sheet;

class TiendasExport implements  FromView, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */

    public function __construct($inputs)
   {
       $this->inputs = $inputs;
   }

    /*public function collection()
     {
         $sql  = DB::table('users_tiendas')
             ->leftjoin('tiendas', function ($join) {
                 $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
             })
             ->select('tiendas.numsuc', 'tiendas.nombre', 'tiendas.mail' );
         $sql->where('users_tiendas.users_Id', "=", auth()->user()->id);
         $sql->where('tiendas.activo', "=", 1);
         $data = $sql->get();


         return  $data;
     }

      public function headings(): array
      {
          return [
              'Clave',
              'Nombre',
              'Mail'
          ];
      }*/

    /*public function registerEvents(): array
    {

    }*/

    public function view(): View
    {
        $sql  = DB::table('users_tiendas')
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
            })
            ->leftjoin('razon_social', function ($join) {
                $join->on('razon_social.Id', '=', 'tiendas.razon_Id');
            })
            ->leftjoin('plazas', function ($join) {
                $join->on('plazas.Id', '=', 'tiendas.plaza_Id');
            })
            ->select('tiendas.numsuc', 'tiendas.nombre', 'tiendas.mail', 'razon_social.nombre as razon', 'plazas.plaza' );
        $sql->where('users_tiendas.users_Id', "=", auth()->user()->id);
        $sql->where('tiendas.activo', "=", 1);
        $data = $sql->get();


        return view('exports.tiendas', [
            'tiendas' => $data,
        ]);
    }
}
