<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mail\MailPassSuc;
use Mail;
use Illuminate\Mail\Mailable;
use Swift_Mailer;
use \Swift_SmtpTransport as SmtpTransport;
use DB;

class asigUsersTienda extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:passsuc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $arr = array();
        $arr[] = array('Id' => 7028, 'name' => 'Tulancingo Truper' , 'mail' => 'fsoto@ferreprecios.com.mx', 'pass' => 'fso163');
        $arr[] = array('Id' => 7029, 'name' => 'FIX Libertad' , 'mail' => 'practicotulancingo@ferreprecios.com', 'pass' => 'pra164');
        $arr[] = array('Id' => 7030, 'name' => 'Hidalgo' , 'mail' => 'hidalgo@ferreprecios.com.mx', 'pass' => 'hid165');
        $arr[] = array('Id' => 7031, 'name' => 'Fix Tulancingo' , 'mail' => 'fixtulancingo@ferreprecios.com.mx', 'pass' => 'fix166');
        $arr[] = array('Id' => 7032, 'name' => 'Atitalaquia (Ferreprecios)' , 'mail' => 'atitalaquia@ferreprecios.com', 'pass' => 'ati167');
        $arr[] = array('Id' => 7033, 'name' => 'Tula' , 'mail' => 'tula@ferreprecios.com.mx', 'pass' => 'tul168');
        $arr[] = array('Id' => 7034, 'name' => 'Mayoreo Cuernavaca' , 'mail' => 'l.tellez@ferreprecios.com.mx', 'pass' => 'lt184');
        $arr[] = array('Id' => 7036, 'name' => 'FIX Huauchinango' , 'mail' => 'huauchinango1@ferreprecios.com.mx', 'pass' => 'hua171');
        $arr[] = array('Id' => 7037, 'name' => 'Huauchinango 2' , 'mail' => 'huauchinango2@ferreprecios.com.mx', 'pass' => 'hua172');
        $arr[] = array('Id' => 7038, 'name' => 'Necaxa (Ferreprecios)' , 'mail' => 'necaxa@ferreprecios.com.mx', 'pass' => 'nec173');
        $arr[] = array('Id' => 7039, 'name' => 'Villa Juarez 1' , 'mail' => 'villajuarez1@ferreprecios.com.mx', 'pass' => 'vil174');
        $arr[] = array('Id' => 7040, 'name' => 'FIX Xicotepec' , 'mail' => 'villajuarez2@ferreprecios.com.mx', 'pass' => 'vil175');
        $arr[] = array('Id' => 7041, 'name' => 'Poza Rica' , 'mail' => 'pozarica@ferreprecios.com.mx', 'pass' => 'poz176');
        $arr[] = array('Id' => 7042, 'name' => 'FIX Poza Rica' , 'mail' => 'fixpozarica@ferreprecios.com', 'pass' => 'poz177');
        $arr[] = array('Id' => 7043, 'name' => 'FIX Cuautla' , 'mail' => 'cuautla@ferreprecios.com.mx', 'pass' => 'cua178');
        $arr[] = array('Id' => 7044, 'name' => 'FIX Jojutla' , 'mail' => 'delsur@ferreprecios.com.mx', 'pass' => 'del179');
        $arr[] = array('Id' => 7045, 'name' => 'Practico Jojutla' , 'mail' => 'practicojojutla@ferreprecios.com.mx', 'pass' => 'pra180');
        $arr[] = array('Id' => 7046, 'name' => 'FIX Zacatepec' , 'mail' => 'zacatepec@ferreprecios.com.mx', 'pass' => 'zac181');
        $arr[] = array('Id' => 7047, 'name' => 'FIX Cuernavaca' , 'mail' => 'cuernavaca@ferreprecios.com.mx', 'pass' => 'cue182');
        $arr[] = array('Id' => 7049, 'name' => 'Mayoreo Tulancingo' , 'mail' => 'c.garcia@ferreprecios.com.mx', 'pass' => 'cg169');
        $arr[] = array('Id' => 7010033, 'name' => 'Zapata' , 'mail' => 'zapata@pinturas57.com.mx', 'pass' => 'zapata12');
        $arr[] = array('Id' => 7010034, 'name' => 'Anenecuilco' , 'mail' => 'anenecuilco@pinturas57.com.mx', 'pass' => 'ane93');
        $arr[] = array('Id' => 7010035, 'name' => '2 de Mayo' , 'mail' => '2demayo@pinturas57.com.mx', 'pass' => '12345');
        $arr[] = array('Id' => 7010036, 'name' => 'Alcanfores' , 'mail' => 'alcanfores@pinturas57.com.mx', 'pass' => 'alc96');
        $arr[] = array('Id' => 7010037, 'name' => 'Amilcingo' , 'mail' => 'amilcingo@pinturas57.com.mx', 'pass' => 'ami97');
        $arr[] = array('Id' => 7010038, 'name' => 'Centro' , 'mail' => 'centro@pinturas57.com.mx', 'pass' => 'cen98');
        $arr[] = array('Id' => 7010039, 'name' => 'Insurgentes' , 'mail' => 'insurgentes@pinturas57.com.mx', 'pass' => 'ins99');
        $arr[] = array('Id' => 7010040, 'name' => 'Brisas' , 'mail' => 'brisas@pinturas57.com.mx', 'pass' => 'bri100');
        $arr[] = array('Id' => 7010041, 'name' => 'Reforma' , 'mail' => 'reforma@pinturas57.com.mx', 'pass' => 'ref101');
        $arr[] = array('Id' => 7010042, 'name' => 'San Jose (Pinturas 57)' , 'mail' => 'sanjose@pinturas57.com.mx', 'pass' => 'jose2');
        $arr[] = array('Id' => 7010043, 'name' => 'Cuautlixco' , 'mail' => 'cuautlixco@pinturas57.com.mx', 'pass' => 'cua103');
        $arr[] = array('Id' => 7010044, 'name' => 'Pino Suarez' , 'mail' => 'pinosuarez@pinturas57.com.mx', 'pass' => 'pin105');
        $arr[] = array('Id' => 7010045, 'name' => 'Juan Morales' , 'mail' => 'juanmorales@pinturas57.com.mx', 'pass' => 'jua106');
        $arr[] = array('Id' => 7010046, 'name' => 'Unila' , 'mail' => 'unila@pinturas57.com.mx', 'pass' => 'uni107');
        $arr[] = array('Id' => 7010047, 'name' => 'Valle Sol' , 'mail' => 'valledelsol@pinturas57.com.mx', 'pass' => 'val108');
        $arr[] = array('Id' => 7010048, 'name' => 'Acatlan' , 'mail' => 'acatlan@pingol.com.mx', 'pass' => 'aca1');
        $arr[] = array('Id' => 7010049, 'name' => 'Matriz' , 'mail' => 'matriz@pingol.com.mx', 'pass' => 'mat2');
        $arr[] = array('Id' => 7010050, 'name' => 'Acaxochitlan' , 'mail' => 'acaxochitlan@pingol.com.mx', 'pass' => 'aca3');
        $arr[] = array('Id' => 7010051, 'name' => 'Cuautepec' , 'mail' => 'cuautepec@pingol.com.mx', 'pass' => 'cua4');
        $arr[] = array('Id' => 7010052, 'name' => 'Santiago' , 'mail' => 'santiago@pingol.com.mx', 'pass' => 'san5');
        $arr[] = array('Id' => 7010053, 'name' => 'Color Car' , 'mail' => 'colorcar@pingol.com.mx', 'pass' => 'col6');
        $arr[] = array('Id' => 7010054, 'name' => 'Y Griega' , 'mail' => 'ygriega@pingol.com.mx', 'pass' => 'ygr7');
        $arr[] = array('Id' => 7010055, 'name' => 'La Abeja' , 'mail' => 'abeja@pingol.com.mx', 'pass' => 'abe8');
        $arr[] = array('Id' => 7010057, 'name' => '21 De Marzo' , 'mail' => '21demarzo@pingol.com.mx', 'pass' => 'Coa1819');
        $arr[] = array('Id' => 7010058, 'name' => 'Boulevard' , 'mail' => 'boulevard@pingol.com.mx', 'pass' => 'bou11');
        $arr[] = array('Id' => 7010059, 'name' => 'Central' , 'mail' => 'central@pingol.com.mx', 'pass' => 'cen12');
        $arr[] = array('Id' => 7010060, 'name' => 'Glorieta' , 'mail' => 'glorieta@pingol.com.mx', 'pass' => 'glo13');
        $arr[] = array('Id' => 7010061, 'name' => 'Jardines' , 'mail' => 'jardines@pingol.com.mx', 'pass' => 'pla66');
        $arr[] = array('Id' => 7010062, 'name' => 'Santa Ana Hueytlalpan' , 'mail' => 'santaanahueytlalpan@pingol.com.mx', 'pass' => 'san15');
        $arr[] = array('Id' => 7010063, 'name' => 'Galerias' , 'mail' => 'galerias@pingol.com.mx', 'pass' => 'gal16');
        $arr[] = array('Id' => 7010064, 'name' => 'Huauchinango' , 'mail' => 'huauchinango@pingol.com.mx', 'pass' => 'hua17');
        $arr[] = array('Id' => 7010065, 'name' => 'Necaxa (Pingol)' , 'mail' => 'necaxa@pingol.com.mx', 'pass' => 'nec18');
        $arr[] = array('Id' => 7010066, 'name' => 'Xicotepec' , 'mail' => 'xicotepec@pingol.com.mx', 'pass' => 'xic19');
        $arr[] = array('Id' => 7010067, 'name' => 'Villa' , 'mail' => 'villa@pingol.com.mx', 'pass' => 'vil20');
        $arr[] = array('Id' => 7010068, 'name' => 'Automotriz' , 'mail' => 'automotriz@pingol.com.mx', 'pass' => 'aut21');
        $arr[] = array('Id' => 7010069, 'name' => 'Ahuazotepec (Pingol)' , 'mail' => 'ahuazotepec@pingol.com.mx', 'pass' => 'ahu22');
        $arr[] = array('Id' => 7010070, 'name' => 'La Villita' , 'mail' => 'lavillita@pingol.com.mx', 'pass' => 'lav23');
        $arr[] = array('Id' => 7010071, 'name' => 'Xico' , 'mail' => 'xico@pingol.com.mx', 'pass' => 'xic24');
        $arr[] = array('Id' => 7010072, 'name' => 'Huachi' , 'mail' => 'huauchi@pingol.com.mx', 'pass' => 'hua25');
        $arr[] = array('Id' => 7010073, 'name' => 'Alamo' , 'mail' => 'alamo1@pingol.com.mx', 'pass' => 'ala26');
        $arr[] = array('Id' => 7010074, 'name' => 'Alamo II' , 'mail' => 'alamo2@pingol.com.mx', 'pass' => 'ala27');
        $arr[] = array('Id' => 7010075, 'name' => 'Cerro Azul' , 'mail' => 'cerroazul@pingol.com.mx', 'pass' => 'cer28');
        $arr[] = array('Id' => 7010076, 'name' => 'Cazones' , 'mail' => 'cazones@pingol.com.mx', 'pass' => 'caz29');
        $arr[] = array('Id' => 7010077, 'name' => 'Coatzintla' , 'mail' => 'coatzintla@pingol.com.mx', 'pass' => 'coa30');
        $arr[] = array('Id' => 7010078, 'name' => 'Cedis Poza Rica' , 'mail' => 'bodegapozarica@pingol.com.mx', 'pass' => 'bod31');
        $arr[] = array('Id' => 7010079, 'name' => '20 De Noviembre' , 'mail' => '20denoviembre@pingol.com.mx', 'pass' => '20d32');
        $arr[] = array('Id' => 7010080, 'name' => 'La 52 (Pingol)' , 'mail' => '52@pingol.com.mx', 'pass' => '5233');
        $arr[] = array('Id' => 7010081, 'name' => 'Macrocomex' , 'mail' => 'macrocomex@pingol.com.mx', 'pass' => 'mac34');
        $arr[] = array('Id' => 7010083, 'name' => 'Petromex' , 'mail' => 'petromex@pingol.com.mx', 'pass' => 'pet36');
        $arr[] = array('Id' => 7010084, 'name' => 'Pozo 13' , 'mail' => 'pozo13@pingol.com.mx', 'pass' => 'Gaviota18');
        $arr[] = array('Id' => 7010085, 'name' => 'Triangulo' , 'mail' => 'triangulo@pingol.com.mx', 'pass' => 'tri38');
        $arr[] = array('Id' => 7010086, 'name' => 'Juarez' , 'mail' => 'juarez@pingol.com.mx', 'pass' => 'jua39');
        $arr[] = array('Id' => 7010087, 'name' => 'Garibaldi' , 'mail' => 'garibaldi@pingol.com.mx', 'pass' => 'gar40');
        $arr[] = array('Id' => 7010088, 'name' => 'Bancos' , 'mail' => 'bancos@pingol.com.mx', 'pass' => 'ban41');
        $arr[] = array('Id' => 7010089, 'name' => 'Proservice Poza Rica' , 'mail' => 'proservice@pingol.com.mx', 'pass' => 'pro42');
        $arr[] = array('Id' => 7010090, 'name' => 'Totolapa' , 'mail' => 'totolapa@pingol.com.mx', 'pass' => 'tot43');
        $arr[] = array('Id' => 7010091, 'name' => 'Gaviotas' , 'mail' => 'gaviotas@pingol.com.mx', 'pass' => 'Gaviota18');
        $arr[] = array('Id' => 7010092, 'name' => 'Carpintienda' , 'mail' => 'carpintienda@pingol.com.mx', 'pass' => 'car45');
        $arr[] = array('Id' => 7010093, 'name' => 'Puebla' , 'mail' => 'puebla@pingol.com.mx', 'pass' => 'pue46');
        $arr[] = array('Id' => 7010094, 'name' => 'Federal De Caminos' , 'mail' => 'federal@pingol.com.mx', 'pass' => 'Federa.18');
        $arr[] = array('Id' => 7010095, 'name' => 'Tihuatlan' , 'mail' => 'tihuatlan@pingol.com.mx', 'pass' => 'tih48');
        $arr[] = array('Id' => 7010096, 'name' => 'Jara' , 'mail' => 'jara@pingol.com.mx', 'pass' => 'jar49');
        $arr[] = array('Id' => 7010097, 'name' => 'Atitalaquia (Pingol)' , 'mail' => 'atitalaquia@pingol.com.mx', 'pass' => 'ati58');
        $arr[] = array('Id' => 7010098, 'name' => 'Atotonilco' , 'mail' => 'atotonilco@pingol.com.mx', 'pass' => 'ato51');
        $arr[] = array('Id' => 7010099, 'name' => 'Cruz Azul' , 'mail' => 'cruzazul@pingol.com.mx', 'pass' => 'cru52');
        $arr[] = array('Id' => 7010100, 'name' => 'San Marcos' , 'mail' => 'sanmarcos@pingol.com.mx', 'pass' => 'san53');
        $arr[] = array('Id' => 7010101, 'name' => 'Crucero' , 'mail' => 'crucero@pingol.com.mx', 'pass' => 'cru54');
        $arr[] = array('Id' => 7010102, 'name' => 'Tlaxcoapan' , 'mail' => 'tlaxcoapan@pingol.com.mx', 'pass' => 'tla55');
        $arr[] = array('Id' => 7010103, 'name' => 'Oficinas' , 'mail' => 'e.contreras@pingol.com.mx', 'pass' => 'ec56');
        $arr[] = array('Id' => 7010104, 'name' => 'Cedis Tula' , 'mail' => 'cedistula@pingol.com.mx', 'pass' => 'atlantes18');
        $arr[] = array('Id' => 7010105, 'name' => 'Mercado' , 'mail' => 'mercado@pingol.com.mx', 'pass' => 'mer58');
        $arr[] = array('Id' => 7010106, 'name' => 'Refineria' , 'mail' => 'refineria@pingol.com.mx', 'pass' => 'ref60');
        $arr[] = array('Id' => 7010107, 'name' => 'Colegio Militar' , 'mail' => 'colegiomilitar@pingol.com.mx', 'pass' => 'col60');
        $arr[] = array('Id' => 7010108, 'name' => 'Ocampo' , 'mail' => 'ocampo@pingol.com.mx', 'pass' => 'ocampo2018');
        $arr[] = array('Id' => 7010109, 'name' => 'Apaxco' , 'mail' => 'apaxco@pingol.com.mx', 'pass' => 'apa62');
        $arr[] = array('Id' => 7010111, 'name' => 'Conejos (Pingol Tula)' , 'mail' => 'conejos@pingol.com.mx', 'pass' => 'con237');
        $arr[] = array('Id' => 7010112, 'name' => 'Atlantes' , 'mail' => 'atlantes@pingol.com.mx', 'pass' => 'atlantes53');
        $arr[] = array('Id' => 7010113, 'name' => 'Jardines De Tula' , 'mail' => 'plazajardines@pingol.com.mx', 'pass' => 'pla66');
        $arr[] = array('Id' => 7010114, 'name' => 'Tecamac' , 'mail' => 'tecamac@pingol.com.mx', 'pass' => 'tec67');
        $arr[] = array('Id' => 7010115, 'name' => 'Pabellon' , 'mail' => 'pabellon@pingol.com.mx', 'pass' => 'Legua.18');
        $arr[] = array('Id' => 7010116, 'name' => 'Tezontepec' , 'mail' => 'tezontepec@pingol.com.mx', 'pass' => 'tez69');
        $arr[] = array('Id' => 7010117, 'name' => 'Ejercito' , 'mail' => 'ejercito@pingol.com.mx', 'pass' => 'eje70');
        $arr[] = array('Id' => 7010118, 'name' => 'Tizayuca' , 'mail' => 'tizayuca1@pingol.com.mx', 'pass' => 'tiz71');
        $arr[] = array('Id' => 7010119, 'name' => 'Tizayuca Il' , 'mail' => 'tizayuca2@pingol.com.mx', 'pass' => 'tiz72');
        $arr[] = array('Id' => 7010120, 'name' => 'Mina' , 'mail' => 'mina@pingol.com.mx', 'pass' => 'min73');
        $arr[] = array('Id' => 7010121, 'name' => 'Bicentenario' , 'mail' => 'bicentenario@pingol.com.mx', 'pass' => 'bic74');
        $arr[] = array('Id' => 7010122, 'name' => 'NiĂ±os' , 'mail' => 'ninos@pingol.com.mx', 'pass' => 'nin75');
        $arr[] = array('Id' => 7010123, 'name' => 'Tolcayuca' , 'mail' => 'tolcayuca@pingol.com.mx', 'pass' => 'tol76');
        $arr[] = array('Id' => 7010124, 'name' => 'Galeana' , 'mail' => 'galeana@pinturas57.com.mx', 'pass' => 'alexander1');
        $arr[] = array('Id' => 7010125, 'name' => 'Auditorio' , 'mail' => 'auditorio@pingol.com.mx', 'pass' => 'mimobem2');
        $arr[] = array('Id' => 7010126, 'name' => 'Tecnica' , 'mail' => 'tecnica@pinturas57.com.mx', 'pass' => 'arnulfo03');
        $arr[] = array('Id' => 7010127, 'name' => 'Universidad' , 'mail' => 'universidad@pinturas57.com.mx', 'pass' => 'univ2018');
        $arr[] = array('Id' => 7010128, 'name' => 'Cedis Jojutla' , 'mail' => 'cedisjojutla@pinturas57.com.mx', 'pass' => 'ced81');
        $arr[] = array('Id' => 7010129, 'name' => 'Puente De Ixtla' , 'mail' => 'puentedeixtla@pinturas57l.com.mx', 'pass' => '5t2tyxh6');
        $arr[] = array('Id' => 7010130, 'name' => 'San Mateo' , 'mail' => 'sanmateo@pinturas57.com.mx', 'pass' => 'eh3yxace');
        $arr[] = array('Id' => 7010131, 'name' => 'Tehuixtla' , 'mail' => 'tehuixtla@pinturas57.com.mx', 'pass' => '96zkfrn3');
        $arr[] = array('Id' => 7010132, 'name' => 'Tequesquitengo' , 'mail' => 'tequesquitengo@pinturas57.com.mx', 'pass' => 'ush325tp');
        $arr[] = array('Id' => 7010133, 'name' => 'Tlaltizapan' , 'mail' => 'tlaltizapan@pingol.com.mx', 'pass' => '28405');
        $arr[] = array('Id' => 7010134, 'name' => 'Tlaquiltenango' , 'mail' => 'tlaquiltenango@pingol.com.mx', 'pass' => 'tlaquim4');
        $arr[] = array('Id' => 7010135, 'name' => 'Xoxocotla' , 'mail' => 'xoxocotla@pingol.com.mx', 'pass' => 'aranda');
        $arr[] = array('Id' => 7010136, 'name' => 'Zacatepec' , 'mail' => 'zacatepec@pinturas57.com.mx', 'pass' => 'hz7ngk3a');
        $arr[] = array('Id' => 7010137, 'name' => 'Vista Hermosa' , 'mail' => 'vistahermosa@pinturas57.com.mx', 'pass' => 'tfzerrxz');
        $arr[] = array('Id' => 7010138, 'name' => 'Santa Rosa 30' , 'mail' => 'santarosa30@pingol.com.mx', 'pass' => 'JOSE2830');
        $arr[] = array('Id' => 7010139, 'name' => 'Cedis Cuautla' , 'mail' => 'bodegacuautla@pinturas57.com.mx', 'pass' => 'bod94');
        $arr[] = array('Id' => 7010140, 'name' => 'Tenextepango' , 'mail' => 'tenextepango@pinturas57.com.mx', 'pass' => 'ten104');
        $arr[] = array('Id' => 7010141, 'name' => 'Allende' , 'mail' => 'allende@pinturas57.com.mx', 'pass' => 'sucsamao');
        $arr[] = array('Id' => 7010142, 'name' => 'Los Fresnos' , 'mail' => 'fresnos@pingol.com.mx', 'pass' => 'FRES402');
        $arr[] = array('Id' => 7010143, 'name' => 'Colosio' , 'mail' => 'colosio@pinturas57.com.mx', 'pass' => 'succolosio');
        $arr[] = array('Id' => 7010144, 'name' => 'Flores Magon' , 'mail' => 'floresmagon@pinturas57.com.mx', 'pass' => 'sucflores');
        $arr[] = array('Id' => 7010145, 'name' => 'La Cruz' , 'mail' => 'lacruz@pinturas57.com.mx', 'pass' => 'suclacruz');
        $arr[] = array('Id' => 7010146, 'name' => 'Mololoa' , 'mail' => 'mololoa@pinturas57.com.mx', 'pass' => 'mololoa1');
        $arr[] = array('Id' => 7010147, 'name' => 'Rio Suchiate' , 'mail' => 'riosuchiate@pinturas57.com.mx', 'pass' => 'rio1994');
        $arr[] = array('Id' => 7010148, 'name' => 'San Juan' , 'mail' => 'sanjuan@pinturas57.com.mx', 'pass' => 'sanjuan1');
        $arr[] = array('Id' => 7010149, 'name' => 'Soriana' , 'mail' => 'soriana@pinturas57.com.mx', 'pass' => 'soriana1');
        $arr[] = array('Id' => 7010150, 'name' => 'Alameda' , 'mail' => 'alameda@pinturas57.com.mx', 'pass' => 'ala118');
        $arr[] = array('Id' => 7010151, 'name' => 'La Cantera' , 'mail' => 'cantera@pinturas57.com.mx', 'pass' => 'Lacantera');
        $arr[] = array('Id' => 7010152, 'name' => 'Xalisco' , 'mail' => 'xalisco@pinturas57.com.mx', 'pass' => 'sucxalisco');
        $arr[] = array('Id' => 7020001, 'name' => 'Cedis Juarez' , 'mail' => 'bodegacdjuarez@ppj.com.mx', 'pass' => 'bod121');
        $arr[] = array('Id' => 7020002, 'name' => 'Triunfo' , 'mail' => 'triunfo@ppj.com.mx', 'pass' => 'tri122');
        $arr[] = array('Id' => 7020003, 'name' => 'Campestre' , 'mail' => 'campestre@ppj.com.mx', 'pass' => 'cam123');
        $arr[] = array('Id' => 7020004, 'name' => 'Paseo' , 'mail' => 'paseo@ppj.com.mx', 'pass' => 'paseo123');
        $arr[] = array('Id' => 7020005, 'name' => 'Guerrero' , 'mail' => 'guerrero@ppj.com.mx', 'pass' => 'gue125');
        $arr[] = array('Id' => 7020006, 'name' => 'Ejercito Nacional' , 'mail' => 'juarez@ppj.com.mx', 'pass' => '16942102');
        $arr[] = array('Id' => 7020007, 'name' => 'Valle' , 'mail' => 'valle@ppj.com.mx', 'pass' => 'val127');
        $arr[] = array('Id' => 7020008, 'name' => 'Santos Degollado' , 'mail' => 'centro@ppj.com.mx', 'pass' => 'cen128');
        $arr[] = array('Id' => 7020009, 'name' => 'Limoneros' , 'mail' => 'limoneros@pinpj.com.mx', 'pass' => 'lim129');
        $arr[] = array('Id' => 7020010, 'name' => 'Carlos Amaya' , 'mail' => 'carlosamaya@pinpj.com.mx', 'pass' => 'car130');
        $arr[] = array('Id' => 7020011, 'name' => 'Las Torres' , 'mail' => 'lastorres@pinpj.com.mx', 'pass' => 'las131');
        $arr[] = array('Id' => 7020012, 'name' => 'Gomez Morin' , 'mail' => 'gomezmorin@ppj.com.mx', 'pass' => 'gom132');
        $arr[] = array('Id' => 7020013, 'name' => 'Aztecas' , 'mail' => 'aztecas@ppj.com.mx', 'pass' => 'azt133');
        $arr[] = array('Id' => 7020014, 'name' => 'Henequen' , 'mail' => 'henequen@ppj.com.mx', 'pass' => 'hen134');
        $arr[] = array('Id' => 7020015, 'name' => 'Lopez Mateos' , 'mail' => 'lopezmateos@ppj.com.mx', 'pass' => 'lop135');
        $arr[] = array('Id' => 7020016, 'name' => 'Patio Zaragoza' , 'mail' => 'patiozaragoza@ppj.com.mx', 'pass' => 'pat136');
        $arr[] = array('Id' => 7020017, 'name' => 'ConstituciĂ³n' , 'mail' => 'constitucion@ppj.com.mx', 'pass' => 'con137');
        $arr[] = array('Id' => 7020018, 'name' => 'Torres II' , 'mail' => 'torres2@ppj.com.mx', 'pass' => 'tor138');
        $arr[] = array('Id' => 7020019, 'name' => 'Casas Grandes' , 'mail' => 'casasgrandes@ppj.com.mx', 'pass' => 'cer45');
        $arr[] = array('Id' => 7020020, 'name' => 'Sendero' , 'mail' => 'sendero@ppj.com.mx', 'pass' => 'sen140');
        $arr[] = array('Id' => 7020021, 'name' => 'Vallarta' , 'mail' => 'vallarta@ppj.com.mx', 'pass' => 'val141');
        $arr[] = array('Id' => 7020022, 'name' => 'Chihuahua' , 'mail' => 'chihuahua@ppj.com.mx', 'pass' => 'chi142');
        $arr[] = array('Id' => 7020023, 'name' => 'Zarco' , 'mail' => 'zarco@ppj.com.mx', 'pass' => 'zar143');
        $arr[] = array('Id' => 7020024, 'name' => 'Ocampo PPJ' , 'mail' => 'ocampo@ppj.com.mx', 'pass' => 'oca144');
        $arr[] = array('Id' => 7020025, 'name' => 'Sicomoro' , 'mail' => 'sicomoro@pinpj.com.mx', 'pass' => 'sic145');
        $arr[] = array('Id' => 7020026, 'name' => '20 de Noviembre PPJ' , 'mail' => '20denoviembre@ppj.com.mx', 'pass' => '20d146');
        $arr[] = array('Id' => 7020027, 'name' => 'Universdad PPJ' , 'mail' => 'universidad@ppj.com.mx', 'pass' => 'uni147');
        $arr[] = array('Id' => 7020028, 'name' => 'Cedis Chihuahua' , 'mail' => 'bodegachihuahua@ppj.com.mx', 'pass' => 'bod148');
        $arr[] = array('Id' => 7020029, 'name' => 'Juan Escutia' , 'mail' => 'juanescutia@ppj.com.mx', 'pass' => 'Ald170');
        $arr[] = array('Id' => 7020030, 'name' => 'Jose Maria Iglesias' , 'mail' => 'josemariaiglesias@ppj.com.mx', 'pass' => 'jos150');
        $arr[] = array('Id' => 7020031, 'name' => 'Fuentes Mares' , 'mail' => 'fuentesmares@ppj.com.mx', 'pass' => 'fue151');
        $arr[] = array('Id' => 7020032, 'name' => 'Americas' , 'mail' => 'americas@ppj.com.mx', 'pass' => 'ame152');
        $arr[] = array('Id' => 7020033, 'name' => 'Juventud' , 'mail' => 'juventud@pinpj.com.mx', 'pass' => 'juventud23');
        $arr[] = array('Id' => 7020034, 'name' => 'Tecnologico' , 'mail' => 'tecnologico@ppj.com.mx', 'pass' => 'tec154');
        $arr[] = array('Id' => 7020035, 'name' => 'Dostoyevsky' , 'mail' => 'dostoievsky@ppj.com.mx', 'pass' => 'dos155');
        $arr[] = array('Id' => 7020036, 'name' => 'Portales' , 'mail' => 'portales@ppj.com.mx', 'pass' => 'por156');
        $arr[] = array('Id' => 7020037, 'name' => 'Juventud II' , 'mail' => 'juventud2@ppj.com.mx', 'pass' => 'juv157');
        $arr[] = array('Id' => 7020038, 'name' => 'Oriente' , 'mail' => 'jardinesdeoriente@ppj.com.mx', 'pass' => 'jar158');
        $arr[] = array('Id' => 7020039, 'name' => 'Casa Grande' , 'mail' => 'casagrande@ppj.com.mx', 'pass' => 'cas159');
        $arr[] = array('Id' => 7020040, 'name' => 'Aldama' , 'mail' => 'aldama@ppj.com.mx', 'pass' => 'ald160');
        $arr[] = array('Id' => 7020042, 'name' => 'Contratista (PPJ)' , 'mail' => 'contratista@ppj.com.mx', 'pass' => 'con123');
        $arr[] = array('Id' => 7020043, 'name' => 'Ventas por Volumen' , 'mail' => 'ventasporvolumen@ppj.com.mx', 'pass' => 'ven162');
        $arr[] = array('Id' => 7030041, 'name' => 'Cantera 2' , 'mail' => 'cantera@pinpj.com.mx', 'pass' => 'can204');
        $arr[] = array('Id' => 7030042, 'name' => 'Magdalena' , 'mail' => 'magdalena@pinturas57.com.mx', 'pass' => 'xs007095');
        $arr[] = array('Id' => 7030043, 'name' => 'Tequila' , 'mail' => 'tequila@pinturas57.com.mx', 'pass' => 'tequila18');
        $arr[] = array('Id' => 7030044, 'name' => 'Amatitan' , 'mail' => 'amatitan@pinturas57.com.mx', 'pass' => 'xs007100');
        $arr[] = array('Id' => 7030048, 'name' => 'Campesina' , 'mail' => 'campesina@ppj.com.mx', 'pass' => 'cam210');
        $arr[] = array('Id' => 7030049, 'name' => 'Kawatzin' , 'mail' => 'kawatzin@pingol.com.mx', 'pass' => 'kaw211');
        $arr[] = array('Id' => 7030050, 'name' => 'Chedraui' , 'mail' => 'chedraui@pinturas57.com.mx', 'pass' => 'succhedrau');
        $arr[] = array('Id' => 7030051, 'name' => 'Porvenir' , 'mail' => 'porvenir@ppj.com.mx', 'pass' => 'por213');
        $arr[] = array('Id' => 7030052, 'name' => 'Quintas Carolinas' , 'mail' => 'quintascarolina@ppj.com.mx', 'pass' => 'qui214');
        $arr[] = array('Id' => 7030054, 'name' => 'Santiago Troncoso' , 'mail' => 'santiagotroncoso@ppj.com.mx', 'pass' => 'san215');
        $arr[] = array('Id' => 7030055, 'name' => 'La Cantera PPJ' , 'mail' => 'cantera@ppj.com.mx', 'pass' => 'can216');
        $arr[] = array('Id' => 7030056, 'name' => 'Paraiso' , 'mail' => 'paraiso@pingol.com.mx', 'pass' => 'par217');
        $arr[] = array('Id' => 7030057, 'name' => 'Jaltepec' , 'mail' => 'jaltepec@pingol.com.mx', 'pass' => 'jal218');
        $arr[] = array('Id' => 7030058, 'name' => 'Huentitan' , 'mail' => 'huentitan@pinturas57.com', 'pass' => 'HUEN2018');
        $arr[] = array('Id' => 7030059, 'name' => 'Artesanos' , 'mail' => 'artesanos@pinturas57.com', 'pass' => 'artesanos');
        $arr[] = array('Id' => 7030060, 'name' => 'Oblatos' , 'mail' => 'oblatos@pinturas57.com', 'pass' => 'oblatoscyl');
        $arr[] = array('Id' => 7030061, 'name' => 'Periferico' , 'mail' => 'periferico@pinturas57.com', 'pass' => 'per222');
        $arr[] = array('Id' => 7030062, 'name' => 'Valdepenas' , 'mail' => 'valdepenas@pinturas57.com', 'pass' => 'val223');
        $arr[] = array('Id' => 7030063, 'name' => 'La Mancha' , 'mail' => 'lamancha@pinturas57.com', 'pass' => 'lam2018');
        $arr[] = array('Id' => 7030064, 'name' => 'Las Misiones' , 'mail' => 'misiones@ppj.com.mx', 'pass' => 'mis225');
        $arr[] = array('Id' => 7030065, 'name' => 'Villa Magna' , 'mail' => 'villamagna@pingol.com.mx', 'pass' => 'vil226');
        $arr[] = array('Id' => 7030066, 'name' => 'Santa MarĂ­a del Oro' , 'mail' => 'santamariadeloro@pinturas57.com.mx', 'pass' => 'sucsamao');
        $arr[] = array('Id' => 7030067, 'name' => 'Oscar Flores' , 'mail' => 'oscarflores@ppj.com.mx', 'pass' => 'osc228');
        $arr[] = array('Id' => 7030068, 'name' => 'Iturbe Â Â ' , 'mail' => 'iturbe@pingol.com.mx', 'pass' => 'itu229');
        $arr[] = array('Id' => 7030069, 'name' => 'Atencingo' , 'mail' => 'atencingo@pumo.com.mx', 'pass' => 'ate230');
        $arr[] = array('Id' => 7030070, 'name' => 'Axochiapan (PUMO)' , 'mail' => 'axochiapan@pumo.com.mx', 'pass' => 'axo231');
        $arr[] = array('Id' => 7030071, 'name' => 'Chiautla' , 'mail' => 'chiautla@pumo.com.mx', 'pass' => 'groma*18');
        $arr[] = array('Id' => 7030072, 'name' => 'Chietla' , 'mail' => 'chietla@pumo.com.mx', 'pass' => 'chi233');
        $arr[] = array('Id' => 7030073, 'name' => 'Lomas Del Valle' , 'mail' => 'lomasdelvalle@pinturas57.com.mx', 'pass' => 'lom234');
        $arr[] = array('Id' => 7030075, 'name' => 'Tlachiahualpa' , 'mail' => 'tlachiahualpa@pingol.com.mx', 'pass' => 'tla236');
        $arr[] = array('Id' => 7030077, 'name' => 'Independencia' , 'mail' => 'independencia@pinturas57.com.mx', 'pass' => 'IND2018');
        $arr[] = array('Id' => 7030078, 'name' => 'Las Palmas' , 'mail' => '52@ferreprecios.com.mx', 'pass' => '52239');
        $arr[] = array('Id' => 7030079, 'name' => 'Central Norte' , 'mail' => 'centralnorte@ferreprecios.com.mx', 'pass' => 'cen240');
        $arr[] = array('Id' => 7030080, 'name' => 'Roma' , 'mail' => 'huauchinangoroma@ferreprecios.com.mx', 'pass' => 'hua241');
        $arr[] = array('Id' => 7030081, 'name' => 'Ahuazotepec (Ferreprecios)' , 'mail' => 'ahuazotepec@ferreprecios.com.mx', 'pass' => 'ahu242');
        $arr[] = array('Id' => 7030082, 'name' => 'Libramiento' , 'mail' => 'libramiento@ferreprecios.com.mx', 'pass' => 'glechuga');
        $arr[] = array('Id' => 7030084, 'name' => 'San Jose (Ferreprecios)' , 'mail' => 'sanjose@ferreprecios.com', 'pass' => 'SANJOSE');
        $arr[] = array('Id' => 7030085, 'name' => 'Central de Abastos' , 'mail' => 'centraldeabastos@ferreprecios.com', 'pass' => 'cen246');
        $arr[] = array('Id' => 7030087, 'name' => 'Fix Acapulco' , 'mail' => 'fixacapulco@ferreprecios.com.mx', 'pass' => 'fix248');
        $arr[] = array('Id' => 7030088, 'name' => 'Fix Temixco' , 'mail' => 'fixtemixco@ferreprecios.com.mx', 'pass' => 'fix249');
        $arr[] = array('Id' => 7030089, 'name' => 'Apatlaco' , 'mail' => 'apatlaco@pinturas57.com.mx', 'pass' => 'daniellope');
        $arr[] = array('Id' => 7030090, 'name' => 'Libertad' , 'mail' => 'libertad@ppj.com.mx', 'pass' => '123456');
        $arr[] = array('Id' => 7030091, 'name' => 'Puerto de Palos' , 'mail' => 'puertodepalos@ppj.com.mx', 'pass' => 'pue252');
        $arr[] = array('Id' => 7030092, 'name' => 'La Legua' , 'mail' => 'lalegua@pingol.com.mx', 'pass' => 'lal253');
        $arr[] = array('Id' => 7030093, 'name' => 'Cora' , 'mail' => 'cora@pinturas57.com.mx', 'pass' => 'succora');
        $arr[] = array('Id' => 7030095, 'name' => 'La Ceiba' , 'mail' => 'ceiba@pingol.com.mx', 'pass' => 'cei255');
        $arr[] = array('Id' => 7030096, 'name' => 'La Uno' , 'mail' => 'launo@pingol.com.mx', 'pass' => 'lau256');
        $arr[] = array('Id' => 7030097, 'name' => 'Pahuatlan (Pingol)' , 'mail' => 'pahuatlan@pingol.com.mx', 'pass' => 'pah270');
        $arr[] = array('Id' => 7030098, 'name' => 'ACF' , 'mail' => 'acf@gruporoma.com', 'pass' => '12345');
        $arr[] = array('Id' => 7030099, 'name' => 'TamuĂ­n' , 'mail' => 'tamuin@pingol.com.mx', 'pass' => 'tam258');
        $arr[] = array('Id' => 7030100, 'name' => 'Valles' , 'mail' => 'ciudadvalles@pingol.com.mx', 'pass' => 'ciu259');
        $arr[] = array('Id' => 7030101, 'name' => 'Madero' , 'mail' => 'madero@pingol.com.mx', 'pass' => 'mad260');
        $arr[] = array('Id' => 7030102, 'name' => 'RĂ­o Nilo' , 'mail' => 'rionilo@pingol.com.mx', 'pass' => 'rio261');
        $arr[] = array('Id' => 7030103, 'name' => 'Norte' , 'mail' => 'norte@pingol.com.mx', 'pass' => 'nor262');
        $arr[] = array('Id' => 7030109, 'name' => 'Valle Imperial' , 'mail' => 'valleimperial@pinturas57.com.mx', 'pass' => 'val263');
        $arr[] = array('Id' => 7030110, 'name' => 'Fix Ozumbilla' , 'mail' => 'fixozumbilla@ferreprecios.com.mx', 'pass' => 'fix264');
        $arr[] = array('Id' => 7030111, 'name' => 'Colonias' , 'mail' => 'colonias@pingol.com.mx', 'pass' => 'col265');
        $arr[] = array('Id' => 7030112, 'name' => 'Progreso' , 'mail' => 'progreso@pingol.com.mx', 'pass' => 'pro266');
        $arr[] = array('Id' => 7030113, 'name' => 'Sacramento' , 'mail' => 'sacramento@ppj.com.mx', 'pass' => 'sac267');
        $arr[] = array('Id' => 7030114, 'name' => 'Villa Ahumada' , 'mail' => 'villaahumada@ppj.com.mx', 'pass' => 'villa123');
        $arr[] = array('Id' => 7030115, 'name' => 'Lowes' , 'mail' => 'lowes@ppj.com.mx', 'pass' => 'low269');
        $arr[] = array('Id' => 7030117, 'name' => 'San Pedro' , 'mail' => 'sanpedro@pingol.com.mx', 'pass' => 'san271');
        $arr[] = array('Id' => 7030118, 'name' => 'San Vicente' , 'mail' => 'sanvicente@pinturas57.com.mx', 'pass' => 's4nv1c3nt3');
        $arr[] = array('Id' => 7030119, 'name' => 'Fix Iguala' , 'mail' => 'fixiguala@ferreprecios.com.mx', 'pass' => 'fix273');
        $arr[] = array('Id' => 7030120, 'name' => 'Fix Tizayuca' , 'mail' => 'fixtizayuca@ferreprecios.com.mx', 'pass' => 'fix274');
        $arr[] = array('Id' => 7030121, 'name' => 'Ajoloapan' , 'mail' => 'ajoloapan@pingol.com.mx', 'pass' => 'ajo275');
        $arr[] = array('Id' => 7030122, 'name' => 'Arenal' , 'mail' => 'arenal@pinturas57.com.mx', 'pass' => 'arenal2018');
        $arr[] = array('Id' => 7030123, 'name' => 'Campobello' , 'mail' => 'campobello@ppj.com.mx', 'pass' => 'cam277');
        $arr[] = array('Id' => 7030124, 'name' => 'Esperanza' , 'mail' => 'esperanza@pinturas57.com.mx', 'pass' => 'ESPERA556');
        $arr[] = array('Id' => 7030125, 'name' => 'La Cerve' , 'mail' => 'cerve@ppj.com.mx', 'pass' => 'cer279');
        $arr[] = array('Id' => 7030126, 'name' => 'Tres Vias' , 'mail' => 'arboledas@ppj.com.mx', 'pass' => 'arb280');
        $arr[] = array('Id' => 7030127, 'name' => 'Cementera' , 'mail' => 'cementera@ppj.com.mx', 'pass' => 'cem281');
        $arr[] = array('Id' => 7030128, 'name' => 'Altavista' , 'mail' => 'altavista@ppj.com.mx', 'pass' => 'alt282');
        $arr[] = array('Id' => 7030129, 'name' => 'Robinson' , 'mail' => 'robinson@ppj.com.mx', 'pass' => 'rob283');
        $arr[] = array('Id' => 7030130, 'name' => 'Atrios' , 'mail' => 'atrios@pinturas57.com.mx', 'pass' => 'atr284');
        $arr[] = array('Id' => 7030131, 'name' => 'Juan Pablo II' , 'mail' => 'juanpablo@pinturas57.com.mx', 'pass' => 'jua285');
        $arr[] = array('Id' => 7030132, 'name' => 'Fix Acapulco 2' , 'mail' => 'fixacapulco2@ferreprecios.com', 'pass' => 'fix286');
        $arr[] = array('Id' => 7030133, 'name' => 'Granjero' , 'mail' => 'granjero@ppj.com.mx', 'pass' => 'gra291');
        $arr[] = array('Id' => 7030134, 'name' => 'Division del Norte (Juarez)' , 'mail' => 'divisiondelnorte@pinpj.com.mx', 'pass' => 'div292');
        $arr[] = array('Id' => 7030135, 'name' => 'Mesa Central' , 'mail' => 'mesacentral@ppj.com.mx', 'pass' => 'mes293');
        $arr[] = array('Id' => 7030137, 'name' => 'Colotero' , 'mail' => 'colotero@pingol.com.mx', 'pass' => 'col294');
        $arr[] = array('Id' => 7030138, 'name' => 'Ichante' , 'mail' => 'ichnate@pingol.com.mx', 'pass' => 'ich295');
        $arr[] = array('Id' => 7030139, 'name' => 'Naranjos' , 'mail' => 'naranjos@pingol.com.mx', 'pass' => 'nar296');
        $arr[] = array('Id' => 7030140, 'name' => 'Casasano' , 'mail' => 'casasano@pinturas57.com.mx', 'pass' => 'cas297');
        $arr[] = array('Id' => 7030141, 'name' => 'Fix IzĂºcar' , 'mail' => 'fixizucar@ferreprecios.com', 'pass' => 'fix298');
        $arr[] = array('Id' => 7030142, 'name' => 'QUMA' , 'mail' => 'quma@pingol.com.mx', 'pass' => 'qum299');
        $arr[] = array('Id' => 7030143, 'name' => 'Volcanes' , 'mail' => 'volcanes@pumo.com.mx', 'pass' => 'vol300');
        $arr[] = array('Id' => 7030144, 'name' => 'Coyotillos' , 'mail' => 'coyotillos@pingol.com.mx', 'pass' => 'coyote256');
        $arr[] = array('Id' => 7030145, 'name' => 'Perimetral' , 'mail' => 'perimetral@pinpj.com.mx', 'pass' => 'per302');
        //$arr[] = array('Id' => 7030150, 'name' => 'Cbetis' , 'mail' => 'cbetis@pingol.com.mx', 'pass' => 'ventum');
        //$arr[] = array('Id' => 7030149, 'name' => 'Ingenio' , 'mail' => 'ingenio@pingol.com.mx', 'pass' => 'ventum');



        for($i = 0; $i < count($arr); $i++)
        {

            //$data['Sucursal'] = $arr[$i]['sucursal'];

            DB::table('users')
                ->where('email', $arr[$i]['mail'])
                ->update([
                    'empresas_Id' =>  7,
                    'empleados_Id' =>  $arr[$i]['Id'],
                    'name' =>  $arr[$i]['name'],
                    'email' =>  $arr[$i]['mail'],
                    'password' =>  bcrypt($arr[$i]['pass']),
                    'Activo' =>  1,
                    'tipo_user' => 2,
                    'fotoEmp' => 'roma.png',
                    'fotoUser' => 'empleado.jpg'
                ]);

           /* DB::table('users')->insert([
                [
                    'empresas_Id' =>  7,
                    'empleados_Id' =>  $arr[$i]['Id'],
                    'name' =>  $arr[$i]['name'],
                    'email' =>  $arr[$i]['mail'],
                    'password' =>  bcrypt($arr[$i]['pass']),
                    'Activo' =>  1,
                    'tipo_user' => 2,
                    'fotoEmp' => 'roma.png',
                    'fotoUser' => 'empleado.jpg'
                ]
            ]);*/


        }



    }
}
