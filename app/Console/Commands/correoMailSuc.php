<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mail\MailPassSuc;
use Mail;
use Illuminate\Mail\Mailable;
use Swift_Mailer;
use \Swift_SmtpTransport as SmtpTransport;


class correoMailSuc extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'correo:passsuc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $arr = array();
        $arr[] = array('sucursal' => 'Acatlan', 'mail' => 'acatlan@pingol.com.mx', 'pass' => 'aca1');
        $arr[] = array('sucursal' => 'Matriz', 'mail' => 'matriz@pingol.com.mx', 'pass' => 'mat2');
        $arr[] = array('sucursal' => 'Acaxochitlan', 'mail' => 'acaxochitlan@pingol.com.mx', 'pass' => 'aca3');
        $arr[] = array('sucursal' => 'Cuautepec', 'mail' => 'cuautepec@pingol.com.mx', 'pass' => 'cua4');
        $arr[] = array('sucursal' => 'Santiago', 'mail' => 'santiago@pingol.com.mx', 'pass' => 'san5');
        $arr[] = array('sucursal' => 'Color Car', 'mail' => 'colorcar@pingol.com.mx', 'pass' => 'col6');
        $arr[] = array('sucursal' => 'Y Griega', 'mail' => 'ygriega@pingol.com.mx', 'pass' => 'ygr7');
        $arr[] = array('sucursal' => 'La Abeja', 'mail' => 'abeja@pingol.com.mx', 'pass' => 'abe8');
        $arr[] = array('sucursal' => '21 De Marzo', 'mail' => '21demarzo@pingol.com.mx', 'pass' => '21d10');
        $arr[] = array('sucursal' => 'Boulevard', 'mail' => 'boulevard@pingol.com.mx', 'pass' => 'bou11');
        $arr[] = array('sucursal' => 'Central', 'mail' => 'central@pingol.com.mx', 'pass' => 'cen12');
        $arr[] = array('sucursal' => 'Glorieta', 'mail' => 'glorieta@pingol.com.mx', 'pass' => 'glo13');
        $arr[] = array('sucursal' => 'Jardines', 'mail' => 'jardines@pingol.com.mx', 'pass' => 'jar56');
        $arr[] = array('sucursal' => 'Santa Ana Hueytlalpan', 'mail' => 'santaanahueytlalpan@pingol.com.mx', 'pass' => 'san15');
        $arr[] = array('sucursal' => 'Galerias', 'mail' => 'galerias@pingol.com.mx', 'pass' => 'gal16');
        $arr[] = array('sucursal' => 'Huauchinango', 'mail' => 'huauchinango@pingol.com.mx', 'pass' => 'hua17');
        $arr[] = array('sucursal' => 'Necaxa', 'mail' => 'necaxa@pingol.com.mx', 'pass' => 'nec18');
        $arr[] = array('sucursal' => 'Xicotepec', 'mail' => 'xicotepec@pingol.com.mx', 'pass' => 'xic19');
        $arr[] = array('sucursal' => 'Villa', 'mail' => 'villa@pingol.com.mx', 'pass' => 'vil20');
        $arr[] = array('sucursal' => 'Automotriz', 'mail' => 'automotriz@pingol.com.mx', 'pass' => 'aut21');
        $arr[] = array('sucursal' => 'La Villita', 'mail' => 'lavillita@pingol.com.mx', 'pass' => 'lav23');
        $arr[] = array('sucursal' => 'Xico', 'mail' => 'xico@pingol.com.mx', 'pass' => 'xic24');
        $arr[] = array('sucursal' => 'Huachi', 'mail' => 'huauchi@pingol.com.mx', 'pass' => 'hua25');
        $arr[] = array('sucursal' => 'Alamo', 'mail' => 'alamo1@pingol.com.mx', 'pass' => 'ala26');
        $arr[] = array('sucursal' => 'Alamo II', 'mail' => 'alamo2@pingol.com.mx', 'pass' => 'ala27');
        $arr[] = array('sucursal' => 'Cerro Azul', 'mail' => 'cerroazul@pingol.com.mx', 'pass' => 'cer28');
        $arr[] = array('sucursal' => 'Cazones', 'mail' => 'cazones@pingol.com.mx', 'pass' => 'caz29');
        $arr[] = array('sucursal' => 'Coatzintla', 'mail' => 'coatzintla@pingol.com.mx', 'pass' => 'coa30');
        $arr[] = array('sucursal' => 'Cedis Poza Rica', 'mail' => 'bodegapozarica@pingol.com.mx', 'pass' => 'bod31');
        $arr[] = array('sucursal' => '20 De Noviembre', 'mail' => '20denoviembre@pingol.com.mx', 'pass' => '20d32');
        $arr[] = array('sucursal' => 'La 52 (Pingol)', 'mail' => '52@pingol.com.mx', 'pass' => '5233');
        $arr[] = array('sucursal' => 'Macrocomex', 'mail' => 'macrocomex@pingol.com.mx', 'pass' => 'mac34');
        $arr[] = array('sucursal' => 'Vendedor Profesional', 'mail' => 'ventasprofesionales.pr@pingol.com.mx', 'pass' => 'ven35');
        $arr[] = array('sucursal' => 'Petromex', 'mail' => 'petromex@pingol.com.mx', 'pass' => 'pet36');
        $arr[] = array('sucursal' => 'Pozo 13', 'mail' => 'pozo13@pingol.com.mx', 'pass' => 'poz37');
        $arr[] = array('sucursal' => 'Triangulo', 'mail' => 'triangulo@pingol.com.mx', 'pass' => 'tri38');
        $arr[] = array('sucursal' => 'Juarez', 'mail' => 'juarez@pingol.com.mx', 'pass' => 'jua39');
        $arr[] = array('sucursal' => 'Garibaldi', 'mail' => 'garibaldi@pingol.com.mx', 'pass' => 'gar40');
        $arr[] = array('sucursal' => 'Bancos', 'mail' => 'bancos@pingol.com.mx', 'pass' => 'ban41');
        $arr[] = array('sucursal' => 'Proservice Poza Rica', 'mail' => 'proservice@pingol.com.mx', 'pass' => 'pro42');
        $arr[] = array('sucursal' => 'Totolapa', 'mail' => 'totolapa@pingol.com.mx', 'pass' => 'tot43');
        $arr[] = array('sucursal' => 'Gaviotas', 'mail' => 'gaviotas@pingol.com.mx', 'pass' => 'gav44');
        $arr[] = array('sucursal' => 'Carpintienda', 'mail' => 'carpintienda@pingol.com.mx', 'pass' => 'car45');
        $arr[] = array('sucursal' => 'Puebla', 'mail' => 'puebla@pingol.com.mx', 'pass' => 'pue46');
        $arr[] = array('sucursal' => 'Federal De Caminos', 'mail' => 'federal@pingol.com.mx', 'pass' => 'fed47');
        $arr[] = array('sucursal' => 'Tihuatlan', 'mail' => 'tihuatlan@pingol.com.mx', 'pass' => 'tih48');
        $arr[] = array('sucursal' => 'Jara', 'mail' => 'jara@pingol.com.mx', 'pass' => 'jar49');
        $arr[] = array('sucursal' => 'Atitalaquia', 'mail' => 'atitalaquia@pingol.com.mx', 'pass' => 'ati58');
        $arr[] = array('sucursal' => 'Atotonilco', 'mail' => 'atotonilco@pingol.com.mx', 'pass' => 'ato51');
        $arr[] = array('sucursal' => 'Cruz Azul', 'mail' => 'cruzazul@pingol.com.mx', 'pass' => 'cru57');
        $arr[] = array('sucursal' => 'San Marcos', 'mail' => 'sanmarcos@pingol.com.mx', 'pass' => 'san53');
        $arr[] = array('sucursal' => 'Crucero', 'mail' => 'crucero@pingol.com.mx', 'pass' => 'cru54');
        $arr[] = array('sucursal' => 'Tlaxcoapan', 'mail' => 'tlaxcoapan@pingol.com.mx', 'pass' => 'tla55');
        $arr[] = array('sucursal' => 'Oficinas', 'mail' => 'e.contreras@pingol.com.mx', 'pass' => 'ec56');
        $arr[] = array('sucursal' => 'Mercado', 'mail' => 'mercado@pingol.com.mx', 'pass' => 'mer58');
        $arr[] = array('sucursal' => 'Refineria', 'mail' => 'refineria@pingol.com.mx', 'pass' => 'ref59');
        $arr[] = array('sucursal' => 'Colegio Militar', 'mail' => 'colegiomilitar@pingol.com.mx', 'pass' => 'col60');
        $arr[] = array('sucursal' => 'Ocampo', 'mail' => 'ocampo@pingol.com.mx', 'pass' => 'ocampo2018');
        $arr[] = array('sucursal' => 'Tequixquiac', 'mail' => 'tequixquiac@pingol.com.mx', 'pass' => 'teq63');
        $arr[] = array('sucursal' => 'Conejos', 'mail' => 'conejos@pingol.com.mx', 'pass' => 'con59');
        $arr[] = array('sucursal' => 'Atlantes', 'mail' => 'atlantes@pingol.com.mx', 'pass' => 'atlantes53');
        $arr[] = array('sucursal' => 'Jardines De Tula', 'mail' => 'plazajardines@pingol.com.mx', 'pass' => 'pla66');
        $arr[] = array('sucursal' => 'Tecamac', 'mail' => 'tecamac@pingol.com.mx', 'pass' => 'tec67');
        $arr[] = array('sucursal' => 'Pabellon', 'mail' => 'pabellon@pingol.com.mx', 'pass' => 'Legua.18');
        $arr[] = array('sucursal' => 'Tezontepec', 'mail' => 'tezontepec@pingol.com.mx', 'pass' => 'tez69');
        $arr[] = array('sucursal' => 'Ejercito', 'mail' => 'ejercito@pingol.com.mx', 'pass' => 'eje70');
        $arr[] = array('sucursal' => 'Tizayuca', 'mail' => 'tizayuca1@pingol.com.mx', 'pass' => 'tiz71');
        $arr[] = array('sucursal' => 'Tizayuca Il', 'mail' => 'tizayuca2@pingol.com.mx', 'pass' => 'tiz72');
        $arr[] = array('sucursal' => 'Mina', 'mail' => 'mina@pingol.com.mx', 'pass' => 'min73');
        $arr[] = array('sucursal' => 'Bicentenario', 'mail' => 'bicentenario@pingol.com.mx', 'pass' => 'bic74');
        $arr[] = array('sucursal' => 'Niños', 'mail' => 'ninos@pingol.com.mx', 'pass' => 'nin75');
        $arr[] = array('sucursal' => 'Tolcayuca', 'mail' => 'tolcayuca@pingol.com.mx', 'pass' => 'tol76');
        $arr[] = array('sucursal' => 'Galeana', 'mail' => 'galeana@pingol.com.mx', 'pass' => 'yqqtsszc');
        $arr[] = array('sucursal' => 'Auditorio', 'mail' => 'auditorio@pingol.com.mx', 'pass' => 'ypn65baw');
        $arr[] = array('sucursal' => 'Tecnica', 'mail' => 'tecnica@pinturas57.com.mx', 'pass' => 'arnulfo03');
        $arr[] = array('sucursal' => 'Universidad', 'mail' => 'universidad@pinturas57.com.mx', 'pass' => 'univ2018');
        $arr[] = array('sucursal' => 'Cedis Jojutla', 'mail' => 'cedisjojutla@pinturas57.com.mx', 'pass' => 'ced81');
        $arr[] = array('sucursal' => 'Puente De Ixtla', 'mail' => 'puentedeixtla@pingol.com.mx', 'pass' => '5t2tyxh6');
        $arr[] = array('sucursal' => 'San Mateo', 'mail' => 'sanmateo@pinturas57.com.mx', 'pass' => 'eh3yxace');
        $arr[] = array('sucursal' => 'Tehuixtla', 'mail' => 'tehuixtla@pingol.com.mx', 'pass' => '96zkfrn3');
        $arr[] = array('sucursal' => 'Tequesquitengo', 'mail' => 'tequesquitengo@pingol.com.mx', 'pass' => 'ush325tp');
        $arr[] = array('sucursal' => 'Tlaltizapan', 'mail' => 'tlaltizapan@pingol.com.mx', 'pass' => '28405');
        $arr[] = array('sucursal' => 'Tlaquiltenango', 'mail' => 'tlaquiltenango@pingol.com.mx', 'pass' => 'tlaquim4');
        $arr[] = array('sucursal' => 'Xoxocotla', 'mail' => 'xoxocotla@pingol.com.mx', 'pass' => 'aranda');
        $arr[] = array('sucursal' => 'Zacatepec', 'mail' => 'zacatepec@pingol.com.mx', 'pass' => 'hz7ngk3a');
        $arr[] = array('sucursal' => 'Vista Hermosa', 'mail' => 'vistahermosa@pingol.com.mx', 'pass' => 'tfzerrxz');
        $arr[] = array('sucursal' => 'Santa Rosa 30', 'mail' => 'santarosa30@pingol.com.mx', 'pass' => 'JOSE2830');
        $arr[] = array('sucursal' => 'Zapata', 'mail' => 'zapata@pinturas57.com.mx', 'pass' => 'zap92');
        $arr[] = array('sucursal' => 'Anenecuilco', 'mail' => 'anenecuilco@pinturas57.com.mx', 'pass' => 'ane93');
        $arr[] = array('sucursal' => 'Cedis Cuautla', 'mail' => 'bodegacuautla@pinturas57.com.mx', 'pass' => 'bod94');
        $arr[] = array('sucursal' => '2 de Mayo', 'mail' => '2demayo@pinturas57.com.mx', 'pass' => '2de95');
        $arr[] = array('sucursal' => 'Alcanfores', 'mail' => 'alcanfores@pinturas57.com.mx', 'pass' => 'alc96');
        $arr[] = array('sucursal' => 'Amilcingo', 'mail' => 'amilcingo@pinturas57.com.mx', 'pass' => 'ami97');
        $arr[] = array('sucursal' => 'Centro', 'mail' => 'centro@pinturas57.com.mx', 'pass' => 'cen98');
        $arr[] = array('sucursal' => 'Insurgentes', 'mail' => 'insurgentes@pinturas57.com.mx', 'pass' => 'ins99');
        $arr[] = array('sucursal' => 'Brisas', 'mail' => 'brisas@pinturas57.com.mx', 'pass' => 'bri100');
        $arr[] = array('sucursal' => 'Reforma', 'mail' => 'reforma@pinturas57.com.mx', 'pass' => 'ref101');
        $arr[] = array('sucursal' => 'San Jose', 'mail' => 'sanjose@pinturas57.com.mx', 'pass' => 'san102');
        $arr[] = array('sucursal' => 'Cuautlixco', 'mail' => 'cuautlixco@pinturas57.com.mx', 'pass' => 'cua103');
        $arr[] = array('sucursal' => 'Tenextepango', 'mail' => 'tenextepango@pinturas57.com.mx', 'pass' => 'ten104');
        $arr[] = array('sucursal' => 'Pino Suarez', 'mail' => 'pinosuarez@pinturas57.com.mx', 'pass' => 'pin105');
        $arr[] = array('sucursal' => 'Unila', 'mail' => 'unila@pinturas57.com.mx', 'pass' => 'uni107');
        $arr[] = array('sucursal' => 'Valle Sol', 'mail' => 'valledelsol@pinturas57.com.mx', 'pass' => 'val108');
        $arr[] = array('sucursal' => 'Allende', 'mail' => 'allende@pinturas57.com.mx', 'pass' => 'allende1');
        $arr[] = array('sucursal' => 'Los Fresnos', 'mail' => 'fresnos@pingol.com.mx', 'pass' => 'fres402');
        $arr[] = array('sucursal' => 'Colosio', 'mail' => 'colosio@pinturas57.com.mx', 'pass' => 'col111');
        $arr[] = array('sucursal' => 'Flores Magon', 'mail' => 'floresmagon@pinturas57.com.mx', 'pass' => 'flo112');
        $arr[] = array('sucursal' => 'La Cruz', 'mail' => 'lacruz@pinturas57.com.mx', 'pass' => 'lac113');
        $arr[] = array('sucursal' => 'Mololoa', 'mail' => 'mololoa@pinturas57.com.mx', 'pass' => 'mololoa1');
        $arr[] = array('sucursal' => 'Rio Suchiate', 'mail' => 'riosuchiate@pinturas57.com.mx', 'pass' => 'RIO1994');
        $arr[] = array('sucursal' => 'San Juan', 'mail' => 'sanjuan@pinturas57.com.mx', 'pass' => 'sanjuan1');
        $arr[] = array('sucursal' => 'Soriana', 'mail' => 'soriana@pinturas57.com.mx', 'pass' => 'soriana1');
        $arr[] = array('sucursal' => 'Alameda', 'mail' => 'alameda@pinturas57.com.mx', 'pass' => 'ala118');
        $arr[] = array('sucursal' => 'La Cantera', 'mail' => 'cantera@pinturas57.com.mx', 'pass' => 'Lacantera');
        $arr[] = array('sucursal' => 'Xalisco', 'mail' => 'xalisco@pinturas57.com.mx', 'pass' => 'xal120');
        $arr[] = array('sucursal' => 'Cedis Juarez', 'mail' => 'bodegacdjuarez@ppj.com.mx', 'pass' => 'bod121');
        $arr[] = array('sucursal' => 'Triunfo', 'mail' => 'triunfo@ppj.com.mx', 'pass' => 'tri122');
        $arr[] = array('sucursal' => 'Campestre', 'mail' => 'campestre@ppj.com.mx', 'pass' => 'cam123');
        $arr[] = array('sucursal' => 'Paseo', 'mail' => 'paseo@ppj.com.mx', 'pass' => 'paseo123');
        $arr[] = array('sucursal' => 'Guerrero', 'mail' => 'guerrero@ppj.com.mx', 'pass' => 'gue125');
        $arr[] = array('sucursal' => 'Ejercito Nacional', 'mail' => 'juarez@ppj.com.mx', 'pass' => '16942102');
        $arr[] = array('sucursal' => 'Valle', 'mail' => 'valle@ppj.com.mx', 'pass' => 'val127');
        $arr[] = array('sucursal' => 'Santos Degollado', 'mail' => 'centro@ppj.com.mx', 'pass' => 'cen128');
        $arr[] = array('sucursal' => 'Limoneros', 'mail' => 'limoneros@ppj.com.mx', 'pass' => 'lim129');
        $arr[] = array('sucursal' => 'Carlos Amaya', 'mail' => 'carlosamaya@ppj.com.mx', 'pass' => 'car130');
        $arr[] = array('sucursal' => 'Las Torres', 'mail' => 'lastorres@ppj.com.mx', 'pass' => 'las131');
        $arr[] = array('sucursal' => 'Gomez Morin', 'mail' => 'gomezmorin@ppj.com.mx', 'pass' => 'gom132');
        $arr[] = array('sucursal' => 'Aztecas', 'mail' => 'aztecas@ppj.com.mx', 'pass' => 'azt133');
        $arr[] = array('sucursal' => 'Henequen', 'mail' => 'henequen@ppj.com.mx', 'pass' => 'hen134');
        $arr[] = array('sucursal' => 'Lopez Mateos', 'mail' => 'lopezmateos@ppj.com.mx', 'pass' => 'lop135');
        $arr[] = array('sucursal' => 'Patio Zaragoza', 'mail' => 'patiozaragoza@ppj.com.mx', 'pass' => 'pat136');
        $arr[] = array('sucursal' => 'Constitución', 'mail' => 'constitucion@ppj.com.mx', 'pass' => 'con137');
        $arr[] = array('sucursal' => 'Torres II', 'mail' => 'torres2@ppj.com.mx', 'pass' => 'tor138');
        $arr[] = array('sucursal' => 'Casas Grandes', 'mail' => 'casasgrandes@ppj.com.mx', 'pass' => 'cas139');
        $arr[] = array('sucursal' => 'Sendero', 'mail' => 'sendero@ppj.com.mx', 'pass' => 'sen140');
        $arr[] = array('sucursal' => 'Vallarta', 'mail' => 'vallarta@ppj.com.mx', 'pass' => 'val141');
        $arr[] = array('sucursal' => 'Chihuahua', 'mail' => 'chihuahua@ppj.com.mx', 'pass' => 'chi142');
        $arr[] = array('sucursal' => 'Zarco', 'mail' => 'zarco@ppj.com.mx', 'pass' => 'zar143');
        $arr[] = array('sucursal' => 'Ocampo PPJ', 'mail' => 'ocampo@ppj.com.mx', 'pass' => 'oca144');
        $arr[] = array('sucursal' => 'Sicomoro', 'mail' => 'sicomoro@ppj.com.mx', 'pass' => 'cas159');
        $arr[] = array('sucursal' => '20 de Noviembre PPJ', 'mail' => '20denoviembre@ppj.com.mx', 'pass' => '20d146');
        $arr[] = array('sucursal' => 'Universdad PPJ', 'mail' => 'universidad@ppj.com.mx', 'pass' => 'uni147');
        $arr[] = array('sucursal' => 'Cedis Chihuahua', 'mail' => 'bodegachihuahua@ppj.com.mx', 'pass' => 'bod148');
        $arr[] = array('sucursal' => 'Juan Escutia', 'mail' => 'juanescutia@ppj.com.mx', 'pass' => 'jua149');
        $arr[] = array('sucursal' => 'Jose Maria Iglesias', 'mail' => 'josemariaiglesias@ppj.com.mx', 'pass' => 'jos150');
        $arr[] = array('sucursal' => 'Fuentes Mares', 'mail' => 'fuentesmares@ppj.com.mx', 'pass' => 'fue151');
        $arr[] = array('sucursal' => 'Americas', 'mail' => 'americas@ppj.com.mx', 'pass' => 'ame152');
        $arr[] = array('sucursal' => 'Juventud', 'mail' => 'juventud@ppj.com.mx', 'pass' => 'juventud23');
        $arr[] = array('sucursal' => 'Tecnologico', 'mail' => 'tecnologico@ppj.com.mx', 'pass' => 'tec154');
        $arr[] = array('sucursal' => 'Dostoyevsky', 'mail' => 'dostoievsky@ppj.com.mx', 'pass' => 'dos155');
        $arr[] = array('sucursal' => 'Portales', 'mail' => 'portales@ppj.com.mx', 'pass' => 'por156');
        $arr[] = array('sucursal' => 'Juventud II', 'mail' => 'juventud2@ppj.com.mx', 'pass' => 'juv157');
        $arr[] = array('sucursal' => 'Oriente', 'mail' => 'jardinesdeoriente@ppj.com.mx', 'pass' => 'jar158');
        $arr[] = array('sucursal' => 'Casa Grande', 'mail' => 'casagrande@ppj.com.mx', 'pass' => 'cas159');
        $arr[] = array('sucursal' => 'Aldama', 'mail' => 'aldama@ppj.com.mx', 'pass' => 'ald160');
        $arr[] = array('sucursal' => 'Ventas por Volumen', 'mail' => 'ventasporvolumen@pinpj.com.mx', 'pass' => 'ven162');
        $arr[] = array('sucursal' => 'Tulancingo Truper', 'mail' => 'fsoto@ferreprecios.com.mx', 'pass' => 'fso163');
        $arr[] = array('sucursal' => 'FIX Libertad', 'mail' => 'practicotulancingo@ferreprecios.com', 'pass' => 'pra164');
        $arr[] = array('sucursal' => 'Hidalgo', 'mail' => 'hidalgo@ferreprecios.com.mx', 'pass' => 'hid165');
        $arr[] = array('sucursal' => 'Fix Tulancingo', 'mail' => 'fixtulancingo@ferreprecios.com.mx', 'pass' => 'fix166');
        $arr[] = array('sucursal' => 'Atitalaquia', 'mail' => 'atitalaquia@ferreprecios.com', 'pass' => 'ati167');
        $arr[] = array('sucursal' => 'Tula', 'mail' => 'tula@ferreprecios.com.mx', 'pass' => 'tul168');
        $arr[] = array('sucursal' => 'Mayoreo Tulancingo', 'mail' => 'c.garcia@ferreprecios.com.mx', 'pass' => 'cg169');
        $arr[] = array('sucursal' => 'FIX Huauchinango', 'mail' => 'huauchinango1@ferreprecios.com.mx', 'pass' => 'hua171');
        $arr[] = array('sucursal' => 'Huauchinango 2', 'mail' => 'huauchinango2@ferreprecios.com.mx', 'pass' => 'hua172');
        $arr[] = array('sucursal' => 'Necaxa', 'mail' => 'necaxa@ferreprecios.com.mx', 'pass' => 'nec173');
        $arr[] = array('sucursal' => 'Villa Juarez 1', 'mail' => 'villajuarez1@ferreprecios.com.mx', 'pass' => 'vil174');
        $arr[] = array('sucursal' => 'FIX Xicotepec', 'mail' => 'villajuarez2@ferreprecios.com.mx', 'pass' => 'vil175');
        $arr[] = array('sucursal' => 'Poza Rica', 'mail' => 'pozarica@ferreprecios.com.mx', 'pass' => 'poz176');
        $arr[] = array('sucursal' => 'FIX Poza Rica', 'mail' => 'practicopozarica@ferreprecios.com.mx', 'pass' => 'poz177');
        $arr[] = array('sucursal' => 'FIX Cuautla', 'mail' => 'cuautla@ferreprecios.com.mx', 'pass' => 'cua178');
        $arr[] = array('sucursal' => 'FIX Jojutla', 'mail' => 'delsur@ferreprecios.com.mx', 'pass' => 'del179');
        $arr[] = array('sucursal' => 'Practico Jojutla', 'mail' => 'practicojojutla@ferreprecios.com.mx', 'pass' => 'pra180');
        $arr[] = array('sucursal' => 'FIX Zacatepec', 'mail' => 'zacatepec@ferreprecios.com.mx', 'pass' => 'zac181');
        $arr[] = array('sucursal' => 'FIX Cuernavaca', 'mail' => 'cuernavaca@ferreprecios.com.mx', 'pass' => 'cue182');
        $arr[] = array('sucursal' => 'Mayoreo Cuernavaca', 'mail' => 'l.tellez@ferreprecios.com.mx', 'pass' => 'lt184');
        $arr[] = array('sucursal' => 'Cantera 2', 'mail' => 'cantera@pingol.com.mx', 'pass' => 'can216');
        $arr[] = array('sucursal' => 'Magdalena', 'mail' => 'magdalena@pinturas57.com.mx', 'pass' => 'mag205');
        $arr[] = array('sucursal' => 'Tequila', 'mail' => 'tequila@pinturas57.com.mx', 'pass' => 'tequila18');
        $arr[] = array('sucursal' => 'Amatitan', 'mail' => 'amatitan@pinturas57.com.mx', 'pass' => 'amatitan18');
        $arr[] = array('sucursal' => 'Ahuazotepec', 'mail' => 'ahuazotepec@pingol.com.mx', 'pass' => 'ahu209');
        $arr[] = array('sucursal' => 'Campesina', 'mail' => 'campesina@ppj.com.mx', 'pass' => 'cam210');
        $arr[] = array('sucursal' => 'Kawatzin', 'mail' => 'kawatzin@pingol.com.mx', 'pass' => 'kaw211');
        $arr[] = array('sucursal' => 'Chedraui', 'mail' => 'chedraui@pinturas57.com.mx', 'pass' => 'che212');
        $arr[] = array('sucursal' => 'Porvenir', 'mail' => 'porvenir@ppj.com.mx', 'pass' => 'por213');
        $arr[] = array('sucursal' => 'Quintas Carolinas', 'mail' => 'quintascarolina@ppj.com.mx', 'pass' => 'qui214');
        $arr[] = array('sucursal' => 'Santiago Troncoso', 'mail' => 'santiagotroncoso@ppj.com.mx', 'pass' => 'san215');
        $arr[] = array('sucursal' => 'La Cantera PPJ', 'mail' => 'cantera@ppj.com.mx', 'pass' => 'can216');
        $arr[] = array('sucursal' => 'Paraiso', 'mail' => 'paraiso@pingol.com.mx', 'pass' => 'par217');
        $arr[] = array('sucursal' => 'Jaltepec', 'mail' => 'jaltepec@pingol.com.mx', 'pass' => 'jal218');
        $arr[] = array('sucursal' => 'Huentitan', 'mail' => 'huentitan@pinturas57.com', 'pass' => 'HUE2018');
        $arr[] = array('sucursal' => 'Artesanos', 'mail' => 'artesanos@pinturas57.com', 'pass' => 'artesanos');
        $arr[] = array('sucursal' => 'Oblatos', 'mail' => 'oblatos@pinturas57.com', 'pass' => 'oblatoscyl');
        $arr[] = array('sucursal' => 'Periferico', 'mail' => 'periferico@pinturas57.com', 'pass' => 'per222');
        $arr[] = array('sucursal' => 'Valdepenas', 'mail' => 'valdepenas@pinturas57.com', 'pass' => 'val223');
        $arr[] = array('sucursal' => 'La Mancha', 'mail' => 'lamancha@pinturas57.com', 'pass' => 'lam2018');
        $arr[] = array('sucursal' => 'Las Misiones', 'mail' => 'misiones@ppj.com.mx', 'pass' => 'mis225');
        $arr[] = array('sucursal' => 'Villa Magna', 'mail' => 'villamagna@pingol.com.mx', 'pass' => 'vil226');
        $arr[] = array('sucursal' => 'Santa María del Oro', 'mail' => 'santamariadeloro@pinturas57.com.mx', 'pass' => 'NUBIA');
        $arr[] = array('sucursal' => 'Oscar Flores', 'mail' => 'oscarflores@ppj.com.mx', 'pass' => 'osc228');
        $arr[] = array('sucursal' => 'Iturbe', 'mail' => 'iturbe@pingol.com.mx', 'pass' => 'itu229');
        $arr[] = array('sucursal' => 'Atencingo', 'mail' => 'atencingo@pumo.com.mx', 'pass' => 'ate230');
        $arr[] = array('sucursal' => 'Axochiapan (PUMO)', 'mail' => 'axochiapan@pumo.com.mx', 'pass' => 'axo231');
        $arr[] = array('sucursal' => 'Chiautla', 'mail' => 'chiautla@pumo.com.mx', 'pass' => 'groma*18');
        $arr[] = array('sucursal' => 'Chietla', 'mail' => 'chietla@pumo.com.mx', 'pass' => 'chi233');
        $arr[] = array('sucursal' => 'Lomas Del Valle', 'mail' => 'lomasdelvalle@pinturas57.com.mx', 'pass' => 'lom234');
        $arr[] = array('sucursal' => 'Temascalapa', 'mail' => 'temascalapa@pingol.com.mx', 'pass' => 'tem235');
        $arr[] = array('sucursal' => 'Tlachiahualpa', 'mail' => 'tlachiahualpa@pingol.com.mx', 'pass' => 'tla236');
        $arr[] = array('sucursal' => 'Conejos', 'mail' => 'conejos@pingol.com.mx', 'pass' => 'con237');
        $arr[] = array('sucursal' => 'Independencia', 'mail' => 'independencia@pinturas57.com.mx', 'pass' => 'IND2018');
        $arr[] = array('sucursal' => 'Las Palmas', 'mail' => '52@ferreprecios.com.mx', 'pass' => '52239');
        $arr[] = array('sucursal' => 'Central Norte', 'mail' => 'centralnorte@ferreprecios.com.mx', 'pass' => 'cen240');
        $arr[] = array('sucursal' => 'Roma', 'mail' => 'huauchinangoroma@ferreprecios.com.mx', 'pass' => 'hua241');
        $arr[] = array('sucursal' => 'Ahuazotepec', 'mail' => 'ahuazotepec@ferreprecios.com.mx', 'pass' => 'ahu242');
        $arr[] = array('sucursal' => 'Libramiento', 'mail' => 'libramiento@ferreprecios.com.mx', 'pass' => 'lib243');
        $arr[] = array('sucursal' => 'San Jose', 'mail' => 'sanjose@ferreprecios.com', 'pass' => 'SANJOSE');
        $arr[] = array('sucursal' => 'Central de Abastos', 'mail' => 'centraldeabastos@ferreprecios.com', 'pass' => 'cen246');
        $arr[] = array('sucursal' => 'Fix Acapulco', 'mail' => 'fixacapulco@ferreprecios.com.mx', 'pass' => 'fix248');
        $arr[] = array('sucursal' => 'Fix Temixco', 'mail' => 'fixtemixco@ferreprecios.com.mx', 'pass' => 'fix249');
        $arr[] = array('sucursal' => 'Apatlaco', 'mail' => 'apatlaco@pinturas57.com.mx', 'pass' => 'apa250');
        $arr[] = array('sucursal' => 'Libertad', 'mail' => 'libertad@ppj.com.mx', 'pass' => '123456');
        $arr[] = array('sucursal' => 'Puerto de Palos', 'mail' => 'puertodepalos@ppj.com.mx', 'pass' => 'pue252');
        $arr[] = array('sucursal' => 'La Legua', 'mail' => 'lalegua@pingol.com.mx', 'pass' => 'lal253');
        $arr[] = array('sucursal' => 'Cora', 'mail' => 'cora@pinturas57.com.mx', 'pass' => 'cor254');
        $arr[] = array('sucursal' => 'La Ceiba', 'mail' => 'ceiba@pingol.com.mx', 'pass' => 'cei255');
        $arr[] = array('sucursal' => 'La Uno', 'mail' => 'launo@pingol.com.mx', 'pass' => 'lau256');
        $arr[] = array('sucursal' => 'Tamuín', 'mail' => 'tamuin@pingol.com.mx', 'pass' => 'tam258');
        $arr[] = array('sucursal' => 'Valles', 'mail' => 'ciudadvalles@pingol.com.mx', 'pass' => 'ciu259');
        $arr[] = array('sucursal' => 'Madero', 'mail' => 'madero@pingol.com.mx', 'pass' => 'mad260');
        $arr[] = array('sucursal' => 'Río Nilo', 'mail' => 'rionilo@pingol.com.mx', 'pass' => 'rio261');
        $arr[] = array('sucursal' => 'Norte', 'mail' => 'norte@pingol.com.mx', 'pass' => 'nor262');
        $arr[] = array('sucursal' => 'Valle Imperial', 'mail' => 'valleimperial@pinturas57.com.mx', 'pass' => 'val263');
        $arr[] = array('sucursal' => 'Fix Ozumbilla', 'mail' => 'fixozumbilla@ferreprecios.com.mx', 'pass' => 'fix264');
        $arr[] = array('sucursal' => 'Colonias', 'mail' => 'colonias@pingol.com.mx', 'pass' => 'col265');
        $arr[] = array('sucursal' => 'Progreso', 'mail' => 'progreso@pingol.com.mx', 'pass' => 'pro266');
        $arr[] = array('sucursal' => 'Sacramento', 'mail' => 'sacramento@ppj.com.mx', 'pass' => 'sac267');
        $arr[] = array('sucursal' => 'Villa Ahumada', 'mail' => 'villaahumada@ppj.com.mx', 'pass' => 'villa123');
        $arr[] = array('sucursal' => 'Lowes', 'mail' => 'lowes@ppj.com.mx', 'pass' => 'low269');
        $arr[] = array('sucursal' => 'Pahuatlan', 'mail' => 'pahuatlan@pingol.com.mx', 'pass' => 'pah270');
        $arr[] = array('sucursal' => 'San Pedro', 'mail' => 'sanpedro@pingol.com.mx', 'pass' => 'san271');
        $arr[] = array('sucursal' => 'San Vicente', 'mail' => 'sanvicente@pinturas57.com.mx', 'pass' => 's4nv1c3nt3');
        $arr[] = array('sucursal' => 'Fix Iguala', 'mail' => 'fixiguala@ferreprecios.com.mx', 'pass' => 'fix273');
        $arr[] = array('sucursal' => 'Fix Tizayuca', 'mail' => 'fixtizayuca@ferreprecios.com.mx', 'pass' => 'fix274');
        $arr[] = array('sucursal' => 'Ajoloapan', 'mail' => 'ajoloapan@pingol.com.mx', 'pass' => 'ajo275');
        $arr[] = array('sucursal' => 'Arenal', 'mail' => 'arenal@pinturas57.com.mx', 'pass' => 'arenal2018');
        $arr[] = array('sucursal' => 'Campobello', 'mail' => 'campobello@ppj.com.mx', 'pass' => 'cam277');
        $arr[] = array('sucursal' => 'Esperanza', 'mail' => 'esperanza@pinturas57.com.mx', 'pass' => 'ESPERA556');
        $arr[] = array('sucursal' => 'La Cerve', 'mail' => 'cerve@ppj.com.mx', 'pass' => 'cer279');
        $arr[] = array('sucursal' => 'Arboledas', 'mail' => 'arboledas@ppj.com.mx', 'pass' => 'arb280');
        $arr[] = array('sucursal' => 'Cementera', 'mail' => 'cementera@ppj.com.mx', 'pass' => 'cem281');
        $arr[] = array('sucursal' => 'Altavista', 'mail' => 'altavista@ppj.com.mx', 'pass' => 'alt282');
        $arr[] = array('sucursal' => 'Robinson', 'mail' => 'robinson@ppj.com.mx', 'pass' => 'rob283');
        $arr[] = array('sucursal' => 'Atrios', 'mail' => 'atrios@pinturas57.com.mx', 'pass' => 'atr284');
        $arr[] = array('sucursal' => 'Juan Pablo II', 'mail' => 'juanpablo@pinturas57.com.mx', 'pass' => 'jua285');
        $arr[] = array('sucursal' => 'Fix Acapulco 2', 'mail' => 'fixacapulco2@ferreprecios.com', 'pass' => 'fix286');
        $arr[] = array('sucursal' => 'Contratista', 'mail' => 'contratista@ppj.com.mx', 'pass' => 'con290');
        $arr[] = array('sucursal' => 'Granjero', 'mail' => 'granjero@ppj.com.mx', 'pass' => 'gra291');
        $arr[] = array('sucursal' => 'Division del Norte (Juarez)', 'mail' => 'divisiondelnorte@pinpj.com.mx', 'pass' => 'div292');
        $arr[] = array('sucursal' => 'Mesa Central', 'mail' => 'mesacentral@ppj.com.mx', 'pass' => 'mes293');
        $arr[] = array('sucursal' => 'Colotero', 'mail' => 'colotero@pingol.com.mx', 'pass' => 'col294');
        $arr[] = array('sucursal' => 'Ichante', 'mail' => 'ichnate@pingol.com.mx', 'pass' => 'ich295');
        $arr[] = array('sucursal' => 'Naranjos', 'mail' => 'naranjos@pingol.com.mx', 'pass' => 'nar296');
        $arr[] = array('sucursal' => 'Casasano', 'mail' => 'casasano@pinturas57.com.mx', 'pass' => 'cas297');
        $arr[] = array('sucursal' => 'Fix Izúcar', 'mail' => 'fixizucar@ferreprecios.com', 'pass' => 'fix298');
        $arr[] = array('sucursal' => 'QUMA', 'mail' => 'quma@pingol.com.mx', 'pass' => 'qum299');
        $arr[] = array('sucursal' => 'Volcanes', 'mail' => 'volcanes@pumo.com.mx', 'pass' => 'vol300');
        $arr[] = array('sucursal' => 'Coyotillos', 'mail' => 'coyotillos@pingol.com.mx', 'pass' => 'Coyote.18');
        $arr[] = array('sucursal' => 'Perimetral', 'mail' => 'perimetral@pinpj.com.mx', 'pass' => 'per302');



        for($i = 0; $i < count($arr); $i++)
        {

            $data['Sucursal'] = $arr[$i]['sucursal'];
            $data['pass']  = $arr[$i]['pass'];
            $data['Supervisor']  = "Ventum";
            $data['Motivo']  = "Creación de usuario tienda";
            Mail::to($arr[$i]['mail'])->send(new MailPassSuc($data));
           // Mail::to('hdam23@gmail.com')->send(new MailPassSuc($data));
        }



    }
}
