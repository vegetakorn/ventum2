<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mail\MailDirTodo;
use Mail;
use Illuminate\Mail\Mailable;
use Swift_Mailer;
use \Swift_SmtpTransport as SmtpTransport;
use DB;
use App\Helpers\Formulas;

class correoDirTodo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'correo:dirtodo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Correo que manda los to-do a los directores';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $formulas = new Formulas();
        $anio = date('Y');
        $anioAnt = date('Y');
        $mes = date('m');

        //fechas a buscar to-do

        $fechaHoy = date('Y-m-d');
        $fecha60 = date("d-m-Y",strtotime($fechaHoy."- 60 days"));

        if($mes == 1)
        {
            $mesAnt = 12;
            $anioAnt = $anio -1;
        }else
        {
            $mesAnt = $mes -1;
        }

        //listamos todas las empresas registradas
        $data['test'] = '';
        $data_emp =   DB::table('empresas')
            ->whereIn('empresas.Id',array(6,7))->get();
        $empresas = collect($data_emp)->toArray();
        $arrEmp = array();
        for($i = 0;$i<count($empresas);$i++) {

            //buscamos todos los supervisores
            $sql  = DB::table('empleados')
                ->leftjoin('puestos', function ($join) {
                    $join->on('puestos.Id', '=', 'empleados.puesto_Id');
                })
                ->leftjoin('users', function ($join) {
                    $join->on('empleados.Id', '=', 'users.empleados_Id');
                })
                ->leftjoin('emp_puestos', function ($join) {
                    $join->on('puestos.Id', '=', 'emp_puestos.puestos_Id');
                })
                ->leftjoin('empresas', function ($join) {
                    $join->on('empresas.Id', '=', 'empleados.empresas_Id');
                })
                ->select('empleados.*', 'puestos.puesto', 'empresas.Nombre as Empresa', 'users.id as user_Id' );

            $sql->where('emp_puestos.tipo_puesto_Id', "=", 2); //los tipo dos son directores
            $sql->where('empleados.empresas_Id', "=", $empresas[$i]->Id);
            $sql->where('empleados.activo', "=", 1);

            $data_sup = $sql->get();
            $supervisores = collect($data_sup)->toArray();
            //buscamos las tiendas asignadas de cada director
            $arrSup = array();
            $sum_todo = 0;
            for($j = 0;$j<count($supervisores);$j++)
            {
                $data_suc=   DB::table('users_tiendas')
                    ->leftjoin('tiendas', function ($join) {
                        $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
                    })
                    ->leftjoin('razon_social', function ($join) {
                        $join->on('razon_social.Id', '=', 'tiendas.razon_Id');
                    })
                    ->leftjoin('plazas', function ($join) {
                        $join->on('plazas.Id', '=', 'tiendas.plaza_Id');
                    })
                    ->select('tiendas.*', 'razon_social.nombre as Razon', 'plazas.plaza' )
                    ->where('users_tiendas.users_Id','=',$supervisores[$j]->user_Id)
                    ->where('tiendas.activo','=',1)
                    ->get();
                $sucursales = collect($data_suc)->toArray();

                $arrSuc = array();
                $arrVis = array();
                $arrTod = array();
                for($k = 0;$k<count($sucursales);$k++)
                {

                    //buscamos la visita mas antigua a la tienda
                    $data_vis=   DB::table('visitas')
                        ->leftjoin('tiendas', function ($join) {
                            $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
                        })
                        ->leftjoin('checklist', function ($join) {
                            $join->on('checklist.Id', '=', 'visitas.checklist_Id');
                        })
                        ->select('visitas.*', 'tiendas.nombre as tienda', 'tiendas.numsuc', 'checklist.nombre as checklist' )

                        ->where('tiendas.activo','=',1)
                        ->where('visitas.Status','=',131)

                        ->where('tiendas.Id', "=", $sucursales[$k]->Id)
                        ->orderByRaw('visitas.HoraInicio ASC')
                        ->first();

                    $visitas = collect($data_vis)->toArray();
                    $visitasCount = count($visitas);
                    if($visitasCount != 0)
                    {
                        //buscamos los to-do mas antiguos
                        $data_todo =   DB::table('visitas_todo')
                            ->leftjoin('visitas', function ($join) {
                                $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
                            })
                            ->leftjoin('tiendas', function ($join) {
                                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
                            })
                            ->leftjoin('checklist', function ($join) {
                                $join->on('checklist.Id', '=', 'visitas.checklist_Id');
                            })
                            ->leftjoin('campos', function ($join) {
                                $join->on('campos.Id', '=', 'visitas_todo.campos_Id');
                            })
                            ->leftjoin('categorias', function ($join) {
                                $join->on('categorias.Id', '=', 'campos.categorias_Id');
                            })
                            ->select('visitas_todo.*', 'tiendas.nombre as tienda', 'tiendas.numsuc', 'checklist.nombre as checklist', 'campos.nombre as campo', 'categorias.nombre as categoria' )
                            ->where('visitas.AnioRev',"=",date('Y', strtotime($fecha60)))
                            ->where('visitas.MesRev',"=",date('m', strtotime($fecha60)))
                            ->where('visitas.Status','=',131)
                            ->where('visitas_todo.Status','<>',154)
                            ->where('tiendas.Id', "=", $sucursales[$k]->Id)
                            ->count();

                        $arrVis[] = array(
                            'Razon' => $sucursales[$k]->Razon,
                            'Plaza' => $sucursales[$k]->plaza,
                            'NumSuc' => $sucursales[$k]->numsuc,
                            'Sucursal' => $sucursales[$k]->nombre,
                            'NoTodo' => $data_todo,
                            'Fecha' => date('d/m/Y', strtotime($visitas['HoraInicio']))
                        );

                        $sum_todo =  $sum_todo + $data_todo;

                    }else
                    {
                        $arrVis[] = array(
                            'Razon' => $sucursales[$k]->Razon,
                            'Plaza' => $sucursales[$k]->plaza,
                            'NumSuc' => $sucursales[$k]->numsuc,
                            'Sucursal' => $sucursales[$k]->nombre,
                            'NoTodo' => 0,
                            'Fecha' => 'Sin información'
                        );
                    }



                }
                if($sum_todo != 0)
                {
                    //enviamos correo
                    $data['Supervisor'] = $supervisores[$j]->nombre." ".$supervisores[$j]->apepat." ".$supervisores[$j]->apemat;
                    $data['TablaTodo'] = $arrVis;

                    /**Configuración de servidor de correo**/

                    if($empresas[$i]->Id == 1)
                    {
                        // Setup a new SmtpTransport instance for Gmail
                        $transport = new SmtpTransport();
                        $transport->setHost('smtp.mailgun.org');
                        $transport->setPort(587);
                        $transport->setEncryption('tls');
                        $transport->setUsername('postmaster@envios.ventumsupervision.com');
                        $transport->setPassword('b4139af002e42bdcdebf82604feeb1c0-c8e745ec-3c370a1d');


                        // Assign a new SmtpTransport to SwiftMailer
                        $driver = new Swift_Mailer($transport);

                        // Assign it to the Laravel Mailer
                        Mail::setSwiftMailer($driver);
                    }else
                    {
                        // Setup a new SmtpTransport instance for Gmail
                        $transport = new SmtpTransport();
                        $transport->setHost('smtp-relay.sendinblue.com');
                        $transport->setPort(587);
                        $transport->setEncryption('tls');
                        $transport->setUsername('ventum@ventummx.com');
                        $transport->setPassword('LTr9qvOFVBC7PX6H');


                        // Assign a new SmtpTransport to SwiftMailer
                        $driver = new Swift_Mailer($transport);

                        // Assign it to the Laravel Mailer
                        Mail::setSwiftMailer($driver);
                    }

                     Mail::to($supervisores[$j]->mail)->send(new MailDirTodo($data));
                   // Mail::to('hdam23@gmail.com')->send(new MailDirTodo($data));
                }

            }


        }


    }
}
