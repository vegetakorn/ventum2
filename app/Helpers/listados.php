<?php
//app/Helpers/Envato/User.php
namespace App\Helpers;

use DB;

class Listados {
    /**
     * @param int $user_id User-id
     *
     * @return string
     */
    public static function listaTiendas($user_Id) {

        $sql  = DB::table('users_tiendas')
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
            })
            ->leftjoin('razon_social', function ($join) {
                $join->on('tiendas.razon_Id', '=', 'razon_social.Id');
            })
            ->leftjoin('plazas', function ($join) {
                $join->on('tiendas.plaza_Id', '=', 'plazas.Id');
            })
            ->select('tiendas.*', 'users_tiendas.Id as usti_id', 'razon_social.nombre as razon', 'plazas.plaza' )
            ->where('users_tiendas.users_Id', "=", $user_Id)
            ->where('tiendas.activo', "=", 1)
            ->get();

        return $sql;
    }
    //listamos tiendas con filtros de razon, plaza y tienda
    public static function listaTiendasFiltro($user_Id, $razon, $plaza, $tienda) {

        $sql  = DB::table('users_tiendas')
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
            })
            ->leftjoin('razon_social', function ($join) {
                $join->on('tiendas.razon_Id', '=', 'razon_social.Id');
            })
            ->leftjoin('plazas', function ($join) {
                $join->on('tiendas.plaza_Id', '=', 'plazas.Id');
            })
            ->select('tiendas.*', 'users_tiendas.Id as usti_id', 'razon_social.nombre as razon', 'plazas.plaza' );

        if($razon != 0)
        {
            $sql->where('tiendas.razon_Id', "=", $razon);
        }
        if($plaza != 0)
        {
            $sql->where('tiendas.plaza_Id', "=", $plaza);
        }

        if($tienda != 0)
        {
            $sql->where('tiendas.Id', "=", $tienda);
        }
        $sql->where('users_tiendas.users_Id', "=", $user_Id)
            ->where('tiendas.activo', "=", 1);

        $data = $sql->get();

        return $data;
    }


    /**Funcion que siempre nos entregará la
     * lista de empleados tipo supervisor encontrados y asignados a un usuario X*
     */
    public static function listaSupervisores($users_Id) {

        //cargamos los supervisores

        $sql = DB::table('emp_puestos')
            ->leftjoin('empleados', function ($join) {
                $join->on('empleados.puesto_Id', '=', 'emp_puestos.puestos_Id');
            })
            ->leftjoin('users_empleados', function ($join) {
                $join->on('empleados.Id', '=', 'users_empleados.empleados_Id');
            })
            ->leftjoin('puestos', function ($join) {
                $join->on('puestos.Id', '=', 'empleados.puesto_Id');
            })
            ->leftjoin('razon_social', function ($join) {
                $join->on('razon_social.Id', '=', 'empleados.razon_Id');
            })
            ->leftjoin('plazas', function ($join) {
                $join->on('plazas.Id', '=', 'empleados.plaza_Id');
            })
            ->leftjoin('users', function ($join) {
                $join->on('users.empleados_Id', '=', 'empleados.Id');
            })
            ->select('empleados.*', 'puestos.puesto', 'users.id as supervisor_Id' )
            ->where('emp_puestos.tipo_puesto_Id','=',1)

            ->where('users_empleados.users_Id','=',$users_Id)
            ->orderBy('razon_social.nombre', 'asc')
            ->orderBy('plazas.plaza', 'asc')
            ->orderBy('puestos.puesto', 'asc')
            ->orderBy('empleados.nombre', 'asc')
            ->get();

        return $sql;
    }


    /**
    * Funcion que regresa un listado de to-do enlistado por antiguedad
     **/

    public static function listaTodoAntiguedad($tiendas_Id) {

        $sql  =  DB::table('visitas_todo')
            ->leftjoin('visitas', function ($join) {
                $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
            })
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
            })
            ->leftjoin('checklist', function ($join) {
                $join->on('checklist.Id', '=', 'visitas.checklist_Id');
            })
            ->leftjoin('campos', function ($join) {
                $join->on('campos.Id', '=', 'visitas_todo.campos_Id');
            })
            ->leftjoin('categorias', function ($join) {
                $join->on('categorias.Id', '=', 'campos.categorias_Id');
            })
            ->select('visitas_todo.*', 'tiendas.nombre as tienda', 'tiendas.numsuc', 'checklist.nombre as checklist',  'campos.nombre as campo', 'categorias.nombre as categoria' )
            ->where('visitas.Status','=',131)
            ->where('visitas_todo.Status','<>',154)
            ->where('tiendas.Id', "=", $tiendas_Id)
            ->orderByRaw('visitas_todo.fecha_inicio ASC')
            ->get();

        return $sql;
    }


    /**
     * Funcion que regresa los checklist que tiene asignado un usuario
     **/

    public static function listaChecklist($empleados_Id, $empresas_Id) {
        //buscamos el id del puesto
        $sql = DB::table('empleados');
        $sql->where('empleados.Id','=',$empleados_Id);
        $puesto = $sql->first();


        if($empleados_Id == 0)
        {
            $sql  =  DB::table('checklist')
                ->where('checklist.empresas_Id','=',$empresas_Id)
                ->get();
        }else
        {

            $data  = DB::table('chk_puesto')
                ->leftjoin('checklist', function ($join) {
                    $join->on('checklist.Id', '=', 'chk_puesto.checklist_Id');
                })
                ->select('checklist.*' );
            $data->where('chk_puesto.puestos_Id', "=", $puesto->puesto_Id);
            $sql  = $data->get();

        }

        return $sql;
    }

    public static function getMes($mes) {
        $arrMes = array("",
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre");


        return $arrMes[$mes];
    }

    public static function getTipoTodo($tipo) {
        $tipo_string = "";
        switch($tipo)
        {
            case 1:
                $tipo_string = "Visita";
                break;
            case 2:
                $tipo_string = "Express";
                break;
            case 3:
                $tipo_string = "Ftp";
                break;
            case 4:
                $tipo_string = "Supervisor";
                break;
        }

        return $tipo_string;
    }

    public static function getStatus($status) {

        $stat = array();
        $status_string = "";
        $status_label = "";
        switch($status)
        {
            case 151:
                $status_string = "Nuevo";
                $status_label = "danger";
                break;
            case 152:
                $status_string = "En Curso";
                $status_label = "warning";
                break;
            case 153:
                $status_string = "Pendiente";
                $status_label = "primary";
                break;
            case 154:
                $status_string = "Concluida";
                $status_label = "success";
                break;
        }

        return $stat = array($status_string,$status_label);
    }

}