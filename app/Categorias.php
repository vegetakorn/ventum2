<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Categorias extends Model
{
    use Notifiable;
    public $timestamps = false;
    //
    protected $table = 'categorias';

    protected $fillable = [
        'checklist_Id', 'nombre','descripcion','activo','evaluacion_cat', 'orden',
    ];
}
