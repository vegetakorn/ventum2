<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class CamposVal extends Model
{
    use Notifiable;
    public $timestamps = false;
    //
    protected $table = 'campos_valores';

    protected $fillable = [
        'campos_Id', 'nombre','valor','gentodo','activo',
    ];
}
