<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use DB;

class VisitaSegMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inputs)
    {
        //
        $this->inputs = $inputs;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {


       /* $subject = 'Visita de Seguimiento a Sucursal';

        return $this->view('visitas.mailseg',['data'=> $this->inputs])
            //->cc($this->inputs['mailSuc'], $this->inputs['nameSuc'])
            //->cc($this->inputs['mailSup'], $this->inputs['nameSup'])
            ->cc('mpena@ventumsupervision.com', 'Moisés Peña' )
            // ->cc('mbermejo@grupoaxo.com', 'Teresa Bermejo' )
            ->subject($subject);*/


        $cont  = count($this->inputs['envios']);
        $subject = 'Visita de Seguimiento a Sucursal ' .$this->inputs['nameSuc']  ;

        $env = $this->view('visitas.mailseg',['data'=> $this->inputs])
            ->cc($this->inputs['mailSuc'], $this->inputs['nameSuc'])
            ->cc($this->inputs['mailSup'], $this->inputs['nameSup'])
            ->cc('mpena@ventumsupervision.com', 'Moisés Peña' );

        for($i = 0; $i < $cont; $i++)
        {
            $env->cc($this->inputs['envios'][$i]['mail'], $this->inputs['envios'][$i]['empleado']);
        }

        $env->subject($subject);

        return $env;
    }
}
