<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use DB;

class MailActividad extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inputs)
    {
        //
        $this->inputs = $inputs;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $subject = 'Actividad asignada a  ' .$this->inputs['Supervisor']  ;

        $env = $this->view('mails.actividad',['data'=> $this->inputs])
            ->cc('harroyo@ventumsupervision.com', 'Héctor Arroyo' )
            ->cc('mpena@ventumsupervision.com', 'Moisés Peña' );

        $env->subject($subject);

        return $env;
    }
}
