<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailSupervisoresMes extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inputs)
    {
        //
        $this->inputs = $inputs;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'Información General de Supervisor  '.$this->inputs['Supervisor']  . ' a fin de mes'  ;

        $env = $this->view('mails.mailsupervisormes',['data'=> $this->inputs]);
        $env->cc('mpena@ventumsupervision.com', 'Moisés Peña' );
        // $env->cc('harroyo@ventumsupervision.com', 'Sistemas' );
        $env->subject($subject);

        return $env;
    }
}
