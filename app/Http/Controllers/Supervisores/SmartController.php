<?php

namespace App\Http\Controllers\Supervisores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Helpers\Listados;

class SmartController extends Controller
{

    protected $path = 'uploads/'; //path para pruebas locales

    public function getListadoTiendas()
    {
        $listado = new Listados();

        return $listado->listaTiendas(auth()->user()->id);
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data['tiendas'] = $this->getListadoTiendas();
        //return view('/actividades/lista')->with( $data);

    }

    public function validaReg(Request $request)
    {
        $sql = DB::table('visitas_smart');

        $sql->where('visitas_smart.visitas_todo_Id','=',$request['todo_Id']);
        $smart = $sql->count();

        return response()->json(['message' => $smart ] );
    }

    public function setSmart(Request $request)
    {


        //generamos el plan smart
       DB::table('visitas_smart')->insert([
            [
                'visitas_todo_Id' => $request['campos_id'],
                'objetivo' => "",
                'indicador_actual' => "",
                'indicador_deseado' => "",
                'requerimientos' => "",
                'comentarios'  => ""
            ]
        ]);

        $idsmart = DB::getPdo()->lastInsertId();

        return response()->json(['message' => $idsmart ] );
    }

    public function getSmart(Request $request)
    {

        $sql = DB::table('visitas_smart');

        $sql->where('visitas_smart.visitas_todo_Id','=',$request['todo_Id']);
        $smart = $sql->first();

        return response()->json(['message' => $smart ] );
    }

    public function updSmart(Request $request)
    {
        DB::table('visitas_smart')
            ->where('visitas_smart.visitas_todo_Id','=',$request['todo_Id'])

            ->update([

                'objetivo' =>  $request['objetivo'],
                'indicador_actual' =>  $request['indicador_actual'],
                'indicador_deseado' =>  $request['indicador_deseado'],
                'requerimientos' =>  $request['requerimientos'],
                'comentarios' =>  $request['comentarios'],

                ]);

        return response()->json(['message' => "OK" ] );
    }

    public function setAccion(Request $request)
    {
        DB::table('visitas_acciones')->insert([
            [
                'visitas_smart_Id' => $request['visitas_smart_Id'],
                'accion' => $request['accion'],
                'tareas' => $request['tareas'],
                'responsable' => $request['responsable'],
                'fhentrega' => $request['fhentrega'],
                'avance'  => 0
            ]
        ]);
        return response()->json(['message' => "OK" ] );
    }

    public function getAcciones(Request $request)
    {
        $sql = DB::table('visitas_acciones');

        $sql->where('visitas_acciones.visitas_smart_Id','=',$request['visitas_smart_Id']);
        $acciones = $sql->get();

        return response()->json(['message' => $acciones] );
    }



}
