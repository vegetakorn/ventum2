<?php

namespace App\Http\Controllers;

use App\Exports\VentasRangoExport;
use App\Helpers\Formulas;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Exports\VentasSucExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Helpers\Listados;
use App\Exports\EvaluacionesExport;

class ReportesController extends Controller
{

    protected $path = 'uploads/'; //path para pruebas locales
    protected $pathUploadCli = '/home/hdammx/public_html/VentumClientes/uploads/'; //path para copia en clientes
    public function getListadoTiendas()
    {
        $listado = new Listados();

        return $listado->listaTiendas(auth()->user()->id);
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $sql = DB::table('puestos');
        $sql->where('puestos.empresas_Id','=',auth()->user()->empresas_Id);
        $data['puestos'] = $sql->get();

        $sql = DB::table('razon_social');
        $sql->where('razon_social.empresas_Id','=',auth()->user()->empresas_Id);
        $data['razones'] = $sql->get();

        $data['tiendas'] = $this->getListadoTiendas();


        return view('/reportes/ventasmes')->with( $data);
    }

    public function sucmes()
    {


        return view('/usuariotienda/reportes/ventasmes');
    }




    public function rango()
    {

        $sql = DB::table('puestos');
        $sql->where('puestos.empresas_Id','=',auth()->user()->empresas_Id);
        $data['puestos'] = $sql->get();

        $sql = DB::table('razon_social');
        $sql->where('razon_social.empresas_Id','=',auth()->user()->empresas_Id);
        $data['razones'] = $sql->get();

        $data['tiendas'] = $this->getListadoTiendas();


        return view('/reportes/ventarango')->with( $data);
    }

    public function sucrango()
    {



        return view('/usuariotienda/reportes/ventarango');
    }

    public function evaluaciones()
    {
        $listado = new Listados();
        $sql = DB::table('puestos');
        $sql->where('puestos.empresas_Id','=',auth()->user()->empresas_Id);
        $data['puestos'] = $sql->get();

        $sql = DB::table('razon_social');
        $sql->where('razon_social.empresas_Id','=',auth()->user()->empresas_Id);
        $data['razones'] = $sql->get();
        $data['checklist']  = $listado->listaChecklist(auth()->user()->empleados_Id , auth()->user()->empresas_Id);
        $data['tiendas'] = $this->getListadoTiendas();


        return view('/reportes/evaluaciones')->with( $data);
    }

    public function exportSuc($razon, $plaza,$anio, $mes, $suc)
    {

        $listado = new Listados();

        //buscamos las ventas mensuales

     $sql = DB::table('ventas_mes')
              ->leftjoin('clasificaciones', function ($join) {
                  $join->on('clasificaciones.Id', '=', 'ventas_mes.clasificacion_Id');
              })
              ->leftjoin('tiendas', function ($join) {
                  $join->on('tiendas.Id', '=', 'ventas_mes.tiendas_Id');
              })->leftjoin('users_tiendas', function ($join) {
                    $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
                })

              ->select('ventas_mes.*', 'clasificaciones.Nombre', 'tiendas.nombre as Tienda', 'tiendas.numsuc' );


           $sql ->where('users_tiendas.users_Id','=',auth()->user()->id);

         if($suc != 0)
         {
             $sql->where('ventas_mes.tiendas_Id', "=", $suc);
         }

        if($plaza != 0)
        {
            $sql->where('tiendas.plaza_Id', "=", $plaza);
        }
        if($razon != 0)
        {
            $sql->where('tiendas.razon_Id', "=", $razon);
        }


          $sql->where('ventas_mes.anio', "=", $anio);


          $sql->where('ventas_mes.mes', "=", $mes);


        $data['ventas']  = $sql->get();

        $data['anio'] = $anio;
        $data['mes'] = $listado->getMes($mes);
        return Excel::download(new VentasSucExport($data), 'ventas_mes.xlsx');



    }


    public function exportRango($razon, $plaza,$fecha_ini, $fecha_fin, $suc)
    {

        $listado = new Listados();
        $formulas = new Formulas();


        $data['fecha_ini'] = $fecha_ini;
        $data['fecha_fin'] = $fecha_fin;


        //buscamos cuantos dias hay entre las fechas seleccionadas
        $dias = $formulas->diferenciaDias(trim(date('y-m-d', strtotime($fecha_ini))), trim(date('y-m-d', strtotime($fecha_fin))));
        $data['dias']  = $dias;

        //listamos las tiendas
       $sql = DB::table('tiendas')
            ->leftjoin('razon_social', function ($join) {
                $join->on('razon_social.Id', '=', 'tiendas.razon_Id');
            })
            ->leftjoin('plazas', function ($join) {
                $join->on('plazas.Id', '=', 'tiendas.plaza_Id');
            })->leftjoin('users_tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
            })
            ->select('tiendas.Id', 'tiendas.nombre', 'tiendas.numsuc')
            ->where('users_tiendas.users_Id','=',auth()->user()->id);


       if($razon != 0)
       {
           $sql->where('tiendas.razon_Id','=',$razon);
       }

        if($plaza != 0)
        {
            $sql->where('tiendas.plaza_Id','=',$plaza);
        }
        if($suc != 0)
        {
            $sql->where('tiendas.Id','=',$suc);
        }
        //llenado para la columna de ventas
        $arrFechas = array();
        for($j = 0;$j < $dias;$j++)
        {
            $nuevafecha = strtotime ( '+'.$j.' day' , strtotime ( $fecha_ini ) ) ;
            $nuevafecha = date ( 'Y-m-d' , $nuevafecha );

            $arrFechas[] = array("fhventa" => $nuevafecha);

        }


        $data_tiendas = $sql->get();

        $tiendas = collect($data_tiendas)->toArray();
        $arrSuc = array();
        for($i = 0;$i<count($tiendas);$i++)
        {

            //buscamos las fechas de venta diaria respecto a las fechas dadas
            $arrVentas = array();
            for($j = 0;$j < $dias;$j++)
            {
                $nuevafecha = strtotime ( '+'.$j.' day' , strtotime ( $fecha_ini ) ) ;
                $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
                $venta_dia = DB::table('ventas_dia')
                            ->where('ventas_dia.tiendas_Id','=',$tiendas[$i]->Id)
                            ->where('ventas_dia.fhventa','=',$nuevafecha)
                            ->get();
                $ventas = collect($venta_dia)->toArray();

                if(count($ventas) != 0)
                {
                    $arrVentas[] = array("fhventa" => $ventas[0]->fhventa, "monto" => $ventas[0]->monto);
                }else
                {
                    $arrVentas[] = array("fhventa" => "", "monto" => 0);
                }


            }

            $arrSuc[] = array("Id" => $tiendas[$i]->Id, "numsuc" => $tiendas[$i]->numsuc, "nombre" => $tiendas[$i]->nombre, "ventas" => $arrVentas);
        }

        $data['tiendas'] = $arrSuc;
        $data['fechas'] = $arrFechas;


        return Excel::download(new VentasRangoExport($data), 'ventas_rango.xlsx');



    }

    public function exportEv($razon, $plaza,$anio, $mes, $suc, $chk)
    {
        $listado = new Listados();
        $formulas = new Formulas();
        $data['mes'] = $listado->getMes($mes);
        $data['anio'] = $anio;
        //buscamos el nombre del checklist
        $sql = DB::table('checklist');
        $sql->where('checklist.Id','=',$chk);
        $checklist = $sql->first();

        $data['checklist'] = $checklist->nombre;

        $sql = DB::table('tiendas');
        $sql->where('tiendas.empresas_Id','=',auth()->user()->empresas_Id);
        $sql->where('tiendas.activo','=',1);

        if($razon != 0)
        {
            $sql->where('tiendas.razon_Id','=',$razon);
        }
        if($plaza != 0)
        {
            $sql->where('tiendas.plaza_Id','=',$plaza);
        }
        if($suc != 0)
        {
            $sql->where('tiendas.Id','=',$suc);
        }

        //vemos cuantos dias hay en el mes seleccionado
        $dias = $formulas->diasMes($mes, $anio);
       // $mes = 10;
        $info = $sql->get();
        $tiendas = json_decode($info, true);
        $arrSuc = array();
        $totGen = 0;
        $rojoGen = 0;
        $amarilloGen = 0;
        $verdeGen = 0;
        for($i = 0;$i<count($tiendas);$i++)
        {
            $arrVis = array();
            $sumCalif = 0;
            $totCalif = 0;
            $rojo = 0;
            $amarillo = 0;
            $verde = 0;
            $color = "";
            //buscamos las calificaciones de cada mes
            for($j = 1; $j <= $dias; $j++)
            {

              $sql2 = DB::table('visitas');
                $sql2->where('visitas.tiendas_Id','=', $tiendas[$i]['Id']);
                $sql2->where('visitas.tipo_visita','=',1);
                $sql2->where('visitas.Status','=',131);
                $sql2->whereDate('visitas.HoraInicio' , $anio."-".$mes."-".$j);
                $info2 = $sql2->first();
                $num = $sql2->count();

                if($num != 0)
                {

                    //definicion de conteos de color
                    if($info2->Calif >= 0 && $info2->Calif <= 59.99)
                    {
                        $rojo++;
                        $color = "#d52829";
                    }else  if($info2->Calif >= 60 && $info2->Calif <= 90.99)
                    {
                        $amarillo++;
                        $color = "#d4d51b";

                    }else  if($info2->Calif >= 91 && $info2->Calif <= 100)
                    {
                        $verde++;
                        $color = "#60f774";
                    }

                    $sumCalif = $sumCalif +   $info2->Calif;
                    $totCalif++;

                    $arrVis[] = array("Calif" => $info2->Calif."%", "Color" => $color );
                }else
                {
                    $arrVis[] = array("Calif" => "-", "Color" => "#FFFFFF" );
                }
               // $arrVis[] = array("Calif" => $anio."-".$mes."-".$j);
            }



            if($totCalif != 0)
            {
                $promedio = round($sumCalif / $totCalif, 2);
            }else
            {
                $promedio = 0;
            }
            $colorFin = "";
            if($promedio >= 0 && $promedio<= 59.99)
            {
                $colorFin = "#d52829";
            }else  if($promedio >= 60 && $promedio<= 90.99)
            {

                $colorFin = "#d4d51b";

            }else  if($promedio >= 91 && $promedio <= 100)
            {

                $colorFin = "#60f774";
            }

            $totGen = $totGen + $totCalif;
            $rojoGen = $rojoGen + $rojo;
            $amarilloGen = $amarilloGen + $amarillo ;
            $verdeGen = $verdeGen + $verde;


           $arrSuc[] = array("numsuc" => $tiendas[$i]['numsuc'],
               "nombre" =>  $tiendas[$i]['nombre'],
               'visitas' => $arrVis,
               'promedio' => $promedio."%",
               'total' =>$totCalif,
               'color' => $colorFin,
               'rojo' => $rojo,
               'amarillo' => $amarillo,
               'verde' => $verde);


        }
        $data['totGen'] = $totGen;
        $data['rojoGen'] = $rojoGen;
        $data['amarilloGen'] = $amarilloGen;
        $data['verdeGen'] = $verdeGen;
        $data['tiendas'] =  $arrSuc;
        $data['dias'] =  $dias;
        return  Excel::download(new EvaluacionesExport($data), 'evaluaciones.xlsx');

    }

}
