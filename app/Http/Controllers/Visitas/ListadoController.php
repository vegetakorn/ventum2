<?php

namespace App\Http\Controllers\Visitas;

use App\Exports\VisitasExport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Barryvdh\DomPDF\Facade as PDF;
use App\Helpers\Listados;
use App\Exports\ActividadesExport;
use Maatwebsite\Excel\Facades\Excel;
class ListadoController extends Controller
{
    //protected $path = '/home/hdammx/public_html/VentumAdmin/uploads/'; //path para host en linea
    protected $path = 'uploads/'; //path para pruebas locales
    protected $pathUploadCli = '/home/hdammx/public_html/VentumClientes/uploads/'; //path para copia en clientes
    public function getListadoTiendas()
    {
        $listado = new Listados();

        return $listado->listaTiendas(auth()->user()->id);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listado = new Listados();
        $data['tiendas'] = $this->getListadoTiendas();
        $data['supervisores'] = $listado->listaSupervisores(auth()->user()->id);
        $sql = DB::table('razon_social');
        $sql->where('razon_social.empresas_Id','=',auth()->user()->empresas_Id);
        $data['razones'] = $sql->get();
        return view('/visitas/lista')->with( $data);
    }

    public function indextienda()
    {
        $listado = new Listados();
        $data['supervisores'] = $listado->listaSupervisores(77);/**TODO: cuando se implemente en otros clientes se debera quitar el usuario 77 y ver la forma de listar supervisores.**/

        return view('/usuariotienda/visitas/lista')->with( $data);
    }

    public function getVisitas(Request $request)
    {

        //obtenemos las visitas buscadas por el filtro
        $sql = DB::table('visitas');
        $sql->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
            })
            ->leftjoin('checklist', function ($join) {
            $join->on('checklist.Id', '=', 'visitas.checklist_Id');
        })->leftjoin('users_tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
            })
            ->leftjoin('users', function ($join) {
                $join->on('users.id', '=', 'visitas.users_Id');
            })
            ->select('visitas.*', 'tiendas.nombre', 'tiendas.numsuc', 'checklist.nombre as Checklist', 'users.name');
        $sql ->where('visitas.Status','=',131);

        if($request['supervisor'] != 0)
        {
            $sql ->where('visitas.users_Id','=',$request['supervisor']);
        }
        //
        $sql ->where('users_tiendas.users_Id','=',auth()->user()->id);
        if($request['tienda'] != 0)
        {
            $sql ->where('visitas.tiendas_Id','=',$request['tienda']);
        }

        if($request['razon'] != 0)
        {
            $sql ->where('tiendas.razon_Id','=',$request['razon']);
        }

        if($request['plaza'] != 0)
        {
            $sql ->where('tiendas.plaza_Id','=',$request['plaza']);
        }

        if($request['sup'] == 1 && $request['seg'] == 0)
        {
            $sql ->where('visitas.tipo_visita','=',1);
        }
        if($request['sup'] == 0 && $request['seg'] == 1)
        {
            $sql ->where('visitas.tipo_visita','=',2);
        }



        $fechas = explode("-",$request['fecha'] );

        $fechaini = explode('/', $fechas[0]);
        $fechafin = explode('/', $fechas[1]);

        $sql ->whereBetween('visitas.HoraInicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])));
        $sql->orderByRaw('tiendas.nombre ASC');
        $sql->orderByRaw('visitas.HoraInicio ASC');
        $visitas = $sql->get();

        return response()->json(['visitas' => $visitas] );
    }

    public function pdf($sup, $seg, $fechaini, $fechafin, $tienda, $id_sup, $razon, $plaza)
    {

        //obtenemos las visitas buscadas por el filtro
        $sql = DB::table('visitas');
        $sql->leftjoin('tiendas', function ($join) {
            $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
        })->leftjoin('users_tiendas', function ($join) {
            $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
        })
            ->leftjoin('checklist', function ($join) {
                $join->on('checklist.Id', '=', 'visitas.checklist_Id');
            }) ->leftjoin('users', function ($join) {
                $join->on('users.id', '=', 'visitas.users_Id');
            })
            ->select('visitas.*', 'tiendas.nombre', 'tiendas.numsuc', 'checklist.nombre as Checklist', 'users.name');
        $sql ->where('visitas.Status','=',131);


        if($id_sup != 0)
        {
            $sql ->where('visitas.users_Id','=',$id_sup);
        }
        $sql ->where('users_tiendas.users_Id','=',auth()->user()->id);
        if($tienda != 0)
        {
            $sql ->where('visitas.tiendas_Id','=',$tienda);
        }
        if($razon != 0)
        {
            $sql ->where('tiendas.razon_Id','=',$razon);
        }

        if($plaza != 0)
        {
            $sql ->where('tiendas.plaza_Id','=',$plaza);
        }

        if($sup == 1 && $seg == 0)
        {
            $sql ->where('visitas.tipo_visita','=',1);
        }
        if($sup == 0 && $seg == 1)
        {
            $sql ->where('visitas.tipo_visita','=',2);
        }



        //$fechas = explode("-",$fecha );

        $fechaini = explode('-', $fechaini);
        $fechafin = explode('-', $fechafin);

        $sql ->whereBetween('visitas.HoraInicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])));

        $data['visitas'] = $sql->get();
        $data['logo'] = auth()->user()->fotoEmp;

        $path = 'reporte_Visitas.pdf';
        return $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('/visitas/listapdf', compact('data'))->download($path);


    }

    public function excel($sup, $seg, $fechaini, $fechafin, $tienda, $id_sup, $razon, $plaza)
    {
        //obtenemos las visitas buscadas por el filtro
        $sql = DB::table('visitas');
        $sql->leftjoin('tiendas', function ($join) {
            $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
        })->leftjoin('users_tiendas', function ($join) {
            $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
        })
            ->leftjoin('checklist', function ($join) {
                $join->on('checklist.Id', '=', 'visitas.checklist_Id');
            }) ->leftjoin('users', function ($join) {
                $join->on('users.id', '=', 'visitas.users_Id');
            })
            ->select('visitas.*', 'tiendas.nombre', 'tiendas.numsuc', 'checklist.nombre as Checklist', 'users.name');
        $sql ->where('visitas.Status','=',131);


        if($id_sup != 0)
        {
            $sql ->where('visitas.users_Id','=',$id_sup);
        }
        $sql ->where('users_tiendas.users_Id','=',auth()->user()->id);
        if($tienda != 0)
        {
            $sql ->where('visitas.tiendas_Id','=',$tienda);
        }
        if($razon != 0)
        {
            $sql ->where('tiendas.razon_Id','=',$razon);
        }

        if($plaza != 0)
        {
            $sql ->where('tiendas.plaza_Id','=',$plaza);
        }

        if($sup == 1 && $seg == 0)
        {
            $sql ->where('visitas.tipo_visita','=',1);
        }
        if($sup == 0 && $seg == 1)
        {
            $sql ->where('visitas.tipo_visita','=',2);
        }

        $data['FechaIni'] = date('d/m/Y', strtotime($fechaini));
        $data['FechaFin'] = date('d/m/Y', strtotime($fechafin));

        //$fechas = explode("-",$fecha );

        $fechaini = explode('-', $fechaini);
        $fechafin = explode('-', $fechafin);

        $sql ->whereBetween('visitas.HoraInicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])));





        $data['visitas'] = $sql->get();
        $data['logo'] = auth()->user()->fotoEmp;

        return  Excel::download(new VisitasExport($data), 'visitas.xlsx');
    }




}
