<?php

namespace App\Http\Controllers\Visitas;

use App\Helpers\Formulas;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Barryvdh\DomPDF\Facade as PDF;
use Mail;
use Illuminate\Mail\Mailable;
use Swift_Mailer;
use \Swift_SmtpTransport as SmtpTransport;
use App\Mail\VisitaMail;
use App\Helpers\Listados;
use App\Mail\MailPassSuc;



class VisitaController extends Controller
{

    //protected $path = '/home/hdammx/public_html/VentumAdmin/uploads/'; //path para host en linea
    protected $path = 'uploads/'; //path para pruebas locales
    protected $pathUploadCli = '/home/hdammx/public_html/VentumClientes/uploads/'; //path para copia en clientes
    public function getListadoTiendas()
    {
        $listado = new Listados();

        return $listado->listaTiendas(auth()->user()->id);
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $listado = new Listados();
        //validamos si la tienda existe en el usuario


        $sql  = DB::table('users_tiendas');
        $sql->where('users_tiendas.users_Id', "=", auth()->user()->id);
        $sql->where('users_tiendas.tiendas_Id', "=", $id);
        $data['existe'] = $sql->count();



        //contamos los to-do concluidos
        $data['concluidos'] = DB::table('visitas_todo')
                            ->leftjoin('visitas', function ($join) {
                                $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
                            })
                            ->leftjoin('tiendas', function ($join) {
                                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
                            })
                            ->where('tiendas.Id', "=", $id)
                            ->where('visitas_todo.Status', "=", 154)->count();

        $data['nuevos'] = DB::table('visitas_todo')
            ->leftjoin('visitas', function ($join) {
                $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
            })
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
            })
            ->where('tiendas.Id', "=", $id)
            ->where('visitas_todo.Status', "=", 151)->count();

        $data['curso'] = DB::table('visitas_todo')
            ->leftjoin('visitas', function ($join) {
                $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
            })
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
            })
            ->where('tiendas.Id', "=", $id)
            ->where('visitas_todo.Status', "=", 152)->count();

        $data['pendientes'] = DB::table('visitas_todo')
            ->leftjoin('visitas', function ($join) {
                $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
            })
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
            })
            ->where('tiendas.Id', "=", $id)
            ->where('visitas_todo.Status', "=", 153)->count();


        $data['totales'] = DB::table('visitas_todo')
            ->leftjoin('visitas', function ($join) {
                $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
            })
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
            })
            ->where('tiendas.Id', "=", $id)->count();

        $data['last_visitas'] = DB::table('visitas')
            ->leftjoin('users', function ($join) {
                $join->on('users.id', '=', 'visitas.users_Id');
            })
            ->select('visitas.*', 'users.name')
            ->where('visitas.Status','=',131)
            ->where('visitas.tipo_visita','=',1)
            ->where('visitas.tiendas_Id','=',$id)
            ->limit(4)
            ->get();


        if($data['existe'] == 1)
        {
            //validamos que el usuario sea supervisor de tienda o superusuario


            $enproc = DB::table('visitas')
                ->where('visitas.Status','=',133)
                ->where('visitas.tipo_visita','=',1)
                ->where('visitas.tiendas_Id','=',$id)
                ->count();

            if($enproc === 1)
            {
                $data['en_proceso'] = DB::table('visitas')
                    ->where('visitas.Status','=',133)
                    ->where('visitas.tipo_visita','=',1)
                    ->where('visitas.tiendas_Id','=',$id)
                    ->first();
            }else
            {
                $data['en_proceso'] =  (object) array("Status" => 132, "Id" => 0);
            }
            $data['info'] =   DB::table('tiendas')
                ->where('tiendas.Id','=',$id)
                ->first();

            $data['config'] =   DB::table('configuracion')
                ->where('configuracion.empresas_Id','=',auth()->user()->empresas_Id)
                ->first();
            //id del cuestionario correspondiente

            $data['cuestionario'] =   DB::table('cuestionario')
                ->where('cuestionario.empresas_Id','=',auth()->user()->empresas_Id)
                ->where('cuestionario.tipos_cuestionario_Id','=',1)
                ->first();

            $data['empresa'] = $data['info']->empresas_Id;

            //buscamos los checklist asignados a puesto
            $data['checklist']  = $listado->listaChecklist(auth()->user()->empleados_Id , auth()->user()->empresas_Id);

            $config = array();
            $config['center'] = 'auto';
            $config['onboundschanged'] = 'if (!centreGot) {
            var mapCentre = map.getCenter();
            marker_0.setOptions({
                position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng())
            });
        }
        centreGot = true;';

            app('map')->initialize($config);

            // set up the marker ready for positioning
            // once we know the users location
            $marker = array();
            app('map')->add_marker($marker);

            $map = app('map')->create_map();
            $data['js'] = $map['js'];
            $data['html'] = $map['html'];

            //buscamos el cuestionario asignado

            $data['tiendas'] = $this->getListadoTiendas();

            return view('/visitas/visita')->with($data);
        }else
        {

            $data['tiendas'] = $this->getListadoTiendas();
            $data['existe'] = 0;
            return view('/tiendas/tiendas')->with( $data);

        }


    }

    public function recuperaVisita(Request $request)
    {
        $formulas = new Formulas();
        $data =   DB::table('visitas')
                        ->leftjoin('checklist', function ($join) {
                            $join->on('checklist.Id', '=', 'visitas.checklist_Id');
                        })->select('visitas.*', 'checklist.nombre as checklist')
                        ->where('visitas.Id','=',$request['id'])
                        ->first();

        $segundos = $formulas->diferenciaSegundos($data->HoraInicio, date('Y-m-d H:i:s'));

        return response()->json(['message' => $data, 'id_visita' => $data->Id, 'campos' => "", 'segundos' =>$segundos] );
    }

    public function valPassTienda(Request $request)
    {
        $data =   DB::table('tiendas')
            ->where('tiendas.Id','=',$request['tiendas_Id'])
            ->where('tiendas.password','=',$request['pass'])
            ->count();
        return response()->json(['message' => $data] );
    }

    public function resetPassTienda(Request $request)
    {

        DB::table('tiendas')
            ->where('tiendas.Id','=',$request['tiendas_Id'])
            ->update([
                    'password' => $request['pass']
                ]
            );

        DB::table('users')
            ->where('users.empleados_Id','=',$request['tiendas_Id'])
            ->update([
                    'password' => bcrypt($request['pass'])
                ]
            );

        //guardamos los campos
        DB::table('cambios_password')->insert([
            [
                'tiendas_Id' => $request['tiendas_Id'],
                'users_Id' => auth()->user()->id,
                'tipo' => $request['tipo'],
                'motivo' => $request['motivo'],
                'pass' => $request['pass'],
                'fecha' => date('Y-m-d H:i:j')
            ]
        ]);

        //buscamos los datos del cambio
        $id_cambio = DB::getPdo()->lastInsertId();

        $datacam =  DB::table('cambios_password')
            ->leftjoin('tiendas', function ($join) {
                $join->on('cambios_password.tiendas_Id', '=', 'tiendas.Id');
            })
            ->leftjoin('users', function ($join) {
                $join->on('cambios_password.users_Id', '=', 'users.id');
            })
            ->select('tiendas.nombre as Tienda', 'tiendas.mail', 'users.name as Usuario', 'cambios_password.*')
            ->where('cambios_password.Id','=',$id_cambio)->first();

        $cambio = collect($datacam)->toArray();


        $data['Sucursal'] = $cambio['Tienda'];
        $data['pass']  = $cambio['pass'];
        $data['Supervisor'] = $cambio['Usuario'];
        $motivo = "";
        if($cambio['motivo'] == 1)
        {
            $motivo = "Olvidé la contraseña";
        }else if($cambio['motivo'] == 2)
        {
            $motivo = "No tengo acceso a la contraseña";
        }else if($cambio['motivo'] == 3)
        {
            $motivo = "Soy el gerente de tienda y deseo cambiarla";
        }


        $data['Motivo'] =$motivo;


        Mail::to($cambio['mail'])->send(new MailPassSuc($data));


        return response()->json(['message' => $cambio['Tienda']] );
    }

    public function valCodeTienda(Request $request)
    {
        $data =   DB::table('tiendas')
            ->where('tiendas.Id','=',$request['tiendas_Id'])
            ->where('tiendas.codigo','=',$request['code'])
            ->count();
        return response()->json(['message' => $data] );
    }


    public function inicia(Request $request)
    {
        $anio = date('Y');
        $mes = date('m');
        $hora = date('Y-m-d H:i:s');
        $sum_campos = 0;

        $data =   DB::table('categorias')
            ->where('categorias.checklist_Id','=',$request['body'])
            ->where('categorias.activo','=',1)->get();
        $categorias = collect($data)->toArray();
        // $campos = array(1,2,3,4,5);
        for($i = 0;$i<count($categorias);$i++)
        {
            $cam = array();
            $catId = $categorias[$i]->Id;
            $datacam =   DB::table('campos')
                 ->leftjoin('campos_valores', function ($join) {
                     $join->on('campos_valores.campos_Id', '=', 'campos.Id');
                 })->select('campos.*', 'campos_valores.valor')
                ->where('campos.categorias_Id','=',$catId)
                ->where('campos_valores.gentodo' , "=", 0)
                ->where('campos.activo','=',1)->get();
            $campos = collect($datacam)->toArray();
            for($j = 0;$j<count($campos);$j++)
            {

                $cam[] = array("Id" => $campos[$j]->Id, "valor" => $campos[$j]->valor, "nombre" => $campos[$j]->nombre, "no_aplica" => $campos[$j]->no_aplica);
                $sum_campos++;
            }
            $cats[] = array("Id" => $categorias[$i]->Id, "nombre" => $categorias[$i]->nombre, "campos" => $cam);
        }

        //guardamos los campos
      DB::table('visitas')->insert([
            [
                'tiendas_Id' => $request['tienda'],
                'checklist_Id' => $request['body'],
                'users_Id' => $request['user'],
                'MesRev' => $mes,
                'AnioRev' => $anio,
                'tipo_visita' => 1,
                'HoraInicio' => $hora,
                'Status' => 133,
                'tipo_aplicacion' => 1
            ]
        ]);
      $vis['id_visita'] = DB::getPdo()->lastInsertId();


        return response()->json(['message' => $cats, 'id_visita' => $vis, 'campos' => $sum_campos] );

        //return view('/checklist/checklist')->with( $data);
    }

    public function getCuestionario(Request $request)
    {
        $data =   DB::table('preguntas')
            ->where('preguntas.cuestionario_Id','=',$request['id'])
            ->get();
        $preguntas = collect($data)->toArray();

        for($i = 0;$i<count($preguntas);$i++)
        {
            $resp = array();
            $pregId = $preguntas[$i]->Id;
            $dataresp =   DB::table('respuestas')
                ->where('respuestas.preguntas_Id','=',$pregId)->get();
            $respuestas = collect($dataresp)->toArray();
            for($j = 0;$j<count($respuestas);$j++)
            {
                $resp[] = array("Id" => $respuestas[$j]->Id, "respuesta" => $respuestas[$j]->respuesta);
            }
            $pregs[] = array("Id" => $preguntas[$i]->Id, "pregunta" => $preguntas[$i]->Pregunta, "tipo" => $preguntas[$i]->tipo, "respuestas" => $resp);
        }

        return response()->json(['message' => $pregs ]);
    }

    public function setCuestionario(Request $request)
    {

        //buscamos si ya existe la prgunta respondida

        $preg =   DB::table('visitas_cuestionario')
            ->where('visitas_cuestionario.pregunta_Id','=',$request['pregunta'])
            ->where('visitas_cuestionario.visitas_Id','=',$request['visitas_Id'])
            ->count();
        //vemos que tipo vamos a capturar
        if($preg == 0)
        {
            //nueva pregunta

            if($request['tipo'] == 0)
            {
                DB::table('visitas_cuestionario')->insert([
                    [
                        'visitas_Id' => $request['visitas_Id'],
                        'pregunta_Id' => $request['pregunta'],
                        'Abierta' => $request['respuesta']
                    ]
                ]);

            }else
            {
                DB::table('visitas_cuestionario')->insert([
                    [
                        'visitas_Id' => $request['visitas_Id'],
                        'pregunta_Id' => $request['pregunta'],
                        'respuesta_Id' => $request['respuesta']
                    ]
                ]);
            }


        }else
        {
            //actualizar preguntas

            if($request['tipo'] == 0)
            {
                DB::table('visitas_cuestionario')
                    ->where('visitas_cuestionario.pregunta_Id','=',$request['pregunta'])
                    ->where('visitas_cuestionario.visitas_Id','=',$request['visitas_Id'])
                    ->update(['Abierta' => $request['respuesta']]);

            }else
            {
                DB::table('visitas_cuestionario')
                    ->where('visitas_cuestionario.pregunta_Id','=',$request['pregunta'])
                    ->where('visitas_cuestionario.visitas_Id','=',$request['visitas_Id'])
                    ->update([ 'respuesta_Id' => $request['respuesta']]);
            }

        }


        return response()->json(['message' => $request['tipo']]);
    }


    public function recupera(Request $request)
    {
        $anio = date('Y');
        $mes = date('m');
        $hora = date('Y-m-d H:i:s');
        $sum_campos = 0;

        $data =   DB::table('categorias')
            ->where('categorias.checklist_Id','=',$request['body'])
            ->where('categorias.activo','=',1)->get();
        $categorias = collect($data)->toArray();
        // $campos = array(1,2,3,4,5);
        for($i = 0;$i<count($categorias);$i++)
        {
            $cam = array();
            $catId = $categorias[$i]->Id;
            $datacam =   DB::table('campos')
                ->leftjoin('campos_valores', function ($join) {
                    $join->on('campos_valores.campos_Id', '=', 'campos.Id');
                })->select('campos.*', 'campos_valores.valor')
                ->where('campos.categorias_Id','=',$catId)
                ->where('campos_valores.gentodo' , "=", 0)
                ->where('campos.activo','=',1)->get();
            $campos = collect($datacam)->toArray();
            for($j = 0;$j<count($campos);$j++)
            {
                //buscamos si tienen dado de alta algun campo
                $campo_notodo =   DB::table('visitas_campos')
                    ->where('visitas_campos.campos_Id','=',$campos[$j]->Id)
                    ->where('visitas_campos.visitas_Id','=',$request['vis'])
                    ->count();
                if($campo_notodo === 1)
                {
                    //si hay campo normal
                    $campo_notodo =   DB::table('visitas_campos')
                        ->where('visitas_campos.campos_Id','=',$campos[$j]->Id)
                        ->where('visitas_campos.visitas_Id','=',$request['vis'])
                        ->first();
                    $info_campo = array("gentodo" => 0,
                                        "existe" => 1,
                                         "comentario" => $campo_notodo->comentario,
                                         "foto" => $campo_notodo->foto,
                                         "no_aplica" => $campo_notodo->no_aplica );
                }else
                {
                    //no hay campo normal, buscamos si es to-do
                    $campo_todo =   DB::table('visitas_todo')
                        ->where('visitas_todo.campos_Id','=',$campos[$j]->Id)
                        ->where('visitas_todo.visitas_Id','=',$request['vis'])
                        ->count();

                    if($campo_todo === 1)
                    {
                        //agreamos informacion adicional al campo que se va a mostrar
                        $campo_todo =   DB::table('visitas_todo')
                                        ->where('visitas_todo.campos_Id','=',$campos[$j]->Id)
                                        ->where('visitas_todo.visitas_Id','=',$request['vis'])
                                        ->first();
                        $info_campo = array("gentodo" => 1,
                                            "existe" => 1,
                                            "comentario" => $campo_todo->descripcion,
                                            "foto" => $campo_todo->ImagenIni,
                                            "no_aplica" => 0 );
                    }else
                    {
                        //agregamos información vacia, el campo no tendra datos adicionales para su configuracion
                        $info_campo = array("gentodo" => 2,
                            "existe" => 0,
                            "comentario" => "",
                            "foto" => "",
                            "no_aplica" => 0 );
                    }
                }

                $cam[] = array("Id" => $campos[$j]->Id, "valor" => $campos[$j]->valor, "nombre" => $campos[$j]->nombre, "no_aplica" => $campos[$j]->no_aplica, "info_campo" => $info_campo);
                $sum_campos++;
            }


            $cats[] = array("Id" => $categorias[$i]->Id, "nombre" => $categorias[$i]->nombre, "campos" => $cam);
        }

        //sumamos los campos que ya estan registrados
        $campo_sumnotodo =   DB::table('visitas_campos')
            ->where('visitas_campos.visitas_Id','=',$request['vis'])
            ->count();
        $campo_sumsitodo =   DB::table('visitas_todo')
            ->where('visitas_todo.visitas_Id','=',$request['vis'])
            ->count();

        $sum_campos_tot = $campo_sumnotodo + $campo_sumsitodo;

        return response()->json(['message' => $cats,  'campos' => $sum_campos, "cam_cal" => $sum_campos_tot] );

        //return view('/checklist/checklist')->with( $data);
    }

    public function categorias(Request $request)
    {

        $data =   DB::table('categorias')
                 ->where('categorias.checklist_Id','=',$request['body'])->get();
        $categorias = collect($data)->toArray();
       // $campos = array(1,2,3,4,5);
        for($i = 0;$i<count($categorias);$i++)
        {
            $cam = array();
            $catId = $categorias[$i]->Id;
            $datacam =   DB::table('campos')
                         ->where('campos.categorias_Id','=',$catId)->get();
            $campos = collect($datacam)->toArray();
            for($j = 0;$j<count($campos);$j++)
            {
                $cam[] = array("Id" => $campos[$j]->Id, "nombre" => $campos[$j]->nombre);
            }
            $cats[] = array("Id" => $categorias[$i]->Id, "nombre" => $categorias[$i]->nombre, "campos" => $cam);
        }
        return response()->json(['message' => $cats] );

        //return view('/checklist/checklist')->with( $data);
    }

    public function registroCampo(Request $request)
    {

        $noaplica = 0;
        if($request['gentodo'] == 3)
        {
            $noaplica = 1;
        }
        /**Buscamos en visitas_campos y en visitas_todo**/
        $campo_notodo =   DB::table('visitas_campos')
            ->where('visitas_campos.campos_Id','=',$request['campo_Id'])
            ->where('visitas_campos.visitas_Id','=',$request['visita_Id'])
            ->count();
        $campo_sitodo =   DB::table('visitas_todo')
            ->where('visitas_todo.campos_Id','=',$request['campo_Id'])
            ->where('visitas_todo.visitas_Id','=',$request['visita_Id'])
            ->count();

        //condicionales
        if($campo_notodo == 1 &&  $campo_sitodo == 1)
        {
            if($request['gentodo'] == 1)
            {
                //borrar registro en visitas_campos, actualizamos en visitas_todo
                DB::table('visitas_campos')->where('campos_Id', '=', $request['campo_Id'])->where('visitas_campos.visitas_Id','=',$request['visita_Id'])->delete();

                DB::table('visitas_todo')
                    ->where('campos_Id', $request['campo_Id'])
                    ->where('visitas_todo.visitas_Id','=',$request['visita_Id'])
                    ->update(['valor' => $request['valor'],
                              'fecha_inicio' => date('Y-m-d'),
                              'descripcion' => $request['comentario'],

                              'Status' => 152]);
            }else
            {


                //borrar registro en visitas_todo, actualizamos en visitas_camopos
                DB::table('visitas_todo')->where('campos_Id', '=', $request['campo_Id'])->where('visitas_todo.visitas_Id','=',$request['visita_Id'])->delete();

                DB::table('visitas_campos')
                    ->where('campos_Id', $request['campo_Id'])
                    ->where('visitas_campos.visitas_Id','=',$request['visita_Id'])
                    ->update(['valor' => $request['valor'],
                             'comentario' => $request['comentario'],
                            'no_aplica' => $noaplica,

                             ]
                            );

            }


        }else if ($campo_notodo == 1 &&  $campo_sitodo == 0)
        {
            if($request['gentodo'] == 1)
            {
                //borrar registro en visitas_campos, actualizamos en visitas_todo
                DB::table('visitas_campos')->where('campos_Id', '=', $request['campo_Id'])->where('visitas_campos.visitas_Id','=',$request['visita_Id'])->delete();

                DB::table('visitas_todo')->insert(
                    [
                        'visitas_Id' => $request['visita_Id'],
                        'campos_Id' => $request['campo_Id'],
                        'valor' => $request['valor'],
                        'fecha_inicio' => date('Y-m-d'),
                        'descripcion' => $request['comentario'],

                        'Status' => 152,

                    ]
                );

            }else
            {

                DB::table('visitas_campos')
                    ->where('campos_Id', $request['campo_Id'])
                    ->where('visitas_campos.visitas_Id','=',$request['visita_Id'])
                    ->update(['valor' => $request['valor'],
                              'comentario' => $request['comentario'],
                              'no_aplica' => $noaplica,

                             ]);

            }


        }else if ($campo_notodo == 0 &&  $campo_sitodo == 1)
        {
            if($request['gentodo'] == 1)
            {

                DB::table('visitas_todo')
                    ->where('campos_Id', $request['campo_Id'])
                    ->where('visitas_todo.visitas_Id','=',$request['visita_Id'])
                    ->update(['valor' => $request['valor'],
                        'fecha_inicio' => date('Y-m-d'),
                        'descripcion' => $request['comentario'],

                        'Status' => 152]);
            }else
            {
                //borrar registro en visitas_todo, actualizamos en visitas_camopos
                DB::table('visitas_todo')->where('campos_Id', '=', $request['campo_Id'])->where('visitas_todo.visitas_Id','=',$request['visita_Id'])->delete();


                DB::table('visitas_campos')->insert(
                    [
                        'visitas_Id' => $request['visita_Id'],
                        'campos_Id' => $request['campo_Id'],
                        'no_aplica' => $noaplica,
                        'valor' => $request['valor'],
                        'comentario' => $request['comentario']

                    ]
                );


            }

        }else if ($campo_notodo == 0 &&  $campo_sitodo == 0)
        {
            if($request['gentodo'] == 1)
            {

                DB::table('visitas_todo')->insert(
                    [
                        'visitas_Id' => $request['visita_Id'],
                        'campos_Id' => $request['campo_Id'],
                        'valor' => $request['valor'],
                        'fecha_inicio' => date('Y-m-d'),
                        'descripcion' => $request['comentario'],

                        'Status' => 151,

                    ]
                );

            }else
            {
                DB::table('visitas_campos')->insert(
                    [
                        'visitas_Id' => $request['visita_Id'],
                        'campos_Id' => $request['campo_Id'],
                        'valor' => $request['valor'],
                        'no_aplica' => $noaplica,
                        'comentario' => $request['comentario']

                    ]
                );
            }
        }

        //sumamos los campos que ya estan registrados
        $campo_sumnotodo =   DB::table('visitas_campos')
            ->where('visitas_campos.visitas_Id','=',$request['visita_Id'])
            ->count();
       $campo_sumsitodo =   DB::table('visitas_todo')
            ->where('visitas_todo.visitas_Id','=',$request['visita_Id'])
            ->count();

        $sumCampos = $campo_sumnotodo + $campo_sumsitodo;

        return response()->json(['message' => $sumCampos ]);
    }


    public function registroFotoCampo(Request $request)
    {

        $file = $request->file('photo');

        $nombre = "_Foto_Campo";
        $extension = $file->getClientOriginalExtension();
        $random = str_random(10);
        $fecha = date('Ymdhis');
        $nombre = $random."_".$fecha."-".$nombre.".".$extension;
        $file->move($this->path.'FotoTodo/', $nombre);
        $ruta = $this->path.'FotoTodo/'.$nombre;


        return response()->json(['message' =>  $nombre, 'path' =>  $ruta]);
    }

    public function registroFotoCampoCom(Request $request)
    {

        $file = $request->file('photo');

        $nombre = "_Foto_Campo_com";
        $extension = $file->getClientOriginalExtension();
        $random = str_random(10);
        $fecha = date('Ymdhis');
        $nombre = $random."_".$fecha."-".$nombre.".".$extension;
        $file->move($this->path.'FotoTodoCom/', $nombre);
        $ruta = $this->path.'FotoTodoCom/'.$nombre;


        return response()->json(['message' =>  $nombre, 'path' =>  $ruta]);
    }

    public function getTodosVisita(Request $request)
    {

        $sql = DB::table('visitas_todo');
        $sql->leftjoin('campos', function ($join) {
            $join->on('campos.Id', '=', 'visitas_todo.campos_Id');
        })->select('visitas_todo.*', 'campos.nombre', 'campos.smart');
        $sql ->where('visitas_todo.visitas_Id','=',$request['visita_Id']);
        $todos = $sql->get();

        return response()->json(['message' => $todos] );

    }

    public function fotos(Request $request)
    {

       $sql = DB::table('visitas_fotos');
        $sql ->where('visitas_fotos.visitas_Id','=',$request['visita_Id']);
        $fotos = $sql->get();

        return response()->json(['message' => $fotos] );


    }

    public function setFotoVis(Request $request)
    {

        DB::table('visitas_fotos')->insert(
            [
                'visitas_Id' => $request['visita_Id'],
                'Foto' => $request['Nombre'],
                'Descripcion' => 'Sin descripción'

            ]
        );

        return response()->json(['message' => $request->all()] );


    }

    public function genGraficaTodo(Request $request)
    {
        $anio = date('Y');
        $mes = date('m');
        $meses = array();
        $todos_creados = array();

        $todos_terminados = array();
        $arrMes = array("", 'Enero', "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

        //listamos los años
        for($i = 1; $i <= $mes; $i++)
        {
            //listamos los meses
            $meses[] = array($arrMes[$i]." ". $anio);
            //buscamos los to-do creados de cada mes
            $creados = DB::table('visitas_todo')
                ->leftjoin('visitas', function ($join) {
                    $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
                })
                ->leftjoin('tiendas', function ($join) {
                    $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
                })
                ->where('tiendas.Id', "=", $request['tiendas_Id'])
                ->whereMonth('visitas_todo.fecha_inicio', "=", $i)
                ->whereYear('visitas_todo.fecha_inicio', "=", $anio)
                ->count();
            $todos_creados[] = $creados;
            $terminados = DB::table('visitas_todo')
                ->leftjoin('visitas', function ($join) {
                    $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
                })
                ->leftjoin('tiendas', function ($join) {
                    $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
                })
                ->where('tiendas.Id', "=", $request['tiendas_Id'])
                ->where('visitas_todo.Status', "=", 154)
                ->whereMonth('visitas_todo.fecha_inicio', "=", $i)
                ->whereYear('visitas_todo.fecha_inicio', "=", $anio)
                ->count();
            $todos_terminados[] = $terminados;



        }
        return response()->json(['meses' => $meses, 'todos' => $todos_creados, 'terminados' => $todos_terminados] );
    }

    public function getInfoTodo(Request $request)
    {
        $sql = DB::table('visitas_todo');
        $sql->leftjoin('campos', function ($join) {
            $join->on('campos.Id', '=', 'visitas_todo.campos_Id');
        })->select('visitas_todo.*', 'campos.nombre');
        $sql ->where('visitas_todo.Id','=',$request['campo_Id']);
        $campo = $sql->first();

        return response()->json(['message' => $campo] );
    }

    public function updateTodo(Request $request)
    {

        DB::table('visitas_todo')
            ->where('Id', $request['campo_Id'])
            ->update(['fecha_inicio' => date('Y-m-d H:j:s'),
                      'descripcion' => $request['comentario'],
                      'Status' => $request['status']]);

        return response()->json(['message' => "OK"] );
    }


    public function putFotoCampo(Request $request)
    {
        $campo_notodo =   DB::table('visitas_campos')
            ->where('visitas_campos.campos_Id','=',$request['campo_Id'])
            ->where('visitas_campos.visitas_Id','=',$request['visita_Id'])
            ->count();

        if($campo_notodo == 1)
        {
            //actualizamos el to-do
            DB::table('visitas_campos')
                ->where('campos_Id', $request['campo_Id'])
                ->where('visitas_campos.visitas_Id','=',$request['visita_Id'])
                ->update([
                    'foto' => $request['foto'],
                ]);

        }else
        {
            $campo_sitodo =   DB::table('visitas_todo')
                ->where('visitas_todo.campos_Id','=',$request['campo_Id'])
                ->where('visitas_todo.visitas_Id','=',$request['visita_Id'])
                ->count();
            if($campo_sitodo == 1)
            {
                DB::table('visitas_todo')
                    ->where('campos_Id', $request['campo_Id'])
                    ->where('visitas_todo.visitas_Id','=',$request['visita_Id'])
                    ->update([
                        'ImagenIni' => $request['foto'],
                        'Status' => 152
                    ]);
            }else
            {
                DB::table('visitas_todo')->insert(
                    [
                        'visitas_Id' => $request['visita_Id'],
                        'campos_Id' => $request['campo_Id'],
                        'valor' => 10,
                        'fecha_inicio' => date('Y-m-d'),
                        'ImagenIni' => $request['foto'],
                        'Status' => 152
                    ]
                );
            }
        }
        return response()->json(['message' => "OK"] );
    }

    public function finalVisita($id)
    {

        $data['visita'] = DB::table('visitas')
                        ->leftjoin('checklist', function ($join) {
                            $join->on('checklist.Id', '=', 'visitas.checklist_Id');
                        })
                        ->leftjoin('tiendas', function ($join) {
                            $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
                        })
                        ->leftjoin('users', function ($join) {
                            $join->on('users.Id', '=', 'visitas.users_Id');
                        })
                        ->select('visitas.*', 'checklist.nombre', 'tiendas.nombre as tienda', 'users.name')
                        ->where('visitas.Id','=',$id)
                        ->first();

        $data['sum_opcional'] =  DB::table('visitas_campos')
            ->where('visitas_Id',"=", $id)
            ->where('no_aplica',"=", 1)
            ->sum('valor');

        $data['sum_final'] =  DB::table('visitas_campos')
                            ->where('visitas_Id',"=", $id)
                            ->sum('valor');

        $data['sum_total'] = DB::table('campos_valores')
                            ->leftjoin('campos', function ($join) {
                                $join->on('campos.Id', '=', 'campos_valores.campos_Id');
                            })
                            ->leftjoin('categorias', function ($join) {
                                $join->on('categorias.Id', '=', 'campos.categorias_Id');
                            })
                            ->leftjoin('checklist', function ($join) {
                                $join->on('checklist.Id', '=', 'categorias.checklist_Id');
                            })
                            ->select('campos_valores.valor')
                            ->where('checklist.Id',"=", $data['visita']->checklist_Id)
                            ->sum('campos_valores.valor');
        $sum_total = $data['sum_total'] - $data['sum_opcional'] ;

        $sum_final = $data['sum_final'] - $data['sum_opcional'] ;

        $data['tiendas'] = $this->getListadoTiendas();

        $data['calif'] = $sum_final / $sum_total * 100;
        //actualizamos visita
        DB::table('visitas')
            ->where('Id', $id)
            ->update(['Calif' => $data['calif']
            ]);
        //llenado de resultados
        $data['categorias'] =   DB::table('categorias')
                                ->where('categorias.checklist_Id','=',$data['visita']->checklist_Id)
                                ->get();
        $categorias = collect($data['categorias'])->toArray();
        // $campos = array(1,2,3,4,5);
        for($i = 0;$i<count($categorias);$i++)
        {
            $catId = $categorias[$i]->Id;
            $todo = DB::table('visitas_todo')
                ->leftjoin('campos', function ($join) {
                    $join->on('campos.Id', '=', 'visitas_todo.campos_Id');
                })
                ->leftjoin('familias', function ($join) {
                    $join->on('familias.Id', '=', 'campos.familias_Id');
                })
                ->select('visitas_todo.*', 'campos.nombre', 'familias.Nombre as familia' )
                ->where('campos.categorias_Id','=',$catId)
                ->where('visitas_todo.visitas_Id',"=", $data['visita']->Id)
                ->get();
            $campos = collect($todo)->toArray();
            $cam = array();
            for($j = 0;$j<count($campos);$j++)
            {
                $cam[] = array( "nombre" => $campos[$j]->nombre,
                                "familia" => $campos[$j]->familia,
                                "descripcion" => $campos[$j]->descripcion,
                                "ImagenIni" => $campos[$j]->ImagenIni);
            }

            $sum_tot_cat = DB::table('campos_valores')
                ->leftjoin('campos', function ($join) {
                    $join->on('campos.Id', '=', 'campos_valores.campos_Id');
                })
                ->leftjoin('categorias', function ($join) {
                    $join->on('categorias.Id', '=', 'campos.categorias_Id');
                })
                ->leftjoin('checklist', function ($join) {
                    $join->on('checklist.Id', '=', 'categorias.checklist_Id');
                })
                ->select('campos_valores.valor')
                ->where('checklist.Id',"=", $data['visita']->checklist_Id)
                ->where('categorias.Id',"=", $catId)
                ->sum('campos_valores.valor');
            $sum_todo_cat =  DB::table('visitas_campos')
                            ->leftjoin('campos', function ($join) {
                                $join->on('campos.Id', '=', 'visitas_campos.campos_Id');
                            })
                            ->leftjoin('categorias', function ($join) {
                                $join->on('categorias.Id', '=', 'campos.categorias_Id');
                            })
                            ->where('categorias.Id',"=", $catId)
                            ->where('visitas_Id',"=", $id)
                            ->sum('valor');

            $cats[] = array("Id" => $categorias[$i]->Id, "sum_tot" => $sum_tot_cat, "sum_real" => $sum_todo_cat,  "nombre" => $categorias[$i]->nombre, "todo" => $cam);


        }
        $data['cats'] = $cats;

        //obtenemos las fotos de la visita
        $data['fotos'] = DB::table('visitas_fotos')
                        ->where('visitas_Id', $id)
                        ->get();
        $data['visita_id'] = $id;

        return view('/visitas/final')->with($data);
    }



    public function setLocalizacion(Request $request)
    {

        DB::table('visitas')
            ->where('Id', $request['id'])
            ->update(['lat' => $request['lat'],
                'lon' => $request['lon']
                ]);

        return response()->json(['message' => "OK"] );
    }

    public function setTimeFinal(Request $request)
    {

        DB::table('visitas')
            ->where('Id', $request['id'])
            ->update(['Duracion' => $request['time'],
                'HoraFin' => date('Y-m-d H:i:s'),
                'Archivo' => 'reporteVisita'.$request['id'].'.pdf',
                'Status' => 131
            ]);


        /**GENERACION DEL PDF**/
        /**
         * toma en cuenta que para ver los mismos
         * datos debemos hacer la misma consulta
         **/

        $data['visita'] = DB::table('visitas')
            ->leftjoin('checklist', function ($join) {
                $join->on('checklist.Id', '=', 'visitas.checklist_Id');
            })
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
            })
            ->leftjoin('plazas', function ($join) {
                $join->on('plazas.Id', '=', 'tiendas.plaza_Id');
            })
            ->leftjoin('razon_social', function ($join) {
                $join->on('razon_social.Id', '=', 'tiendas.razon_Id');
            })
            ->leftjoin('users', function ($join) {
                $join->on('users.Id', '=', 'visitas.users_Id');
            })
            ->select('visitas.*', 'checklist.nombre as checklist', 'tiendas.nombre as tienda', 'tiendas.numsuc', 'tiendas.mail as mailsuc', 'users.email as mailsup', 'users.name', 'plazas.plaza', 'razon_social.nombre as razon', 'razon_social.foto as razonFoto')
            ->where('visitas.Id','=',$request['id'])
            ->first();

        $data['sum_opcional'] =  DB::table('visitas_campos')
            ->where('visitas_Id',"=", $request['id'])
            ->where('no_aplica',"=", 1)
            ->sum('valor');

        $data['sum_final'] =  DB::table('visitas_campos')
            ->where('visitas_Id',"=", $request['id'])
            ->sum('valor');

        $data['sum_total'] = DB::table('campos_valores')
            ->leftjoin('campos', function ($join) {
                $join->on('campos.Id', '=', 'campos_valores.campos_Id');
            })
            ->leftjoin('categorias', function ($join) {
                $join->on('categorias.Id', '=', 'campos.categorias_Id');
            })
            ->leftjoin('checklist', function ($join) {
                $join->on('checklist.Id', '=', 'categorias.checklist_Id');
            })
            ->select('campos_valores.valor')
            ->where('checklist.Id',"=", $data['visita']->checklist_Id)
            ->sum('campos_valores.valor');
        $sum_total = $data['sum_total'] - $data['sum_opcional'] ;

        $sum_final = $data['sum_final'] - $data['sum_opcional'] ;

        if($sum_total != 0)
        {
            $data['calif'] = $sum_final / $sum_total * 100;
        }else
        {
            $data['calif'] = 0;
        }

        //actualizamos visita
        DB::table('visitas')
            ->where('Id', $request['id'])
            ->update(['Calif' => $data['calif']
            ]);
        //llenado de resultados
        $data['categorias'] =   DB::table('categorias')
            ->where('categorias.checklist_Id','=',$data['visita']->checklist_Id)
            ->get();
        $categorias = collect($data['categorias'])->toArray();
        // $campos = array(1,2,3,4,5);
        for($i = 0;$i<count($categorias);$i++)
        {
            $catId = $categorias[$i]->Id;
            $todo = DB::table('visitas_todo')
                ->leftjoin('campos', function ($join) {
                    $join->on('campos.Id', '=', 'visitas_todo.campos_Id');
                })
                ->leftjoin('familias', function ($join) {
                    $join->on('familias.Id', '=', 'campos.familias_Id');
                })
                ->select('visitas_todo.*', 'campos.nombre', 'familias.Nombre as familia' )
                ->where('campos.categorias_Id','=',$catId)
                ->where('visitas_todo.visitas_Id',"=", $data['visita']->Id)
                ->get();
            $campos = collect($todo)->toArray();
            $cam = array();
            for($j = 0;$j<count($campos);$j++)
            {
                $cam[] = array( "nombre" => $campos[$j]->nombre,
                    "familia" => $campos[$j]->familia,
                    "descripcion" => $campos[$j]->descripcion,
                    "ImagenIni" => $campos[$j]->ImagenIni);
            }

            /**campos que no fueron to-do**/
            $todono = DB::table('visitas_campos')
                ->leftjoin('campos', function ($join) {
                    $join->on('campos.Id', '=', 'visitas_campos.campos_Id');
                })
                ->leftjoin('familias', function ($join) {
                    $join->on('familias.Id', '=', 'campos.familias_Id');
                })
                ->select('visitas_campos.*', 'campos.nombre', 'familias.Nombre as familia' )
                ->where('campos.categorias_Id','=',$catId)
                ->where('visitas_campos.visitas_Id',"=", $data['visita']->Id)
                ->get();
            $campos_no = collect($todono)->toArray();
            $cams = array();
            $na = array();
            for($k = 0;$k<count($campos_no);$k++)
            {
                if($campos_no[$k]->comentario != "No se agregó comentario" || $campos_no[$k]->foto != "no_imagen.jpg")
                {

                    if($campos_no[$k]->no_aplica == 0)
                    {
                        $cams[] = array( "nombre" => $campos_no[$k]->nombre,
                            "familia" => $campos_no[$k]->familia,
                            "descripcion" => $campos_no[$k]->comentario,
                            "ImagenIni" => $campos_no[$k]->foto);

                    }


                }

                if($campos_no[$k]->no_aplica == 1)
                {
                    $na[] = array( "nombre" => $campos_no[$k]->nombre,
                        "familia" => $campos_no[$k]->familia,
                        "descripcion" => $campos_no[$k]->comentario,
                        "ImagenIni" => $campos_no[$k]->foto);

                }

            }
            /****/

            $sum_tot_cat = DB::table('campos_valores')
                ->leftjoin('campos', function ($join) {
                    $join->on('campos.Id', '=', 'campos_valores.campos_Id');
                })
                ->leftjoin('categorias', function ($join) {
                    $join->on('categorias.Id', '=', 'campos.categorias_Id');
                })
                ->leftjoin('checklist', function ($join) {
                    $join->on('checklist.Id', '=', 'categorias.checklist_Id');
                })
                ->select('campos_valores.valor')
                ->where('checklist.Id',"=", $data['visita']->checklist_Id)
                ->where('categorias.Id',"=", $catId)
                ->sum('campos_valores.valor');
            $sum_todo_cat =  DB::table('visitas_campos')
                ->leftjoin('campos', function ($join) {
                    $join->on('campos.Id', '=', 'visitas_campos.campos_Id');
                })
                ->leftjoin('categorias', function ($join) {
                    $join->on('categorias.Id', '=', 'campos.categorias_Id');
                })
                ->where('categorias.Id',"=", $catId)
                ->where('visitas_Id',"=", $request['id'])
                ->sum('valor');

            $cats[] = array("Id" => $categorias[$i]->Id,
                "sum_tot" => $sum_tot_cat,
                "sum_real" => $sum_todo_cat,
                "nombre" => $categorias[$i]->nombre,
                "todo" => $cam,
                "campos" => $cams,
                "no_aplica" => $na);


        }

        $data['cats'] = $cats;

        //obtenemos las fotos de la visita
        $data['fotos'] = DB::table('visitas_fotos')
            ->where('visitas_Id', $request['id'])
            ->get();
        $data['visita_id'] = $request['id'];

        /**cuestionario de visita**/


        $data['cuestionario'] = DB::table('visitas_cuestionario')
                                ->leftjoin('preguntas', function ($join) {
                                    $join->on('preguntas.Id', '=', 'visitas_cuestionario.pregunta_Id');
                                })
                                ->leftjoin('respuestas', function ($join) {
                                    $join->on('respuestas.Id', '=', 'visitas_cuestionario.respuesta_Id');
                                })
                                ->select('visitas_cuestionario.*', 'preguntas.Pregunta', 'preguntas.tipo', 'respuestas.respuesta')
                                ->where('visitas_Id', $request['id'])
                                ->get();

        //$pdf = PDF::loadView('/visitas/reporte', compact('data'));
        $nameReporte = "reporteVisita_".$request['id'].".pdf" ;
         //$pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('/visitas/reporte', compact('data'))->save('/uploads/Reportes/ok.pdf');
        //$pdf = DF::loadView('/visitas/reporte', compact('data'))->save( 'uploads/pdfname.pdf' );
        $path = 'uploads/Reportes/reporteVisita'.$request['id'].'.pdf';
        $data['mailSuc'] = $data['visita']->mailsuc;
        $data['nameSuc'] = $data['visita']->tienda;
        $data['mailSup'] = $data['visita']->mailsup;
        $data['nameSup'] = $data['visita']->name;

        /**ENVIOS DE CORREO ADICIONALES, de la tabla visitas_correo**/
        $dat_correo =   DB::table('visitas_correo')
            ->where('visitas_correo.empresas_Id','=',auth()->user()->empresas_Id)
            ->get();
        $no_correo =   DB::table('visitas_correo')
            ->where('visitas_correo.empresas_Id','=',auth()->user()->empresas_Id)
            ->count();
        if($no_correo != 0)
        {
            $envios = collect($dat_correo)->toArray();
            $env = array();
            for($i = 0;$i<count($envios);$i++)
            {
                //validamos que el envio sea de checklist o normal
                if($envios[$i]->tipo == 0 || $envios[$i]->tipo == 1 )
                {
                    //buscamos los empleados conforme a la configuracion del envio
                    $sql =  DB::table('empleados')
                        ->where('empleados.empresas_Id','=',auth()->user()->empresas_Id)
                        ->where('empleados.activo',"=", 1);
                    //filtramos los puestos
                    $sql->where('empleados.puesto_Id',"=", $envios[$i]->puesto_Id);
                    //filtramos el empleado
                    if($envios[$i]->empleados_Id  != 0)
                    {
                        $sql->where('empleados.Id',"=", $envios[$i]->empleados_Id);
                    }
                    //filtramos la razon
                    if($envios[$i]->razon_Id  != 0)
                    {
                        $sql->where('empleados.razon_Id',"=", $envios[$i]->razon_Id);
                    }
                    //filtramos la plaza
                    if($envios[$i]->plaza_Id  != 0)
                    {
                        $sql->where('empleados.plaza_Id',"=", $envios[$i]->plaza_Id);
                    }
                    $data_emp = $sql->get();

                    $empleados = collect($data_emp)->toArray();

                    //preparamos el envio
                    for($j = 0;$j<count($empleados);$j++)
                    {
                        $env[] = array("mail" => $empleados[$j]->mail, "empleado" => $empleados[$j]->nombre." ".$empleados[$j]->apepat." ".$empleados[$j]->apemat);
                    }


                }
            }


        }else
        {
            $env[] = array("mail" => "harroyo@ventumsupervision.com", "empleado" => "Sin configuracion de correos adicionales");
        }
        $data['envios'] = $env;

        /**FIN**/


        /**Prueba de envio de correo con multiples drivers**/
        // Backup your default mailer

        /**MAIL_DRIVER=
        MAIL_PORT=
        MAIL_HOST=
        MAIL_USERNAME=
        MAIL_PASSWORD=**/
        if(auth()->user()->empresas_Id == 6)
        {
            // Setup a new SmtpTransport instance for Gmail
            $transport = new SmtpTransport();
            $transport->setHost('smtp.mailgun.org');
            $transport->setPort(587);
            $transport->setEncryption('tls');
            $transport->setUsername('postmaster@envios.ventumsupervision.com');
            $transport->setPassword('b4139af002e42bdcdebf82604feeb1c0-c8e745ec-3c370a1d');


            // Assign a new SmtpTransport to SwiftMailer
            $driver = new Swift_Mailer($transport);

            // Assign it to the Laravel Mailer
            Mail::setSwiftMailer($driver);
        }else
        {
            // Setup a new SmtpTransport instance for Gmail
            $transport = new SmtpTransport();
            $transport->setHost('smtp-relay.sendinblue.com');
            $transport->setPort(587);
            $transport->setEncryption('tls');
            $transport->setUsername('ventum@ventummx.com');
            $transport->setPassword('LTr9qvOFVBC7PX6H');


            // Assign a new SmtpTransport to SwiftMailer
            $driver = new Swift_Mailer($transport);

            // Assign it to the Laravel Mailer
            Mail::setSwiftMailer($driver);
        }

        // Send your message
        Mail::to('hdam23@gmail.com')->send(new VisitaMail($data));

        /****/

        /**FIN DEL PDF**/
         PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('/visitas/reporte', compact('data'))->save($path);
        return response()->json(['message' => $data['envios'][0]['mail']] );
    }

    private function setEnv($key, $value)
    {
        file_put_contents(app()->environmentFilePath(), str_replace(
            $key . '=' . env($value),
            $key . '=' . $value,
            file_get_contents(app()->environmentFilePath())
        ));
    }

    public function cancelaVisita(Request $request)
    {

        DB::table('visitas')
            ->where('Id', $request['id'])
            ->update([
                'HoraFin' => date('Y-m-d H:i:s'),
                'Status' => 132
            ]);

        //borrar registro en visitas_todo, actualizamos en visitas_camopos
        DB::table('visitas_todo')->where('visitas_Id', '=', $request['id'])->delete();
        DB::table('visitas_campos')->where('visitas_Id', '=', $request['id'])->delete();

        return response()->json(['message' => "OK"] );
    }

    public function setDescFoto(Request $request)
    {
        DB::table('visitas_fotos')
            ->where('Id', $request['id'])
            ->update(['Descripcion' => $request['comentario']
            ]);

        return response()->json(['message' => "OK"] );
    }

    public function putFotoCom(Request $request)
    {
        DB::table('visitas_campo_foto')->insert(
            [
                'visitas_campo_Id' => $request['campo_Id'],
                'foto' => $request['foto'],
                'descripcion' => $request['comentario']
            ]
        );

        $data =   DB::table('visitas_campo_foto')
                  ->where('visitas_campo_foto.visitas_campo_Id','=',$request['campo_Id'])
                  ->get();
        return response()->json(['message' => $data] );
    }

    public function getFotoComs(Request $request)
    {
        $data =   DB::table('visitas_campo_foto')
            ->where('visitas_campo_foto.visitas_campo_Id','=',$request['campo_Id'])
            ->get();
        return response()->json(['message' => $data] );
    }

    public function visitaPdf($id)
    {
        /**
         * toma en cuenta que para ver los mismos
         * datos debemos hacer la misma consulta
         **/

        $data['visita'] = DB::table('visitas')
            ->leftjoin('checklist', function ($join) {
                $join->on('checklist.Id', '=', 'visitas.checklist_Id');
            })
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
            })
            ->leftjoin('plazas', function ($join) {
                $join->on('plazas.Id', '=', 'tiendas.plaza_Id');
            })
            ->leftjoin('razon_social', function ($join) {
                $join->on('razon_social.Id', '=', 'tiendas.razon_Id');
            })
            ->leftjoin('users', function ($join) {
                $join->on('users.Id', '=', 'visitas.users_Id');
            })
            ->select('visitas.*', 'checklist.nombre as checklist', 'tiendas.nombre as tienda', 'tiendas.numsuc', 'users.name', 'plazas.plaza', 'razon_social.nombre as razon', 'razon_social.foto as razonFoto')
            ->where('visitas.Id','=',$id)
            ->first();

        $data['sum_opcional'] =  DB::table('visitas_campos')
            ->where('visitas_Id',"=", $id)
            ->where('no_aplica',"=", 1)
            ->sum('valor');

        $data['sum_final'] =  DB::table('visitas_campos')
            ->where('visitas_Id',"=", $id)
            ->sum('valor');

        $data['sum_total'] = DB::table('campos_valores')
            ->leftjoin('campos', function ($join) {
                $join->on('campos.Id', '=', 'campos_valores.campos_Id');
            })
            ->leftjoin('categorias', function ($join) {
                $join->on('categorias.Id', '=', 'campos.categorias_Id');
            })
            ->leftjoin('checklist', function ($join) {
                $join->on('checklist.Id', '=', 'categorias.checklist_Id');
            })
            ->select('campos_valores.valor')
            ->where('checklist.Id',"=", $data['visita']->checklist_Id)
            ->sum('campos_valores.valor');
        $sum_total = $data['sum_total'] - $data['sum_opcional'] ;

        $sum_final = $data['sum_final'] - $data['sum_opcional'] ;


        $data['calif'] = $sum_final / $sum_total * 100;
        //actualizamos visita
        DB::table('visitas')
            ->where('Id', $id)
            ->update(['Calif' => $data['calif']
            ]);
        //llenado de resultados
        $data['categorias'] =   DB::table('categorias')
            ->where('categorias.checklist_Id','=',$data['visita']->checklist_Id)
            ->get();
        $categorias = collect($data['categorias'])->toArray();
        // $campos = array(1,2,3,4,5);
        for($i = 0;$i<count($categorias);$i++)
        {
            $catId = $categorias[$i]->Id;
            $todo = DB::table('visitas_todo')
                ->leftjoin('campos', function ($join) {
                    $join->on('campos.Id', '=', 'visitas_todo.campos_Id');
                })
                ->leftjoin('familias', function ($join) {
                    $join->on('familias.Id', '=', 'campos.familias_Id');
                })
                ->select('visitas_todo.*', 'campos.nombre', 'familias.Nombre as familia' )
                ->where('campos.categorias_Id','=',$catId)
                ->where('visitas_todo.visitas_Id',"=", $data['visita']->Id)
                ->get();
            $campos = collect($todo)->toArray();
            $cam = array();
            for($j = 0;$j<count($campos);$j++)
            {
                $cam[] = array( "nombre" => $campos[$j]->nombre,
                    "familia" => $campos[$j]->familia,
                    "descripcion" => $campos[$j]->descripcion,
                    "ImagenIni" => $campos[$j]->ImagenIni);
            }

            /**campos que no fueron to-do**/
            $todono = DB::table('visitas_campos')
                ->leftjoin('campos', function ($join) {
                    $join->on('campos.Id', '=', 'visitas_campos.campos_Id');
                })
                ->leftjoin('familias', function ($join) {
                    $join->on('familias.Id', '=', 'campos.familias_Id');
                })
                ->select('visitas_campos.*', 'campos.nombre', 'familias.Nombre as familia' )
                ->where('campos.categorias_Id','=',$catId)
                ->where('visitas_campos.visitas_Id',"=", $data['visita']->Id)
                ->get();
            $campos_no = collect($todono)->toArray();
            $cams = array();
            $na = array();
            for($k = 0;$k<count($campos_no);$k++)
            {
                if($campos_no[$k]->comentario != "No se agregó comentario" || $campos_no[$k]->foto != "no_imagen.jpg")
                {

                        if($campos_no[$k]->no_aplica == 0)
                        {
                            $cams[] = array( "nombre" => $campos_no[$k]->nombre,
                            "familia" => $campos_no[$k]->familia,
                            "descripcion" => $campos_no[$k]->comentario,
                            "ImagenIni" => $campos_no[$k]->foto);

                        }


                }

                if($campos_no[$k]->no_aplica == 1)
                {
                    $na[] = array( "nombre" => $campos_no[$k]->nombre,
                        "familia" => $campos_no[$k]->familia,
                        "descripcion" => $campos_no[$k]->comentario,
                        "ImagenIni" => $campos_no[$k]->foto);

                }

            }
            /****/

            $sum_tot_cat = DB::table('campos_valores')
                ->leftjoin('campos', function ($join) {
                    $join->on('campos.Id', '=', 'campos_valores.campos_Id');
                })
                ->leftjoin('categorias', function ($join) {
                    $join->on('categorias.Id', '=', 'campos.categorias_Id');
                })
                ->leftjoin('checklist', function ($join) {
                    $join->on('checklist.Id', '=', 'categorias.checklist_Id');
                })
                ->select('campos_valores.valor')
                ->where('checklist.Id',"=", $data['visita']->checklist_Id)
                ->where('categorias.Id',"=", $catId)
                ->sum('campos_valores.valor');
            $sum_todo_cat =  DB::table('visitas_campos')
                ->leftjoin('campos', function ($join) {
                    $join->on('campos.Id', '=', 'visitas_campos.campos_Id');
                })
                ->leftjoin('categorias', function ($join) {
                    $join->on('categorias.Id', '=', 'campos.categorias_Id');
                })
                ->where('categorias.Id',"=", $catId)
                ->where('visitas_Id',"=", $id)
                ->sum('valor');

            $cats[] = array("Id" => $categorias[$i]->Id,
                            "sum_tot" => $sum_tot_cat,
                            "sum_real" => $sum_todo_cat,
                            "nombre" => $categorias[$i]->nombre,
                            "todo" => $cam,
                            'campos' =>$cams,
                            "no_aplica" => $na);


        }
        $data['cats'] = $cats;

        //obtenemos las fotos de la visita
        $data['fotos'] = DB::table('visitas_fotos')
            ->where('visitas_Id', $id)
            ->get();
        $data['visita_id'] = $id;
        /**cuestionario de visita**/
        $data['cuestionario'] = DB::table('visitas_cuestionario')
            ->leftjoin('preguntas', function ($join) {
                $join->on('preguntas.Id', '=', 'visitas_cuestionario.pregunta_Id');
            })
            ->leftjoin('respuestas', function ($join) {
                $join->on('respuestas.Id', '=', 'visitas_cuestionario.respuesta_Id');
            })
            ->select('visitas_cuestionario.*', 'preguntas.Pregunta', 'preguntas.tipo', 'respuestas.respuesta')
            ->where('visitas_Id', $id)
            ->get();
       // return view('/visitas/reporte')->with($data);
       //$pdf = PDF::loadView('/visitas/reporte', compact('data'));
        $path = 'reporte'.$id.'.pdf';





      return $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('/visitas/reporte', compact('data'))->download($path);
        /*$path = public_path('uploads/Reportes/reporteVisita'.$request['id'].'.pdf');

        return  PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('/visitas/reporte', compact('data'))->save($path);**/

       // return "OK";
    }




}
