<?php

namespace App\Http\Controllers\Visitas;

use App\Helpers\Formulas;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Barryvdh\DomPDF\Facade as PDF;
use Mail;
use Illuminate\Mail\Mailable;
use Swift_Mailer;
use \Swift_SmtpTransport as SmtpTransport;
use App\Mail\VisitaSegMail;
use App\Helpers\Listados;

class SeguimientoController extends Controller
{
    //protected $path = '/home/hdammx/public_html/VentumAdmin/uploads/'; //path para host en linea
    protected $path = 'uploads/'; //path para pruebas locales
    protected $pathUploadCli = '/home/hdammx/public_html/VentumClientes/uploads/'; //path para copia en clientes
    public function getListadoTiendas()
    {
        $listado = new Listados();

        return $listado->listaTiendas(auth()->user()->id);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $listado = new Listados();
        $data['tiendas'] = $this->getListadoTiendas();

        $enproc = DB::table('visitas')
            ->where('visitas.Status','=',133)
            ->where('visitas.tiendas_Id','=',$id)
            ->where('visitas.tipo_visita','=',2)
            ->count();


        //contamos los to-do concluidos
        $data['concluidos'] = DB::table('visitas_todo')
            ->leftjoin('visitas', function ($join) {
                $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
            })
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
            })
            ->where('tiendas.Id', "=", $id)
            ->where('visitas_todo.Status', "=", 154)->count();

        $data['nuevos'] = DB::table('visitas_todo')
            ->leftjoin('visitas', function ($join) {
                $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
            })
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
            })
            ->where('tiendas.Id', "=", $id)
            ->where('visitas_todo.Status', "=", 151)->count();

        $data['curso'] = DB::table('visitas_todo')
            ->leftjoin('visitas', function ($join) {
                $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
            })
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
            })
            ->where('tiendas.Id', "=", $id)
            ->where('visitas_todo.Status', "=", 152)->count();

        $data['pendientes'] = DB::table('visitas_todo')
            ->leftjoin('visitas', function ($join) {
                $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
            })
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
            })
            ->where('tiendas.Id', "=", $id)
            ->where('visitas_todo.Status', "=", 153)->count();


        $data['totales'] = DB::table('visitas_todo')
            ->leftjoin('visitas', function ($join) {
                $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
            })
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
            })
            ->where('tiendas.Id', "=", $id)->count();

        $data['last_visitas'] = DB::table('visitas')
            ->leftjoin('users', function ($join) {
                $join->on('users.id', '=', 'visitas.users_Id');
            })
            ->select('visitas.*', 'users.name')
            ->where('visitas.Status','=',131)
            ->where('visitas.tipo_visita','=',1)
            ->where('visitas.tiendas_Id','=',$id)
            ->limit(4)
            ->get();

        if($enproc === 1)
        {
            $data['en_proceso'] = DB::table('visitas')
                ->where('visitas.Status','=',133)
                ->where('visitas.tiendas_Id','=',$id)
                ->where('visitas.tipo_visita','=',2)
                ->first();
        }else
        {
            $data['en_proceso'] =  (object) array("Status" => 132, "Id" => 0);
        }
        $data['info'] =   DB::table('tiendas')
            ->where('tiendas.Id','=',$id)
            ->first();
        $data['empresa'] = $data['info']->empresas_Id;
        $data['checklist']  =   $listado->listaChecklist(auth()->user()->empleados_Id , auth()->user()->empresas_Id);
        $config = array();
        $config['center'] = 'auto';
        $config['onboundschanged'] = 'if (!centreGot) {
            var mapCentre = map.getCenter();
            marker_0.setOptions({
                position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng())
            });
        }
        centreGot = true;';

        $data['config'] =   DB::table('configuracion')
            ->where('configuracion.empresas_Id','=',auth()->user()->empresas_Id)
            ->first();

        app('map')->initialize($config);


        $data['cuestionario'] =   DB::table('cuestionario')
            ->where('cuestionario.empresas_Id','=',auth()->user()->empresas_Id)
            ->where('cuestionario.tipos_cuestionario_Id','=',2)
            ->first();
        // set up the marker ready for positioning
        // once we know the users location
        $marker = array();
        app('map')->add_marker($marker);

        $map = app('map')->create_map();
        $data['js'] = $map['js'];
        $data['html'] = $map['html'];

        $sql = DB::table('familias');

        $data['familias'] = $sql->get();


        return view('/visitas/seguimiento')->with($data);;
    }

    public function inicia(Request $request)
    {
        $anio = date('Y');
        $mes = date('m');
        $hora = date('Y-m-d H:i:s');
        $campos = 0;


        //generamos la visita
        DB::table('visitas')->insert([
            [
                'tiendas_Id' => $request['tienda'],
                'checklist_Id' => $request['body'],
                'users_Id' => $request['user'],
                'MesRev' => $mes,
                'AnioRev' => $anio,
                'tipo_visita' => 2,
                'HoraInicio' => $hora,
                'Status' => 133,
                'tipo_aplicacion' => 1
            ]
        ]);
        $vis['id_visita'] = DB::getPdo()->lastInsertId();

        //obtenemos los to-do's existentes de la tienda
        $sql = DB::table('visitas_todo');
        $sql->leftjoin('campos', function ($join) {
                $join->on('campos.Id', '=', 'visitas_todo.campos_Id');
            })
            ->leftjoin('visitas', function ($join) {
                $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
            })
            ->select('visitas_todo.*', 'campos.nombre', 'visitas.tiendas_Id');
        $sql ->where('visitas.tiendas_Id','=',$request['tienda']);
        $todos = $sql->get();

        //obtenemos las familias para la busqueda





        return response()->json(['message' => "", 'id_visita' => $vis, 'todos' => $todos] );

        //return view('/checklist/checklist')->with( $data);
    }

    public function categorias(Request $request)
    {
        //obtenemos los to-do's existentes de la tienda
        $sql = DB::table('categorias');
        $sql->leftjoin('checklist', function ($join) {
            $join->on('checklist.Id', '=', 'categorias.checklist_Id');
        })
            ->select('categorias.*');
        $sql ->where('categorias.checklist_Id','=',$request['checklist']);
        $cats = $sql->get();



        return response()->json(['message' => $cats] );
    }


    public function campos(Request $request)
    {
        //obtenemos los to-do's existentes de la tienda
        $sql = DB::table('campos');
        $sql->leftjoin('categorias', function ($join) {
            $join->on('categorias.Id', '=', 'campos.categorias_Id');
        })
            ->select('campos.*');
        $sql ->where('campos.categorias_Id','=',$request['categoria']);
        $cats = $sql->get();

        return response()->json(['message' => $cats] );
    }

    public function addToDo(Request $request)
    {
        $fecha_ini = date('Y-m-d H:i:s');
       //agregamos el to-do

        DB::table('visitas_todo')->insert([
            [
                'visitas_Id' => $request['visita_Id'],
                'campos_Id' => $request['campo_Id'],
                'valor' => 0,
                'fecha_inicio' => $fecha_ini,
                'fecha_upd' => $fecha_ini,
                'Status' => 152,
                'tipo' => $request['tipo'],
                'visita_id_ant' => $request['visita_Id'],
                'descripcion' => $request['texto']
            ]
        ]);
        return response()->json(['message' => "OK"] );
    }

    public function getToDo(Request $request)
    {

        //obtenemos los to-do's existentes de la tienda
        $sql = DB::table('visitas_todo');
        $sql->leftjoin('campos', function ($join) {
            $join->on('campos.Id', '=', 'visitas_todo.campos_Id');
        })
        ->leftjoin('visitas', function ($join) {
            $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
        })
        ->leftjoin('familias', function ($join) {
            $join->on('familias.Id', '=', 'campos.familias_Id');
        })
        ->select('visitas_todo.*', 'campos.nombre', 'campos.familias_Id',  'campos.smart', 'familias.Nombre', 'visitas.tiendas_Id');
        $sql ->where('visitas.tiendas_Id','=',$request['tienda']);

        if($request['chk'] == 1 && $request['exp'] == 0)
        {
            $sql ->where('visitas_todo.tipo','=',1);
        }
        if($request['chk'] == 0 && $request['exp'] == 1)
        {
            $sql ->where('visitas_todo.tipo','=',2);
        }
        if($request['fam'] != 0)
        {
            $sql ->where('campos.familias_Id','=',$request['fam']);
        }
        if($request['nuevo'] == 1 && $request['cur'] == 1 && $request['pend'] == 0)
        {
            $sql ->whereIn('visitas_todo.Status',array(151,152));
        }else
        if($request['nuevo'] == 1 && $request['cur'] == 0 && $request['pend'] == 1)
        {
            $sql ->whereIn('visitas_todo.Status',array(151,153));
        }else
        if($request['nuevo'] == 0 && $request['cur'] == 1 && $request['pend'] == 1)
        {
            $sql ->whereIn('visitas_todo.Status',array(152,153));
        }else
        if($request['nuevo'] == 1 && $request['cur'] == 0 && $request['pend'] == 0)
        {
            $sql ->whereIn('visitas_todo.Status',array(151));
        }else
        if($request['nuevo'] == 0 && $request['cur'] == 1 && $request['pend'] == 0)
        {
            $sql ->whereIn('visitas_todo.Status',array(152));
        }else
        if($request['nuevo'] == 0 && $request['cur'] == 0 && $request['pend'] == 1)
        {
            $sql ->whereIn('visitas_todo.Status',array(153));
        }

        $fechas = explode("-",$request['fecha'] );

        $fechaini = explode('/', $fechas[0]);
        $fechafin = explode('/', $fechas[1]);

        $sql ->whereBetween('visitas_todo.fecha_inicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])));

        $todos = $sql->get();

        return response()->json(['todos' => $todos] );
    }


    public function updateTodoSeg(Request $request)
    {
     if($request['visita_new'] != $request['visita_ant'])
        {
            DB::table('visitas_todo')
                ->where('Id', $request['campo_Id'])
                ->update(['fecha_upd' => date('Y-m-d H:j:s'),
                    'descripcion' => $request['comentario'],
                    'visitas_Id' => $request['visita_new'],
                    'visita_id_ant' => $request['visita_ant'],
                    'Status' => $request['status']]);
        }else
        {
            DB::table('visitas_todo')
                ->where('Id', $request['campo_Id'])
                ->update(['fecha_upd' => date('Y-m-d H:j:s'),
                    'descripcion' => $request['comentario'],
                    'Status' => $request['status']]);
        }

        $sql = DB::table('visitas_todo');
        $sql->leftjoin('campos', function ($join) {
            $join->on('campos.Id', '=', 'visitas_todo.campos_Id');
        })
            ->leftjoin('visitas', function ($join) {
                $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
            })
            ->select('visitas_todo.*', 'campos.nombre', 'visitas.tiendas_Id');
        $sql ->where('visitas_todo.Id','=',$request['campo_Id']);
        $todos = $sql->get();

        return response()->json(['message' => $todos] );
    }

    public function putFotoCampoSeg(Request $request)
    {
        if($request['visita_new'] != $request['visita_ant'])
        {
            DB::table('visitas_todo')
                ->where('Id', $request['campo_Id'])
                ->update(['fecha_upd' => date('Y-m-d H:j:s'),
                    'ImagenIni' => $request['foto'],
                    'visitas_Id' => $request['visita_new'],
                    'visita_id_ant' => $request['visita_ant'],
                    'Status' => 152]);

        }else
        {
            DB::table('visitas_todo')
                ->where('Id', $request['campo_Id'])
                ->update(['fecha_upd' => date('Y-m-d H:j:s'),
                    'ImagenIni' => $request['foto'],
                    'Status' => 152]);

        }

        return response()->json(['message' => $request->all()] );

    }

    public function delFromSeg(Request $request)
    {
        if($request['vis'] === $request['visant'])
        {
            //borramos el to-do
            DB::table('visitas_todo')->where('Id', '=', $request['Id'])->delete();

        }else
        {
            //actualizamos el to-do
            DB::table('visitas_todo')
                ->where('Id', $request['Id'])
                ->update(['fecha_upd' => date('Y-m-d H:j:s'),
                    'visitas_Id' => $request['visant'],
                    'visita_id_ant' => 0,
                    'Status' => 152
                ]);
        }

        return response()->json(['message' => $request->all()] );

    }

    public function cancelaVisitaSeg(Request $request)
    {


        DB::table('visitas')
            ->where('Id', $request['id'])
            ->update(['Status' => 132]);

        //retornamos los to-do cambiados a como estaban antes
        $data =  DB::table('visitas_todo')
                ->where('visitas_Id', $request['id'])
                ->get();
        $todos = collect($data)->toArray();

        // $campos = array(1,2,3,4,5);
        $cam = array();
        for($i = 0;$i<count($todos);$i++)
        {
            $visitas_ant = $todos[$i]->visita_id_ant;
            $vista_seg   = $request['id'];
            $todo_Id     = $todos[$i]->Id;


            if($visitas_ant == $vista_seg )
            {
                //borramos el to-do
                DB::table('visitas_todo')->where('Id', '=', $todo_Id)->delete();

            }else
            {
                //actualizamos el to-do
              DB::table('visitas_todo')
                    ->where('Id', $todo_Id)
                    ->update(['fecha_upd' => date('Y-m-d H:j:s'),
                        'visitas_Id' => $visitas_ant,
                        'visita_id_ant' => 0,
                        'Status' => 152
                    ]);
            }


        }

        return response()->json(['message' =>$todos ] );
    }

    public function recuperaVisitaSeg(Request $request)
    {
        $formulas = new Formulas();
        $sql = DB::table('visitas')
            ->where('Id', $request['id'])
            ->first();

        $segundos = $formulas->diferenciaSegundos($sql->HoraInicio, date('Y-m-d H:i:s'));
        return response()->json(['segundos' => $segundos ] );
    }

    public function finalVisita($id)
    {

        $data['visita'] = DB::table('visitas')
            ->leftjoin('checklist', function ($join) {
                $join->on('checklist.Id', '=', 'visitas.checklist_Id');
            })
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
            })
            ->leftjoin('users', function ($join) {
                $join->on('users.Id', '=', 'visitas.users_Id');
            })
            ->select('visitas.*', 'checklist.nombre', 'tiendas.nombre as tienda', 'users.name')
            ->where('visitas.Id','=',$id)
            ->first();
        //obtenemos los to-do resueltos de la visita
        $data['todo_fin'] =  DB::table('visitas_todo')
            ->where('visitas_todo.Status',"=", 154)
            ->where('visitas_todo.visitas_Id',"=", $id)
            ->count();
        $data['todo_new'] =  DB::table('visitas_todo')
            ->where('visitas_todo.Status',"=", 151)
            ->where('visitas_todo.visitas_Id',"=", $id)
            ->count();
        $data['todo_cur'] =  DB::table('visitas_todo')
            ->where('visitas_todo.Status',"=", 152)
            ->where('visitas_todo.visitas_Id',"=", $id)
            ->count();
        $data['todo_pend'] =  DB::table('visitas_todo')
            ->where('visitas_todo.Status',"=", 153)
            ->where('visitas_todo.visitas_Id',"=", $id)
            ->count();

        //consultamos los to-do
        //llenado de resultados
        $data['todos'] = DB::table('visitas_todo')
                ->leftjoin('campos', function ($join) {
                    $join->on('campos.Id', '=', 'visitas_todo.campos_Id');
                })
                ->leftjoin('familias', function ($join) {
                    $join->on('familias.Id', '=', 'campos.familias_Id');
                })
                ->select('visitas_todo.*', 'campos.nombre', 'familias.Nombre as familia' )
                ->where('visitas_todo.visitas_Id',"=", $id)
                ->get();


        //obtenemos las fotos de la visita
        $data['fotos'] = DB::table('visitas_fotos')
            ->where('visitas_Id', $id)
            ->get();
        $data['visita_id'] = $id;

        $data['tiendas'] = $this->getListadoTiendas();


        return view('/visitas/finalseg')->with($data);
    }



    public function visitaPdf($id)
    {
        /**
         * toma en cuenta que para ver los mismos
         * datos debemos hacer la misma consulta
         **/
        $data['visita'] = DB::table('visitas')
            ->leftjoin('checklist', function ($join) {
                $join->on('checklist.Id', '=', 'visitas.checklist_Id');
            })
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
            })
            ->leftjoin('plazas', function ($join) {
                $join->on('plazas.Id', '=', 'tiendas.plaza_Id');
            })
            ->leftjoin('razon_social', function ($join) {
                $join->on('razon_social.Id', '=', 'tiendas.razon_Id');
            })
            ->leftjoin('users', function ($join) {
                $join->on('users.Id', '=', 'visitas.users_Id');
            })
            ->select('visitas.*', 'checklist.nombre as checklist', 'tiendas.nombre as tienda', 'tiendas.numsuc', 'tiendas.mail as mailsuc', 'users.email as mailsup', 'users.name', 'plazas.plaza', 'razon_social.nombre as razon', 'razon_social.foto as razonFoto')
            ->where('visitas.Id','=',$id)
            ->first();

        //obtenemos las fotos de la visita
        $data['fotos'] = DB::table('visitas_fotos')
            ->where('visitas_Id', $id)
            ->get();
        $data['visita_id'] = $id;

      //  obtenemos los to-do resueltos de la visita
        $data['todo_fin'] =  DB::table('visitas_todo')
            ->where('visitas_todo.Status',"=", 154)
            ->where('visitas_todo.visitas_Id',"=", $id)
            ->count();
        $data['todo_new'] =  DB::table('visitas_todo')
            ->where('visitas_todo.Status',"=", 151)
            ->where('visitas_todo.visitas_Id',"=", $id)
            ->count();
        $data['todo_cur'] =  DB::table('visitas_todo')
            ->where('visitas_todo.Status',"=", 152)
            ->where('visitas_todo.visitas_Id',"=", $id)
            ->count();
        $data['todo_pend'] =  DB::table('visitas_todo')
            ->where('visitas_todo.Status',"=", 153)
            ->where('visitas_todo.visitas_Id',"=", $id)
            ->count();

        $data['todos'] = DB::table('visitas_todo')
            ->leftjoin('campos', function ($join) {
                $join->on('campos.Id', '=', 'visitas_todo.campos_Id');
            })
            ->leftjoin('familias', function ($join) {
                $join->on('familias.Id', '=', 'campos.familias_Id');
            })
            ->select('visitas_todo.*', 'campos.nombre', 'familias.Nombre as familia' )
            ->where('visitas_todo.visitas_Id',"=", $id)
            ->get();

        $data['cuestionario'] = DB::table('visitas_cuestionario')
            ->leftjoin('preguntas', function ($join) {
                $join->on('preguntas.Id', '=', 'visitas_cuestionario.pregunta_Id');
            })
            ->leftjoin('respuestas', function ($join) {
                $join->on('respuestas.Id', '=', 'visitas_cuestionario.respuesta_Id');
            })
            ->select('visitas_cuestionario.*', 'preguntas.Pregunta', 'preguntas.tipo', 'respuestas.respuesta')
            ->where('visitas_Id', $id)
            ->get();

        $path = 'reporte'.$id.'.pdf';
        return $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('/visitas/reporteseg', compact('data'))->download($path);


    }


    public function setTimeFinalSeg(Request $request)
    {

        DB::table('visitas')
            ->where('Id', $request['id'])
            ->update(['Duracion' => $request['time'],
                'HoraFin' => date('Y-m-d H:i:s'),
                'Archivo' => 'reporteVisita'.$request['id'].'.pdf',
                'Status' => 131
            ]);

        //obtenemos las fotos de la visita
        $data['fotos'] = DB::table('visitas_fotos')
            ->where('visitas_Id', $request['id'])
            ->get();
        $data['visita_id'] = $request['id'];

        $id = $request['id'];
        /**GENERACION DEL PDF**/
        /**
         * toma en cuenta que para ver los mismos
         * datos debemos hacer la misma consulta
         **/

        $data['visita'] = DB::table('visitas')
            ->leftjoin('checklist', function ($join) {
                $join->on('checklist.Id', '=', 'visitas.checklist_Id');
            })
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
            })
            ->leftjoin('plazas', function ($join) {
                $join->on('plazas.Id', '=', 'tiendas.plaza_Id');
            })
            ->leftjoin('razon_social', function ($join) {
                $join->on('razon_social.Id', '=', 'tiendas.razon_Id');
            })
            ->leftjoin('users', function ($join) {
                $join->on('users.Id', '=', 'visitas.users_Id');
            })
            ->select('visitas.*', 'checklist.nombre as checklist', 'tiendas.nombre as tienda', 'tiendas.numsuc', 'tiendas.mail as mailsuc', 'users.email as mailsup', 'users.name', 'plazas.plaza', 'razon_social.nombre as razon', 'razon_social.foto as razonFoto')
            ->where('visitas.Id','=',$request['id'])
            ->first();

        //  obtenemos los to-do resueltos de la visita
        $data['todo_fin'] =  DB::table('visitas_todo')
            ->where('visitas_todo.Status',"=", 154)
            ->where('visitas_todo.visitas_Id',"=", $id)
            ->count();
        $data['todo_new'] =  DB::table('visitas_todo')
            ->where('visitas_todo.Status',"=", 151)
            ->where('visitas_todo.visitas_Id',"=", $id)
            ->count();
        $data['todo_cur'] =  DB::table('visitas_todo')
            ->where('visitas_todo.Status',"=", 152)
            ->where('visitas_todo.visitas_Id',"=", $id)
            ->count();
        $data['todo_pend'] =  DB::table('visitas_todo')
            ->where('visitas_todo.Status',"=", 153)
            ->where('visitas_todo.visitas_Id',"=", $id)
            ->count();

        $data['todos'] = DB::table('visitas_todo')
            ->leftjoin('campos', function ($join) {
                $join->on('campos.Id', '=', 'visitas_todo.campos_Id');
            })
            ->leftjoin('familias', function ($join) {
                $join->on('familias.Id', '=', 'campos.familias_Id');
            })
            ->select('visitas_todo.*', 'campos.nombre', 'familias.Nombre as familia' )
            ->where('visitas_todo.visitas_Id',"=", $id)
            ->get();

        /**cuestionario de visita**/


        $data['cuestionario'] = DB::table('visitas_cuestionario')
            ->leftjoin('preguntas', function ($join) {
                $join->on('preguntas.Id', '=', 'visitas_cuestionario.pregunta_Id');
            })
            ->leftjoin('respuestas', function ($join) {
                $join->on('respuestas.Id', '=', 'visitas_cuestionario.respuesta_Id');
            })
            ->select('visitas_cuestionario.*', 'preguntas.Pregunta', 'preguntas.tipo', 'respuestas.respuesta')
            ->where('visitas_Id', $request['id'])
            ->get();


        $path = 'uploads/Reportes/reporteVisita'.$request['id'].'.pdf';

         $data['mailSuc'] = $data['visita']->mailsuc;
          $data['nameSuc'] = $data['visita']->tienda;
        $data['mailSup'] = $data['visita']->mailsup;
        $data['nameSup'] = $data['visita']->name;
        /**ENVIOS DE CORREO ADICIONALES, de la tabla visitas_correo**/
        $dat_correo =   DB::table('visitas_correo')
            ->where('visitas_correo.empresas_Id','=',auth()->user()->empresas_Id)
            ->get();
        $no_correo =   DB::table('visitas_correo')
            ->where('visitas_correo.empresas_Id','=',auth()->user()->empresas_Id)
            ->count();
        if($no_correo != 0)
        {
            $envios = collect($dat_correo)->toArray();
            $env = array();
            for($i = 0;$i<count($envios);$i++)
            {
                //validamos que el envio sea de checklist o normal
                if($envios[$i]->tipo == 0 || $envios[$i]->tipo == 1 )
                {
                    //buscamos los empleados conforme a la configuracion del envio
                    $sql =  DB::table('empleados')
                        ->where('empleados.empresas_Id','=',auth()->user()->empresas_Id)
                        ->where('empleados.activo',"=", 1);
                    //filtramos los puestos
                    $sql->where('empleados.puesto_Id',"=", $envios[$i]->puesto_Id);
                    //filtramos el empleado
                    if($envios[$i]->empleados_Id  != 0)
                    {
                        $sql->where('empleados.Id',"=", $envios[$i]->empleados_Id);
                    }
                    //filtramos la razon
                    if($envios[$i]->razon_Id  != 0)
                    {
                        $sql->where('empleados.razon_Id',"=", $envios[$i]->razon_Id);
                    }
                    //filtramos la plaza
                    if($envios[$i]->plaza_Id  != 0)
                    {
                        $sql->where('empleados.plaza_Id',"=", $envios[$i]->plaza_Id);
                    }
                    $data_emp = $sql->get();

                    $empleados = collect($data_emp)->toArray();

                    //preparamos el envio
                    for($j = 0;$j<count($empleados);$j++)
                    {
                        $env[] = array("mail" => $empleados[$j]->mail, "empleado" => $empleados[$j]->nombre." ".$empleados[$j]->apepat." ".$empleados[$j]->apemat);
                    }


                }
            }


        }else
        {
            $env[] = array("mail" => "harroyo@ventumsupervision.com", "empleado" => "Sin configuracion de correos adicionales");
        }
        $data['envios'] = $env;


        /**FIN**/

        if(auth()->user()->empresas_Id == 6)
        {
            // Setup a new SmtpTransport instance for Gmail
            $transport = new SmtpTransport();
            $transport->setHost('smtp.mailgun.org');
            $transport->setPort(587);
            $transport->setEncryption('tls');
            $transport->setUsername('postmaster@envios.ventumsupervision.com');
            $transport->setPassword('b4139af002e42bdcdebf82604feeb1c0-c8e745ec-3c370a1d');


            // Assign a new SmtpTransport to SwiftMailer
            $driver = new Swift_Mailer($transport);

            // Assign it to the Laravel Mailer
            Mail::setSwiftMailer($driver);
        }else
        {
            // Setup a new SmtpTransport instance for Gmail
            $transport = new SmtpTransport();
            $transport->setHost('smtp-relay.sendinblue.com');
            $transport->setPort(587);
            $transport->setEncryption('tls');
            $transport->setUsername('ventum@ventummx.com');
            $transport->setPassword('LTr9qvOFVBC7PX6H');


            // Assign a new SmtpTransport to SwiftMailer
            $driver = new Swift_Mailer($transport);

            // Assign it to the Laravel Mailer
            Mail::setSwiftMailer($driver);
        }


        Mail::to('hdam23@gmail.com')->send(new VisitaSegMail($data));


        /**FIN DEL PDF**/
        PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('/visitas/reporteseg', compact('data'))->save($path);
        return response()->json(['message' => "OK"] );
    }


}
