<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Helpers\Listados;
class PlazasController extends Controller
{

    protected $path = 'uploads/'; //path para pruebas locales
    protected $pathUploadCli = '/home/hdammx/public_html/VentumClientes/uploads/'; //path para copia en clientes
    /**Creamos la instancia de listados**/
    public function getListadoTiendas()
    {
        $listado = new Listados();

        return $listado->listaTiendas(auth()->user()->id);
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sql = DB::table('plazas')
            ->leftjoin('razon_social', function ($join) {
                $join->on('plazas.razon_social_Id', '=', 'razon_social.Id');
            })

            ->select('plazas.*', 'razon_social.nombre as razon' );
        $sql->where('razon_social.empresas_Id', "=", auth()->user()->empresas_Id);
        $data['plazas'] = $sql->get();

        $data['tiendas'] = $this->getListadoTiendas();

        return view('/plazas/plazas')->with( $data);
    }

    public function fotoPlaza(Request $request)
    {

        $file = $request->file('photo');

        $nombre = "_Foto_Plaza";
        $extension = $file->getClientOriginalExtension();
        $random = str_random(10);
        $fecha = date('Ymdhis');
        $nombre = $random."_".$fecha."-".$nombre.".".$extension;
        $file->move($this->path.'Plazas/', $nombre);
        $ruta = $this->path.'Plazas/'.$nombre;


        return response()->json(['message' =>  $nombre, 'path' =>  $ruta]);
    }

    public function cargaInfoPlaza(Request $request)
    {

        $sql = DB::table('razon_social');

        $sql->where('razon_social.empresas_Id','=',auth()->user()->empresas_Id);
        $razones = $sql->get();

        return response()->json([ 'razones' => $razones ] );
    }

    public function addPlaza(Request $request)
    {

        DB::table('plazas')->insert([
            [
                'clave' =>  $request['clave'],
                'plaza' =>  $request['nombre'],
                'foto' =>  $request['foto'],
                'razon_social_Id' =>  $request['razon_social_Id']
            ]
        ]);


        return response()->json(['message' => $request->all() ] );
    }

    public function cargaInfoPlazaEditar(Request $request)
    {
        //obtenemos la información general del empleado
        $sql = DB::table('plazas');
        $sql ->leftjoin('razon_social', function ($join) {
            $join->on('razon_social.Id', '=', 'plazas.razon_social_Id');
        });
        $sql->select( 'plazas.*',  'razon_social.nombre as razon' );
        $sql->where('plazas.Id', "=", $request['id']);

        $plaza = $sql->first();


        $sql = DB::table('razon_social');

        $sql->where('razon_social.empresas_Id','=',auth()->user()->empresas_Id);
        $razones = $sql->get();


        return response()->json(['plaza' => $plaza, 'razones' => $razones ]);
    }

    public function updatePlaza(Request $request)
    {

        DB::table('plazas')
            ->where('Id', $request['id'])
            ->update([
                'clave' =>  $request['clave'],
                'plaza' =>  $request['nombre'],
                'foto' =>  $request['foto'],
                'razon_social_Id' =>  $request['razon_social_Id']
            ]);

        return response()->json(['message' => $request->all() ] );
    }

    public function bajaPlaza(Request $request)
    {

        DB::table('tiendas')
            ->where('razon_Id', $request['id'])
            ->update([

                'plaza_Id' =>  0
            ]);

        DB::table('empleados')
            ->where('razon_Id', $request['id'])
            ->update([

                'plaza_Id' =>  0
            ]);


        DB::table('plazas')
            ->where('Id', '=', $request['id'])->delete();



        return response()->json(['message' => $request->all() ] );
    }




}
