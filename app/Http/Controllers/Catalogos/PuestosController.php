<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Helpers\Listados;

class PuestosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getListadoTiendas()
    {
        $listado = new Listados();

        return $listado->listaTiendas(auth()->user()->id);
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //  dd(auth()->user()->empresas_Id);
       $sql = DB::table('puestos');

        $sql->where('puestos.empresas_Id','=',auth()->user()->empresas_Id);
        $data['puestos'] = $sql->get();
        //dd($data['puestos'] );

        $data['tiendas'] = $this->getListadoTiendas();


        return view('/puestos/lista')->with( $data);


    }

    public function addPuesto(Request $request)
    {
        //generamos la visita
        DB::table('puestos')->insert([
            [
                'clave' => $request['clave'],
                'puesto' => $request['nombre'],
                'descripcion' => $request['descripcion'],
                'empresas_Id' => auth()->user()->empresas_Id
            ]
        ]);

        $id_puesto = DB::getPdo()->lastInsertId();

        return response()->json(['message' => "" , 'idpuesto' => $id_puesto] );

    }


    public function addChkPuesto(Request $request)
    {
        //generamos la visita
        if($request['type'] == "true")
        {
            DB::table('chk_puesto')->insert([
                [
                    'checklist_Id' => $request['checklist_Id'],
                    'puestos_Id' => $request['puestos_Id']
                ]
            ]);
        }else
        {
            DB::table('chk_puesto')
                ->where('checklist_Id', '=', $request['checklist_Id'])
                ->where('puestos_Id', '=', $request['puestos_Id'])
                ->delete();
        }




        return response()->json(['message' => $request['type']] );

    }
    public function updatePuesto(Request $request)
    {
        //generamos la visita


        DB::table('puestos')
            ->where('Id', $request['puestos_Id'])
            ->update(['clave' => $request['clave'],
                        'puesto' => $request['nombre'],
                        'descripcion' => $request['descripcion']
                    ]);

        return response()->json(['message' => "" ] );

    }

    public function editPuesto(Request $request)
    {
        $info_puesto =   DB::table('puestos')
            ->where('puestos.Id','=',$request['id'])
            ->first();

        $info_tipo =   DB::table('emp_puestos')
            ->where('emp_puestos.puestos_Id','=',$request['id'])
            ->get();

        $info_checklist =   DB::table('checklist')
            ->where('checklist.empresas_Id','=',auth()->user()->empresas_Id)
            ->get();

        $info_selected = DB::table('chk_puesto')
            ->where('chk_puesto.puestos_Id','=',$request['id'])
            ->get();

        return response()->json(['puesto' =>  $info_puesto, 'tipos' => $info_tipo, 'checklist' => $info_checklist, 'chkSel' => $info_selected] );

    }


    public function setTipo(Request $request)
    {
        $countipo= DB::table('emp_puestos')
            ->where('emp_puestos.puestos_Id','=',$request['puesto_id'])
            ->where('emp_puestos.tipo_puesto_Id','=',$request['tipo'])
            ->count();

        if($countipo === 1)
        {
           //ya existe, lo quitamos
            DB::table('emp_puestos')
                ->where('emp_puestos.puestos_Id', '=', $request['puesto_id'])
                ->where('emp_puestos.tipo_puesto_Id','=',$request['tipo'])
                ->delete();
        }else
        {
            //no existe lo agregamos
            DB::table('emp_puestos')->insert([
                [
                    'puestos_Id' => $request['puesto_id'],
                    'tipo_puesto_Id' => $request['tipo']
                ]
            ]);
        }
        return response()->json(['message' => "" ] );

    }

    public function bajaPuesto(Request $request)
    {

        DB::table('empleados')
            ->where('puesto_Id', $request['id'])
            ->update([
                'puesto_Id' =>  0
            ]);


        DB::table('puestos')
            ->where('Id', '=', $request['id'])->delete();

        return response()->json(['message' => $request->all() ] );
    }


}
