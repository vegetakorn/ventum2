<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Helpers\Listados;

class FamiliasController extends Controller
{

    protected $path = 'uploads/'; //path para pruebas locales
    protected $pathUploadCli = '/home/hdammx/public_html/VentumClientes/uploads/'; //path para copia en clientes
    /**Creamos la instancia de listados**/
    public function getListadoTiendas()
    {
        $listado = new Listados();

        return $listado->listaTiendas(auth()->user()->id);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sql = DB::table('familias');

        $sql->where('familias.empresas_Id','=',auth()->user()->empresas_Id);
        $data['familias'] = $sql->get();
        //dd($data['puestos'] );

        $data['tiendas'] = $this->getListadoTiendas();

        return view('/familias/familias')->with( $data);
    }



    public function addFamilia(Request $request)
    {

        DB::table('familias')->insert([
            [
                'empresas_Id' =>  auth()->user()->empresas_Id,
                'Nombre' =>  $request['nombre']
            ]
        ]);

        return response()->json(['message' => $request->all() ] );
    }

    public function cargaInfoFamEditar(Request $request)
    {
        //obtenemos la información general del empleado
        $sql = DB::table('familias');
        $sql->where('familias.Id', "=", $request['id']);
        $familia = $sql->first();

        return response()->json(['familia' => $familia ]);
    }

    public function updateFamilia(Request $request)
    {

        DB::table('familias')
            ->where('Id', $request['id'])
            ->update([
                'Nombre' =>  $request['nombre']
            ]);

        return response()->json(['message' => $request->all() ] );
    }




}
