<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Helpers\Listados;

class CuestionariosController extends Controller
{
    /**Creamos la instancia de listados**/
    public function getListadoTiendas()
    {
        $listado = new Listados();

        return $listado->listaTiendas(auth()->user()->id);
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $sql = DB::table('users');
        $sql->leftjoin('empresas', function ($join) {
            $join->on('empresas.Id', '=', 'users.empresas_id');
        })->select('users.id', 'users.empresas_Id', 'empresas.Logo');
        $sql->where('users.id','=',auth()->user()->id);
        $datos = $sql->get();

        $sesion  = json_decode($datos, true);
        $data['cuestionarios']  =   DB::table('cuestionario')
            ->where('cuestionario.empresas_Id','=',$sesion[0]['empresas_Id'])
            ->get();

        $data['tiendas'] = $this->getListadoTiendas();

        return view('/cuestionarios/cuestionarios')->with( $data);
    }

    public function preguntas(Request $request)
    {
        $data =   DB::table('preguntas')

            ->where('preguntas.cuestionario_Id','=',$request['id'])->get();

        return response()->json(['message' => $data] );

        //return view('/checklist/checklist')->with( $data);
    }

    public function respuestas(Request $request)
    {

        $data =   DB::table('preguntas')

            ->where('preguntas.Id','=',$request['body'])->first();

        //buscamos las respuestas
        $respuestas =   DB::table('respuestas')

            ->where('respuestas.preguntas_Id','=',$request['body'])->get();


        return response()->json(['pregunta' => $data, 'respuestas' => $respuestas] );

        //return view('/checklist/checklist')->with( $data);
    }

    public function savePregunta(Request $request)
    {
        DB::table('preguntas')->insert([
            [
                'cuestionario_Id' =>  $request['id_cuest'],
                'Pregunta' =>  $request['pregunta'],
                'tipo' =>  $request['tipo'],
                'Descripcion' =>  ""
            ]
        ]);


        return response()->json(['pregunta' => "OK"] );


    }

    public function addResp(Request $request)
    {
        DB::table('respuestas')->insert([
            [
                'preguntas_Id' =>  $request['id_preg'],
                'respuesta' =>  $request['respuesta']
            ]
        ]);

        //buscamos las respuestas
        $data =   DB::table('respuestas')

            ->where('respuestas.preguntas_Id','=',$request['id_preg'])->get();


        return response()->json(['respuestas' => $data] );


    }
}
