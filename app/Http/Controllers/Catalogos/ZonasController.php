<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Helpers\Listados;
class ZonasController extends Controller
{

    protected $path = 'uploads/'; //path para pruebas locales
    protected $pathUploadCli = '/home/hdammx/public_html/VentumClientes/uploads/'; //path para copia en clientes
    public function getListadoTiendas()
    {
        $listado = new Listados();

        return $listado->listaTiendas(auth()->user()->id);
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sql = DB::table('zonas');

        $sql->where('zonas.empresas_Id','=',auth()->user()->empresas_Id);
        $data['zonas'] = $sql->get();
        //dd($data['puestos'] );

        $data['tiendas'] = $this->getListadoTiendas();

        return view('/zonas/zonas')->with( $data);
    }

    public function fotoZona(Request $request)
    {

        $file = $request->file('photo');

        $nombre = "_Foto_Zona";
        $extension = $file->getClientOriginalExtension();
        $random = str_random(10);
        $fecha = date('Ymdhis');
        $nombre = $random."_".$fecha."-".$nombre.".".$extension;
        $file->move($this->path.'Zonas/', $nombre);
        $ruta = $this->path.'Zonas/'.$nombre;


        return response()->json(['message' =>  $nombre, 'path' =>  $ruta]);
    }

    public function addZona(Request $request)
    {

        DB::table('zonas')->insert([
            [
                'empresas_Id' =>  auth()->user()->empresas_Id,
                'clave' =>  $request['clave'],
                'zona' =>  $request['nombre'],
                'foto' =>  $request['foto']
            ]
        ]);


        return response()->json(['message' => $request->all() ] );
    }

    public function cargaInfoZonaEditar(Request $request)
    {
        //obtenemos la información general del empleado
        $sql = DB::table('zonas');
        $sql->where('zonas.Id', "=", $request['id']);
        $zona = $sql->first();

        return response()->json(['zona' => $zona ]);
    }

    public function updateZona(Request $request)
    {

        DB::table('zonas')
            ->where('Id', $request['id'])
            ->update([
                'clave' =>  $request['clave'],
                'zona' =>  $request['nombre'],
                'foto' =>  $request['foto']
            ]);

        return response()->json(['message' => $request->all() ] );
    }

    public function bajaZona(Request $request)
    {

        DB::table('tiendas')
            ->where('zona', $request['id'])
            ->update([
                'zona' =>  0,

            ]);




        DB::table('zonas')
            ->where('Id', '=', $request['id'])->delete();



        return response()->json(['message' => $request->all() ] );
    }





}
