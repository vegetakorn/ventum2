<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Mail;
use Illuminate\Mail\Mailable;
use Swift_Mailer;
use \Swift_SmtpTransport as SmtpTransport;
use App\Mail\MailPassUser;


class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendMailRec(Request $request)
    {
        //generamos una contraseña aleatoria

        $pass_1 = date('Ymd');
        $pass_2 = rand(0,200);
        $pass = $pass_1."".$pass_2;
        $data['pass'] = $pass;
        $pass = bcrypt($pass);

        $data['Usuario'] =$request['mail'];


        DB::table('users')
            ->where('email', $request['mail'])
            ->update([
                'password' =>  $pass
            ]);

        //vemos a que empresa pertenece

        $empresa = DB::table('users')
            ->where('email', $request['mail'])
            ->first();

        //enviamos el correo de la contraseña restaurada
        // Send your message
        if($empresa->empresas_Id == 6)
        {
            // Setup a new SmtpTransport instance for Gmail
            $transport = new SmtpTransport();
            $transport->setHost('smtp.mailgun.org');
            $transport->setPort(587);
            $transport->setEncryption('tls');
            $transport->setUsername('postmaster@envios.ventumsupervision.com');
            $transport->setPassword('b4139af002e42bdcdebf82604feeb1c0-c8e745ec-3c370a1d');


            // Assign a new SmtpTransport to SwiftMailer
            $driver = new Swift_Mailer($transport);

            // Assign it to the Laravel Mailer
            Mail::setSwiftMailer($driver);
        }else
        {
            // Setup a new SmtpTransport instance for Gmail
            $transport = new SmtpTransport();
            $transport->setHost('smtp-relay.sendinblue.com');
            $transport->setPort(587);
            $transport->setEncryption('tls');
            $transport->setUsername('ventum@ventummx.com');
            $transport->setPassword('LTr9qvOFVBC7PX6H');


            // Assign a new SmtpTransport to SwiftMailer
            $driver = new Swift_Mailer($transport);

            // Assign it to the Laravel Mailer
            Mail::setSwiftMailer($driver);
        }
        Mail::to($request['mail'])->send(new MailPassUser($data));


        return response()->json(['message' => "OK" ] );
    }
}
