<?php

namespace App\Http\Controllers\Configuracion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Helpers\Listados;
class CorreosController extends Controller
{

    protected $path = 'uploads/'; //path para pruebas locales
    protected $pathUploadCli = '/home/hdammx/public_html/VentumClientes/uploads/'; //path para copia en clientes
    public function getListadoTiendas()
    {
        $listado = new Listados();

        return $listado->listaTiendas(auth()->user()->id);
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $sql = DB::table('puestos');
        $sql->where('puestos.empresas_Id','=',auth()->user()->empresas_Id);
        $data['puestos'] = $sql->get();

        $sql = DB::table('razon_social');
        $sql->where('razon_social.empresas_Id','=',auth()->user()->empresas_Id);
        $data['razones'] = $sql->get();

        $data['tiendas'] = $this->getListadoTiendas();


        return view('/correos/correos')->with( $data);
    }

    public function cargaEmpleados(Request $request)
    {
        $sql = DB::table('empleados');

        $sql->where('empleados.puesto_Id','=',$request['id']);
        $empleados = $sql->get();

        return response()->json(['empleados' => $empleados] );
    }


    public function agregaEnvio(Request $request)
    {

        DB::table('visitas_correo')->insert([
            [
                'puesto_Id'    =>  $request['puesto'],
                'empleados_Id' =>  $request['empleado'],
                'razon_Id'     =>  $request['razon'],
                'plaza_Id'     =>  $request['plaza'],
                'tiendas_Id'   =>  $request['sucursal'],
                'tipo'         =>  $request['tipo'],
                'empresas_Id'  =>  auth()->user()->empresas_Id
            ]
        ]);


        return response()->json(['correos' => "OK"] );
    }


    public function cargaConfiguracionesCorreo(Request $request)
    {

        $sql = DB::table('visitas_correo');

        $sql->leftjoin('puestos', function ($join) {
            $join->on('puestos.Id', '=', 'visitas_correo.puesto_Id');
         });

        $sql->leftjoin('empleados', function ($join) {
            $join->on('empleados.Id', '=', 'visitas_correo.empleados_Id');
        });

        $sql->leftjoin('razon_social', function ($join) {
            $join->on('razon_social.Id', '=', 'visitas_correo.razon_Id');
        });

        $sql->leftjoin('plazas', function ($join) {
            $join->on('plazas.Id', '=', 'visitas_correo.plaza_Id');
        });
        $sql->leftjoin('tiendas', function ($join) {
            $join->on('tiendas.Id', '=', 'visitas_correo.tiendas_Id');
        });
        $sql->select('visitas_correo.*', 'puestos.puesto', 'empleados.nombre', 'empleados.apepat', 'empleados.apemat', 'razon_social.nombre as razon', 'plazas.plaza', 'tiendas.nombre as tienda');

        $sql->where('visitas_correo.empresas_Id','=',auth()->user()->empresas_Id);
        $correos = $sql->get();

        return response()->json(['correos' => $correos] );
    }

    public function bajaCorreo(Request $request)
    {

        DB::table('visitas_correo')
            ->where('Id', '=', $request['id'])
            ->delete();

        return response()->json(['correos' => "OK"] );
    }


}
