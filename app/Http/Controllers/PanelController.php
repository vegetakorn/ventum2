<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Helpers\Listados;
class PanelController extends Controller
{

    protected $path = 'uploads/'; //path para pruebas locales
    protected $pathUploadCli = '/home/hdammx/public_html/VentumClientes/uploads/'; //path para copia en clientes
    public function getListadoTiendas()
    {
        $listado = new Listados();

        return $listado->listaTiendas(auth()->user()->id);
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['configuracion']  =   DB::table('configuracion')
            ->where('configuracion.empresas_Id','=',auth()->user()->empresas_Id )->first();

        $data['tiendas'] = $this->getListadoTiendas();

        return view('/panel/panel')->with( $data);;
    }

    public function setConfig(Request $request)
    {

        DB::table('configuracion')
            ->where('empresas_Id', auth()->user()->empresas_Id )
            ->update([
                'valida_vis' =>  $request['valida_vis']
            ]);

        return response()->json(['valida_vis' => $request['valida_vis'] ] );

    }

    public function getFunciones(Request $request)
    {

        $sql = DB::table('users_modulos');

        $sql->where('users_modulos.users_Id','=',auth()->user()->id);
        $modulos = $sql->get();

        return response()->json(['modulos' => $modulos ] );

    }

    public function getFuncionesTienda(Request $request)
    {

        $sql = DB::table('users_widgets_tiendas');

        $sql->where('users_widgets_tiendas.tiendas_Id','=',$request['id']);
        $modulos = $sql->get();

        return response()->json(['modulos' => $modulos ] );

    }


    public function getWidgets(Request $request)
    {

        $sql = DB::table('users_widgets');

        $sql->where('users_widgets.users_Id','=',auth()->user()->id);
        $widgets = $sql->get();

        return response()->json(['widgets' => $widgets ] );

    }




}
