<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Exports\VentasExport;
use Maatwebsite\Excel\Facades\Excel;
use Rap2hpoutre\FastExcel\FastExcel;
use App\Helpers\Listados;


class VentasController extends Controller
{
    public function getListadoTiendas()
    {
        $listado = new Listados();

        return $listado->listaTiendas(auth()->user()->id);
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['tiendas'] = $this->getListadoTiendas();

        return view('/ventas/ventas')->with( $data);

    }

    public function getVentasMes(Request $request)
    {

        //buscamos las ventas mensuales

        $sql = DB::table('ventas_mes')
            ->leftjoin('clasificaciones', function ($join) {
                $join->on('clasificaciones.Id', '=', 'ventas_mes.clasificacion_Id');
            })
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'ventas_mes.tiendas_Id');
            })
            ->select('ventas_mes.*', 'clasificaciones.Nombre', 'tiendas.nombre as Tienda' );

        $sql->where('ventas_mes.tiendas_Id', "=", $request['id']);
        $sql->where('ventas_mes.anio', "=", $request['anio']);

        if ($request['mes'] != 0)
        {
            $sql->where('ventas_mes.mes', "=", $request['mes']);
        }

        $ventas = $sql->get();
        return response()->json(['ventas' => $ventas ]);


    }

    public function getVtaDiaria(Request $request)
    {

        //buscamos las ventas mensuales
        $fecha_ini = $request['anio']."-".$request['mes']."-01";
        $fecha_fin = $request['anio']."-".$request['mes']."-31";

        $sql = DB::table('ventas_dia')

            ->select('ventas_dia.*');

        $sql->where('ventas_dia.tiendas_Id', "=", $request['tiendas_Id'])
            ->whereBetween('ventas_dia.fhventa', array($fecha_ini, $fecha_fin));
        $ventas = $sql->get();
        return response()->json(['ventas' => $ventas ]);


    }

    public function export($anio, $mes, $suc)
    {

        //buscamos las ventas mensuales

      /*  $sql = DB::table('ventas_mes')
            ->leftjoin('clasificaciones', function ($join) {
                $join->on('clasificaciones.Id', '=', 'ventas_mes.clasificacion_Id');
            })
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'ventas_mes.tiendas_Id');
            })
            ->select('ventas_mes.*', 'clasificaciones.Nombre', 'tiendas.nombre as Tienda' );

        $sql->where('ventas_mes.tiendas_Id', "=", $request['id']);
        $sql->where('ventas_mes.anio', "=", $request['anio']);


        $sql->where('ventas_mes.mes', "=", $request['mes']);


        $ventas = $sql->get();
        //return response()->json(['ventas' => $ventas ]);*/
        $data['anio'] = $suc;
        return Excel::download(new VentasExport($data), 'ventas_mes.xlsx');


    }

    public function updateMeses(Request $request)
    {
        //primero listamos todas las tiendas
        $data_suc = DB::table('tiendas')
            ->where('tiendas.empresas_Id', '=', auth()->user()->empresas_Id )
            ->where('tiendas.activo', '=', 1 )
            ->get();
        $tiendas = collect($data_suc)->toArray();

        for($i = 0;$i<count($tiendas);$i++)
        {

            //buscamos el mes
            $mes_reg = DB::table('ventas_mes')
                ->where('ventas_mes.tiendas_Id', '=', $tiendas[$i]->Id )
                ->where('ventas_mes.mes', '=', $request['mes'] )
                ->where('ventas_mes.anio', '=', $request['anio'] )
                ->count();
            if($mes_reg != 0)
            {
                //listamos los dias registrados
                $dias_reg = DB::table('ventas_dia')
                    ->where('ventas_dia.tiendas_Id', '=', $tiendas[$i]->Id )
                    ->whereMonth('ventas_dia.fhventa', '=', $request['mes'] )
                    ->whereYear('ventas_dia.fhventa', '=', $request['anio'] )
                    ->get();
                $dias = collect($dias_reg)->toArray();
                $ventas_mes = 0;
                $tickets_mes = 0;
                $artxtck_mes = 0;
                for($i = 0;$i<count($dias);$i++)
                {
                    $ventas_mes = $ventas_mes + $dias[$i]->monto;
                    $tickets_mes = $tickets_mes + $dias[$i]->tickets;
                    $artxtck_mes = $artxtck_mes + $dias[$i]->artxticket;

                }
                //actualizamos la informacion de la venta mensual
                DB::table('ventas_mes')
                    ->where('ventas_mes.tiendas_Id', '=', $tiendas[$i]->Id )
                    ->where('ventas_mes.mes', '=', $request['mes'] )
                    ->where('ventas_mes.anio', '=', $request['anio'] )
                    ->update([
                        'ventas' =>  $ventas_mes,
                        'tickets' =>  $tickets_mes,
                        'artxtck' => $artxtck_mes
                    ]);
                //calculamos la informacion mensual
                $mes_info = DB::table('ventas_mes')
                    ->where('ventas_mes.tiendas_Id', '=', $tiendas[$i]->Id )
                    ->where('ventas_mes.mes', '=', $request['mes'] )
                    ->where('ventas_mes.anio', '=', $request['anio'] )
                    ->first();

                //preparamos los campos a actualizar
                if($mes_info->tickets != 0)
                {
                    $tckprom_mes = round($mes_info->ventas/$mes_info->tickets,2);
                }else
                {
                    $tckprom_mes = 0;
                }

                if($mes_info->presupuesto != 0)
                {
                    $avance = round($mes_info->ventas/$mes_info->presupuesto * 100,2);
                }else
                {
                    $avance = 0;
                }

                if($mes_info->ventas > $mes_info->presupuesto)
                {
                    $sucpres = 1;
                }else
                {
                    $sucpres = 0;
                }
                //actualizamos los calculos en el mes
                DB::table('ventas_mes')
                    ->where('ventas_mes.tiendas_Id', '=', $tiendas[$i]->Id )
                    ->where('ventas_mes.mes', '=', $request['mes'] )
                    ->where('ventas_mes.anio', '=', $request['year'] )
                    ->update([
                        'tckprom' =>  $tckprom_mes,
                        'avance' =>  $avance,
                        'suc_pres' => $sucpres
                    ]);


            }


        }


        return response()->json(['ventas' => "" ]);


    }

    public function subeArchivoVentas(Request $request)
    {
        //buscamos la carpeta en donde se va a guardar
        $sql  = DB::table('empresas')
            ->select('empresas.RFC' );
        $sql->where('empresas.Id', "=", auth()->user()->empresas_Id);
        $Carpeta = $sql->get();
        $Carpeta = collect($Carpeta)->toArray();

       $file = $request->file('excel');

        $nombre = "_Archivo_Ventas";
        $extension = $file->getClientOriginalExtension();
        $random = str_random(10);
        $fecha = date('Ymdhis');
        $nombre = $random."_".$fecha."-".$nombre.".".$extension;
        $file->move(public_path('Clientes/'.$Carpeta[0]->RFC).'/Ventas', $nombre);

        $archivo_lectura = public_path('Clientes/'.$Carpeta[0]->RFC).'/Ventas/'.$nombre;
        //$ruta = $this->path.'FotoTodo/'.$nombre;*/
        //$path = public_path('clientes/cron/');
        $collection = (new FastExcel)->import($archivo_lectura);

        for($i= 0; $i < count($collection); $i++)
        {
            if($collection[$i]['ID'] != "")
            {
                //buscamos el numero de sucursal de la empresa
                $id_count = DB::table('tiendas')
                    ->where('tiendas.numsuc', '=', $collection[$i]['ID'] )
                    ->where('tiendas.empresas_Id', '=', auth()->user()->empresas_Id )
                    ->count();
                if($id_count != 0)
                {
                    //obtenemos el id de la tienda
                    $id_suc = DB::table('tiendas')
                        ->where('tiendas.numsuc', '=', $collection[$i]['ID'] )
                        ->where('tiendas.empresas_Id', '=', auth()->user()->empresas_Id )
                        ->first();
                    //preparamos los registros a capturar
                    $fhventa = date('Y-m-d', strtotime($request['dia']));
                    $fhupload = date('Y-m-d');
                    $mes = date('m', strtotime($request['dia']));
                    $anio = date('Y', strtotime($request['dia']));
                    $monto = $collection[$i]['Ventas'];
                    $tickets = $collection[$i]['Tickets'];
                    $artxtck = $collection[$i]['ArtxTck'];
                    if($tickets != 0)
                    {
                        $tckprom = round($monto / $tickets, 2);
                    }else
                    {
                        $tckprom = 0;
                    }

                    //buscamos si ya existe la venta
                    $dia_reg = DB::table('ventas_dia')
                        ->where('ventas_dia.tiendas_Id', '=', $id_suc->Id )
                        ->where('ventas_dia.fhventa', '=', $fhventa )
                        ->count();
                    if($dia_reg != 0)
                    {
                        DB::table('ventas_dia')
                            ->where('ventas_dia.tiendas_Id', '=', $id_suc->Id )
                            ->where('ventas_dia.fhventa', '=', $fhventa )
                            ->update([
                                'monto' =>  $monto,
                                'tickets' =>  $tickets,
                                'artxticket' => $artxtck,
                                'tckprom'  => $tckprom
                            ]);

                    }else
                    {
                        DB::table('ventas_dia')->insert([
                            [
                                'tiendas_Id' =>  $id_suc->Id,
                                'fhventa' =>  $fhventa,
                                'fhupload' =>  $fhupload,
                                'monto' =>  $monto,
                                'tickets' =>  $tickets,
                                'artxticket' => $artxtck,
                                'tckprom'  => $tckprom
                            ]
                        ]);
                    }


                }
            }else
            {
                break;
            }
        }

        return response()->json(['path' =>  $collection, 'mes' => $mes, 'anio' => $anio]);
    }

    public function subeArchivoPres(Request $request)
    {
        //buscamos la carpeta en donde se va a guardar
        $sql  = DB::table('empresas')
            ->select('empresas.RFC' );
        $sql->where('empresas.Id', "=", auth()->user()->empresas_Id);
        $Carpeta = $sql->get();
        $Carpeta = collect($Carpeta)->toArray();

        $file = $request->file('excel');

        $nombre = "_Archivo_Presupuestos";
        $extension = $file->getClientOriginalExtension();
        $random = str_random(10);
        $fecha = date('Ymdhis');
        $nombre = $random."_".$fecha."-".$nombre.".".$extension;
        $file->move(public_path('Clientes/'.$Carpeta[0]->RFC).'/Ventas', $nombre);

        $archivo_lectura = public_path('Clientes/'.$Carpeta[0]->RFC).'/Ventas/'.$nombre;
        //$ruta = $this->path.'FotoTodo/'.$nombre;*/
        //$path = public_path('clientes/cron/');
        $collection = (new FastExcel)->import($archivo_lectura);
        //$collection[0]['Producto']
       for($i= 0; $i < count($collection); $i++)
        {
            if($collection[$i]['ID'] != "")
            {
                //buscamos el numero de sucursal de la empresa
                $id_count = DB::table('tiendas')
                    ->where('tiendas.numsuc', '=', $collection[$i]['ID'] )
                    ->where('tiendas.empresas_Id', '=', auth()->user()->empresas_Id )
                    ->count();
                if($id_count != 0)
                {
                    //obtenemos el id de la tienda
                    $id_suc = DB::table('tiendas')
                        ->where('tiendas.numsuc', '=', $collection[$i]['ID'] )
                        ->where('tiendas.empresas_Id', '=', auth()->user()->empresas_Id )
                        ->first();
                    //preparamos los registros a capturar
                    $mes = $request['mes'];
                    $anio = $request['anio'] ;
                    $presupuesto = $collection[$i]['Presupuesto'];
                    $dias = $collection[$i]['Dias'];
                    $status = 1;
                    $metadia = round($presupuesto/$dias, 2);

                    //buscamos si ya existe el mes, si no se actualiza
                    $mes_reg = DB::table('ventas_mes')
                        ->where('ventas_mes.tiendas_Id', '=', $id_suc->Id )
                        ->where('ventas_mes.mes', '=', $mes )
                        ->where('ventas_mes.anio', '=', $anio )
                        ->count();
                    if($mes_reg != 0)
                    {
                        DB::table('ventas_mes')
                            ->where('ventas_mes.tiendas_Id', '=', $id_suc->Id )
                            ->where('ventas_mes.mes', '=', $mes )
                            ->where('ventas_mes.anio', '=', $anio )
                            ->update([
                                'presupuesto' =>  $presupuesto,
                                'dias' =>  $dias,
                                'metadia' => $metadia
                            ]);
                    }else
                    {
                        DB::table('ventas_mes')->insert([
                            [
                                'tiendas_Id' =>  $id_suc->Id,
                                'mes' =>  $mes,
                                'anio' =>  $anio,
                                'presupuesto' =>  $presupuesto,
                                'dias' =>  $dias,
                                'status' =>  $status,
                                'tickets' => 0,
                                'artxtck' => 0,
                                'metadia' => $metadia,
                                'tckprom' => 0,
                                'clasificacion_Id' => 8
                            ]
                        ]);
                    }

                }
            }else
            {
                break;
            }
        }

        return response()->json(['path' => $collection ]);
    }
}
