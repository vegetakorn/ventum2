@extends('adminlte::pagesuc')
@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>Reporte de Ventas Mensual </h1>
@stop

@section('content')


    <div id="filtro" class="box box-success box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Ventas Mensuales</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <strong><i class="fa fa-tachometer margin-r-5"></i> Reporte en el que podrás ver las ventas del mes de la sucursal</strong>
            <hr>
            <div class="radioVal">

            </div>
            <hr>
            <strong><i class="fa fa-book margin-r-5"></i> Selecciona Año</strong>
            <select  onchange="setButton()" class="form-control select2" id="year" name="years" style="width: 100%;">
                <option  value="{{date('Y')}}" selected="selected"><?php echo date('Y')?></option>
                <option value="<?php echo date('Y') - 1?>"><?php echo date('Y') - 1?></option>

            </select>
            <hr>
            <strong><i class="fa fa-book margin-r-5"></i> Seleccionar Mes</strong>
            <select onchange="setButton()" class="form-control select2" id="mes" name="meses" style="width: 100%;">

                <option value="1">Enero</option>
                <option value="2">Febrero</option>
                <option value="3">Marzo</option>
                <option value="4">Abril</option>
                <option value="5">Mayo</option>
                <option value="6">Junio</option>
                <option value="7">Julio</option>
                <option value="8">Agosto</option>
                <option value="9">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11" selected>Noviembre</option>
                <option value="12">Diciembre</option>

            </select>
            <hr>
            <div class="btnVer">


            </div>

        </div>
        <!-- /.box-body -->
    </div>


@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link href="{{ asset('dist/dropzone.css') }}" rel="stylesheet">

    <style>
        .badge-important {

        }
        .image-upload > input
        {
            display: none;
        }

    </style>

    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }

    </style>

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

@stop

@section('js')
    @route
    <script src="{{ asset('bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('dist/timer.jquery.min.js')}}"></script>
    <script>

        $(document).ready(function() {
            var tiendas_Id = '{{ Auth::user()->empleados_Id }}';
            var url = route('exportVentaSuc', {razon: 0,
                plaza: 0,
                anio: document.getElementById('year').value,
                mes:document.getElementById('mes').value,
                suc:tiendas_Id});

            $('.btnVer').html('<a href="'+url+'" class="btn btn-block btn-primary ">Genera Reporte</a>')

        } );
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })


        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            })

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            });



        });


        function generaReporte()
        {

        }

        f

        function setButton()
        {
            var tiendas_Id = '{{ Auth::user()->empleados_Id }}';
            var url = route('exportVentaSuc', {razon: 0,
                plaza: 0,
                anio: document.getElementById('year').value,
                mes:document.getElementById('mes').value,
                suc:tiendas_Id});

            $('.btnVer').html('<a href="'+url+'" class="btn btn-block btn-primary ">Genera Reporte</a>')
        }




    </script>

@stop