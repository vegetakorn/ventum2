@extends('adminlte::page')

@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>Widgets Configurados </h1>
@stop

@section('content')
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row" id="widget2">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">

            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-briefcase"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Sucursales </span>
              <span class="info-box-number">{{$notiendas}}<small></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-person-stalker"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Empleados</span>
              <span class="info-box-number">{{$noempleados}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-pricetags"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Tickets</span>
              <span class="info-box-number">{{$sumTck}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-compose"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Visitas</span>
              <span class="info-box-number">{{$novisitas}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row" id="widget1">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title infoFinanciera">Información Financiera</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-toggle="modal" data-target="#modal-default"><i class="fa  fa-sort-amount-desc"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8 ">
                    <div class="barcanvas" >
                              <label>Gráfica de Ventas/Presupuestos vs Tendencia</label>
                        <div class="graficaVentas">
                            <canvas class="grafcanvas" id="barcanvas"  ></canvas>
                        </div>



                        </div>
                  <!-- /.chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <p class="text-center">
                    <strong>Metas del Mes</strong>
                  </p>
                    <!-- /.progress-group -->
                    <div class="progress-group barSinpres">
                        <span class="progress-text">Sucursales sin presupuesto</span>
                        <span class="progress-number"><b>26</b>/45</span>

                        <div class="progress progress-sm active">
                            <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                <span class="sr-only">20% Complete</span>
                            </div>
                        </div>
                    </div>
                    <!-- /.progress-group -->
                  <div class="progress-group barPresupuesto">
                    <span class="progress-text">Sucursales en Presupuesto</span>
                    <span class="progress-number"><b>10</b>/ de 15</span>

                     <div class="progress progress-sm active">
		                <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 66%">
		                  <span class="sr-only">20% Complete</span>
		                </div>
		              </div>
                  </div>

                  <!-- /.progress-group -->
                  <div class="progress-group vtavspres" >
                    <span class="progress-text">Ventas vs Presupuesto</span>
                    <span class="progress-number"><b>35%</b>/ de 100%</span>

                    <div class="progress progress-sm active">
		                <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 35%">
		                  <span class="sr-only">20% Complete</span>
		                </div>
		              </div>
                  </div>
                  <!-- /.progress-group
                  <div class="progress-group">
                    <span class="progress-text">Tiendas Visitadas</span>
                    <span class="progress-number"><b>5</b>/ de 15</span>

                     <div class="progress progress-sm active">
		                <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 30%">
		                  <span class="sr-only">20% Complete</span>
		                </div>
		              </div>
                  </div>-->

                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right sumVentas">
                    <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span>
                    <h5 class="description-header">$35,210.43</h5>
                    <span class="description-text">Venta Neta Acumulada</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right sumPresupuestos">
                    <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span>
                    <h5 class="description-header">$100,390.90</h5>
                    <span class="description-text">Presupuesto Neto Acumulado</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right avancePres">
                    <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span>
                    <h5 class="description-header">93.55%</h5>
                    <span class="description-text">Avance Presupuesto Teórico</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6 totSucursales">
                  <div class="description-block ">
                    <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 18%</span>
                    <h5 class="description-header"></h5>
                    <span class="description-text">Total de Sucursales</span>
                  </div>
                  <!-- /.description-block -->
                </div>
              </div>
                <hr>
              <!-- /.row -->
                <div class="row signosVitales">

                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    <div class="row" id="widget4">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Mis Empleados</h3>

                <div class="box-tools pull-right">
                    <!--<span class="label label-danger">2 Nuevos </span>-->
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <div class="box-body">
                    <ul class="products-list product-list-in-box">
                        @foreach($empleados as $empleados)

                            <li class="item">
                                <div class="product-img">
                                    <img src="{{ asset('uploads/Empleados/'.$empleados->foto)}}" alt="Product Image">
                                </div>
                                <div class="product-info">
                                    <a href="javascript:void(0)" class="product-title">{{$empleados->nombre}} {{$empleados->apepat}} {{$empleados->apemat}}
                                        <!--<span class="label label-success pull-right">$1800</span>--></a>
                                    <span class="product-description">
                          {{$empleados->puesto}}
                        </span>
                                </div>
                            </li>

                        @endforeach


                    </ul>
                </div>

                <!-- /.users-list -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <a href="{{route('empleados.empleados')}}" class="uppercase">Ver mis empleados</a>
            </div>
            <!-- /.box-footer -->
        </div>

    </div>
    <div class="row" id="widget3">
        <!-- TABLE: LATEST ORDERS -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">To-Do's Antigüos </h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                        <tr>
                            <th>Sucursal</th>
                            <th>Campo</th>
                            <th>Descripción</th>
                            <th>Tipo</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($todos as $todo)

                                <tr>
                                    <td width="20%"><a >{{$todo->tienda}}</a>  ({{date('d/m/Y H:i:s', strtotime($todo->fecha_inicio))}})</td>
                                    <td width="30%">{{$todo->campo}}</td>
                                    <td width="30%">{{$todo->descripcion}}</td>

                                    @if($todo->tipo == 1)
                                        <td width="20%">Checklist</td>
                                    @endif
                                    @if($todo->tipo == 2)
                                        <td width="20%">To-Do Express</td>
                                    @endif
                                    @if($todo->tipo == 3)
                                        <td width="20%">To-Do FTP</td>
                                    @endif
                                    @if($todo->tipo == 4)
                                        <td width="20%">To-Do Supervisor</td>
                                    @endif

                                    @if($todo->Status == 151)
                                        <td width="20%"><span class="label label-danger">Nuevo</span></td>
                                    @endif
                                    @if($todo->Status == 152)
                                        <td width="20%"><span class="label label-warning">En Curso</span></td>
                                    @endif
                                    @if($todo->Status == 153)
                                        <td width="20%"><span class="label label-primary">Pendiente</span></td>
                                    @endif
                                </tr>
                            @endforeach

                        </tbody>
                    </table>

                    <!-- /.box-header -->
                    <div id="canvas-holder" style="width:95%">
                        <canvas id="chart-area" />
                    </div>
                </div>
                <!-- /.table-responsive -->
            </div>
           
        </div>
    </div>
    <div class="row" id="widget5">
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Reporte de Desempeño</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>

                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <strong><i class="fa fa-tachometer margin-r-5"></i> Selecciona Fecha</strong>

                <div class="form-group">


                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input  type="text" class="form-control pull-right" name="rango" id="reservation">
                    </div>
                    <!-- /.input group -->
                </div>
                <div class="box-footer clearfix">
                    <a onclick="getDesemp()" class="btn btn-sm btn-info btn-flat pull-right">Generar</a>

                </div>
                <hr>
                <div class="desempeno">

                </div>
            </div>

        </div>
    </div>
    <div class="row " id="widget6">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Signos Vitales de {{$mes}}</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-toggle="modal" data-target="#modal-signos"><i class="fa  fa-sort-amount-desc"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body " >
                <div class="signosvitales">

                </div>

                <!-- /.table-responsive -->
            </div>

        </div>
    </div>




       <!-- <div style="width: 75%">
            <canvas id="canvasNew"></canvas>
        </div>
        <button id="randomizeData">Randomize Data</button>-->


        <div class="modal fade" id="modal-default" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Filtro de Información Financiera</h4>
                    </div>
                    <div class="modal-body">
                        <strong><i class="fa fa-book margin-r-5"></i> Selecciona Año</strong>
                        <select class="form-control select2" id="year" name="years" style="width: 100%;">

                            <option  value="{{date('Y')}}" selected="selected" ><?php echo date('Y')?></option>
                            <option value="<?php echo date('Y') - 1?>"><?php echo date('Y') - 1?></option>

                        </select>
                        <hr>
                        <strong><i class="fa fa-book margin-r-5"></i> Seleccionar Mes</strong>
                        <select class="form-control select2" id="mes" name="meses" style="width: 100%;">
                            <option value="0" selected="selected" >Selecciona Mes</option>
                            <option value="1">Enero</option>
                            <option value="2">Febrero</option>
                            <option value="3">Marzo</option>
                            <option value="4">Abril</option>
                            <option value="5">Mayo</option>
                            <option value="6">Junio</option>
                            <option value="7">Julio</option>
                            <option value="8">Agosto</option>
                            <option value="9">Septiembre</option>
                            <option value="10">Octubre</option>
                            <option value="11">Noviembre</option>
                            <option value="12">Diciembre</option>

                        </select>
                        <hr>
                        <strong><i class="fa fa-book margin-r-5"></i> Seleccionar Razón</strong>
                        <select onchange="getPlazas()" class="form-control select2" id="razon" name="razones" style="width: 100%;">
                            <option value="0" selected="selected" >Selecciona Razón</option>
                            @foreach($razones as $razon)
                                <option value="{{$razon->Id}}"  >{{$razon->nombre}}</option>
                            @endforeach
                        </select>
                        <hr>
                        <div class="plazas">
                            <input type="hidden" id="plaza" value="0" >
                        </div>
                        <hr>
                        <div class=" tiendas">
                            <input type="hidden" id="suc" value="0" >
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
                        <button onclick="genGraficaTendencia()" type="button" class="btn btn-primary">Filtrar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="modal fade" id="modal-signos" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Filtro de Signos Vitales</h4>
                    </div>
                    <div class="modal-body">
                        <strong><i class="fa fa-book margin-r-5"></i> Selecciona Año</strong>
                        <select class="form-control select2" id="yearS" name="years" style="width: 100%;">

                            <option  value="{{date('Y')}}" selected="selected" ><?php echo date('Y')?></option>
                            <option value="<?php echo date('Y') - 1?>"><?php echo date('Y') - 1?></option>

                        </select>
                        <hr>
                        <strong><i class="fa fa-book margin-r-5"></i> Seleccionar Mes</strong>
                        <select class="form-control select2" id="mesS" name="meses" style="width: 100%;">
                            <option value="0" selected="selected" >Selecciona Mes</option>
                            <option value="1">Enero</option>
                            <option value="2">Febrero</option>
                            <option value="3">Marzo</option>
                            <option value="4">Abril</option>
                            <option value="5">Mayo</option>
                            <option value="6">Junio</option>
                            <option value="7">Julio</option>
                            <option value="8">Agosto</option>
                            <option value="9">Septiembre</option>
                            <option value="10">Octubre</option>
                            <option value="11">Noviembre</option>
                            <option value="12">Diciembre</option>

                        </select>
                        <hr>
                        <strong><i class="fa fa-book margin-r-5"></i> Seleccionar Razón</strong>
                        <select onchange="getPlazasS()" class="form-control select2" id="razonS" name="razones" style="width: 100%;">
                            <option value="0" selected="selected" >Selecciona Razón</option>
                            @foreach($razones as $razon)
                                <option value="{{$razon->Id}}"  >{{$razon->nombre}}</option>
                            @endforeach
                        </select>
                        <hr>
                        <div class="plazasS">
                            <input type="hidden" id="plazaS" value="0" >
                        </div>
                        <hr>
                        <div class=" tiendasS">
                            <input type="hidden" id="sucS" value="0" >
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
                        <button onclick="genSignosVitales()" type="button" class="btn btn-primary">Filtrar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


@stop
@section('css')
  <link rel="stylesheet" href="{{ asset('bower_components/jvectormap/jquery-jvectormap.css')}}">
  <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<style>

    .barcanvas {
        overflow-x: scroll;
        overflow-y: hidden;
        white-space: nowrap;


    }

</style>


            <!-- daterange picker -->
            <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
            <!-- bootstrap datepicker -->
            <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
  
@stop

@section('js')
<script src="{{ asset('bower_components/moment/min/moment.min.js')}}"></script>
    <!-- FastClick -->
<script src="{{ asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- Sparkline -->
<script src="{{ asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap  -->
<script src="{{ asset('bower_components/admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{ asset('bower_components/admin-lte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- SlimScroll -->
<script src="{{ asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{ asset('bower_components/chart.js/dist/Chart.js')}}"></script>
<!-- ChartJS-->
<script src="{{ asset('bower_components/chart.js/dist/Chart.bundle.js')}}"></script>
<script src="{{ asset('bower_components/chart.js/samples/utils.js')}}"></script>
<script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

 <script>

     function getDesemp()
     {
         var fecha = document.getElementsByName('rango')[0].value;
         var url = '{{route('getDesempeno')}}'
         $.ajax({
             method: 'POST',
             url: url,
             data: {fecha:  fecha }
         })
             .done(function(msg){

                 var text = "";
                 var fechas = setFechaPdf(document.getElementsByName('rango')[0].value);
                 var fecha_ini = setFechaFormat(fechas[0]);
                 var fecha_fin = setFechaFormat(fechas[1]);
                 var urlpdf = route('evalDesempPdf', [fecha_ini, fecha_fin]);
                 var urlexcel = route('evalDesempXls', [fecha_ini, fecha_fin]);



                 text +=    ' <div class="box-footer clearfix">\n' +
                     '               <a href="'+urlexcel+'" class="btn btn-social-icon btn-success pull-right"><i class="fa fa-file-excel-o"></i></a>      \n' +
                     ' <a href="'+urlpdf+'" class="btn btn-social-icon btn-danger pull-right"><i class="fa fa-file-pdf-o"></i></a>\n' +
                     '                 </div>\n' +
                     '                 <hr>' +
                     '<div class="table-responsive">\n' +
                     '                    <table class="table table-bordered" style="border-color: #747474" >\n' +
                     '                        <thead>\n' +
                     '                        <tr>\n' +
                     '                            <th style="border-color: #747474">Id</th>\n' +
                     '                            <th style="border-color: #747474">Nombre</th>\n' +
                     '                            <th style="border-color: #747474">Puesto</th>\n' +
                     '                            <th style="border-color: #747474">Razón</th>\n' +
                     '                            <th style="border-color: #747474">Plaza</th>\n' +
                     '                            <th style="border-color: #747474">Sucursales</th>\n' +
                     '                            <th style="border-color: #747474">Checklist</th>\n' +
                     '                            <th style="border-color: #747474">Seguimiento</th>\n' +
                     '                            <th style="border-color: #747474">Anteriores</th>\n' +
                     '                            <th style="border-color: #747474">Creados</th>\n' +
                     '                            <th style="border-color: #747474">Nuevos</th>\n' +
                     '                            <th style="border-color: #747474">En curso</th>\n' +
                     '                            <th style="border-color: #747474">Pendientes</th>\n' +
                     '                            <th style="border-color: #747474">Concluidos</th>\n' +
                     '                            <th style="border-color: #747474">Efectividad</th>\n' +

                     '\n' +
                     '                        </tr>\n' +
                     '                        </thead>\n' +
                     '                        <tbody>\n';



                 for(var i = 0; i < msg['supervisores'].length; i++)
                 {
                     text += ' <tr>\n' +
                         '                                    <td style="border-color: #bababa; text-align: center">'+msg['supervisores'][i]['clave']+'</td>\n' +
                         '                                    <td style="border-color: #bababa; text-align: center">'+msg['supervisores'][i]['nombre']+'</td>\n' +
                         '                                    <td style="border-color: #bababa; text-align: center">'+msg['supervisores'][i]['puesto']+'</td>\n' +
                         '                                    <td style="border-color: #bababa; text-align: center">'+msg['supervisores'][i]['razon']+'</td>\n' +
                         '                                    <td style="border-color: #bababa; text-align: center">'+msg['supervisores'][i]['plaza']+'</td>\n' +
                         '                                    <td style="border-color: #bababa; text-align: center">'+msg['supervisores'][i]['tiendas']+'</td>\n' +
                         '                                    <td style="border-color: #bababa; text-align: center">'+msg['supervisores'][i]['checklist']+'</td>\n' +
                         '                                    <td style="border-color: #bababa; text-align: center">'+msg['supervisores'][i]['seguimientos']+'</td>\n' +
                         '                                    <td style="border-color: #bababa; text-align: center">'+msg['supervisores'][i]['anteriores']+'</td>\n' +
                         '                                    <td style="border-color: #bababa; text-align: center">'+msg['supervisores'][i]['creados']+'</td>\n' +
                         '                                    <td style="border-color: #bababa; text-align: center">'+msg['supervisores'][i]['nuevos']+'</td>\n' +
                         '                                    <td style="border-color: #bababa; text-align: center">'+msg['supervisores'][i]['curso']+'</td>\n' +
                         '                                    <td style="border-color: #bababa; text-align: center">'+msg['supervisores'][i]['pendientes']+'</td>\n' +
                         '                                    <td style="border-color: #bababa; text-align: center">'+msg['supervisores'][i]['concluidos']+'</td>\n' +
                         '                                    <td style="border-color: #bababa; text-align: center">'+msg['supervisores'][i]['efectividad']+'%</td>\n' +

                         '                            </tr>';
                 }

                 text +=     '                        </tbody>\n' +
                     '                    </table>\n' +

                     '                </div>\n' +
                     '                <!-- /.table-responsive -->';
                 $('.desempeno').html( text )

                 //genera boton de excel


             });
     }

     function setFechaPdf(fecha) {
         var str = fecha;
         var res = str.split("-");
         return res;
     }

     function setFechaFormat(fecha) {
         var str = fecha;
         var res = str.split("/");
         return res[0]+'-'+res[1]+'-'+res[2];
     }


     function getPlazas()
     {
         // alert(document.getElementById('razon').value) plazas capturadas
         //vamos a obtener el listado de cosas para asignar al empleado
         var url = '{{route('cargaPlazas')}}'
         $.ajax({
             method: 'POST',
             url: url,
             data: {id:  document.getElementById('razon').value  }
         })
             .done(function(msg){

                 //console.log(msg['puestos']);
                 var text = "";

                 text += '<b>Plazas</b> <select onchange="getTiendas()" class="form-control select2" id="plaza" name="plaza" style="width: 100%;">\n' +
                     '<option value="0" selected>Selecciona una plaza </option>' ;


                 for(var i = 0; i < msg['plazas'].length; i++)
                 {
                     text += ' <option value="'+msg['plazas'][i]['Id']+'">'+msg['plazas'][i]['plaza']+'</option>\n';
                 }

                 text +=    '                           </select>';
                 $('.plazas').html( text )


             });

     }

     function getTiendas()
     {
         // alert(document.getElementById('razon').value) plazas capturadas
         //vamos a obtener el listado de cosas para asignar al empleado
         var url = '{{route('cargaTiendas')}}'
         $.ajax({
             method: 'POST',
             url: url,
             data: {id:  document.getElementById('plaza').value  }
         })
             .done(function(msg){

                 //console.log(msg['tiendas']);
                 var text = "";

                 text += '<b>Sucursales</b> <select  class="form-control select2" id="suc" name="suc" style="width: 100%;">\n' +
                     '<option value="0" selected>Selecciona una sucursal </option>' ;


                 for(var i = 0; i < msg['tiendas'].length; i++)
                 {
                     text += ' <option value="'+msg['tiendas'][i]['Id']+'">'+msg['tiendas'][i]['nombre']+'</option>\n';
                 }

                 text +=    '                           </select>';
                 $('.tiendas').html( text )


             });

     }


     function getPlazasS()
     {
         // alert(document.getElementById('razon').value) plazas capturadas
         //vamos a obtener el listado de cosas para asignar al empleado
         var url = '{{route('cargaPlazas')}}'
         $.ajax({
             method: 'POST',
             url: url,
             data: {id:  document.getElementById('razonS').value  }
         })
             .done(function(msg){

                // console.log(msg['puestos']);
                 var text = "";

                 text += '<b>Plazas</b> <select onchange="getTiendasS()" class="form-control select2" id="plazaS" name="plaza" style="width: 100%;">\n' +
                     '<option value="0" selected>Selecciona una plaza </option>' ;


                 for(var i = 0; i < msg['plazas'].length; i++)
                 {
                     text += ' <option value="'+msg['plazas'][i]['Id']+'">'+msg['plazas'][i]['plaza']+'</option>\n';
                 }

                 text +=    '                           </select>';
                 $('.plazasS').html( text )


             });

     }

     function getTiendasS()
     {
         // alert(document.getElementById('razon').value) plazas capturadas
         //vamos a obtener el listado de cosas para asignar al empleado
         var url = '{{route('cargaTiendas')}}'
         $.ajax({
             method: 'POST',
             url: url,
             data: {id:  document.getElementById('plazaS').value  }
         })
             .done(function(msg){

                // console.log(msg['tiendas']);
                 var text = "";

                 text += '<b>Sucursales</b> <select  class="form-control select2" id="sucS" name="suc" style="width: 100%;">\n' +
                     '<option value="0" selected>Selecciona una sucursal </option>' ;


                 for(var i = 0; i < msg['tiendas'].length; i++)
                 {
                     text += ' <option value="'+msg['tiendas'][i]['Id']+'">'+msg['tiendas'][i]['nombre']+'</option>\n';
                 }

                 text +=    '                           </select>';
                 $('.tiendasS').html( text )


             });

     }


     var randomScalingFactor = function() {
        return Math.round(Math.random() * 100);
    };

    var config3 = {
        type: 'pie',
        data: {
            datasets: [{
                data: [
                    15,
                    10,
                    7,
                    6,
                    2,
                ],
                backgroundColor: [
                    window.chartColors.red,
                    window.chartColors.orange,
                    window.chartColors.yellow,
                    window.chartColors.green,
                    window.chartColors.blue,
                ],
                label: 'Dataset 1'
            }],
            labels: [
                "Fachada Sucia",
                "Servicio Postventa",
                "Personal con Gafete",
                "Baños Limpios",
                "Libre de Obstáculos"
            ]
        },
        options: {
            responsive: true
        }
    };

   

    document.getElementById('randomizeData').addEventListener('click', function() {
        config.data.datasets.forEach(function(dataset) {
            dataset.data = dataset.data.map(function() {
                return randomScalingFactor();
            });
        });

        window.myPie.update();
    });

    var colorNames = Object.keys(window.chartColors);
   

   
    </script>
 <script>
        var config2 = {
            type: 'line',
            data: {
                labels: ["Julio 17", "Agosto 17", "Septiembre 17", "Octubre 17", "Noviembre 17", "Diciembre 17", "Enero 18"],
                datasets: [{
                    label: "Tickect Prom.",
                    fill: false,
                    backgroundColor: window.chartColors.blue,
                    borderColor: window.chartColors.blue,
                    data: [
                        200,
                        100,
                        159,
                        254,
                        300,
                        302,
                        50
                    ],
                }, {
                    label: "Trans. Diarias",
                    fill: false,
                    backgroundColor: window.chartColors.green,
                    borderColor: window.chartColors.green,
                    borderDash: [5, 5],
                    data: [
                        30,
                        10,
                        15,
                        25,
                        45,
                        60,
                        50
                    ],
                }, {
                    label: "No. Art. por Ticket Promedio",
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    data: [
                        2.4,
                        3.5,
                        1.5,
                        2.8,
                        3.0,
                        2,
                        3.2
                    ],
                    fill: true,
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:false,
                    text:'Chart.js Line Chart'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Mes'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Valor'
                        }
                    }]
                }
            }
        };

       
    </script>

        <script>
            var barChartData = {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                datasets: [{
                    label: 'Dataset 1',
                    backgroundColor: window.chartColors.red,
                    stack: 'Stack 0',
                    data: [
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor()
                    ]
                }, {
                    label: 'Dataset 2',
                    backgroundColor: window.chartColors.blue,
                    stack: 'Stack 0',
                    data: [
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor()
                    ]
                }, {
                    label: 'Dataset 3',
                    backgroundColor: window.chartColors.green,
                    stack: 'Stack 1',
                    data: [
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor()
                    ]
                }]

            };


            document.getElementById('randomizeData').addEventListener('click', function() {
                barChartData.datasets.forEach(function(dataset) {
                    dataset.data = dataset.data.map(function() {
                        return randomScalingFactor();
                    });
                });
                window.myBar.update();
            });
        </script>

 <script>
        var MONTHS = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
        var config = {
            type: 'line',
            data: {
                labels: ["Enero 17", "Febrero 17", "Marzo 17", "Abril 17", "Mayo 17", "Junio 17", "Julio 17",
                "Agosto 17","Septiembre 17","Octubre 17","Noviembre 17","Diciembre 17","Enero 18"],
                datasets: [{
                    label: "Ventas",
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    data: [
                        100,
                        200,
                        150,
                        120,
                        50,
                        80,
                        98,
                        120,
                        220,
                        170,
                        190,
                        40,
                        60
               
                    ],
                    fill: false,
                }, {
                    label: "Presupuesto",
                    fill: false,
                    backgroundColor: window.chartColors.blue,
                    borderColor: window.chartColors.blue,
                    data: [
                        120,
                        220,
                        170,
                        190,
                        40,
                        60,
                        70,
                        120,
                        50,
                        80,
                        98,
                        120,
                        220
                    ],
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:'Ventas Mensuales 2017-2018'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Mes'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Monto (MDP)'
                        }
                    }]
                }
            }
        };

        window.onload = function() {
            var ctx = document.getElementById("canvas").getContext("2d");
            window.myLine = new Chart(ctx, config);

            var ctx2 = document.getElementById("canvas2").getContext("2d");
            window.myLine2 = new Chart(ctx2, config2);

             var ctx3 = document.getElementById("chart-area").getContext("2d");
        window.myPie = new Chart(ctx3, config3);

             
        };

      
    </script>

    @routes
    <script>
        $(document).ready(function() {


            //Date range picker
            $('#reservation').daterangepicker({
                "startDate": moment().startOf('month'),
                "endDate": moment().add(1, 'day'),
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Aplicar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "De",
                    "toLabel": "a",
                    "customRangeLabel": "Custom",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Septiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                }
            })


            var url = '{{route('getTiendas')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:0}
            })
                .done(function(msg){

                   // console.log(msg['tiendas']);
                    var text = "";

                    for(var i = 0; i < msg['tiendas'].length; i++)
                    {
                        var URLdomain = window.location.host;
                        var r = 'https://'+URLdomain+'/visitas/visita/'+msg['tiendas'][i]['Id'];
                        // text += ' <li><a href="#"><i class="fa fa-circle-o text-red"></i>'+msg['message'][i]['nombre']+'</a></li>'
                       // var r = 'visitas/visita/'+msg['tiendas'][i]['Id'];
                        text += '<li ><!-- start message -->\n' +
                            '                                                            <a href="'+r+'"  >\n' +
                            '                                                                <div class="pull-left ">\n' +
                            '                                                                    <h6>\n' +
                            '                                                                       '+msg['tiendas'][i]['nombre']+'\n' +
                            '                                                                    </h6>\n' +
                            '                                                                </div>\n' +
                            '\n' +
                            '                                                            </a>\n' +
                            '                                                        </li>'
                    }

                    $('.listaTiendas').html( text )
                    $('.noTiendas').html( msg['no'] )



                } );


            $("#widget1").hide();
            $("#widget2").hide();
            $("#widget3").hide();
            $("#widget4").hide();
            $("#widget5").hide();
            $("#widget6").hide();

            var url = '{{route('getWidgets')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:0}
            })
                .done(function(msg){

                   // console.log(msg['widgets']);
                    var text = "";

                    for(var i = 0; i < msg['widgets'].length; i++)
                    {
                        var mod = $("#widget"+msg['widgets'][i]['widgets_Id']);
                        mod.show();
                    }



                } );


            genGraficaTendencia();
            genSignosVitales();

        } );


        function genGraficaTendencia()
        {

            var isMobile = {
                Android: function() {
                    return navigator.userAgent.match(/Android/i);
                },
                BlackBerry: function() {
                    return navigator.userAgent.match(/BlackBerry/i);
                },
                iOS: function() {
                    return navigator.userAgent.match(/iPhone|iPad|iPod/i);
                },
                Opera: function() {
                    return navigator.userAgent.match(/Opera Mini/i);
                },
                Windows: function() {
                    return navigator.userAgent.match(/IEMobile/i);
                },
                any: function() {
                    return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
                }
            };


            $('#barcanvas').remove();
            //variables de tiempo
            var mes = document.getElementById('mes').value;
            var anio = document.getElementById('year').value;
            //variables de sucursales

            var razon = document.getElementById('razon').value;
            var plaza = document.getElementById('plaza').value;
            var sucursal = document.getElementById('suc').value;
            $('.infoFinanciera').html('');

            bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Generando información...</div>' })

            var url = '{{route('genGraficaVta')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {mes:mes, anio: anio, razon: razon, plaza: plaza, sucursal: sucursal}
            })
                .done(function(msg){

                    var meses = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
                    var mesPos = '{{$mes_num}}'
                    if(mes != 0)
                    {
                        mesPos = parseInt(mes);
                    }
                    $('.infoFinanciera').html('Información financiera correspondiente al mes de ' + meses[mesPos] + ' de ' + anio );
                    /*$('.graficaVentas').html('<div class="barcanvas">\n' +
                                        '                    <label>Gráfica de Ventas/Presupuestos vs Tendencia</label>\n' +
                                        '                     <canvas  id="barcanvas" style=" width: '+msg['ancho']+'px; height: 400px;" width="'+msg['ancho']+'" height="400" ></canvas>\n' +
                                        '\t\t\t        <!---->\n' +
                                        '\t\t\t    </div>');*/



                    //console.log(msg['grafica']);
                    //console.log(msg['info']);


                    $('.barPresupuesto').html('<span class="progress-text">Sucursales en Presupuesto</span>\n' +
                        '                    <span class="progress-number"><b>'+msg['enpres']+'</b>/ de '+msg['totsuc']+'</span>\n' +
                        '\n' +
                        '                     <div class="progress progress-sm active">\n' +
                        '\t\t                <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: '+msg['porEnpres']+'%">\n' +
                        '\t\t                  <span class="sr-only">20% Complete</span>\n' +
                        '\t\t                </div>\n' +
                        '\t\t              </div>');

                    $('.barSinpres').html('<span class="progress-text">Sucursales con Presupuesto</span>\n' +
                        '                    <span class="progress-number"><b>'+msg['nopres']+'</b>/ de '+msg['totsuc']+'</span>\n' +
                        '\n' +
                        '                     <div class="progress progress-sm active">\n' +
                        '\t\t                <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: '+msg['porSinpres']+'%">\n' +
                        '\t\t                  <span class="sr-only">20% Complete</span>\n' +
                        '\t\t                </div>\n' +
                        '\t\t              </div>');

                    var txt = '<span class="progress-text">Ventas vs Presupuesto</span>\n' +
                        '                    <span class="progress-number"><b>'+msg['vtavspres']+'%</b>/ de 100%</span>\n' +
                        '\n' +
                        '                     <div class="progress progress-sm active">\n' +
                        '\t\t                <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: '+msg['vtavspres']+'%">\n' +
                        '\t\t                  <span class="sr-only">20% Complete</span>\n' +
                        '\t\t                </div>\n' +
                        '\t\t              </div>';

                    txt += '<span class="progress-text">Sucursales Tendencia > Presupuesto</span>\n' +
                        '                    <span class="progress-number"><b>'+msg['tendenciaMayor']+'</b>/ de '+msg['totsuc']+'</span>\n' +
                        '\n' +
                        '                     <div class="progress progress-sm active">\n' +
                        '\t\t                <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: '+msg['porTendenciaMayor']+'%">\n' +
                        '\t\t                  <span class="sr-only">20% Complete</span>\n' +
                        '\t\t                </div>\n' +
                        '\t\t              </div>'

                    $('.vtavspres').html(txt);



                    $('.sumVentas').html( ' <!--<span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span>-->\n' +
                        '                    <h5 class="description-header">$'+msg['sumVta']+'</h5>\n' +
                        '                    <span class="description-text">Venta Neta Acumulada</span>')
                    $('.sumPresupuestos').html( ' <!--<span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span>-->\n' +
                        '                    <h5 class="description-header">$'+msg['sumPres']+'</h5>\n' +
                        '                    <span class="description-text">Presupuesto Neto Acumulado</span>')
                    $('.avancePres').html( ' <!--<span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span>-->\n' +
                        '                    <h5 class="description-header">'+msg['avanceTeorico']+'%</h5>\n' +
                        '                    <span class="description-text">Avance Presupuesto Teórico</span>')
                    $('.totSucursales').html( ' <div class="description-block ">\n' +
                        '                    <span class="description-percentage text-red"><i ></i> '+msg['totsuc']+'</span>\n' +
                        '                    <h5 class="description-header"></h5>\n' +
                        '                    <span class="description-text">Total de Sucursales</span>\n' +
                        '                  </div>')


                    $('.signosVitales').html('<div class="col-sm-3 col-xs-6">\n' +
                        '                        <div class="description-block border-right sumVentas">\n' +

                        '                            <h5 class="description-header">$'+msg['tckProm']+'</h5>\n' +
                        '                            <span class="description-text">Ticket Promedio</span>\n' +
                        '                        </div>\n' +
                        '                        <!-- /.description-block -->\n' +
                        '                    </div>\n' +
                        '                    <!-- /.col -->\n' +
                        '                    <div class="col-sm-3 col-xs-6">\n' +
                        '                        <div class="description-block border-right sumPresupuestos">\n' +

                        '                            <h5 class="description-header">'+msg['transDia']+'</h5>\n' +
                        '                            <span class="description-text">Transacciones Diarias</span>\n' +
                        '                        </div>\n' +
                        '                        <!-- /.description-block -->\n' +
                        '                    </div>\n' +
                        '                    <!-- /.col -->\n' +
                        '                    <div class="col-sm-3 col-xs-6">\n' +
                        '                        <div class="description-block border-right avancePres">\n' +

                        '                            <h5 class="description-header">'+msg['noArtxtckprom']+'</h5>\n' +
                        '                            <span class="description-text">No. Art. por Ticket Promedio</span>\n' +
                        '                        </div>\n' +
                        '                        <!-- /.description-block -->\n' +
                        '                    </div>\n' +
                        '                    <!-- /.col -->\n' +
                        '                    <div class="col-sm-3 col-xs-6 totSucursales">\n' +
                        '                        <div class="description-block ">\n' +

                        '                            <h5 class="description-header">'+msg['sumTickets']+'</h5>\n' +
                        '                            <span class="description-text">Transacciones al mes</span>\n' +
                        '                        </div>\n' +
                        '                        <!-- /.description-block -->\n' +
                        '                    </div>');
                  //  $(".grafcanvas").css("width", msg['ancho']);

                    //$(".barcanvas").css("height", 200);


                  //  $("#barcanvas").width(3000).height(450);

                    $('.graficaVentas').html(' <canvas  id="barcanvas"  ></canvas>');

                    var dataTiendas = [];
                    for(var i = 0; i < msg['grafica'].length; i++)
                    {
                        dataTiendas.push( msg['grafica'][i]['tienda']);
                    }

                    var dataVentas = [];
                    for(var i = 0; i < msg['grafica'].length; i++)
                    {
                        dataVentas.push( msg['grafica'][i]['ventas']);
                    }

                    var dataPres = [];
                    for(var i = 0; i < msg['grafica'].length; i++)
                    {
                        dataPres.push( msg['grafica'][i]['presupuesto']);
                    }
                    var dataTendencia = [];
                    for(var i = 0; i < msg['grafica'].length; i++)
                    {
                        dataTendencia.push( msg['grafica'][i]['tendencia']);
                    }



                    var barChartData = {
                        labels: dataTiendas,
                        datasets: [{
                            label: 'Ventas',
                            backgroundColor: window.chartColors.green,
                            stack: 'Stack 0',
                            data: dataVentas
                        }, {
                            label: 'Presupuesto',
                            backgroundColor: window.chartColors.red,
                            stack: 'Stack 0',
                            data: dataPres
                        }, {
                            label: 'Tendencia',
                            backgroundColor: window.chartColors.blue,
                            stack: 'Stack 1',
                            data: dataTendencia
                        }]

                    };





                  //  $("#barcanvas").height('450').width(450);


                    if( isMobile.any() )
                    {
                        if(msg['totsuc'] <=5)
                        {
                            $("#barcanvas").height('450').width(450);
                        }
                        if(msg['totsuc'] >5 && msg['totsuc'] <=15)
                        {
                            $("#barcanvas").height('450').width(2400);
                        }
                        if(msg['totsuc'] >15 && msg['totsuc'] <=30)
                        {
                            $("#barcanvas").height('450').width(4800);
                        }

                        if(msg['totsuc'] >60 )
                        {
                            $("#barcanvas").height('450').width(9000);
                        }
                    }else
                    {
                        $("#barcanvas").height(450).width(msg['ancho']);
                    }





                    var ctx = document.getElementById('barcanvas').getContext('2d');
                    window.myBar = new Chart(ctx, {
                        type: 'bar',
                        data: barChartData,

                        options: {
                            title: {
                                display: false,
                                text: 'Ventas/Presupuestos vs Tendencia'
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false
                            },
                            responsive: false,
                            scales: {
                                xAxes: [{
                                    stacked: false,
                                }],
                                yAxes: [{
                                    stacked: false,
                                    ticks: {
                                        beginAtZero: true,


                                        // Return an empty string to draw the tick line but hide the tick label
                                        // Return `null` or `undefined` to hide the tick line entirely
                                        userCallback: function(value, index, values) {
                                            // Convert the number to a string and splite the string every 3 charaters from the end
                                            value = value.toString();
                                            value = value.split(/(?=(?:...)*$)/);

                                            // Convert the array to a string and format the output
                                            value = value.join('.');
                                            return '$' + value;
                                        }
                                    }
                                }]
                            },
                            tooltips: {
                                mode: 'label',
                                callbacks: {
                                    label: function(t, d) {
                                        var dstLabel = d.datasets[t.datasetIndex].label;
                                        var yLabel = t.yLabel;
                                        return dstLabel + ': $' + number_format(yLabel,2) + ' ';
                                    }
                                }
                            }
                        }
                    });

                    $('#modal-default').modal('hide');
                    bootbox.hideAll();



                } );





        }


        function genSignosVitales()
        {
            var url = '{{route('filtroSignos')}}'
            var razon =  document.getElementById('razonS').value;
            var plaza =  document.getElementById('plazaS').value;
            var tienda =  document.getElementById('sucS').value;
            var anio =  document.getElementById('yearS').value;
            var mes =  document.getElementById('mesS').value;

            bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Generando información...</div>' })
            $.ajax({
                method: 'POST',
                url: url,
                data: {razon:razon, plaza: plaza, tienda: tienda, anio: anio, mes: mes}
            })
                .done(function(msg){
                    bootbox.hideAll();
                    console.log(msg['signos']);
                    $('#modal-signos').modal('hide');
                   var text = ' <table id="signos_vitales" class="table table-bordered table-striped ">\n' +
                        '                    <thead>\n' +
                        '                    <tr>\n' +
                        '                        <th>Razón</th>\n' +
                        '                        <th>Plaza</th>\n' +
                        '                        <th>#</th>\n' +
                        '                        <th>Sucursal</th>\n' +
                        '                        <th>Tickets</th>\n' +
                        '                        <th>Trans. Diarias</th>\n' +
                        '                        <th>Art. x Ticket</th>\n' +
                        '\n' +
                        '\n' +
                        '                    </tr>\n' +
                        '                    </thead>\n' +
                        '\n' +
                        '\n' +
                        '                    <tbody>\n' +

                        '\n';


                    for(var i = 0; i < msg['signos'].length; i++) {


                         text += ' <tr>\n' +
                            '                            <td>'+msg['signos'][i]['razon']+'</td>\n' +
                            '                            <td>'+msg['signos'][i]['plaza']+'</td>\n' +
                            '                            <td><a >'+msg['signos'][i]['numsuc']+'</a></td>\n' +
                            '                            <td>'+msg['signos'][i]['nombre']+'</td>\n' +
                            '                            <td><span class="description-percentage '+msg['signos'][i]['color_tck']+'"><i class="fa '+msg['signos'][i]['flecha_tck']+'"></i> '+msg['signos'][i]['ticket']+' </span></td>\n' +
                            '                            <td><span class="description-percentage '+msg['signos'][i]['color_trans']+'"><i class="fa '+msg['signos'][i]['flecha_trans']+'"></i> '+msg['signos'][i]['trans']+' </span></td>\n' +
                            '                            <td><span class="description-percentage '+msg['signos'][i]['color_art']+'"><i class="fa '+msg['signos'][i]['flecha_art']+'"></i>'+msg['signos'][i]['artxtck']+' </span></td>\n' +
                            '                        </tr>';

                    }


                    text += '\n' +
                        '                    </tbody>\n' +
                        '                </table>';

                    $('.signosvitales').html(text);

                    setTable();



                } );


        }



        function number_format(number, decimals, dec_point, thousands_sep) {
// *     example: number_format(1234.56, 2, ',', ' ');
// *     return: '1 234,56'
            number = (number + '').replace(',', '').replace(' ', '');
            var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                s = '',
                toFixedFix = function (n, prec) {
                    var k = Math.pow(10, prec);
                    return '' + Math.round(n * k) / k;
                };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '').length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1).join('0');
            }
            return s.join(dec);
        }

        function setTable()
        {
            $('#signos_vitales').DataTable( {
                "pagingType": "full_numbers",
                "scrollY":        '60vh',
                "scrollCollapse": false,
                "language": {
                    "lengthMenu": "Mostrando _MENU_ registros por página ",
                    "zeroRecords": "Sin registros encontrados",
                    "info": "Mostrando página  _PAGE_ de _PAGES_ páginas",
                    "infoEmpty": "Sin regustros encontrados",
                    "infoFiltered": "(filtrados de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    }
                }
            } );
        }

    </script>


    
@stop