@extends('adminlte::page')
@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>
        Cuestionarios

    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Cuestionarios</li>
    </ol>


@stop

@section('content')
    <div class="row">
        <div class="col-md-3">
            <!-- <a href="compose.html" class="btn btn-primary btn-block margin-bottom">Compose</a>-->
            <!-- /. box -->
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Cuestionarios</h3>

                    <div class="box-tools">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body ">
                    <select class="form-control select2" id="cuest"   name="cuestionario" style="width: 100%;">
                        <option selected="selected">--Selecciona--</option>
                        @foreach($cuestionarios as $test)
                            <option value="{{$test->Id}}"> {{$test->Nombre}}</option>

                        @endforeach

                    </select>

                </div>
                <!-- /.box-body -->
            </div>

            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Preguntas</h3>

                    <div class="box-tools">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body ">
                    <ul class="nav nav-pills nav-stacked ajax-content">



                    </ul>
                    <button type="button" onclick="setPregunta()" id="btnAddPregunta" class="btn btn-block btn-success">Agregar Pregunta</button>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9" >
            <div class="box" id="boxPregunta">
                <div class="box-header">

                </div>
                <!-- /.box-header -->
                <div class="box-body ajax-pregunta">


                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <div class="box box-success" id="boxAddPregunta">
                <div class="box-header with-border">
                    <h3 class="box-title">Agregar Pregunta</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form role="form">
                        <!-- text input -->
                        <div class="form-group">
                            <label>Pregunta</label>
                            <input type="text" class="form-control" name="txtPregunta" id="txtPregunta" placeholder="Escribe la pregunta ...">
                        </div>
                        <!-- radio -->
                        <div class="form-group">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="optionsRadios" id="optionsRadios1" value="1" checked="">
                                    Pregunta de opción múltiple
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="optionsRadios" id="optionsRadios2" value="0">
                                    Pregunta Abierta
                                </label>
                            </div>

                        </div>
                        <div class="form-group pull-right">
                            <button type="button" onclick="save()" class="btn btn btn-success">Guardar</button>
                            <button type="button" onclick="cancelar()" class="btn btn btn-danger">Cancelar</button>
                        </div>




                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.row -->
        </div>

    </div>
    <!-- /.col -->

@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@stop

@section('js')
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $('#example1').DataTable( {
            "pagingType": "full_numbers",
            "scrollY":        '60vh',
            "scrollCollapse": true,
            "language": {
                "lengthMenu": "Mostrando _MENU_ registros por página ",
                "zeroRecords": "Sin registros encontrados",
                "info": "Mostrando página  _PAGE_ de _PAGES_ páginas",
                "infoEmpty": "Sin registros encontrados",
                "infoFiltered": "(filtrados de _MAX_ registros totales)",
                "search": "Buscar:",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Último",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                }
            }
        } );


        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : false
            })
        })
    </script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            })

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            })



        })
    </script>
    <script>

        var token = '{{csrf_token()}}';
        var url   = '{{route('getpreg')}}'
        var urlresp   = '{{route('getresp')}}';
        var urlpreg   = '{{route('savePregunta')}}'
        var addresp   = '{{route('addResp')}}';
    </script>
    <script>


        $(document).ready(function() {
            $("#btnAddPregunta").attr("disabled", "disabled");
            $("#boxPregunta").hide();
            $("#boxAddPregunta").hide();

        } );

        function setPregunta()
        {
            //alert("Agregar pregunta del cuestionario"+ document.getElementsByName('cuestionario')[0].value);
            $("#boxAddPregunta").show();
            $("#boxPregunta").hide();
            $("#btnAddPregunta").attr("disabled", "disabled");
            pregunta: document.getElementsByName('txtPregunta')[0].value ="";
        }
        function cancelar()
        {
            $("#boxAddPregunta").hide();
            $("#boxPregunta").hide();
        }
        function save()
        {
            var tipo = 0;
            if($("#optionsRadios1").is(':checked')) {
                tipo = 1
            }

            // alert(document.getElementsByName('optionsRadios')[0].value)
            if(document.getElementsByName('txtPregunta')[0].value != "")
            {
                $.ajax({
                    method: 'POST',
                    url: urlpreg,
                    data: {id_cuest:document.getElementsByName('cuestionario')[0].value,
                            pregunta: document.getElementsByName('txtPregunta')[0].value,
                            tipo:tipo}
                })
                    .done(function(msg){


                        $("#boxAddPregunta").hide();
                        $.ajax({
                            method: 'POST',
                            url: url,
                            data: {id:document.getElementsByName('cuestionario')[0].value, postId: '', _token: token}
                        })
                            .done(function(msg){
                                //  console.log(msg['message'])
                                var text = "";
                                for(var i = 0; i < msg['message'].length; i++)
                                {
                                    // text += ' <li><a href="#"><i class="fa fa-circle-o text-red"></i>'+msg['message'][i]['nombre']+'</a></li>'
                                    text += '<li> <label>  <input  type="radio" onclick="getRespuestas('+msg['message'][i]['Id']+'); "  name="r1" class="iradio_flat-green"> '+msg['message'][i]['Pregunta']+ '</label> </li>'
                                }
                                $('.ajax-content').html( text );
                                $("#btnAddPregunta").removeAttr('disabled');
                            } );

                    });
            }else
            {
                bootbox.alert("Debes escribir una pregunta")
            }

        }


        function getRespuestas($msj)
        {
            $("#boxAddPregunta").hide();
            //alert($msj);
            $.ajax({
                method: 'POST',
                url: urlresp,
                data: {body:$msj, postId: '', _token: token}
            })
                .done(function(msg){
                    $("#boxPregunta").show();
                    console.log(msg['pregunta'])
                    console.log(msg['respuestas'])
                    var text = "";
                    var tipo = "Abierta";
                    var img = '{{ asset('images/buttons/focus.png')}}';
                    var add = '{{ asset('images/buttons/add1.png')}}';

                    if(msg['pregunta']['tipo'] == 1)
                    {
                        tipo = "Opción Múltiple";
                    }


                    text += '<div class="box box-widget">\n' +
                        '            <div class="box-header with-border">\n' +
                        '              <div class="user-block">\n' +
                         '<img class="img-circle" src="'+img+'"  alt="User Image">'+
                        '                <span class="username"><a href="#">'+msg['pregunta']['Pregunta']+'</a></span>\n' +
                        '                <span class="description">Tipo: '+tipo+'</span>\n' +
                        '              </div>\n' +
                        '              <!-- /.user-block -->\n' +
                        '              <div class="box-tools">\n' +

                        '              </div>\n' +
                        '              <!-- /.box-tools -->\n' +
                        '            </div>\n' +
                        '            <!-- /.box-header -->\n' +
                        '            <div class="box-body">\n' +
                        '              <!-- Attachment -->\n' +

                        '       <h4>Respuestas</h4>      <!-- /.box-body -->\n' +
                        '            <div class="box-footer box-comments">\n' +
                        '              <div class="box-comment">\n' +
                        '                 <!-- User image -->\n' ;
                    if(msg['pregunta']['tipo'] == 1)
                    {
                        text += '                <div class="listRespuesta"> <div class="comment-text">\n' +
                            '                      <span class="username">\n' +
                            '                       \n' +
                            '                </div></div>\n' +
                            '                <!-- /.comment-text -->\n' +
                            '              </div>\n' +
                            '              <!-- /.box-comment -->\n' +
                            '            </div>\n' +
                            '            <!-- /.box-footer -->\n' +
                            '            <div class="box-footer">\n' +

                            '                <img onclick="addRespuesta('+$msj+')" class="img-responsive img-circle img-sm " src="'+add+'" alt="Alt Text">\n' +
                            '                <!-- .img-push is used to add margin to elements next to floating images -->\n' +
                            '                <div class="img-push">\n' +
                            '                  <input type="text" class="form-control" name="respuesta" placeholder="Agregar Respuesta">\n' +

                            '                </div>\n' ;
                    }


                        text +=  '            </div>\n' +
                        '            <!-- /.box-footer -->\n' +
                        '          </div>';

                    $('.ajax-pregunta').html( text )

                    var textr = "";
                    for(var i = 0; i < msg['respuestas'].length; i++)
                    {
                        textr += '<div class="comment-text">\n' +
                            '                      <span class="username">\n' +
                            '                  -  '+msg['respuestas'][i]['respuesta']+'     \n' +
                            '                </div>';
                    }
                    $('.listRespuesta').html( textr )




                });
        }

        function addRespuesta($msj)
        {

            if(document.getElementsByName('respuesta')[0].value != "")
            {
                $.ajax({
                    method: 'POST',
                    url: addresp,
                    data: {id_preg:$msj,
                        respuesta: document.getElementsByName('respuesta')[0].value
                    }
                })
                    .done(function(msg){
                        var text = "";
                        for(var i = 0; i < msg['respuestas'].length; i++)
                        {
                            text += '<div class="comment-text">\n' +
                            '                      <span class="username">\n' +
                            '                   '+msg['respuestas'][i]['respuesta']+'     \n' +
                            '                </div>';
                        }
                     $('.listRespuesta').html( text )

                        document.getElementsByName('respuesta')[0].value = "";

                    });

            }else
            {
                bootbox.alert("Debes escribir una respuesta");
            }
            //alert("agregar respuesta de la pregunta" + $msj)
        }

        $('#cuest').on('change' , function () {
            // alert(document.getElementsByName('checklist')[0].value);

            $("#boxAddPregunta").hide();
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:document.getElementsByName('cuestionario')[0].value, postId: '', _token: token}
            })
                .done(function(msg){
                  //  console.log(msg['message'])
                    var text = "";
                    for(var i = 0; i < msg['message'].length; i++)
                    {
                        // text += ' <li><a href="#"><i class="fa fa-circle-o text-red"></i>'+msg['message'][i]['nombre']+'</a></li>'
                        text += '<li> <label>  <input  type="radio" onclick="getRespuestas('+msg['message'][i]['Id']+'); "  name="r1" class="iradio_flat-green"> '+msg['message'][i]['Pregunta']+ '</label> </li>'
                    }
                    $('.ajax-content').html( text );
                    $("#btnAddPregunta").removeAttr('disabled');
                } );
        })
    </script>

@stop