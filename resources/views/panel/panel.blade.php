@extends('adminlte::page')
@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>Configuración general del sistema</h1>
@stop

@section('content')


    <div id="filtro" class="box box-primary box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Elegir opciones </h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">



            <strong><i class="fa fa-tachometer margin-r-5"></i> ¿Cómo deseas validar una visita de checklist?</strong>
            <hr>
            <div class="radioVal">

            </div>


            <hr>

            <button onclick="guardaConfig()"  class="btn btn-primary btn-block margin-bottom">Guardar</button>
        </div>
        <!-- /.box-body -->
    </div>

    <div class="listadoVisitas" >

    </div>
@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link href="{{ asset('dist/dropzone.css') }}" rel="stylesheet">

    <style>
        .badge-important {

        }
        .image-upload > input
        {
            display: none;
        }

    </style>

    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }

    </style>

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

@stop

@section('js')

    <script src="{{ asset('bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('dist/timer.jquery.min.js')}}"></script>
    <script>

        $(document).ready(function() {

           // alert('{{$configuracion->valida_vis}}')

           var sinVal = "";
           var valPass = "";
           var valCode = "";
           var valMap = "";
           var valida_vis ='{{$configuracion->valida_vis}}';

            switch(valida_vis)
            {
                case "0": case 0:
                    sinVal = "checked";
                    break;
                case 1: case "1":
                    valPass = "checked";
                    break;
                case 2: case "2":
                    valCode = "checked";
                    break;
                case 3: case "3":
                    valMap = "checked";
                    break;

            }

            var text = '<div class="form-group">\n' +
                '                    <label class="">\n' +
                '                        <input  type="radio" '+sinVal+'  value="0" name="r1" class="iradio_flat-green sinValidar">\n' +
                '                        Sin validación al terminar visita\n' +
                '                    </label>\n' +
                '                    <hr>\n' +
                '                    <label class="">\n' +
                '                        <input  type="radio"  '+valPass+'  value="1" name="r1" class="iradio_flat-green valPass">\n' +
                '                        Validar por contraseña de tienda\n' +
                '                    </label>\n' +
                '                    <hr>\n' +
                '                    <label class="">\n' +
                '                        <input  type="radio"  '+valCode+'  value="2" name="r1" class="iradio_flat-green valCode">\n' +
                '                        Validar por escaneo de código\n' +
                '                    </label>\n' +
                '                    <hr>\n' +
                '                    <label class="">\n' +
                '                        <input  type="radio"  '+valMap+'  value="3" name="r1" class="iradio_flat-green valMap">\n' +
                '                        Validar por geolocalización\n' +
                '                    </label>\n' +
                '\n' +
                '                </div>';

            $('.radioVal').html(text);

        } );
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })


        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            })

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            });



        });

        function guardaConfig()
        {
          // alert( $("input:checked" ).val())
            var url = '{{route('setConfig')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {valida_vis: $("input:checked" ).val()}
            })
                .done(function(msg){

                   bootbox.alert('¡Configuración guardada con éxito!')

                } );


        }



    </script>

@stop