<?php ini_set('memory_limit', '-1'); ?>
<html>
<head>
    <style>
        body {
            font-family: "Helvetica Neue", Helvetica, Arial;
            font-size: 14px;
            line-height: 20px;
            font-weight: 400;
            color: #fff;
            -webkit-font-smoothing: antialiased;
            font-smoothing: antialiased;
            background: #fff;
        }
        @media screen and (max-width: 580px) {
            body {
                font-size: 16px;
                line-height: 22px;
            }
        }

        .wrapper {
            margin: 0 auto;
            padding: 40px;
            max-width: 800px;
        }

        table {
            border-spacing: 1;
            border-collapse: collapse;
            background: white;
            border-radius: 6px;
            overflow: hidden;
            max-width: 800px;
            width: 100%;
            margin: 0 auto;
            position: relative;
        }
        table * {
            position: relative;
        }
        table td, table th {
            padding-left: 8px;
        }
        table thead tr {
            height: 60px;
            background: #5385ff;
            font-size: 16px;
        }
        table tbody tr {
            height: 48px;
            border-bottom: 1px solid #E3F1D5;
        }
        table tbody tr:last-child {
            border: 0;
        }
        table td, table th {
            text-align: left;
        }
        table td.l, table th.l {
            text-align: right;
        }
        table td.c, table th.c {
            text-align: center;
        }
        table td.r, table th.r {
            text-align: center;
        }

        @media screen and (max-width: 35.5em) {
            table {
                display: block;
            }
            table > *, table tr, table td, table th {
                display: block;
            }
            table thead {
                display: none;
            }
            table tbody tr {
                height: auto;
                padding: 8px 0;
            }
            table tbody tr td {
                padding-left: 10%;
                margin-bottom: 12px;
            }
            table tbody tr td:last-child {
                margin-bottom: 0;
            }
            table tbody tr td:before {
                position: absolute;
                font-weight: 700;
                width: 40%;
                left: 10px;
                top: 0;
            }
            table tbody tr td:nth-child(1):before {
                content: "Code";
            }
            table tbody tr td:nth-child(2):before {
                content: "Stock";
            }
            table tbody tr td:nth-child(3):before {
                content: "Cap";
            }
            table tbody tr td:nth-child(4):before {
                content: "Inch";
            }
            table tbody tr td:nth-child(5):before {
                content: "Box Type";
            }
        }


        blockquote {
            color: white;
            text-align: center;
        }




    </style>

<body>



<div style="width: 100%; height: 7%; background-color: #FFFFFF;">
    <img src="http://ventumsupervision.com/uploads/Empresas/{{$data['logo']}}" style="float: left" width="150px" height="60px"  >
    <img src="http://ventumsupervision.com/images/logo_ventum.png" style="float: right" width="150px" height="60px"  >
</div>
</br>
<hr>
<div style="width: 100%; height: 4%;  text-align: center;background-color: #008ae6; border: 4px solid #ffffff; padding-top: 20px; border-radius: 15px;">

    <span style="color: #FFFFFF; font-size: x-large; padding-top: 50px; padding-left: 10px;"> Reporte de  Evaluación de Desempeño del {{date('d/m/Y', strtotime($data['fecha_ini']))}} al {{date('d/m/Y', strtotime($data['fecha_fin']))}} </span>

</div>


<table style="border-bottom: 4px solid #2e3a78" width="100%">
    <thead>
    <tr>
        <th style="text-align: center; background-color: #3d518f"  width="5%">Clave</th>
        <th style="text-align: center; background-color: #3d518f" width="15%">Nombre</th>
        <th style="text-align: center; background-color: #3d518f" width="15%">Puesto</th>
        <th style="text-align: center; background-color: #3d518f" width="15%">Razón</th>
        <th style="text-align: center; background-color: #3d518f" width="15%">Plaza</th>
        <th style="text-align: center; background-color: #3d518f" width="15%">Sucursales</th>
        <th style="text-align: center; background-color: #3d518f" width="15%">Checklist</th>
        <th style="text-align: center; background-color: #3d518f" width="15%">Seguimiento</th>
        <th style="text-align: center; background-color: #3d518f" width="15%">Anteriores</th>
        <th style="text-align: center; background-color: #3d518f" width="15%">Creados</th>
        <th style="text-align: center; background-color: #3d518f" width="15%">Nuevos</th>
        <th style="text-align: center; background-color: #3d518f" width="15%">En curso</th>
        <th style="text-align: center; background-color: #3d518f" width="15%">Pendientes</th>
        <th style="text-align: center; background-color: #3d518f" width="15%">Concluidos</th>
        <th style="text-align: center; background-color: #3d518f" width="15%">Efectividad</th>

    </tr>

    <thead>
    <tbody>
    @foreach($data['supervisores'] as $sup)
        <tr>
            <td  style="text-align: center;color: #0b3e6f;border-style: solid; border-bottom: #9d9d9d">{{$sup['clave']}}</td>
            <td  style="text-align: center;color: #0b3e6f;border-style: solid; border-bottom: #9d9d9d"   >{{$sup['nombre']}}</td>
            <td  style="text-align: center;color: #0b3e6f;border-style: solid; border-bottom: #9d9d9d"   >{{$sup['puesto']}}</td>
            <td  style="text-align: center;color: #0b3e6f;border-style: solid; border-bottom: #9d9d9d"   >{{$sup['razon']}}</td>
            <td  style="text-align: center;color: #0b3e6f;border-style: solid; border-bottom: #9d9d9d"   >{{$sup['plaza']}}</td>
            <td  style="text-align: center;color: #0b3e6f;border-style: solid; border-bottom: #9d9d9d"   >{{$sup['tiendas']}}</td>
            <td  style="text-align: center;color: #0b3e6f;border-style: solid; border-bottom: #9d9d9d"   >{{$sup['checklist']}}</td>
            <td  style="text-align: center;color: #0b3e6f;border-style: solid; border-bottom: #9d9d9d"   >{{$sup['seguimientos']}}</td>
            <td  style="text-align: center;color: #0b3e6f;border-style: solid; border-bottom: #9d9d9d"  >{{$sup['anteriores']}}</td>
            <td  style="text-align: center;color: #0b3e6f;border-style: solid; border-bottom: #9d9d9d"  >{{$sup['creados']}}</td>
            <td  style="text-align: center;color: #0b3e6f;border-style: solid; border-bottom: #9d9d9d"   >{{$sup['nuevos']}}</td>
            <td  style="text-align: center;color: #0b3e6f;border-style: solid; border-bottom: #9d9d9d"   >{{$sup['curso']}}</td>
            <td  style="text-align: center;color: #0b3e6f;border-style: solid; border-bottom: #9d9d9d"  >{{$sup['pendientes']}}</td>
            <td  style="text-align: center;color: #0b3e6f;border-style: solid; border-bottom: #9d9d9d"   >{{$sup['concluidos']}}</td>
            <td  style="text-align: center;color: #0b3e6f;border-style: solid; border-bottom: #9d9d9d"  >{{$sup['efectividad']}}%</td>
        </tr>
    @endforeach

    </tbody>
    <table/>



</body>
</html>