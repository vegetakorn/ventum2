@extends('adminlte::page')
@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>Listado de visitas a sucursales</h1>
@stop

@section('content')


    <div id="filtro" class="box box-warning box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Buscar Visitas</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <label >Selecciona razón:</label>

            <select onchange="getPlazas()" class="form-control select2" id="raz" name="razon" style="width: 100%;">
                <option value="0" selected="selected"><--Todas las razones--></option>
                @foreach($razones as $raz)
                    <option value="{{$raz->Id}}"> {{$raz->nombre}}</option>

                @endforeach

            </select>
            <div class="plazas">
                <input type="hidden" value="0" id="plaza" >
            </div>
            <div class="tiendas">
                <input type="hidden" value="0" id="suc" >
            </div>
            <hr>
            <strong><i class="fa fa-tachometer margin-r-5"></i> Busca supervisor</strong>
            <select class="form-control select2" id="sup" name="supervisor" style="width: 100%;">
                <option value="0" selected="selected">--Todos--</option>
                @foreach($supervisores as $puesto)
                    <option value="{{$puesto->supervisor_Id}}" >{{$puesto->nombre}} {{$puesto->apepat}}  {{$puesto->apemat}} </option>
                @endforeach
            </select>
            <hr>
                <strong><i class="fa fa-tachometer margin-r-5"></i> Por Tipo</strong>
                <div class="form-group">
                    <label class="">
                        <input type="checkbox" id="boxSup" class="minimal" checked="" style="position: absolute; opacity: 0;">
                        Visita de Supervisión
                    </label>
                    <label class="">

                    </label>
                    <label class="">
                        <input  type="checkbox" id="boxSeg" class="minimal" checked="" style="position: absolute; opacity: 0;">
                        Visita de Seguimiento
                    </label>

                </div>

            <hr>
                <strong><i class="fa fa-tachometer margin-r-5"></i> Por Fecha</strong>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" name="rango" id="reservation">
                    </div>
                    <!-- /.input group -->
                </div>
            <hr>
            <button onclick="filtraTodo()"  class="btn btn-warning btn-block margin-bottom">Buscar</button>
        </div>
        <!-- /.box-body -->
    </div>

    <div class="listadoVisitas" >

    </div>
@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link href="{{ asset('dist/dropzone.css') }}" rel="stylesheet">

    <style>
        .badge-important {

        }
        .image-upload > input
        {
            display: none;
        }

    </style>

    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }

    </style>

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

@stop

@section('js')

    <script src="{{ asset('bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('dist/timer.jquery.min.js')}}"></script>
    <script>
        function filtraTodo()
        {
            bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Generando información...</div>' })
            //supervisor seleccionada
            var sup = document.getElementById('sup').value;

            if(sup != "")
            {
                var tipoSup = 0;
                var tipoSeg = 0;

                //tipo de todo
                if(document.getElementById('boxSup').checked) {
                    tipoSup = 1;
                }
                if(document.getElementById('boxSeg').checked) {
                    tipoSeg = 1;
                }
                //tienda seleccionada
                var razon = document.getElementById('raz').value;
                var plaza = document.getElementById('plaza').value;
                var suc = document.getElementById('suc').value;



                //por fecha
                var fecha = document.getElementsByName('rango')[0].value;


                var url = '{{route('getVisitas')}}';

                // alert('Muestra fotos')

                $.ajax({
                    method: 'POST',
                    url: url,
                    data: { sup: tipoSup,seg: tipoSeg, fecha:fecha, tienda: suc, supervisor:sup, razon: razon, plaza:plaza}
                })
                    .done(function(msg){

                        bootbox.hideAll()

                        var fechas = setFechaPdf(document.getElementsByName('rango')[0].value);
                        var fecha_ini = setFechaFormat(fechas[0]);
                        var fecha_fin = setFechaFormat(fechas[1]);
                        var urlpdf = route('listaVisitas', [tipoSup, tipoSeg, fecha_ini, fecha_fin, suc, sup, razon, plaza]);
                        var urlexcel = route('listaVisitasXls', [tipoSup, tipoSeg, fecha_ini, fecha_fin, suc, sup, razon, plaza]);
                        console.log(msg['consulta']);
                        $('#filtro').boxWidget('collapse');
                        var text = '<div class="box">\n' +
                            '        <div class="box-header">\n' +
                            '            <h3 class="box-title">Visitas Encontradas</h3>\n' +
                            ' <a href="'+urlpdf+'" class="btn btn-social-icon btn-danger pull-right"><i class="fa fa-file-pdf-o"></i></a>'+
                            ' <a href="'+urlexcel+'" class="btn btn-social-icon btn-success pull-right"><i class="fa fa-file-excel-o"></i></a>'+
                            '        </div>\n' +
                            '        <!-- /.box-header -->\n' +
                            '        <div class="box-body no-padding">\n' +
                            '            <table class="table table-striped">\n' +
                            '                <tbody><tr>\n' +
                            '                    <th style="width: 10px">#</th>\n' +
                            '                    <th style="width: 80px">Sucursal</th>\n' +
                            '                    <th style="width: 80px">Supervisor</th>\n' +
                            '                    <th style="width: 80px">Fecha</th>\n' +
                            '                    <th style="width: 80px">Duración</th>\n' +
                            '                    <th style="width: 80px">Checklist</th>\n' +
                            '                    <th style="width: 20px">Reporte</th>\n' +
                            '                    <th style="width: 40px">Evaluación</th>\n' +
                            '                </tr>\n' ;



                        for(var i = 0; i < msg['visitas'].length; i++)
                        {
                            var fecha = msg['visitas'][i]['HoraInicio'];
                            var dt = new Date(fecha);
                            var dias = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado' ];
                            var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' ];
                            var Dia = dias[dt.getDay()];
                            var Mes = meses[dt.getMonth()];
                            var Anio = dt.getFullYear();
                            var Hora = dt.getUTCHours();
                            var Minuto = dt.getUTCMinutes();
                            var checklist = "";
                            var evaluacion = "";
                            var url = '{{url('uploads/Reportes/')}}';
                            var url_reporte = url+'/'+msg['visitas'][i]['Archivo'];
                            if(msg['visitas'][i]['tipo_visita'] == 2)
                            {
                                checklist = 'Seguimiento';
                                evaluacion = '<td><span class="badge bg-light-blue">N/A</span></td>\n';
                            }else
                            {
                                checklist = msg['visitas'][i]['Checklist'];
                                if(msg['visitas'][i]['Calif'] > 0 && msg['visitas'][i]['Calif'] <= 80)
                                {
                                    evaluacion = '<td><span class="badge bg-red">'+msg['visitas'][i]['Calif']+'%</span></td>\n';
                                }else  if(msg['visitas'][i]['Calif'] > 80.1 && msg['visitas'][i]['Calif'] <= 90)
                                {
                                    evaluacion = '<td><span class="badge bg-yellow">'+msg['visitas'][i]['Calif']+'%</span></td>\n';
                                }else  if(msg['visitas'][i]['Calif'] > 90.1 && msg['visitas'][i]['Calif'] <= 100)
                                {
                                    evaluacion = '<td><span class="badge bg-green">'+msg['visitas'][i]['Calif']+'%</span></td>\n';
                                }

                            }
                            var reporte = ' <td>Histórico</td>\n';
                            if(msg['visitas'][i]['Archivo'] != "H")
                            {
                                reporte = ' <td><a href="'+url_reporte+'" target="_blank" type="button" class="btn btn-block btn-primary btn-xs">Descargar</a></td>\n';
                            }

                            text +=    '                    <tr>\n' +
                                '                        <td>'+msg['visitas'][i]['numsuc']+'</td>\n' +
                                '                        <td>'+msg['visitas'][i]['nombre']+'</td>\n' +
                                '                        <td>'+msg['visitas'][i]['name']+'</td>\n' +
                                '                        <td>'+fecha+'</td>\n' +
                                '                        <td>'+msg['visitas'][i]['Duracion']+'</td>\n' +
                                '                        <td>\n' +
                                '                            '+checklist+'\n' +
                                '                        </td>\n' +
                                '             '+reporte+'          ' +
                                '                   '+evaluacion+'     ' +
                                '                    </tr>\n' ;
                        }

                        text +=   '\n' +
                            '\n' +
                            '                </tbody></table>\n' +
                            '        </div>\n' +
                            '        <!-- /.box-body -->\n' +
                            '    </div>';

                        $('.listadoVisitas').html( text );

                        //document.getElementById("dataTodo").scrollIntoView();
                    });
            }else
            {
                bootbox.alert("Este supervisor no tiene usuario asignado en el sistema, no se puede consultar sus visitas")
            }


        }

        function setFechaPdf(fecha) {
            var str = fecha;
            var res = str.split("-");
            return res;
        }

        function setFechaFormat(fecha) {
            var str = fecha;
            var res = str.split("/");
            return res[0]+'-'+res[1]+'-'+res[2];
        }

        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();

            $('#example1').DataTable( {
                "pagingType": "full_numbers",
                "scrollY":        '60vh',
                "scrollCollapse": true,
                "language": {
                    "lengthMenu": "Mostrando _MENU_ registros por página ",
                    "zeroRecords": "Sin registros encontrados",
                    "info": "Mostrando página  _PAGE_ de _PAGES_ páginas",
                    "infoEmpty": "Sin regustros encontrados",
                    "infoFiltered": "(filtrados de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    }
                }
            } );
            //Date range picker
            $('#reservation').daterangepicker({
                "startDate": moment().add(-90, 'day'),
                "endDate": moment().add(1, 'day'),
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Aplicar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "De",
                    "toLabel": "a",
                    "customRangeLabel": "Custom",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Septiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                }
            })
        } );
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })


        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            })

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            });



        });

        function getPlazas()
        {
            // alert(document.getElementById('razon').value) plazas capturadas
            //vamos a obtener el listado de cosas para asignar al empleado
            var url = '{{route('cargaPlazas')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  document.getElementById('raz').value  }
            })
                .done(function(msg){


                    var text = "";

                    text += '<b>Selecciona una plaza:</b> <select onchange="getTiendas()" class="form-control select2" id="plaza" name="plaza" style="width: 100%;">\n' +
                        '<option value="0" selected><--Todas las plazas--> </option>' ;


                    for(var i = 0; i < msg['plazas'].length; i++)
                    {
                        text += ' <option value="'+msg['plazas'][i]['Id']+'">'+msg['plazas'][i]['plaza']+'</option>\n';
                    }

                    text +=    '                           </select>';
                    $('.plazas').html( text )


                });

        }

        function getTiendas()
        {
            // alert(document.getElementById('razon').value) plazas capturadas
            //vamos a obtener el listado de cosas para asignar al empleado
            var url = '{{route('cargaTiendas')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  document.getElementById('plaza').value  }
            })
                .done(function(msg){

                    console.log(msg['tiendas']);
                    var text = "";

                    text += '<b>Selecciona la sucursal:</b> <select  class="form-control select2" id="suc" name="suc" style="width: 100%;">\n' +
                        '<option value="0" selected><--Todas las sucursales--> </option>' ;


                    for(var i = 0; i < msg['tiendas'].length; i++)
                    {
                        text += ' <option value="'+msg['tiendas'][i]['Id']+'">'+msg['tiendas'][i]['nombre']+'</option>\n';
                    }

                    text +=    '                           </select>';
                    $('.tiendas').html( text )


                });

        }



    </script>

@stop