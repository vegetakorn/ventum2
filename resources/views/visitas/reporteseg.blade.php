<?php ini_set('memory_limit', '-1'); ?>
<html>
<head>
    <style>
        body {
            font-family: "Helvetica Neue", Helvetica, Arial;
            font-size: 14px;
            line-height: 20px;
            font-weight: 400;
            color: #fff;
            -webkit-font-smoothing: antialiased;
            font-smoothing: antialiased;
            background: #fff;
        }
        @media screen and (max-width: 580px) {
            body {
                font-size: 16px;
                line-height: 22px;
            }
        }

        .wrapper {
            margin: 0 auto;
            padding: 40px;
            max-width: 800px;
        }

        table {
            border-spacing: 1;
            border-collapse: collapse;
            background: white;
            border-radius: 6px;
            overflow: hidden;
            max-width: 800px;
            width: 100%;
            margin: 0 auto;
            position: relative;
        }
        table * {
            position: relative;
        }
        table td, table th {
            padding-left: 8px;
        }
        table thead tr {
            height: 60px;
            background: #5385ff;
            font-size: 16px;
        }
        table tbody tr {
            height: 48px;
            border-bottom: 1px solid #E3F1D5;
        }
        table tbody tr:last-child {
            border: 0;
        }
        table td, table th {
            text-align: left;
        }
        table td.l, table th.l {
            text-align: right;
        }
        table td.c, table th.c {
            text-align: center;
        }
        table td.r, table th.r {
            text-align: center;
        }

        @media screen and (max-width: 35.5em) {
            table {
                display: block;
            }
            table > *, table tr, table td, table th {
                display: block;
            }
            table thead {
                display: none;
            }
            table tbody tr {
                height: auto;
                padding: 8px 0;
            }
            table tbody tr td {
                padding-left: 45%;
                margin-bottom: 12px;
            }
            table tbody tr td:last-child {
                margin-bottom: 0;
            }
            table tbody tr td:before {
                position: absolute;
                font-weight: 700;
                width: 40%;
                left: 10px;
                top: 0;
            }
            table tbody tr td:nth-child(1):before {
                content: "Code";
            }
            table tbody tr td:nth-child(2):before {
                content: "Stock";
            }
            table tbody tr td:nth-child(3):before {
                content: "Cap";
            }
            table tbody tr td:nth-child(4):before {
                content: "Inch";
            }
            table tbody tr td:nth-child(5):before {
                content: "Box Type";
            }
        }


        blockquote {
            color: white;
            text-align: center;
        }





    </style>

<body>



<div style="width: 100%; height: 7%; background-color: #FFFFFF;">
    <img src="http://ventumsupervision.com/uploads/Razones/{{$data['visita']->razonFoto}}" style="float: left" width="150px" height="60px"  >
    <img src="http://ventumsupervision.com/images/logo_ventum.png" style="float: right" width="150px" height="60px"  >
</div>
<div style="width: 100%; height: 4%;  text-align: center;background-color: #008ae6; border: 4px solid #ffffff; padding-top: 20px; border-radius: 15px;">

    <span style="color: #FFFFFF; font-size: x-large; padding-top: 50px; padding-left: 10px;">Reporte de seguimiento de la tienda {{$data['visita']->numsuc}} {{$data['visita']->tienda}}</span>

</div>

<div style="width: 100%; height: 5%">
    <div style="width: 50%; text-align: center; border-radius: 15px; border: 1px solid #fff;
     background-color: #fff; float: left; color: #0b58a2">
        Fecha : <span style="color: #535353;"><?php echo date('d/m/Y' , strtotime($data['visita']->HoraFin)) ?></span>
    </div>
    <div style="width: 50%; text-align: center; border-radius: 15px; border: 1px solid #fff;
     background-color: #fff; float: left; color: #0b58a2">
        Supervisor : <span style="color: #535353;">{{$data['visita']->name}}</span>
    </div>

</div>

<div style="width: 100%; height: 20%; color: #0b58a2; text-align: center;background-color: #fff; border: 8px solid #ffffff;  border-radius: 15px;">
    Visita realizada en <span style="color: #1e1b1d">{{$data['visita']->Duracion}}</span> con la siguiente ubicación:
    <p><img  style="border: 1px solid #0b58a2;border-radius: 10px;" src="https://maps.google.com/maps/api/staticmap?center={{$data['visita']->lat}},{{$data['visita']->lon}}&amp;zoom=19&amp;size=640x150&markers=color:blue%7Clabel:V%7C19.4647721,-99.0588265"  /></p>

</div>
<table style="border-bottom: 4px solid #0b3e6f">
    <thead>
    <tr>
        <th colspan="2" style="text-align: center">Resultado General</th>

    </tr>
    <tr>
        <th width="70%"></th>
        <th width="15%"></th>
    </tr>

    <thead>
    <tbody>
    <tr>
        <td style="color: #535353" width="80%" >To-Do's Concluidos</td>
        <td style="color: #535353" width="20%" >{{$data['todo_fin']}}</td>
    </tr>
    <tr>
        <td style="color: #535353" width="80%" >To-Do's Nuevos</td>
        <td style="color: #535353" width="20%" >{{$data['todo_new']}}</td>
    </tr>
    <tr>
        <td style="color: #535353" width="80%" >To-Do's en Curso</td>
        <td style="color: #535353" width="20%" >{{$data['todo_cur']}}</td>
    </tr>
    <tr>

        <td style="color: #535353" width="80%" >To-Do's Pendientes</td>
        <td style="color: #535353" width="20%" >{{$data['todo_pend']}}</td>
    </tr>
    <tr>
        <td style="color: #0b3e6f" colspan="2"></td>


    </tr>
    </tbody>
    <table/>
    <table style="border-bottom: 4px solid #6f150f">
        <thead>
        <tr>
            <th  style="text-align: center; background-color: #eb5647" colspan="2">To-Do's Gestionados</th>

        </tr>

        <thead>
        <tbody>
        @foreach($data['todos'] as $todo )
            @if($todo->tipo  == 1)
                <td width="15%">

                    <img style="border: 1px solid #a2191c;border-radius: 10px;" class="attachment-img" src="http://ventumsupervision.com/uploads/FotoTodo/{{$todo->ImagenIni}}" width="150px" height="100px" alt="Attachment Image">
                </td>
                <td style="color: #535353" width="80%" >
                    {{$todo->nombre}}
                    <br> <span style="font-size: xx-small; color: #0b3e6f">Familia: {{$todo->familia}}</span>
                    <br> <span style="font-size: x-small; color: #475f69">Comentario: {{$todo->descripcion}}  </span>
                </td>


                </tr>
            @else
                <td width="15%">

                    <img style="border: 1px solid #a2191c;border-radius: 10px;" class="attachment-img" src="http://ventumsupervision.com/uploads/FotoTodo/{{$todo->ImagenIni}}" width="150px" height="100px" alt="Attachment Image">
                </td>
                <td style="color: #535353" width="80%" >
                    To-Do Express
                    <br> <span style="font-size: xx-small; color: #0b3e6f">Familia: To-Do Express</span>
                    <br> <span style="font-size: x-small; color: #475f69">Comentario: {{$todo->descripcion}}  </span>
                </td>


                </tr>
            @endif
            <tr>


        @endforeach
        <tr>
            <td style="color: #0b3e6f" colspan="2"></td>


        </tr>
        </tbody>
        <table/>

        @if (count($data['cuestionario']) > 0)
            <table style="border-bottom: 4px solid #0b3e6f">
                <thead>
                <tr>
                    <th colspan="2" style="text-align: center">Cuestionario de la Visita</th>
                </tr>
                <tr>
                    <th width="50%" style="text-align: center">Pregunta</th>
                    <th width="50%" style="text-align: center">Respuesta</th>
                </tr>
                <thead>
                <tbody>
                @foreach($data['cuestionario'] as $cuest )
                    <tr>
                        <td style="color: #0b3e6f">{{$cuest->Pregunta}}</td>


                        @if($cuest->tipo == 1)
                            <td style="color: #0b3e6f">{{$cuest->respuesta}}</td>
                        @else
                            <td style="color: #0b3e6f">{{$cuest->Abierta}}</td>
                        @endif

                        @endforeach
                    </tr>
                    <tr>
                        <td style="color: #0b3e6f" colspan="2"></td>
                    </tr>
                </tbody>
                <table/>
                @endif

        @if (count($data['fotos']) > 0)
            <table style="border-bottom: 4px solid #3d518f">
                <thead>
                <tr>
                    <th style="text-align: center; background-color: #7496b5" colspan="2">Fotos de la Visita</th>

                </tr>

                <thead>
                <tbody>

                <tr>
                    <th colspan="2" style="padding-bottom: 50px"></th>

                </tr>
                <tr>
                    <td style="color: #7496b5" colspan="2">
                        <div class="float-left">
                            @foreach($data['fotos'] as $foto )

                                <img style="border: 1px solid #2e3a78;border-radius: 10px;"  src="http://ventumsupervision.com/uploads/FotoVisita/{{$foto->Foto}}" width="150px" height="100px" alt="Attachment Image">


                            @endforeach
                        </div>

                    </td>
                </tr>
                <tr>
                    <td style="color: #7496b5" colspan="2"></td>


                </tr>
                </tbody>
                <table/>
    @endif




</body>
</html>