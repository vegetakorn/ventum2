@extends('adminlte::page')
@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>Ventas diarias por rango de fechas</h1>
@stop

@section('content')


    <div id="filtro" class="box box-warning box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Buscar ventas</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <label >Selecciona razón:</label>

            <select onchange="getPlazas()" class="form-control select2" id="raz" name="razon" style="width: 100%;">
                <option value="0" selected="selected"><--Todas las razones--></option>
                @foreach($razones as $raz)
                    <option value="{{$raz->Id}}"> {{$raz->nombre}}</option>

                @endforeach

            </select>
            <div class="plazas">
                <input type="hidden" value="0" id="plaza" >
            </div>
            <div class="tiendas">
                <input type="hidden" value="0" id="suc" >
            </div>


            <hr>
            <strong><i class="fa fa-tachometer margin-r-5"></i> Por Fecha</strong>

            <div class="form-group">


                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input onchange="setButton()" type="text" class="form-control pull-right" name="rango" id="reservation">
                </div>
                <!-- /.input group -->
            </div>

            <hr>
            <div class="btnVer">


            </div>
        </div>
        <!-- /.box-body -->
    </div>




@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link href="{{ asset('dist/dropzone.css') }}" rel="stylesheet">

    <style>
        .badge-important {

        }
        .image-upload > input
        {
            display: none;
        }

    </style>

    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }

    </style>

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

@stop

@section('js')

    <script src="{{ asset('bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('dist/timer.jquery.min.js')}}"></script>
    <script>


        $(document).ready(function() {



            $('[data-toggle="tooltip"]').tooltip();

            //Date range picker
            $('#reservation').daterangepicker({
                "startDate": moment().startOf('month'),
                "endDate": moment().add(1, 'day'),
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Aplicar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "De",
                    "toLabel": "a",
                    "customRangeLabel": "Custom",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Septiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                }
            })

            setFechaIni();
        } );
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })


        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            })

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            });



        });
        function setFechaIni()
        {
               var fechas = setFechaPdf(document.getElementsByName('rango')[0].value);
            var fecha_ini = setFechaFormat(fechas[0]);
             var fecha_fin = setFechaFormat(fechas[1]);

             var url = route('exportVentaRango', {razon: document.getElementById('raz').value,
                 plaza: document.getElementById('plaza').value,
                 fecha_ini: fecha_ini,
                 fecha_fin:fecha_fin,
                 suc:document.getElementById('suc').value});

            $('.btnVer').html('<a href="'+url+'" class="btn btn-block btn-primary ">Genera Reporte</a>')
        }
        function getPlazas()
        {
            // alert(document.getElementById('razon').value) plazas capturadas
            //vamos a obtener el listado de cosas para asignar al empleado
            var fechas = setFechaPdf(document.getElementsByName('rango')[0].value);
            var fecha_ini = setFechaFormat(fechas[0]);
            var fecha_fin = setFechaFormat(fechas[1]);

            var url = route('exportVentaRango', {razon: document.getElementById('raz').value,
                plaza: document.getElementById('plaza').value,
                fecha_ini: fecha_ini,
                fecha_fin:fecha_fin,
                suc:document.getElementById('suc').value});

            $('.btnVer').html('<a href="'+url+'" class="btn btn-block btn-primary ">Genera Reporte</a>')

            var url = '{{route('cargaPlazas')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  document.getElementById('raz').value  }
            })
                .done(function(msg){


                    var text = "";

                    text += '<b>Selecciona una plaza:</b> <select onchange="getTiendas()" class="form-control select2" id="plaza" name="plaza" style="width: 100%;">\n' +
                        '<option value="0" selected><--Todas las plazas--> </option>' ;


                    for(var i = 0; i < msg['plazas'].length; i++)
                    {
                        text += ' <option value="'+msg['plazas'][i]['Id']+'">'+msg['plazas'][i]['plaza']+'</option>\n';
                    }

                    text +=    '                           </select>';
                    $('.plazas').html( text )


                });

        }

        function getTiendas()
        {
            // alert(document.getElementById('razon').value) plazas capturadas
            //vamos a obtener el listado de cosas para asignar al empleado
            var fechas = setFechaPdf(document.getElementsByName('rango')[0].value);
            var fecha_ini = setFechaFormat(fechas[0]);
            var fecha_fin = setFechaFormat(fechas[1]);

            var url = route('exportVentaRango', {razon: document.getElementById('raz').value,
                plaza: document.getElementById('plaza').value,
                fecha_ini: fecha_ini,
                fecha_fin:fecha_fin,
                suc:document.getElementById('suc').value});

            $('.btnVer').html('<a href="'+url+'" class="btn btn-block btn-primary ">Genera Reporte</a>')
            var url = '{{route('cargaTiendas')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  document.getElementById('plaza').value  }
            })
                .done(function(msg){

                    console.log(msg['tiendas']);
                    var text = "";

                    text += '<b>Selecciona la sucursal:</b> <select onchange="setButton()"  class="form-control select2" id="suc" name="suc" style="width: 100%;">\n' +
                        '<option value="0" selected><--Todas las sucursales--> </option>' ;


                    for(var i = 0; i < msg['tiendas'].length; i++)
                    {
                        text += ' <option value="'+msg['tiendas'][i]['Id']+'">'+msg['tiendas'][i]['nombre']+'</option>\n';
                    }

                    text +=    '                           </select>';
                    $('.tiendas').html( text )


                });

        }

        function setButton()
        {
            var fechas = setFechaPdf(document.getElementsByName('rango')[0].value);
            var fecha_ini = setFechaFormat(fechas[0]);
            var fecha_fin = setFechaFormat(fechas[1]);

            var url = route('exportVentaRango', {razon: document.getElementById('raz').value,
                plaza: document.getElementById('plaza').value,
                fecha_ini: fecha_ini,
                fecha_fin:fecha_fin,
                suc:document.getElementById('suc').value});

            $('.btnVer').html('<a href="'+url+'" class="btn btn-block btn-primary ">Genera Reporte</a>')
        }

        function setFechaPdf(fecha) {
            var str = fecha;
            var res = str.split("-");
            return res;
        }

        function setFechaFormat(fecha) {
            var str = fecha;
            var res = str.split("/");
            return res[0]+'-'+res[1]+'-'+res[2];
        }





    </script>

@stop