@extends('adminlte::page')
@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>Listado de to-dos de visitas</h1>
@stop

@section('content')


    <div id="filtro" class="box box-warning box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Filtrar To-Do's</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <label >Selecciona razón:</label>

            <select onchange="getPlazas()" class="form-control select2" id="raz" name="razon" style="width: 100%;">
                <option value="0" selected="selected"><--Todas las razones--></option>
                @foreach($razones as $raz)
                    <option value="{{$raz->Id}}"> {{$raz->nombre}}</option>

                @endforeach

            </select>
            <div class="plazas">
                <input type="hidden" value="0" id="plaza" >
            </div>
            <div class="tiendas">
                <input type="hidden" value="0" id="suc" >
            </div>
            <hr>
            <strong><i class="fa fa-tachometer margin-r-5"></i> Busca supervisor</strong>
            <select class="form-control select2" id="sup" name="supervisor" style="width: 100%;">
                <option value="0" selected="selected">--Todos--</option>
                @foreach($supervisores as $puesto)
                    <option value="{{$puesto->supervisor_Id}}" >{{$puesto->nombre}} {{$puesto->apepat}}  {{$puesto->apemat}}  </option>
                @endforeach
            </select>
            <hr>
            <strong><i class="fa fa-tachometer margin-r-5"></i> Por Checklist</strong>
            <select  onchange="buscaCat()" class="form-control select2" id="chk" name="checklist" style="width: 100%;">
                <option value="0" selected="selected">--Todos--</option>
                @foreach($checklist as $chk)
                    <option value="{{$chk->Id}}"> {{$chk->nombre}}</option>

                @endforeach

            </select>
            <hr>
            <div class="categoria-select" >
                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                <input type="hidden" id="cat" class="minimal" value="0" >

            </div>
            <strong><i class="fa fa-tachometer margin-r-5"></i> Por Tipo</strong>
            <div class="form-group">
                <label class="">
                    <input type="checkbox" id="boxCheck" class="minimal" checked="" style="position: absolute; opacity: 0;">
                    To-do de Checklist
                </label>
                <label class="">

                </label>
                <label class="">
                    <input  type="checkbox" id="boxExp" class="minimal" checked="" style="position: absolute; opacity: 0;">
                    To-do Express
                </label>
                <label class="">

                </label>
                <label class="">
                    <input  type="checkbox" id="boxFtp" class="minimal" checked="" style="position: absolute; opacity: 0;">
                    To-do por FTP
                </label>
                <label class="">

                </label>
                <label class="">
                    <input  type="checkbox" id="boxSup" class="minimal" checked="" style="position: absolute; opacity: 0;">
                    To-do a Supervisor
                </label>

            </div>

            <hr>
            <strong><i class="fa fa-tachometer margin-r-5"></i> Por Familia</strong>

            <select class="form-control select2" id="fam" name="familia" style="width: 100%;">
                <option value="0" selected="selected">--Todas--</option>
                @foreach($familias as $fam)
                    <option value="{{$fam->Id}}"> {{$fam->Nombre}}</option>

                @endforeach

            </select>
            <hr>
            <strong><i class="fa fa-tachometer margin-r-5"></i> Por Estatus</strong>
            <div class="form-group">
                <label class="">
                    <input type="checkbox" id="boxNew" class="minimal" checked="" style="position: absolute; opacity: 0;">
                    Nuevos
                </label>
                <label class="">

                </label>
                <label class="">
                    <input type="checkbox" id="boxCur"  class="minimal" checked="" style="position: absolute; opacity: 0;">
                    En Curso
                </label>
                <label class="">

                </label>
                <label class="">
                    <input type="checkbox" id="boxPend"  class="minimal" checked="" style="position: absolute; opacity: 0;">
                    Pendientes
                </label>

            </div>

            <hr>
            <strong><i class="fa fa-tachometer margin-r-5"></i> Por Fecha</strong>

            <div class="form-group">


                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" name="rango" id="reservation">
                </div>
                <!-- /.input group -->
            </div>

            <hr>
            <button onclick="filtraTodo()"  class="btn btn-warning btn-block margin-bottom">Buscar</button>
        </div>
        <!-- /.box-body -->
    </div>


    <!-- PRODUCT LIST -->
    <div id="todolist" class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Listado de To-Do's Encontrados</h3>

            <div class="box-tools pull-right botonPdf">

            </div>

        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <ul class="products-list product-list-in-box">
                <!-- /.item -->
                <div class="todo-list" >
                    <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->

                </div>

            </ul>

        </div>
        <!-- /.box-body -->
        <div class="box-footer text-center">

        </div>
        <!-- /.box-footer -->
    </div>
    <div id="editTodo">
        <!-- Widget: user widget style 1 -->
        <div class="box box-widget widget-user-2 infoTodo">

        </div>
        <!-- /.widget-user -->
    </div>

    <div class="planSmart" id="planSmart" >


    </div>



@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link href="{{ asset('dist/dropzone.css') }}" rel="stylesheet">

    <style>
        .badge-important {

        }
        .image-upload > input
        {
            display: none;
        }

    </style>

    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }

    </style>

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

@stop

@section('js')

    <script src="{{ asset('bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('dist/timer.jquery.min.js')}}"></script>
    <script>


        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();

            $('#example1').DataTable( {
                "pagingType": "full_numbers",
                "scrollY":        '60vh',
                "scrollCollapse": true,
                "language": {
                    "lengthMenu": "Mostrando _MENU_ registros por página ",
                    "zeroRecords": "Sin registros encontrados",
                    "info": "Mostrando página  _PAGE_ de _PAGES_ páginas",
                    "infoEmpty": "Sin regustros encontrados",
                    "infoFiltered": "(filtrados de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    }
                }
            } );
            //Date range picker
            $('#reservation').daterangepicker({
                "startDate": moment().add(-90, 'day'),
                "endDate": moment().add(1, 'day'),
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Aplicar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "De",
                    "toLabel": "a",
                    "customRangeLabel": "Custom",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Septiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                }
            })
        } );
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })


        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            })

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            });



        });

        function buscaCat()
        {
            var selChk = document.getElementsByName('checklist')[0].value;
            var url = '{{route('getCategorias')}}';

            $.ajax({
                method: 'POST',
                url: url,
                data: {checklist:selChk}
            })
                .done(function(msg){
                    console.log(msg['message']);
                    var text = "";
                    text += ' <strong><i class="fa fa-book margin-r-5"></i> Elige Categoría</strong>' +
                        '<select  class="form-control select2" id="cat" name="categorias-sel" style="width: 100%;">' +
                        '<option value="0" selected="selected">--Selecciona--</option>';
                    for(var i = 0; i < msg['message'].length; i++)
                    {
                        var cat = msg['message'][i]['nombre'];
                        var cat_id = msg['message'][i]['Id'];
                        text += ' <option value="'+cat_id+'">'+cat+'</option>';

                    }
                    text += '</select><hr>';

                    $('.categoria-select').html( text );

                });

            //alert(selChk)
        }

        function saveTodo(campo)
        {

            var coment = "comentario"+campo;
            var select = "stat"+campo;;
            var input_coment = document.getElementById(coment).value;
            var input_select = document.getElementById(select).value;
            var url = '{{route('updateTodo')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {campo_Id:campo, comentario: input_coment, status: input_select}
            })
                .done(function(msg){
                    console.log(msg['message']);

                    showDespues();
                });
        }

        function showDespues()
        {
            $("#todolist").show();
            $("#editTodo").hide();
            $("#filtro").show();
            filtraTodo()
        }

        function setTodoCom(cam)
        {
            var foto_com_id = "fotocomid"+cam;
            var com_value = "comvalue"+cam;
            var com  = document.getElementById(com_value).value;
            var foto  = document.getElementById(foto_com_id).value;
            var url = '{{route('putFotoCom')}}';
            if(com == "")
            {
                bootbox.alert("Debes escribir un comentario");
            }else
            {
                //bootbox.alert(foto);


                $.ajax({
                    method: 'POST',
                    url: url,
                    data: {campo_Id:cam, foto: foto, comentario: com}
                })
                    .done(function(msg){


                        console.log(msg['message']);
                        var text = "";
                        var comentarios = 'comentario'+cam;
                        var url_cam = '{{ asset('images/buttons/camera.png')}}';
                        var URLdomain = window.location.host;
                        var pathImg = 'http://'+URLdomain+'/uploads/FotoTodoCom/';

                        text += '<ul class="nav nav-stacked " style="padding-bottom: 20px">\n';
                        for(var i = 0; i < msg['message'].length; i++)
                        {
                            text +=  '<li><a href="#">'+msg['message'][i]['descripcion']+' <span class="pull-right "><img  width="20px" height="20px" src="'+pathImg+msg['message'][i]['foto']+'" ></span></a></li>';
                        }
                        text += '</ul>\n';

                        //text += '<li><a href="#">Comentario 1 <span class="pull-right "><img  width="20px" height="20px" src="'+url_cam+'" ></span></a></li>';
                        $('.setListCom').html( text );

                        document.getElementById(com_value).value = "";
                        document.getElementById(foto_com_id).value = "no_imagen.jpg";
                    });
            }
        }


        function setFotoCampoTodoCom(cam, vis)
        {

            var $avatarImage, $avatarInput, $avatarForm, $rutaFoto;


            $avatarImage = $('#avatarImageTc'+cam);
            $avatarInput = $('#avatarInputTc'+cam);
            $avatarForm = $('#avatarFormTc'+cam);
            $rutaFoto = $('.rutaFotoTc'+cam);
            var foto_com_id = "fotocomid"+cam;

            $avatarInput.click();
            $avatarInput.on('change', function () {
                var formData = new FormData();
                formData.append('photo', $avatarInput[0].files[0]);

                //$('#modal-info').modal('toggle');

                $.ajax({
                    url: $avatarForm.attr('action') + '?' + $avatarForm.serialize(),
                    method: $avatarForm.attr('method'),
                    data: formData,
                    processData: false,
                    contentType: false
                }).done(function (msg) {
                    // $('#modal-info').modal('toggle');
                    console.log(msg['message']);

                    document.getElementById(foto_com_id).value = msg['message'];


                });
            });

        }

        function getPlazas()
        {
            // alert(document.getElementById('razon').value) plazas capturadas
            //vamos a obtener el listado de cosas para asignar al empleado
            var url = '{{route('cargaPlazas')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  document.getElementById('raz').value  }
            })
                .done(function(msg){


                    var text = "";

                    text += '<b>Selecciona una plaza:</b> <select onchange="getTiendas()" class="form-control select2" id="plaza" name="plaza" style="width: 100%;">\n' +
                        '<option value="0" selected><--Todas las plazas--> </option>' ;


                    for(var i = 0; i < msg['plazas'].length; i++)
                    {
                        text += ' <option value="'+msg['plazas'][i]['Id']+'">'+msg['plazas'][i]['plaza']+'</option>\n';
                    }

                    text +=    '                           </select>';
                    $('.plazas').html( text )


                });

        }

        function getTiendas()
        {
            // alert(document.getElementById('razon').value) plazas capturadas
            //vamos a obtener el listado de cosas para asignar al empleado
            var url = '{{route('cargaTiendas')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  document.getElementById('plaza').value  }
            })
                .done(function(msg){

                    console.log(msg['tiendas']);
                    var text = "";

                    text += '<b>Selecciona la sucursal:</b> <select  class="form-control select2" id="suc" name="suc" style="width: 100%;">\n' +
                        '<option value="0" selected><--Todas las sucursales--> </option>' ;


                    for(var i = 0; i < msg['tiendas'].length; i++)
                    {
                        text += ' <option value="'+msg['tiendas'][i]['Id']+'">'+msg['tiendas'][i]['nombre']+'</option>\n';
                    }

                    text +=    '                           </select>';
                    $('.tiendas').html( text )


                });

        }
        function clearComentTodo(cam)
        {
            var comentario_Id = "comentario"+cam;
            document.getElementById(comentario_Id).value = "";
        }

        function setFotoCampoTodo(cam, vis)
        {

            var $avatarImage, $avatarInput, $avatarForm, $rutaFoto;


            $avatarImage = $('#avatarImageT'+cam);
            $avatarInput = $('#avatarInputT'+cam);
            $avatarForm = $('#avatarFormT'+cam);
            $rutaFoto = $('.rutaFotoT'+cam);
            $avatarInput.click();
            $avatarInput.on('change', function () {
                var formData = new FormData();
                formData.append('photo', $avatarInput[0].files[0]);

                $('#modal-info').modal('toggle');

                $.ajax({
                    url: $avatarForm.attr('action') + '?' + $avatarForm.serialize(),
                    method: $avatarForm.attr('method'),
                    data: formData,
                    processData: false,
                    contentType: false
                }).done(function (msg) {
                    // alert(cam);
                    /*if (data.success)
                        $avatarImage.attr('src', data.path);*/
                    var URLdomain = window.location.host;
                    var pathImg = 'http://'+URLdomain+'/'+msg['path'];

                    $avatarImage.attr('src', pathImg)
                    console.log(msg['message']);
                    $('#modal-info').modal('toggle');
                    //console.log(msg['path']);
                    //console.log(pathImg);
                    var url = '{{route('putFotoCampo')}}'
                    $.ajax({
                        method: 'POST',
                        url: url,
                        data: {campo_Id:cam, visita_Id: vis, foto: msg['message']}
                    })
                        .done(function(msg){
                            console.log(msg['message']);
                        });

                });
            });

        }

        function showInfoToDo(campo, campos_id, vis)
        {
            $("#todolist").hide();
            $("#filtro").hide();
            $("#editTodo").show();

            var text = "";
            var URLdomain = window.location.host;
            var pathImg = 'http://'+URLdomain+'/uploads/FotoTodo/';

            var url_cam = '{{ asset('images/buttons/camera.png')}}';

            url = '{{route('getInfoTodo')}}';

            $.ajax({
                method: 'POST',
                url: url,
                data: {campo_Id:campo}
            })
                .done(function(msg){
                    console.log(msg['message']);

                    var status_id = msg['message']['Status'];
                    var status_name = "";
                    switch(status_id)
                    {
                        case 151:
                            status_name = "Nuevo";
                            break;
                        case 152:
                            status_name = "En curso";
                            break;
                        case 153:
                            status_name = "Pendiente";
                            break;
                        case 154:
                            status_name = "Concluida";
                            break;
                    }
                    var url_fot = '{{route('regFotoCampo')}}';
                    var url_fot_com = '{{route('regFotoCampoCom')}}';

                    text += '<div class="widget-user-header bg-blue">\n' +
                        '    <form action="'+url_fot+'" method="post" style="display: none" id="avatarFormT'+campos_id+'">'+
                        '       <input type="file" id="avatarInputT'+campos_id+'" name="photo">'+
                        '    </form>'+
                        '    <div class="widget-user-image rutaFotoT">'+
                        '   <img onclick="setFotoCampoTodo('+campos_id+','+vis+')"  src="'+pathImg+msg['message']['ImagenIni']+'"id="avatarImageT'+campos_id+'">'+
                        '    </div>'+
                        // '                    <div class="widget-user-image"  >\n' +
                        //'                        <img  src="'+pathImg+msg['message']['ImagenIni']+'" alt="User Avatar">\n' +
                        //'                    </div>\n' +
                        '                    <!-- /.widget-user-image -->\n' +
                        '\n' +
                        '                    <h3  class="widget-user-username nombre_campo">'+msg['message']['nombre']+'</h3>\n' +
                        '                    <div style="padding-top: 20px">\n' +
                        '                        <strong><i class="fa fa-book margin-r-5"></i> Descripción</strong>\n' +
                        '                        <input onclick="clearComentTodo('+campo+')" id="comentario'+campo+'" type="text" value="'+msg['message']['descripcion']+'" class="form-control">\n' +
                        '                        <strong><i class="fa fa-flag-o margin-r-5"></i> Status</strong>\n' +
                        '                        <select class="form-control select2" id="stat'+campo+'" name="status" style="width: 100%;">\n' +
                        '                            <option value="'+status_id+'" selected="selected">'+status_name+'</option>\n' +
                        '                            <option  value="152">En Curso</option>\n' +
                        '                            <option value="153">Pendiente</option>\n' +
                        '                            <option value="154">Concluida</option>\n' +
                        '\n' +
                        '                        </select>\n' +
                        '                        <strong><i class="fa fa-commenting margin-r-5"></i> Comentarios</strong>\n' +
                        '                        <table width="100%">\n' +
                        '                            <tr>\n' +
                        '                                <td width="10%">\n' +
                        //'                                    <img  class="direct-chat-img" src="'+url_cam+'" >\n' +
                        '    <form action="'+url_fot_com+'" method="post" style="display: none" id="avatarFormTc'+campo+'">\n'+
                        '       <input type="file" class="direct-chat-img" id="avatarInputTc'+campo+'" name="photo">\n'+
                        '    </form>\n'+
                        '    <div class="widget-user-image rutaFotoTc">\n'+
                        '   <img onclick="setFotoCampoTodoCom('+campo+','+vis+')"  src="'+url_cam+'" id="avatarImageTc'+campo+'">\n'+
                        '    </div>\n'+
                        '                                </td>\n' +
                        '                                <td width="80%">\n' +
                        '                                    <input id="comvalue'+campo+'" type="text" placeholder="comentario" class="form-control">\n' +
                        '                                </td>\n' +
                        '                                <td width="10%">\n' +
                        '                                <input type="hidden" id="fotocomid'+campo+'" value="no_imagen.jpg">\n' +
                        '                                    <a onclick="setTodoCom('+campo+')"   class="btn btn-success btn-block pull-right">Agregar</a>\n' +
                        '                                </input>\n' +
                        '                            </tr>\n' +
                        '                        </table>\n' +
                        '                    </div>\n' +
                        '\n' +
                        '                </div>';
                    text += '<div class="box-footer no-padding">\n' +
                        '\n' +
                        '<div class="setListCom">'+

                        '</div >'+
                        '                    <a onclick="saveTodo('+campo+')"   class="btn btn-primary btn-block pull-right">Regresar</a>\n' +
                        ' </div>';

                    $('.infoTodo').html( text )


                    document.getElementById("editTodo").scrollIntoView();
                    var url_coms = '{{route('getFotoComs')}}'

                    $.ajax({
                        method: 'POST',
                        url: url_coms,
                        data: {campo_Id:campo}
                    })
                        .done(function(msg){

                            console.log(msg['message']);
                            var text = "";
                            var comentarios = 'comentario'+campo;
                            var url_cam = '{{ asset('images/buttons/camera.png')}}';
                            var URLdomain = window.location.host;
                            var pathImg = 'http://'+URLdomain+'/uploads/FotoTodoCom/';

                            text += '<ul class="nav nav-stacked " style="padding-bottom: 20px">\n';
                            for(var i = 0; i < msg['message'].length; i++)
                            {
                                text +=  '<li><a href="#">'+msg['message'][i]['descripcion']+' <span class="pull-right "><img  width="20px" height="20px" src="'+pathImg+msg['message'][i]['foto']+'" ></span></a></li>';
                            }
                            text += '</ul>\n';
                            $('.setListCom').html( text );
                        });
                });
        }

        function filtraTodo()
        {
            bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Buscando información...</div>' })
            var tipoChk = 0;
            var tipoExp = 0;

            //tipo de todo
            if(document.getElementById('boxCheck').checked) {
                tipoChk = 1;
            }
            if(document.getElementById('boxExp').checked) {
                tipoExp = 1;
            }
            //familia seleccionada
            var fam = document.getElementsByName('familia')[0].value;
            //por status
            var nuevo = 0;
            var curso = 0;
            var pendiente = 0;
            if(document.getElementById('boxNew').checked) {
                nuevo = 1;
            }

            if(document.getElementById('boxCur').checked) {
                curso = 1;
            }
            if(document.getElementById('boxPend').checked) {
                pendiente = 1;
            }

            //por fecha
            var fecha = document.getElementsByName('rango')[0].value;


            var url = '{{route('getToDoFiltro')}}';
            var razon_Id = document.getElementById('raz').value;
            var plaza_Id = document.getElementById('plaza').value;
            var tiendas_Id = document.getElementById('suc').value;
            var supervisor_Id = document.getElementById('sup').value;
            var chk_Id = document.getElementById('chk').value;
            var cat_Id = document.getElementById('cat').value;
            // alert('Muestra fotos')

            $.ajax({
                method: 'POST',
                url: url,
                data: { chk: tipoChk,
                    exp: tipoExp,
                    fam: fam,
                    nuevo: nuevo,
                    cur: curso,
                    pend:pendiente,
                    fecha:fecha,
                    tienda: tiendas_Id,
                    chk_Id: chk_Id,
                    cat_Id: cat_Id,
                    sup_id: supervisor_Id,
                    razon: razon_Id,
                    plaza:plaza_Id}
            })
                .done(function(msg){

                    bootbox.hideAll();

                    var fechas = setFechaPdf(document.getElementsByName('rango')[0].value);
                    var fecha_ini = setFechaFormat(fechas[0]);
                    var fecha_fin = setFechaFormat(fechas[1]);
                    var urlpdf = route('listaTodo', [tipoChk, tipoExp, document.getElementsByName('familia')[0].value, nuevo, curso, pendiente, fecha_ini, fecha_fin, tiendas_Id, chk_Id, cat_Id, razon_Id, plaza_Id, supervisor_Id]);
                    var urlexcel = route('listaTodoExcel', [tipoChk, tipoExp, document.getElementsByName('familia')[0].value, nuevo, curso, pendiente, fecha_ini, fecha_fin, tiendas_Id, chk_Id, cat_Id, razon_Id, plaza_Id, supervisor_Id]);

                    console.log(msg['message']);
                    var text = "";
                    var button = '{{ asset('images/buttons/browser.png')}}';
                    //text = msg['message'][1]['nombre'];
                    var campo = "";
                    var cat = "";
                    var chk = "";
                    var fam = "";
                    var desc = "";
                    var status_id = msg['message']['Status'];
                    var status_name = "";
                    var status_class = "";


                    for(var i = 0; i < msg['message'].length; i++)
                    {
                        var creacion = msg['message'][i]['fecha_inicio'];
                        status_id = msg['message'][i]['Status'];

                        switch(status_id)
                        {
                            case 151:
                            case '151':
                                status_name = "Nuevo";
                                status_class = "label-danger";
                                break;
                            case 152:
                            case '152':
                                status_name = "En curso";
                                status_class = "label-warning";
                                break;
                            case 153:
                            case '153':
                                status_name = "Pendiente";
                                status_class = "label-info";
                                break;
                            case 154:
                            case '154':
                                status_name = "Concluida";
                                status_class = "label-success";
                                break;
                        }
                        if(msg['message'][i]['nombre'] == null)
                        {
                            campo = "To-Do Express";
                        }else
                        {
                            campo = msg['message'][i]['nombre'];
                        }

                        if(msg['message'][i]['familia'] == null)
                        {
                            fam = "To-Do Express";
                        }else
                        {
                            fam = msg['message'][i]['familia'];
                        }

                        if(msg['message'][i]['checklist'] == null)
                        {
                            chk = "To-Do Express";
                        }else
                        {
                            chk = msg['message'][i]['checklist'];
                        }

                        if(msg['message'][i]['categoria'] == null)
                        {
                            cat = "To-Do Express";
                        }else
                        {
                            cat = msg['message'][i]['categoria'];
                        }
                        var smart = ""
                        smart = '<a  class="product-title">'+msg['message'][i]['nombre']+' <span class="label '+status_class+'   pull-right">'+status_name+'</span></a>';
                        if(msg['message'][i]['smart'] == 1)
                        {
                            smart = '<a  class="product-title">'+msg['message'][i]['nombre']+' <a onclick="valSmart('+msg['message'][i]['Id']+')" class="label label-primary">Smart</a> <span class="label '+status_class+'   pull-right">'+status_name+'</span></a>';
                        }
                        //msg['message'][i]['nombre']
                        // text += msg['message'][i]['descripcion'];
                        if(msg['message'][i]['Status'] != 154)
                        {
                            text += ' <!-- /.item -->\n' +

                                '                    <li class="item">\n' +
                                '                        <div class="product-img">\n' +
                                '                            <img data-toggle="tooltip" onclick="showInfoToDo('+ msg['message'][i]['Id']+','+msg['message'][i]['campos_Id']+', '+msg['message'][i]['visitas_Id']+')"  title="Agregar Información" src="'+button+'"  alt="Product Image">\n' +
                                '                        </div>\n' +
                                '                        <div class="product-info">\n' +
                                ''+smart+ ''+
                                '                        <span class="product-description">\n' +
                                '                          '+msg['message'][i]['descripcion']+'\n' +
                                '                        </span>\n' +
                                '                        <span >\n' +
                                '                          <strong>Sucursal: </strong>'+msg['message'][i]['tienda']+'\n' +
                                '                        </span></br>\n' +
                                '                        <span >\n' +
                                '                          <strong>Checklist: </strong>'+chk+'\n' +
                                '                        </span></br>\n' +
                                '                        <span >\n' +
                                '                          <strong>Categoría: </strong>'+cat+'\n' +
                                '                        </span></br>\n' +
                                '                        <span >\n' +
                                '                          <strong>Familia: </strong>'+fam+'\n' +
                                '                        </span></br>\n' +
                                '                        <span >\n' +
                                '                          <strong>Fecha Creación: </strong>'+creacion+'\n' +
                                '                        </span>\n' +
                                '                      </div>\n' +
                                '                    </li>';
                        }

                    }

                    $('.botonPdf').html(  ' <a href="'+urlpdf+'" class="btn btn-social-icon btn-danger pull-right"><i class="fa fa-file-pdf-o"></i></a>' +
                        '   <a href="'+urlexcel+'" class="btn btn-social-icon btn-success pull-right"><i class="fa fa-file-excel-o"></i></a>' );
                    $('.todo-list').html( text );

                    $('#filtro').boxWidget('collapse');


                });

            // alert(fecha)
        }

        function setFechaPdf(fecha) {
            var str = fecha;
            var res = str.split("-");
            return res;
        }

        function setFechaFormat(fecha) {
            var str = fecha;
            var res = str.split("/");
            return res[0]+'-'+res[1]+'-'+res[2];
        }

        function valSmart(id)
        {

            var url_smart = '{{route('validaReg')}}';
            $.ajax({
                method: 'POST',
                url: url_smart,
                data: {todo_Id: id}
            })
                .done(function(msg){
                    console.log(msg['message']);

                    if(msg['message'] == 1)
                    {
                        getSmart(id);
                    }else
                    {
                        setSmart(id);
                    }

                    // var visita_Id = document.getElementById('id_visita').value;

                });
        }

        function setSmart(id)
        {

            //alert(id)
            bootbox.confirm({
                title: "Plan Smart",
                message: "Si continuas, se creará el registro correspondiente del plan smart, ¿Deseas proceder?",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> Cancelar'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Iniciar'
                    }
                },
                callback: function (result) {
                    console.log(result);

                    if(result === true)
                    {
                        var post_id = id;
                        var url_can = '{{route('setSmart')}}';

                        $.ajax({
                            method: 'POST',
                            url: url_can,
                            data: {campos_id:post_id}
                        })
                            .done(function(msg){
                                console.log(msg['message']);
                                $('.id_smart').html( '<input type="hidden" value="'+msg['message']+'" id="id_smart" >' )

                                getSmart(id);

                            });
                    }
                }
            });
        }

        function getSmart(id)
        {

            // var visita_Id = document.getElementById('id_visita').value;
            var url_smart = '{{route('getSmart')}}';

            $.ajax({
                method: 'POST',
                url: url_smart,
                data: {todo_Id:id}
            })
                .done(function(msg){
                    console.log(msg['message']);



                    $("#filtro").hide();
                    $("#todolist").hide();


                    var text = "";

                    text +=  '    <!--PLAN SMART-->\n' +
                        '\n' +
                        '<div class="box box-warning ">\n' +
                        '    <div class="box-header with-border">\n' +
                        '        <h3 class="box-title">Plan smart para campo</h3>\n' +
                        '    </div>\n' +
                        '    <!-- /.box-header -->\n' +
                        '    <div class="box-body">\n' +
                        '        <form role="form">\n' +
                        '            <!-- text input -->\n' +
                        '            <div class="form-group">\n' +
                        '                <label>Objetivo</label>\n' +
                        '                <input type="text" class="form-control" value="'+msg['message']['objetivo']+'" id="inputObjetivo" placeholder="Describe el objetivo del plan">\n' +
                        '            </div>\n' +
                        '            <div class="row">\n' +
                        '                <div class="col-md-6">\n' +
                        '                    <div class="form-group">\n' +
                        '                        <label>Indicador Actual</label>\n' +
                        '                        <input type="text" class="form-control" value="'+msg['message']['indicador_actual']+'" id="inputActual" placeholder="Describe el indicador actual">\n' +
                        '                    </div>\n' +
                        '                </div>\n' +
                        '                <div class="col-md-6">\n' +
                        '                    <div class="form-group">\n' +
                        '                        <label>Indicador Deseado</label>\n' +
                        '                        <input type="text" class="form-control" value="'+msg['message']['indicador_deseado']+'" id="inputDeseado" placeholder="Describe el indicador deseado">\n' +
                        '                    </div>\n' +
                        '                </div>\n' +
                        '            </div><hr>\n' +
                        '            <label>Listado de Acciones</label>\n' +
                        '            <div class="row">\n' +
                        '                <div class="col-md-6">\n' +
                        '                    <div class="form-group">\n' +
                        '                        <input type="text" class="form-control" id="inputAccion" placeholder="Describe la accion">\n' +
                        '                    </div>\n' +
                        '                </div>\n' +
                        '                <div class="col-md-6">\n' +
                        '                    <div class="form-group">\n' +
                        '                        <input type="text" class="form-control" id="inputTarea" placeholder="Describe la tarea">\n' +
                        '                    </div>\n' +
                        '                </div>\n' +
                        '\n' +
                        '            </div>\n' +
                        '            <div class="row">\n' +
                        '                <div class="col-md-6">\n' +
                        '                    <div class="form-group">\n' +
                        '                        <input type="text" class="form-control" id="inputResponsable" placeholder="Indica el responsable">\n' +
                        '                    </div>\n' +
                        '                </div>\n' +
                        '                <div class="col-md-6">\n' +
                        '                    <div class="form-group">\n' +
                        '                                   <div class="input-group date">\n' +
                        '                                        <div class="input-group-addon">\n' +
                        '                                            <i class="fa fa-calendar"></i>\n' +
                        '                                        </div>\n' +
                        '                                        <input type="text" placeholder="Fecha de Entrega" data-date-format="yyyy-mm-dd" class="form-control pull-right" id="datepicker">\n' +
                        '                                    </div>'+
                        '                    </div>\n' +
                        '                </div>\n' +
                        '                <div class="form-group" style="padding-right: 20px">\n' +
                        '                    <a onclick="setAcciones()" class="btn btn-success pull-right" >Agregar</a>\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '\n' +
                        '\n' +
                        '            <hr>\n' +
                        '            <div class="box-body">\n' +
                        '                <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">\n' +
                        '                    Acciones\n' +
                        '                </h4>\n';

                    text +=      '    <div class="listaAcciones"  ></div>        <!-- textarea -->\n' +
                        '            <div class="form-group">\n' +
                        '                <label>Requerimientos</label>\n' +
                        '                <textarea class="form-control" rows="3"  id="inputReq" placeholder="Detalla los requerimientos ...">'+msg['message']['requerimientos']+'</textarea>\n' +
                        '            </div>\n' +
                        '            <div class="form-group">\n' +
                        '                <label>Comentarios</label>\n' +
                        '                <textarea class="form-control"  rows="3" id="inputComs" placeholder="Comentarios adicionales ...">'+msg['message']['comentarios']+'</textarea>\n' +
                        '            </div>\n' +
                        '\n' +
                        '\n' +
                        '\n' +
                        '        </form>\n' +
                        ' <a onclick="cancelSmart()"  class="btn btn-danger  margin-bottom pull-right">Cancelar</a>'+
                        ' <a  onclick="updSmart('+id+')" class="btn btn-primary  margin-bottom pull-right">Guardar</a>'+

                        '    </div>\n' +
                        '    <!-- /.box-body -->\n' +
                        '</div>';

                    $('.id_smart').html( '<input type="hidden" value="'+msg['message']['Id']+'" id="id_smart" >' )



                    $('.planSmart').html( text );

                    document.getElementById("planSmart").scrollIntoView();

                    getAcciones(msg['message']['Id']);



                    //Date picker
                    $('#datepicker').datepicker({
                        autoclose: true,
                        format: 'yyyy-mm-dd'
                    })


                    // var visita_Id = document.getElementById('id_visita').value;

                });
        }

        function cancelSmart()
        {

            $("#filtro").show();
            $("#todolist").show();
            $('.planSmart').html( '' );


        }

        function setAcciones()
        {
            var url_can = '{{route('setAccion')}}';


            if(document.getElementById('inputAccion').value != "")
            {
                if(document.getElementById('inputAccion').value != "")
                {
                    var visitas_smart_Id = document.getElementById('id_smart').value;
                    var accion = document.getElementById('inputAccion').value;
                    var tareas =  document.getElementById('inputTarea').value;
                    var responsable =  document.getElementById('inputResponsable').value;
                    var fhentrega = document.getElementById('datepicker').value;


                    $.ajax({
                        method: 'POST',
                        url: url_can,
                        data: {visitas_smart_Id:visitas_smart_Id,
                            accion:accion,
                            tareas:tareas,
                            responsable:responsable,
                            fhentrega:fhentrega
                        }
                    })
                        .done(function(msg){
                            console.log(msg['message']);

                            document.getElementById('inputAccion').value = "";
                            document.getElementById('inputTarea').value = "";
                            document.getElementById('inputResponsable').value = "";
                            document.getElementById('datepicker').value = "";
                            getAcciones(visitas_smart_Id);

                        });
                }else
                {
                    bootbox.alert("Debes definir una fecha de entrega para continuar")
                }

            }else
            {
                bootbox.alert("Debes describir una acción para continuar")
            }


        }

        function getAcciones(id)
        {
            var url_can = '{{route('getAcciones')}}';
            $.ajax({
                method: 'POST',
                url: url_can,
                data: {visitas_smart_Id:id,

                }
            })
                .done(function(msg){
                    console.log(msg['message']);
                    var text = "";
                    for(var i = 0; i < msg['message'].length; i++)
                    {
                        text += '   <!--Inicio for acciones -->            <div class="media">\n' +
                            '                    <div class="media-body">\n' +
                            '                        <div class="clearfix">\n' +
                            '                            <p class="pull-right">\n' +
                            '                                <!--<a href="https://themequarry.com/theme/ample-admin-the-ultimate-dashboard-template-ASFEDA95" class="btn btn-success btn-sm ad-click-event">\n' +
                            '                                    LEARN MORE\n' +
                            '                                </a>-->\n' +
                            '                            </p>\n' +
                            '\n' +
                            '                            <h4 style="margin-top: 0">Acción: '+msg['message'][i]['accion']+' <span style="color: #0A4DB7">('+msg['message'][i]['responsable']+')</span> <strong>'+msg['message'][i]['avance']+'%</strong> </h4>\n' +
                            '\n' +
                            '                            <p>Tareas: '+msg['message'][i]['tareas']+' </p>\n' +
                            '                            <p style="margin-bottom: 0">\n' +
                            '                                <i class="fa fa-calendar-check-o margin-r5"></i> Entrega: '+msg['message'][i]['fhentrega']+'\n' +
                            '                            </p>\n' +
                            ' <hr>\n' +

                            '                            <p style="margin-bottom: 0">\n' +
                            '                                <div class="col-sm-6">\n' +
                            '                                    <!-- Progress bars -->\n' +
                            '                                    <p>\n' +

                            '                                           </p>\n' +
                            '                                    <p>\n' +

                            '                                    </p>\n' +
                            '\n' +
                            '\n' +

                            '                                </div>\n' +
                            '\n' +
                            '                            </p>\n' +
                            '                        </div>\n' +
                            '                    </div>\n' +
                            '                </div>\n' +
                            '            </div>\n' +
                            ' <!--Fin de for acciones-->\n';

                        $('.listaAcciones').html( text );

                    }

                });
        }

        function updSmart(id)
        {
            var url_can = '{{route('updSmart')}}';


            var objetivo = document.getElementById('inputObjetivo').value;
            var indicador_actual = document.getElementById('inputActual').value;
            var indicador_deseado =  document.getElementById('inputDeseado').value;
            var requerimientos =  document.getElementById('inputReq').value;
            var comentarios = document.getElementById('inputComs').value;

            if(document.getElementById('inputObjetivo').value != "")
            {
                $.ajax({
                    method: 'POST',
                    url: url_can,
                    data: {objetivo:objetivo,
                        indicador_actual:indicador_actual,
                        indicador_deseado:indicador_deseado,
                        requerimientos:requerimientos,
                        comentarios:comentarios,
                        todo_Id: id
                    }
                })
                    .done(function(msg){
                        console.log(msg['message']);
                        cancelSmart();

                    });
            }else
            {
                bootbox.alert('Debes definir un objetivo del plan');
            }

        }



    </script>

@stop