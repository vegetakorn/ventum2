@extends('adminlte::page')
@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>Familias</h1>
@stop

@section('content')

    <div class="box" id="listaFamilias">
        <div class="box-header">
            <button type="button" onclick="add()" class="btn  btn-success pull-right">Agregar Familia</button>
        </div>
        <!-- /.box-header


         -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Familia</th>


                </tr>
                </thead>


                <tbody>
                @foreach($familias as $familia)

                    <tr>
                        <td >
                            <!-- chat item -->
                            <div class="item">

                                <p class="message">
                                    <a href="#" class="name">
                                        <small class="text-muted pull-right">


                                            <a   ><img data-toggle="tooltip" onclick="editar({{$familia->Id}})" title="Editar" width="40" height="40" src="{{ asset('images/buttons/pencil.png')}}"  alt="Icono"></a >


                                        </small>


                                    </a>
                                    <a href="#" class="name">
                                        <h3>&nbsp;<strong>{{$familia->Nombre}}  </strong> </h3>
                                    </a>

                                    <!-- /.attachment -->
                            </div>
                            <!-- /.item -->
                        </td>

                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <div class="row" id="agregarFamilia">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">



                    <!--<p class="text-muted text-center">Software Engineer</p>-->


                </div>
                <!-- /.box-body -->
            </div>

        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">

                    <li class="active"><a href="#settings" data-toggle="tab">Información de Familia</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="settings">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label for="inputName" class="col-sm-2 control-label">Nombre</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputName" placeholder="Nombre">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">


                                    <div class="btnGuardar">

                                        <a onclick="save()" class="btn btn-primary btnGuardar">Guardar</a>
                                        <a onclick="cancel()" class="btn btn-danger">Cancelar</a>
                                    </div>


                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>

    <div class="foto_zona" >
        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
        <input type="hidden" id="foto_zona" value="no_imagen.jpg" >
    </div>


@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

@stop

@section('js')
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script>

        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();

            //Date picker
            $('#datepicker').datepicker({
                autoclose: true
            })

            $('#example1').DataTable( {
                "pagingType": "full_numbers",
                "scrollY":        '60vh',
                "scrollCollapse": true,
                "language": {
                    "lengthMenu": "Mostrando _MENU_ registros por página ",
                    "zeroRecords": "Sin registros encontrados",
                    "info": "Mostrando página  _PAGE_ de _PAGES_ páginas",
                    "infoEmpty": "Sin regustros encontrados",
                    "infoFiltered": "(filtrados de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    }
                }
            } );
        } );
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('yyyy-mm-dd', { 'placeholder': 'yyyy-mm-dd' })
            //Datemask2 mm/dd/yyyy
            $('#datemask2').inputmask('yyyy-mm-dd', { 'placeholder': 'yyyy-mm-dd' })
            //Money Euro
            $('[data-mask]').inputmask()

            //Date range picker
            $('#reservation').daterangepicker()
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                {
                    ranges   : {
                        'Today'       : [moment(), moment()],
                        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate  : moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                }
            )

            //Date picker
            $('#datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            })

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            })
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            })

            //Colorpicker
            $('.my-colorpicker1').colorpicker()
            //color picker with addon
            $('.my-colorpicker2').colorpicker()

            //Timepicker
            $('.timepicker').timepicker({
                showInputs: false
            })
        })


        $(document).ready(function() {

            $("#agregarFamilia").hide();
            //$("#agregaInfo").hide();



        } );


        function update(id)
        {
            if(document.getElementById('inputName').value != "")
            {
                url = '{{route('updateFamilia')}}';
                $.ajax({
                    method: 'POST',
                    url: url,
                    data: {id: id,

                        nombre: document.getElementById('inputName').value,

                    }
                })
                    .done(function(msg){

                        console.log(msg['message']);
                        var url = "";
                        url = '{{route('familias.familias')}}';//'visitas/final/'+post_id;//
                        window.location.href = url;

                    });

            }else
            {
                bootbox.alert('Debes indicar el nombre de la familia')
            }
        }

        function add()
        {
            //alert('OK')
            $("#listaFamilias").hide();
            $("#agregarFamilia").show();

            //vamos a obtener el listado de cosas para asignar al empleado
            var url = '{{route('cargaInfoEmpleado')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {   }
            })
                .done(function(msg){

                });



        }

        function editar(id)
        {
            $("#listaFamilias").hide();
            $("#agregarFamilia").show();

            //vamos a obtener el listado de cosas para asignar al empleado
            var url = '{{route('cargaInfoFamEditar')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id: id   }
            })
                .done(function(msg){

                    console.log(msg['familia']);
                    var text = "";

                    var URLdomain = window.location.host;
                    document.getElementById('inputName').value = msg['familia']['Nombre'];

                });



            $('.btnGuardar').html( ' <a onclick="update('+id+')" class="btn btn-primary ">Actualizar</a>' +
                ' <a onclick="cancel()" class="btn btn-danger">Cancelar</a>' )
        }




        function cancel()
        {
            //alert('OK')
            url = '{{route('familias.familias')}}';//'visitas/final/'+post_id;//
            window.location.href = url;

        }

        function save() {
            url = '{{route('addFamilia')}}';
            // alert('Muestra despues')

            if (document.getElementById('inputName').value != "")
            {
                $.ajax({
                    method: 'POST',
                    url: url,
                    data: {
                        nombre: document.getElementById('inputName').value
                    }
                })
                    .done(function(msg){

                        console.log(msg['message']);
                        var url = "";
                        url = '{{route('familias.familias')}}';//'visitas/final/'+post_id;//
                        window.location.href = url;

                    });
            }else
            {
                bootbox.alert("Debes indicar el nombre de la familia");
            }


        }


    </script>


@stop