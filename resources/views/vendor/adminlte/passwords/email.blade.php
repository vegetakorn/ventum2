@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/css/auth.css') }}">
    @yield('css')
@stop

@section('body_class', 'login-page')

@section('body')
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Recuperar Contraseña</p>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <form >


                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <input type="email" id="email" name="email" class="form-control" value="{{ $email or old('email') }}"
                           placeholder="Correo Electrónico">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <a onclick="setMail()"
                        class="btn btn-primary btn-block btn-flat"
                >Recuperar contraseña</a>
            </form>
        </div>
        <!-- /.login-box-body -->
    </div><!-- /.login-box -->
@stop

@section('adminlte_js')
    @yield('js')

    <script>

        function setMail()
        {

            url = '{{route('sendMailRec')}}';
            // alert('Muestra despues')

            $.ajax({
                method: 'POST',
                url: url,
                data: {mail: document.getElementById('email').value   }
            })
                .done(function(msg){

                    console.log(msg['mail']);
                    bootbox.alert("Se ha enviado a tu correo la contraseña nueva, podrás cambiarla al ingresar al sistema")

                });
        }
    </script>

@stop

