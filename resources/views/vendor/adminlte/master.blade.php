<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title_prefix', config('adminlte.title_prefix', ''))
@yield('title', config('adminlte.title', 'AdminLTE 2'))
@yield('title_postfix', config('adminlte.title_postfix', ''))</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/Ionicons/css/ionicons.min.css') }}">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/iCheck/skins/all.css') }}">
    @if(config('adminlte.plugins.select2'))
        <!-- Select2 -->
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">
    @endif

   <!-- <style>
        .main-sidebar { background-color: #39cccc !important }
    </style>-->

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/AdminLTE.min.css') }}">

    @if(config('adminlte.plugins.datatables'))
        <!-- DataTables -->
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
    @endif

    @yield('adminlte_css')

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition @yield('body_class')">

@yield('body')

<script src="{{ asset('vendor/adminlte/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>

@if(config('adminlte.plugins.select2'))
    <!-- Select2 -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
@endif

<!-- iCheck 1.0.1 -->
<script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('alert/bootbox.min.js') }}" ></script>
@routes
<script>
    $(document).ready(function() {
        var url = '{{route('getTiendas')}}'
        $.ajax({
            method: 'POST',
            url: url,
            data: {id:0}
        })
            .done(function(msg){

                console.log(msg['tiendas']);
                var text = "";

                for(var i = 0; i < msg['tiendas'].length; i++)
                {
                    // text += ' <li><a href="#"><i class="fa fa-circle-o text-red"></i>'+msg['message'][i]['nombre']+'</a></li>'
                    var URLdomain = window.location.host;
                    var r = 'https://'+URLdomain+'/visitas/visita/'+msg['tiendas'][i]['Id'];
                    text += '<li ><!-- start message -->\n' +
                        '                                                            <a style="" href="'+r+'"  >\n' +
                        '                                                                <div class="pull-left ">\n' +
                        '                                                                    <h6 >\n' +
                        '                                                                       '+msg['tiendas'][i]['nombre']+'\n' +
                        '                                                                    </h6>\n' +
                        '                                                                </div>\n' +
                        '\n' +
                        '                                                            </a>\n' +
                        '                                                        </li>'


                }

                $('.listaTiendas').html( text )
                $('.noTiendas').html( msg['no'] )

            } );





    } );
</script>
<script>
    $(document).ready(function() {
        checkStatus();

        window.addEventListener("online", function()
        {
                checkStatus();

        });
        window.addEventListener("offline", function()
        {
            checkStatus();

        });

        $("#modulo1").hide();
        $("#modulo2").hide();
        $("#modulo3").hide();
        $("#modulo4").hide();
        $("#modulo5").hide();
        $("#modulo6").hide();
        $("#modulo7").hide();
        $("#modulo8").hide();
        $("#modulo9").hide();
        $("#modulo10").hide();
        $("#modulo11").hide();
        $("#modulo12").hide();
        $("#modulo13").hide();
        $("#modulo14").hide();
        $("#modulo15").hide();
        $("#modulo16").hide();
        $("#modulo17").hide();
        $("#modulo18").hide();
        $("#modulo19").hide();

        var url = '{{route('getFunciones')}}'
        $.ajax({
            method: 'POST',
            url: url,
            data: {id:0}
        })
            .done(function(msg){

                console.log(msg['modulos']);
                var text = "";

                for(var i = 0; i < msg['modulos'].length; i++)
                {
                    var mod = $("#modulo"+msg['modulos'][i]['modulos_Id']);
                    mod.show();
                }



            } );




    } );

    var checkStatus = function()
    {
        if(navigator.onLine)
        {
            //bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner label-success"></i> Estás conectado a internet</div>' })

            bootbox.hideAll();
        }else
        {
            bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner label-danger"></i>No estás conectado a internet, verifica tu conexión ' +
                    '<p> el sistema se conectará automaticamente al volver a tener señal</p>' +
                    '<p>No se podrá guardar ningún cambio mientras estés fuera de linea</p></div>' })
        }
    }

</script>

@yield('adminlte_js')





</body>
</html>
