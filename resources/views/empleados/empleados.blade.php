@extends('adminlte::page')
@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>Empleados</h1>
@stop

@section('content')

    <div class="box" id="listaEmpleados">
        <div class="box-header">
            <a href="{{route('exportEmp')}}" class="btn btn-social-icon btn-success pull-right"><i class="fa fa-file-excel-o"></i></a>
            <button type="button" onclick="add()" class="btn  btn-primary pull-right">Agregar Empleado</button>
            <input type="hidden" id="id_cuestionario" value="{{$cuestionario->Id}}" >
        </div>
        <!-- /.box-header


         -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Empleado</th>


                </tr>
                </thead>


                <tbody>
                @foreach($empleados as $empleados)
                    @if($empleados->nombre != "")
                        <tr>
                            <td >
                                <!-- chat item -->
                                <div class="item">


                                    <img src="{{ asset('uploads/Empleados/'.$empleados->foto ) }}" width="80px" height="80px" class="img-circle" >

                                    <p class="message">
                                        <a href="#" class="name">
                                            <small class="text-muted pull-right">


                                                <a   ><img data-toggle="tooltip" onclick="editar({{$empleados->Id}})" title="Editar" width="40" height="40" src="{{ asset('images/buttons/pencil.png')}}"  alt="Icono"></a >

                                                <a    ><img onclick="baja({{$empleados->Id}})" data-toggle="tooltip" title="Baja" width="40" height="40" src="{{ asset('images/buttons/stop.png')}}"  alt="Icono"></a >
                                            </small>


                                        </a>
                                        <a href="#" class="name">
                                            <h3>{{$empleados->clave}}&nbsp;<strong>{{$empleados->nombre}}  </strong> {{$empleados->apepat}}  {{$empleados->apemat}}</h3>
                                        </a>
                                    @if($empleados->activo == 1)
                                        <h4>Activo</h4>
                                    @else
                                        <h4>Baja</h4>
                                        @endif
                                        </p>
                                        <h6>{{$empleados->mail}}&nbsp;</h6>
                                        </p>
                                        <!-- /.attachment -->
                                </div>
                                <!-- /.item -->
                            </td>

                        </tr>
                    @endif

                @endforeach

                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <div class="row" id="agregarEmpleado">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">

                    <form action="{{route('regFotoEmpleado')}}" method="post" style="display: none" id="avatarForm">
                        <input type="file" id="avatarInput" name="photo">
                    </form>
                    <div class="rutaFoto">
                        <img onclick="setFoto()" class="profile-user-img img-responsive img-circle" id="avatarImage" src="{{ asset('uploads/Empleados/empleado.jpg')}}" alt="User profile picture">

                    </div>

                    <h3 class="profile-username text-center">Foto de Empleado</h3>

                    <!--<p class="text-muted text-center">Software Engineer</p>-->

                   <ul class="list-group list-group-unbordered">

                       <li class="list-group-item puestos">

                       </li>
                       <li class="list-group-item razones">

                       </li>
                       <li class="list-group-item plazas">
                           <input type="hidden" id="plaza" value="0" >
                       </li>
                       <li class="list-group-item tiendas">
                           <input type="hidden" id="suc" value="0" >
                       </li>
                       <li class="list-group-item zonas">
                           <input type="hidden" id="zona" value="0" >
                       </li>

                   </ul>


               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->


        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">

                    <li class="active"><a href="#settings" data-toggle="tab">Datos Personales</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="settings">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label for="inputClave" class="col-sm-2 control-label">Clave</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputClave" placeholder="Clave">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-sm-2 control-label">Nombre</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputName" placeholder="Nombre">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-sm-2 control-label">Apellido Paterno</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputApePat" placeholder="Apellido Paterno">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-sm-2 control-label">Apellido Materno</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputApeMat" placeholder="Apellido Materno">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input" class="col-sm-2 control-label">Fecha de Ingreso</label>

                                <div class="col-sm-10">
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" data-date-format="yyyy-mm-dd" class="form-control pull-right" id="datepicker">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail" class="col-sm-2 control-label">E-mail</label>

                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="input" class="col-sm-2 control-label">Teléfono</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputTel" placeholder="Teléfono">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputExperience" class="col-sm-2 control-label">Domicilio</label>

                                <div class="col-sm-10">
                                    <textarea class="form-control" id="inputDom" placeholder="Domicilio"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-sm-2 control-label">CURP</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputCurp" placeholder="CURP">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-sm-2 control-label">NSS</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputNss" placeholder="NSS">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">


                                    <div class="btnGuardar">

                                        <a onclick="save()" class="btn btn-primary btnGuardar">Guardar</a>
                                        <a onclick="cancel()" class="btn btn-danger">Cancelar</a>
                                    </div>


                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
    <div class="foto_perfil" >
        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
        <input type="hidden" id="foto_perfil" value="empleado.jpg" >
    </div>

    <div class="tienda_ant" >

            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
            <input type="hidden" id="tienda_ant" value="0" >

    </div>

    <div id="cuestionario" class="cargaCuestionario">
        <!-- aqui se carga el cuestionario de la visita -->
    </div>


@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

@stop

@section('js')
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script>

        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();

            //Date picker
            $('#datepicker').datepicker({
                autoclose: true
            })

            $('#example1').DataTable( {
                "pagingType": "full_numbers",
                "scrollY":        '60vh',
                "scrollCollapse": true,
                "language": {
                    "lengthMenu": "Mostrando _MENU_ registros por página ",
                    "zeroRecords": "Sin registros encontrados",
                    "info": "Mostrando página  _PAGE_ de _PAGES_ páginas",
                    "infoEmpty": "Sin regustros encontrados",
                    "infoFiltered": "(filtrados de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    }
                }
            } );
        } );
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('yyyy-mm-dd', { 'placeholder': 'yyyy-mm-dd' })
            //Datemask2 mm/dd/yyyy
            $('#datemask2').inputmask('yyyy-mm-dd', { 'placeholder': 'yyyy-mm-dd' })
            //Money Euro
            $('[data-mask]').inputmask()

            //Date range picker
            $('#reservation').daterangepicker()
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                {
                    ranges   : {
                        'Today'       : [moment(), moment()],
                        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate  : moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                }
            )

            //Date picker
            $('#datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            })

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            })
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            })

            //Colorpicker
            $('.my-colorpicker1').colorpicker()
            //color picker with addon
            $('.my-colorpicker2').colorpicker()

            //Timepicker
            $('.timepicker').timepicker({
                showInputs: false
            })
        })


        $(document).ready(function() {

            $("#agregarEmpleado").hide();
            //$("#agregaInfo").hide();



        } );


        function update(id)
        {
            if(document.getElementById('inputName').value != "")
            {
                if(document.getElementById('datepicker').value != "")
                {

                }else
                {
                    bootbox.alert('Debes elegir una fecha de ingreso')
                }


                url = '{{route('updateEmpleado')}}';
                // alert('Muestra despues')

                $.ajax({
                    method: 'POST',
                    url: url,
                    data: {id: id,
                        clave: document.getElementById('inputClave').value,
                        nombre: document.getElementById('inputName').value,
                        apepat: document.getElementById('inputApePat').value,
                        apemat: document.getElementById('inputApeMat').value,
                        fhingreso: document.getElementById('datepicker').value,
                        empresas_Id: '{{auth()->user()->empresas_Id}}',
                        domicilio:document.getElementById('inputDom').value,
                        telefono: document.getElementById('inputTel').value,
                        curp:document.getElementById('inputCurp').value,
                        mail:document.getElementById('inputEmail').value,
                        nss:document.getElementById('inputNss').value,
                        foto: document.getElementById('foto_perfil').value,
                        puesto_Id: document.getElementById('puesto').value,
                        plaza_Id: document.getElementById('plaza').value,
                        razon_Id: document.getElementById('razon').value,
                        suc: document.getElementById('suc').value,
                        suc_ant: document.getElementById('tienda_ant').value,
                        zona: document.getElementById('zona').value
                    }
                })
                    .done(function(msg){

                        console.log(msg['message']);
                        var url = "";
                        url = '{{route('empleados.empleados')}}';//'visitas/final/'+post_id;//
                        window.location.href = url;

                    });

            }else
            {
                bootbox.alert('Debes nombrar al empleado')
            }
        }

        function add()
        {
            //alert('OK')
            $("#listaEmpleados").hide();
            $("#agregarEmpleado").show();

            //vamos a obtener el listado de cosas para asignar al empleado
            var url = '{{route('cargaInfoEmpleado')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {   }
            })
                .done(function(msg){

                    console.log(msg['puestos']);
                    var text = "";

                    text += '<b>Puesto</b> <select class="form-control select2" id="puesto" name="puesto" style="width: 100%;">\n' +
                        '<option value="0" selected>Selecciona un puesto </option>' ;


                    for(var i = 0; i < msg['puestos'].length; i++)
                    {
                        text += ' <option value="'+msg['puestos'][i]['Id']+'">'+msg['puestos'][i]['puesto']+'</option>\n';
                    }

                    text +=    '                           </select>';
                    $('.puestos').html( text )

                    var razon = "";
                    razon += '<b>Razón</b> <select onchange="getPlazas()" class="form-control select2" id="razon" name="razon" style="width: 100%;">' +
                        '<option value="0" selected>Selecciona una razón </option>' ;
                    for(var i = 0; i < msg['razones'].length; i++)
                    {
                        razon += ' <option value="'+msg['razones'][i]['Id']+'" >'+msg['razones'][i]['nombre']+'</option>\n';
                    }
                    razon +=    '                           </select>';
                    $('.razones').html( razon )
            });



        }

        function editar(id)
        {
            $("#listaEmpleados").hide();
            $("#agregarEmpleado").show();

            //vamos a obtener el listado de cosas para asignar al empleado
            var url = '{{route('cargaInfoEmpleadoEditar')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id: id   }
            })
                .done(function(msg){

                    console.log(msg['nombre']);
                    var text = "";

                    var URLdomain = window.location.host;
                    var pathImg = 'http://'+URLdomain+'/uploads/Empleados/'+msg['empleado']['foto'];
                    $avatarImage = $('#avatarImage');

                    $avatarImage.attr('src', pathImg)


                    $('.foto_perfil').html('<input type="hidden" id="foto_perfil" value="' + msg['empleado']['foto'] + '" >')

                    document.getElementById('inputClave').value = msg['empleado']['clave'];
                    document.getElementById('inputName').value = msg['empleado']['nombre'];
                    document.getElementById('inputApePat').value = msg['empleado']['apepat'];
                    document.getElementById('inputApeMat').value = msg['empleado']['apemat'];
                    document.getElementById('datepicker').value = msg['empleado']['fhingreso'];
                    document.getElementById('inputDom').value = msg['empleado']['domicilio'];
                    document.getElementById('inputTel').value = msg['empleado']['telefono'];
                    document.getElementById('inputCurp').value = msg['empleado']['curp'];
                    document.getElementById('inputEmail').value = msg['empleado']['mail'];
                  //  $('.inputEmail').attr('disabled', 'disabled')
                    $("#inputEmail").prop('disabled', true);
                    document.getElementById('inputNss').value = msg['empleado']['ssocial'];
                    //document.getElementById('inputClave').value = msg['empleado']['clave'];
                    var puesto = '<option value="0" selected>Sin puesto asignado</option>';
                    var raz  = '<option value="0" selected>Sin razón asignada</option>';
                    var plaza  = '<option value="0" selected>Sin plaza asignada</option>';
                    var sucursal = '<option value="0" selected>Sin sucursal asignada</option>';
                    var zona = '<option value="0" selected>Sin zona asignada</option>';

                    if(msg['empleado']['puesto'] != null)
                    {
                       // alert('No tiene puesto asignado')
                        puesto = '<option value="'+msg['empleado']['puesto_Id']+'" selected>'+msg['empleado']['puesto'] +'</option>';
                    }
                    if(msg['empleado']['zona'] != null)
                    {

                        zona = '<option value="'+msg['empleado']['zId']+'" selected>'+msg['empleado']['zona'] +'</option>';
                    }
                    var txtZona = "";
                    txtZona += '<b>Zona</b> <select class="form-control select2" id="zona" name="zona" style="width: 100%;">' +zona+'' ;
                    for(var i = 0; i < msg['zonas'].length; i++)
                    {
                        txtZona += ' <option value="'+msg['zonas'][i]['Id']+'" >'+msg['zonas'][i]['zona']+'</option>\n';
                    }
                    txtZona +=    '                           </select>';
                    $('.zonas').html( txtZona );

                    if(msg['empleado']['razon'] != null)
                    {
                        // alert('No tiene puesto asignado')
                        raz = '<option value="'+msg['empleado']['razon_Id']+'" selected>'+msg['empleado']['razon'] +'</option>';


                        if(msg['empleado']['plaza'] != null)
                        {
                            plaza = '<option value="'+msg['empleado']['plaza_Id']+'" selected>'+msg['empleado']['plaza']+' </option>'
                            var url = '{{route('cargaPlazas')}}'
                            $.ajax({
                                method: 'POST',
                                url: url,
                                data: {id:  msg['empleado']['razon_Id']  }
                            })
                                .done(function(msg){

                                    console.log(msg['puestos']);
                                    var text = "";

                                    text += '<b>Plazas</b> <select onchange="getTiendas()" class="form-control select2" id="plaza" name="plaza" style="width: 100%;">'+plaza+' ';



                                    for(var i = 0; i < msg['plazas'].length; i++)
                                    {
                                        text += ' <option value="'+msg['plazas'][i]['Id']+'">'+msg['plazas'][i]['plaza']+'</option>\n';
                                    }

                                    text +=    '                           </select>';
                                    $('.plazas').html( text )


                                });

                            if(msg['empleado']['sucursal'] != null)
                            {
                                sucursal = '<option value="'+msg['empleado']['tiendas_Id']+'" selected>'+msg['empleado']['sucursal'] +'</option>';
                                $('.tienda_ant').html('<input type="hidden" id="tienda_ant" value="' + msg['empleado']['tiendas_Id'] + '" >');
                                var url = '{{route('cargaTiendas')}}'
                                $.ajax({
                                    method: 'POST',
                                    url: url,
                                    data: {id:  msg['empleado']['plaza_Id'] }
                                })
                                    .done(function(msg){

                                        console.log(msg['tiendas']);


                                        var text = "";

                                        text += '<b>Sucursales</b> <select  class="form-control select2" id="suc" name="suc" style="width: 100%;">'+sucursal+'';


                                        for(var i = 0; i < msg['tiendas'].length; i++)
                                        {
                                            text += ' <option value="'+msg['tiendas'][i]['Id']+'">'+msg['tiendas'][i]['nombre']+'</option>\n';
                                        }

                                        text +=    '                           </select>';
                                        $('.tiendas').html( text )


                                    });


                            }else
                            {
                                $('.tienda_ant').html('<input type="hidden" id="tienda_ant" value="0" >');
                            }
                        }
                    }






                    text += '<b>Puesto</b> <select class="form-control select2" id="puesto" name="puesto" style="width: 100%;">\n'+puesto+''  ;


                    for(var i = 0; i < msg['puestos'].length; i++)
                    {
                        text += ' <option value="'+msg['puestos'][i]['Id']+'">'+msg['puestos'][i]['puesto']+'</option>\n';
                    }

                    text +=    '                           </select>';
                    $('.puestos').html( text )

                    var razon = "";
                    razon += '<b>Razón</b> <select onchange="getPlazas()" class="form-control select2" id="razon" name="razon" style="width: 100%;">' +raz+'' ;
                    for(var i = 0; i < msg['razones'].length; i++)
                    {
                        razon += ' <option value="'+msg['razones'][i]['Id']+'" >'+msg['razones'][i]['nombre']+'</option>\n';
                    }
                    razon +=    '                           </select>';
                    $('.razones').html( razon );


                });



            $('.btnGuardar').html( ' <a onclick="update('+id+')" class="btn btn-primary ">Actualizar</a>' +
                ' <a onclick="cancel()" class="btn btn-danger">Cancelar</a>' )
        }


        function baja(id)
        {
            //preguntamos antes si queremos cargar el cuestionario de la visita
            var box = bootbox.confirm({
                title: "Baja de empleado",
                message: "¿Deseas dar de baja al empleado?.",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> No'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Si'
                    }
                },
                callback: function (result) {
                    console.log(result);

                    if(result == true)
                    {
                        var box = bootbox.confirm({
                            title: "Cuestionario de  Baja de Empleado",
                            message: "¿Deseas realizar el cuestionario de baja de empleado?.",
                            buttons: {
                                cancel: {
                                    label: '<i class="fa fa-times"></i> No'
                                },
                                confirm: {
                                    label: '<i class="fa fa-check"></i> Si'
                                }
                            },
                            callback: function (result) {
                                console.log(result);

                                if(result == true)
                                {
                                    // alert('realizar cuestionario')
                                    box.modal('hide');
                                    goCuestionario(id);

                                }else
                                {
                                    execBaja(id);
                                }

                            }
                        });

                        // alert('realizar cuestionario')
                      /*  */


                    }else
                    {

                    }

                }
            });
        }

        function execBaja(id)
        {
            bootbox.hideAll();
            var url = '{{route('bajaEmpleado')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  id  }
            })
                .done(function(msg){

                    var url = "";
                    url = '{{route('empleados.empleados')}}';//'visitas/final/'+post_id;//
                    window.location.href = url;

                });
        }

        function goCuestionario(id)
        {
            var text = "";
            $("#cuestionario").show();

            $("#listaEmpleados").hide();
            var url = '{{route('getCuestionario')}}';

            ;

            $.ajax({
                method: 'POST',
                url: url,
                data: {id:document.getElementById('id_cuestionario').value}
            })
                .done(function(msg){

                    console.log(msg['message']);
                    text += ' <div class="box box-default" >\n' +
                        '            <div class="box-header with-border">\n' +
                        '                <h3 class="box-title">Cuestionario de la Visita</h3>\n' +
                        '            </div>\n' +
                        '\n' +
                        '            <div class="box-body">\n' +
                        '                <div class="row">\n' +
                        '                   <div class="col-md-6">\n';


                    for(var i = 0; i < msg['message'].length; i++)
                    {
                        text +=  '                        <div class="form-group">\n' +
                            '                            <label >'+msg['message'][i]['pregunta']+'</label>\n';
                        if(msg['message'][i]['tipo'] == 0)
                        {
                            text +=   '  <input maxlength="100" type="text" id="abierta'+msg['message'][i]['Id']+'" class="form-control" onchange="setAbierta('+msg['message'][i]['Id']+','+id+')"  placeholder="Pregunta abierta">\n' ;
                        }else {

                            for(var j = 0; j < msg['message'][i]['respuestas'].length; j++)
                            {
                                text +=   '</br> <label> <input type="radio" name="optionsRadios'+msg['message'][i]['Id']+'" onclick="setOption('+msg['message'][i]['respuestas'][j]['Id']+','+msg['message'][i]['Id']+','+id+')" id="optionsRadios2" value="'+msg['message'][i]['respuestas'][j]['Id']+'"> '+msg['message'][i]['respuestas'][j]['respuesta']+' </label>\n' ;
                            }

                        }


                        text +=      '                        </div>';
                    }



                    text += '\n' +
                        '                                    </div>\n' +
                        '                                </div>\n' +
                        '                                <!-- /.row -->\n' +
                        '                            </div>\n' +
                        '                            <!-- /.box-body -->\n' +
                        '                            <div class="box-footer">\n' +
                        '                 <button   onclick="finalCuestionario('+id+')"  class="btn btn-success btn-block pull-right">Continuar</button>\n' +
                        '                            </div>\n' +
                        '                        </div>';

                    $('.cargaCuestionario').html( text );
                });


            //  document.getElementById("fotolist ").scrollIntoView();

        }

        function getPlazas()
        {
           // alert(document.getElementById('razon').value) plazas capturadas
            //vamos a obtener el listado de cosas para asignar al empleado
            var url = '{{route('cargaPlazas')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  document.getElementById('razon').value  }
            })
                .done(function(msg){

                    console.log(msg['puestos']);
                    var text = "";

                    text += '<b>Plazas</b> <select onchange="getTiendas()" class="form-control select2" id="plaza" name="plaza" style="width: 100%;">\n' +
                        '<option value="0" selected>Selecciona una plaza </option>' ;


                    for(var i = 0; i < msg['plazas'].length; i++)
                    {
                        text += ' <option value="'+msg['plazas'][i]['Id']+'">'+msg['plazas'][i]['plaza']+'</option>\n';
                    }

                    text +=    '                           </select>';
                    $('.plazas').html( text )


                });

        }

        function getTiendas()
        {
            // alert(document.getElementById('razon').value) plazas capturadas
            //vamos a obtener el listado de cosas para asignar al empleado
            var url = '{{route('cargaTiendas')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  document.getElementById('plaza').value  }
            })
                .done(function(msg){

                    console.log(msg['tiendas']);
                    var text = "";

                    text += '<b>Sucursales</b> <select  class="form-control select2" id="suc" name="suc" style="width: 100%;">\n' +
                        '<option value="0" selected>Selecciona una sucursal </option>' ;


                    for(var i = 0; i < msg['tiendas'].length; i++)
                    {
                        text += ' <option value="'+msg['tiendas'][i]['Id']+'">'+msg['tiendas'][i]['nombre']+'</option>\n';
                    }

                    text +=    '                           </select>';
                    $('.tiendas').html( text )


                });

        }

        function setFoto()
        {
            $avatarImage = $('#avatarImage');
            $avatarInput = $('#avatarInput');
            $avatarForm = $('#avatarForm');
            $rutaFoto = $('.rutaFoto');
            $avatarInput.click();
            $avatarInput.on('change', function () {
                var formData = new FormData();
                formData.append('photo', $avatarInput[0].files[0]);

                $('#modal-info').modal('toggle');

                $.ajax({
                    url: $avatarForm.attr('action') + '?' + $avatarForm.serialize(),
                    method: $avatarForm.attr('method'),
                    data: formData,
                    processData: false,
                    contentType: false
                }).done(function (msg) {
                    // alert(cam);
                    /*if (data.success)
                        $avatarImage.attr('src', data.path);*/
                    var URLdomain = window.location.host;
                    var pathImg = 'http://'+URLdomain+'/'+msg['path'];

                    $avatarImage.attr('src', pathImg)
                    console.log(msg['message']);
                    $('#modal-info').modal('toggle');

                    $('.foto_perfil').html('<input type="hidden" id="foto_perfil" value="' + msg['message'] + '" >')


                });
            });
        }


        function cancel()
        {
            //alert('OK')
            url = '{{route('empleados.empleados')}}';//'visitas/final/'+post_id;//
            window.location.href = url;

        }

        function save()
        {
            url_mail = '{{route('validaMail')}}';
            if(document.getElementById('inputEmail').value != "")
            {
                //validamos si existe el correo electrónico
                $.ajax({
                    method: 'POST',
                    url: url_mail,
                    data: {mail: document.getElementById('inputEmail').value  }
                })
                    .done(function(msg){

                        console.log(msg['message']);

                        if(msg['message'] != 0)
                        {
                            bootbox.alert('El correo electrónico ya existe, favor de capturar otro')
                        }else
                        {

                            validaEmpleado()
                        }

                    });

            }else
            {
                validaEmpleado()
            }

        }

        function validaEmpleado()
        {

            if(document.getElementById('inputEmail').value != "")
            {
                if(document.getElementById('inputName').value != "")
                {
                    if(document.getElementById('datepicker').value != "")
                    {
                        url = '{{route('addEmpleado')}}';
                        // alert('Muestra despues')

                        $.ajax({
                            method: 'POST',
                            url: url,
                            data: { clave: document.getElementById('inputClave').value,
                                nombre: document.getElementById('inputName').value,
                                apepat: document.getElementById('inputApePat').value,
                                apemat: document.getElementById('inputApeMat').value,
                                fhingreso: document.getElementById('datepicker').value,
                                empresas_Id: '{{auth()->user()->empresas_Id}}',
                                domicilio:document.getElementById('inputDom').value,
                                telefono: document.getElementById('inputTel').value,
                                curp:document.getElementById('inputCurp').value,
                                mail:document.getElementById('inputEmail').value,
                                nss:document.getElementById('inputNss').value,
                                foto: document.getElementById('foto_perfil').value,
                                puesto_Id: document.getElementById('puesto').value,
                                plaza_Id: document.getElementById('plaza').value,
                                razon_Id: document.getElementById('razon').value,
                                suc: document.getElementById('suc').value,
                                zona: document.getElementById('zona').value
                            }
                        })
                            .done(function(msg){

                                console.log(msg['message']);
                                var url = "";
                                url = '{{route('empleados.empleados')}}';//'visitas/final/'+post_id;//
                                window.location.href = url;

                            });
                    }else
                    {
                        bootbox.alert('Debes elegir una fecha de ingreso')
                    }




                }else
                {
                    bootbox.alert('Debes nombrar al empleado')
                }
            }else
            {
                bootbox.alert('Debes escribir un correo electrónico')
            }

        }


        function setAbierta(id, emp_id)
        {
            var abiertaId = "abierta"+id;

            //alert(document.getElementById(abiertaId).value);
            var url = '{{route('setCuestBaja')}}';

            $.ajax({
                method: 'POST',
                url: url,
                data: {pregunta:id, tipo:0, respuesta:document.getElementById(abiertaId).value, empleados_Id: emp_id}
            })
                .done(function(msg){
                    console.log(msg['message'])
                });
        }

        function setOption(respuesta, pregunta, emp_Id)
        {
            //alert(respuesta+'---'+ pregunta)
            var url = '{{route('setCuestBaja')}}';

            $.ajax({
                method: 'POST',
                url: url,
                data: {pregunta:pregunta, tipo:1, respuesta:respuesta, empleados_Id:emp_Id}
            })
                .done(function(msg){
                    console.log(msg['message'])

                });
        }

        function finalCuestionario(id)
        {
            $("#cuestionario").hide();

            $("#listaEmpleados").show();
            execBaja(id)

            //bootbox.alert("La baja del empleado ha sido exitosa");
        }

    </script>


@stop