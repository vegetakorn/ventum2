@extends('adminlte::page')
@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>Usuarios</h1>
@stop

@section('content')

    <div class="box" id="listaUsuarios">

        <div class="box-header">
            <button type="button" onclick="addUser()" class="btn  btn-success pull-right">Agregar Usuario</button>
        </div>
        <!-- /.box-header


         -->
        <div class="box-body">

            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Usuario</th>


                </tr>
                </thead>


                <tbody>
                @foreach($usuarios as $usuario)

                    <tr>
                        <td >
                            <!-- chat item -->
                            <div class="item">

                                <p class="message">
                                    <a href="#" class="name">
                                        <small class="text-muted pull-right">


                                            <a   ><img data-toggle="tooltip" onclick="editar({{$usuario->id}})" title="Editar" width="40" height="40" src="{{ asset('images/buttons/pencil.png')}}"  alt="Icono"></a >

                                            <a onclick="bajaUsuario({{$usuario->id}})"   ><img data-toggle="tooltip" title="Baja" width="40" height="40" src="{{ asset('images/buttons/stop.png')}}"  alt="Icono"></a >
                                        </small>


                                    </a>
                                    <a href="#" class="name">
                                        <h3><strong>{{$usuario->name}}  </strong> </h3>
                                    </a>
                                @if($usuario->Activo == 1)
                                    <h4>Activo</h4>
                                @else
                                    <h4>Inactivo</h4>
                                    @endif
                                    </p>
                                    <h6>{{$usuario->email}}&nbsp;</h6>
                                <input type="hidden" value="{{$usuario->email}}" id="emp{{$usuario->id}}" />
                                    </p>
                                    <!-- /.attachment -->
                            </div>
                            <!-- /.item -->
                        </td>

                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
       <!-- <div id="scanner-container"></div>
        <input type="button" id="btn" value="Start/Stop the scanner" />
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
        <div class="box box-warning" id="agregaUser">
            <div class="box-header with-border">
                <h3 class="box-title">Agregar Usuario</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form role="form">
                    <!-- text input -->
                    <div class="form-group">
                        <label>Empleado:</label>
                        <select onchange="setMail()" class="form-control select2"  id="inputEmp" style="width: 100%;">
                            <option value="0" >--elegir empleado--</option>
                            @foreach($empleados as $empleado)
                                <option value="{{$empleado->Id}}">{{$empleado->nombre}} {{$empleado->apepat}}  {{$empleado->apemat}}  </option>
                            @endforeach

                        </select>

                    </div>

                    <div class="form-group">
                        <label>E-Mail</label>
                        <input  type="email" disabled class="form-control" id="inputMail" placeholder="Correo del usuario, necesario para iniciar sesión" >
                    </div>
                    <div class="form-group">
                        <label>Contraseña</label>
                        <input type="password" class="form-control" id="inputPass" placeholder="Contraseña" >
                    </div>




                </form>
                <button onclick="saveUser()" class="btn  btn-success pull-right">Guardar</button>
                <button onclick="cancel()" class="btn  btn-danger pull-right">Cancelar</button>
            </div>
        </div>
        <!-- /.box-body -->
    <div class="box box-warning" id="editaUser">
        <div class="box-header with-border">
            <h3 class="box-title">Editar Usuario</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body ">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">General</a></li>
                    <li><a href="#tab_2" data-toggle="tab">Permisos</a></li>
                    <li><a href="#tab_3" data-toggle="tab">Widgets</a></li>
                    <li><a href="#tab_4" data-toggle="tab">Sucursales</a></li>
                    <li><a href="#tab_5" data-toggle="tab">Empleados</a></li>
                    <li><a href="#tab_6" data-toggle="tab">Colores</a></li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane active showEdicion" id="tab_1">

                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane showModulos" id="tab_2">

                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane showWidgets" id="tab_3">

                    </div>
                    <!-- /.tab-pane -->
                    <!-- /.tab-pane -->
                    <div class="tab-pane showSucursales" id="tab_4">
                        <!-- Profile Image -->
                        <div class="box box-primary">
                            <div class="box-body box-profile">

                                <h3 class="profile-username text-center">Asignar Tiendas</h3>
                                <h5 class=" text-center">Busca las tiendas que deseas asignar</h5>

                                <!--<p class="text-muted text-center">Software Engineer</p>-->

                                <ul class="list-group list-group-unbordered">

                                    <li class="list-group-item razonessuc">

                                    </li>
                                    <li class="list-group-item plazasuc">
                                        <input type="hidden" id="plazasuc" value="0" >
                                    </li>



                                </ul>
                                <button onclick="showSuc()" class="btn  btn-primary pull-right">Buscar</button>

                            </div>
                            <!-- /.box-body -->
                            <div class="getTiendas" >

                            </div>

                            <div class="sucAsignados">

                            </div>


                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <!-- /.tab-pane -->
                    <div class="tab-pane showEmpleados" id="tab_5">
                        <!-- Profile Image -->
                        <div class="box box-primary">
                            <div class="box-body box-profile">

                                <h3 class="profile-username text-center">Asignar Empleados</h3>
                                <h5 class=" text-center">Busca los empleados que deseas asignar</h5>

                                <!--<p class="text-muted text-center">Software Engineer</p>-->

                                <ul class="list-group list-group-unbordered">

                                    <li class="list-group-item puestos">

                                    </li>
                                    <li class="list-group-item razones">

                                    </li>
                                    <li class="list-group-item plazas">
                                        <input type="hidden" id="plaza" value="0" >
                                    </li>


                                </ul>
                                <button onclick="showEmps()" class="btn  btn-primary pull-right">Buscar</button>

                            </div>
                            <!-- /.box-body -->
                            <div class="getEmpleados" >

                            </div>

                            <div class="empAsignados">

                            </div>


                        </div>
                    </div>
                    <div class="tab-pane showColores" id="tab_6">

                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->

            </div>

        </div>
        <div class="updateEmp">

        </div>

        <button onclick="cancel()" class="btn  btn-danger pull-right">Cancelar</button>
    </div>

    <div class="id_emp" >
        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
        <input type="hidden" id="id_emp" value="0" >
    </div>


@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

@stop

@section('js')
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>

        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();

            //Initialize Select2 Elements
            $('.select2').select2()

            $('#example1').DataTable( {
                "pagingType": "full_numbers",
                "scrollY":        '60vh',
                "scrollCollapse": true,
                "language": {
                    "lengthMenu": "Mostrando _MENU_ registros por página ",
                    "zeroRecords": "Sin registros encontrados",
                    "info": "Mostrando página  _PAGE_ de _PAGES_ páginas",
                    "infoEmpty": "Sin regustros encontrados",
                    "infoFiltered": "(filtrados de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    }
                }
            } );
        } );
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
    <script src="{{ asset('bower_components/quagga/dist/quagga.min.js')}}"></script>
    <style>
        #interactive.viewport {position: relative; width: 100%; height: auto; overflow: hidden; text-align: center;}
        #interactive.viewport > canvas, #interactive.viewport > video {max-width: 100%;width: 100%;}
        canvas.drawing, canvas.drawingBuffer {position: absolute; left: 0; top: 0;}
    </style>
    <script type="text/javascript">
        $(function() {
            // Create the QuaggaJS config object for the live stream
            var liveStreamConfig = {
                inputStream: {
                    type : "LiveStream",
                    constraints: {
                        width: {min: 640},
                        height: {min: 480},
                        aspectRatio: {min: 1, max: 100},
                        facingMode: "environment" // or "user" for the front camera
                    }
                },
                locator: {
                    patchSize: "medium",
                    halfSample: true
                },
                numOfWorkers: (navigator.hardwareConcurrency ? navigator.hardwareConcurrency : 4),
                decoder: {
                    "readers":[
                        {"format":"ean_reader","config":{}}
                    ]
                },
                locate: true
            };
            // The fallback to the file API requires a different inputStream option.
            // The rest is the same
            var fileConfig = $.extend(
                {},
                liveStreamConfig,
                {
                    inputStream: {
                        size: 800
                    }
                }
            );
            // Start the live stream scanner when the modal opens
            $('#livestream_scanner').on('shown.bs.modal', function (e) {
                Quagga.init(
                    liveStreamConfig,
                    function(err) {
                        if (err) {
                            $('#livestream_scanner .modal-body .error').html('<div class="alert alert-danger"><strong><i class="fa fa-exclamation-triangle"></i> '+err.name+'</strong>: '+err.message+'</div>');
                            Quagga.stop();
                            return;
                        }
                        Quagga.start();
                    }
                );
            });

            // Make sure, QuaggaJS draws frames an lines around possible
            // barcodes on the live stream
            Quagga.onProcessed(function(result) {
                var drawingCtx = Quagga.canvas.ctx.overlay,
                    drawingCanvas = Quagga.canvas.dom.overlay;

                if (result) {
                    if (result.boxes) {
                        drawingCtx.clearRect(0, 0, parseInt(drawingCanvas.getAttribute("width")), parseInt(drawingCanvas.getAttribute("height")));
                        result.boxes.filter(function (box) {
                            return box !== result.box;
                        }).forEach(function (box) {
                            Quagga.ImageDebug.drawPath(box, {x: 0, y: 1}, drawingCtx, {color: "green", lineWidth: 2});
                        });
                    }

                    if (result.box) {
                        Quagga.ImageDebug.drawPath(result.box, {x: 0, y: 1}, drawingCtx, {color: "#00F", lineWidth: 2});
                    }

                    if (result.codeResult && result.codeResult.code) {
                        Quagga.ImageDebug.drawPath(result.line, {x: 'x', y: 'y'}, drawingCtx, {color: 'red', lineWidth: 3});
                    }
                }
            });

            // Once a barcode had been read successfully, stop quagga and
            // close the modal after a second to let the user notice where
            // the barcode had actually been found.
            Quagga.onDetected(function(result) {
                if (result.codeResult.code){
                    $('#scanner_input').val(result.codeResult.code);
                    Quagga.stop();
                    setTimeout(function(){ $('#livestream_scanner').modal('hide'); }, 1000);
                }
            });

            // Stop quagga in any case, when the modal is closed
            $('#livestream_scanner').on('hide.bs.modal', function(){
                if (Quagga){
                    Quagga.stop();
                }
            });

            // Call Quagga.decodeSingle() for every file selected in the
            // file input
            $("#livestream_scanner input:file").on("change", function(e) {
                if (e.target.files && e.target.files.length) {
                    Quagga.decodeSingle($.extend({}, fileConfig, {src: URL.createObjectURL(e.target.files[0])}), function(result) {alert(result.codeResult.code);});
                }
            });
        });

        $(document).ready(function() {

            $("#agregaUser").hide();
            $("#editaUser").hide();

        } );

        function showSuc()
        {


            var razones = document.getElementById('razonsuc').value;
            var plazas = document.getElementById('plazasuc').value;


            url = '{{route('showSuc')}}';
            // alert('Muestra despues')

            $.ajax({
                method: 'POST',
                url: url,
                data: { razon: razones, plaza: plazas  }
            })
                .done(function(msg){

                    console.log(msg['modEmp']);
                    var modEmp = "";
                    var id = document.getElementById('id_emp').value;
                    modEmp += '<div class="box box-warning" style="position: relative; left: 0px; top: 0px;">\n' +
                        '                                <div class="box-header ui-sortable-handle" style="cursor: move;">\n' +
                        '                                    <i class="ion ion-clipboard"></i>\n' +
                        '\n' +

                        '                                    <h3 class="box-title">Selecciona las sucursales a asignar</h3>\n' +
                        '\n' +
                        '                                </div>\n' +
                        '                                <!-- /.box-header -->\n' +
                        '                                <div class="box-body">\n' +
                        '                                    <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->\n' +
                        '                                    <ul class="todo-list ui-sortable">';
                    for(var i = 0; i < msg['modEmp'].length; i++) {

                        modEmp += '<li>\n' +
                            '                                            <!-- drag handle -->\n' +
                            '                                            <!-- todo text -->\n' +
                            '                     <input onclick="setTienda('+msg['modEmp'][i]['Id']+', '+id+')"  type="checkbox" id="suc'+msg['modEmp'][i]['Id']+'"  >\n' +
                            '                                            <span class="text">'+msg['modEmp'][i]['nombre']+'</span>\n' +
                            '                                            <!-- Emphasis label -->\n' +

                            '\n' +
                            '                                            <!-- General tools such as edit or delete-->\n' +
                            '                                            <div class="tools">\n' +

                            '                                            </div>\n' +
                            '                                        </li>';
                    }

                    modEmp += '</ul>\n' +
                        '                                </div>\n' +
                        '\n' +
                        '                            </div>';
                    $('.getTiendas').html( modEmp )


                });
        }

        function showEmps()
        {

            var puestos = document.getElementById('puesto').value;
            var razones = document.getElementById('razon').value;
            var plazas = document.getElementById('plaza').value;


            url = '{{route('showEmps')}}';
            // alert('Muestra despues')

            $.ajax({
                method: 'POST',
                url: url,
                data: {puesto: puestos, razon: razones, plaza: plazas  }
            })
                .done(function(msg){

                    console.log(msg['modEmp']);
                    var modEmp = "";
                    var id = document.getElementById('id_emp').value;
                    modEmp += '<div class="box box-warning" style="position: relative; left: 0px; top: 0px;">\n' +
                        '                                <div class="box-header ui-sortable-handle" style="cursor: move;">\n' +
                        '                                    <i class="ion ion-clipboard"></i>\n' +
                        '\n' +

                        '                                    <h3 class="box-title">Selecciona los empleados a asignar</h3>\n' +
                        '\n' +
                        '                                </div>\n' +
                        '                                <!-- /.box-header -->\n' +
                        '                                <div class="box-body">\n' +
                        '                                    <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->\n' +
                        '                                    <ul class="todo-list ui-sortable">';
                    for(var i = 0; i < msg['modEmp'].length; i++) {

                        modEmp += '<li>\n' +
                            '                                            <!-- drag handle -->\n' +
                            '                                            <!-- todo text -->\n' +
                            '                     <input onclick="setEmpleado('+msg['modEmp'][i]['Id']+', '+id+')"  type="checkbox" id="emp'+msg['modEmp'][i]['Id']+'"  >\n' +
                            '                                            <span class="text">'+msg['modEmp'][i]['nombre']+' '+msg['modEmp'][i]['apepat']+' '+msg['modEmp'][i]['apemat']+'</span>\n' +
                            '                                            <!-- Emphasis label -->\n' +

                            '\n' +
                            '                                            <!-- General tools such as edit or delete-->\n' +
                            '                                            <div class="tools">\n' +

                            '                                            </div>\n' +
                            '                                        </li>';
                    }

                    modEmp += '</ul>\n' +
                        '                                </div>\n' +
                        '\n' +
                        '                            </div>';
                    $('.getEmpleados').html( modEmp )


                });
        }

        function setMail()
        {
            //var mail = 'emp'+document.getElementById('inputEmp').value
            //document.getElementById('inputMail').value =   document.getElementById('emp1').value ;
            url = '{{route('getMailEmp')}}';
            // alert('Muestra despues')

            $.ajax({
                method: 'POST',
                url: url,
                data: {id: document.getElementById('inputEmp').value   }
            })
                .done(function(msg){

                    console.log(msg['mail']);
                    document.getElementById('inputMail').value =msg['mail']['mail'];

                });
        }

        function saveEditEmp(id)
        {
            //var mail = 'emp'+document.getElementById('inputEmp').value
            //document.getElementById('inputMail').value =   document.getElementById('emp1').value ;
            url = '{{route('saveEditEmp')}}';
            // alert('Muestra despues')
            if(document.getElementById('inputPassE').value != "")
            {
                $.ajax({
                    method: 'POST',
                    url: url,
                    data: {pass: document.getElementById('inputPassE').value, id_emp : id   }
                })
                    .done(function(msg){

                        var url = '{{route('usuarios.lista')}}';//'visitas/final/'+post_id;//
                        window.location.href = url;
                    });
            }else
            {
                var url = '{{route('usuarios.lista')}}';//'visitas/final/'+post_id;//
                window.location.href = url;
            }

        }


        function addUser()
        {
            //alert('OK')
            $("#listaUsuarios").hide();
            $("#agregaUser").show();



        }


        function saveUser()
        {

            if(document.getElementById('inputEmp').value != 0)
            {
                if(document.getElementById('inputPass').value != "")
                {

                    url = '{{route('saveUsuario')}}';

                    var emp = $("#inputEmp :selected").text();
                  //  alert(emp)
                    $.ajax({
                        method: 'POST',
                        url: url,
                        data: {emp_id: document.getElementById('inputEmp').value ,
                                mail:  document.getElementById('inputMail').value ,
                                pass: document.getElementById('inputPass').value,
                                name: emp}

                    })
                        .done(function(msg){
                            console.log(msg['mail']);
                           if(msg['mail'] == 0)
                            {
                                url = '{{route('usuarios.lista')}}';//'visitas/final/'+post_id;//
                                window.location.href = url;
                            }else
                            {
                                bootbox.alert("Ya existe este usuario, favor de elegir otro empleado");
                            }


                        });

                }else
                {
                    bootbox.alert("Debes escribir una contraseña para el usuario");
                }


            }else
            {
                bootbox.alert("Debes seleccionar un empleado para crear");
            }

        }


        function cancel()
        {
            //alert('OK')
            url = '{{route('usuarios.lista')}}';//'visitas/final/'+post_id;//
            window.location.href = url;

        }


        function editar(id)
        {
            $("#listaUsuarios").hide();
            $("#editaUser").show();

            $('.updateEmp').html( '<button onclick="saveEditEmp('+id+')" class="btn  btn-success pull-right">Guardar</button>' )
            $('.id_emp').html( ' <input type="hidden" id="id_emp" value="'+id+'" >' )
            url = '{{route('getInfoUsuario')}}';


            //  alert(emp)
            $.ajax({
                method: 'POST',
                url: url,
                data: {user_id:id}

            })
                .done(function(msg){
                    console.log(msg['modulos']);
                    var text = "";

                    text +=
                        '        <!-- /.box-header -->\n' +
                        '        <div class="box-body">\n' +
                        '            <form role="form">\n' +
                        '                <!-- text input -->\n' +
                        '                <div class="form-group">\n' +
                        '                    <label>Empleado: </label>\n' +
                        '                   <label>'+msg['user']['name']+'</label>\n' +
                        '                </div>\n' +
                        '                <div class="form-group">\n' +
                        '                    <label>Mail:</label>\n' +
                        '                    <label>'+msg['user']['mail']+'</label> \n' +
                        '                </div>\n' +
                        '                <!-- textarea -->\n' +
                        '                <div class="form-group">\n' +
                        '                    <label>Contraseña</label>\n' +
                        '                    <input id="inputPassE" type="password" value="" class="form-control" rows="3" placeholder="Solo escribir si deseas cambiar contraseña">\n' +
                        '                </div>\n' +
                        '        </div>';

                    $('.showEdicion').html( text )

                    var txtMods = "";
                    for(var i = 0; i < msg['modulos'].length; i++) {

                        var mod = msg['modulos'][i]['Id'];
                        var nombreMod= msg['modulos'][i]['modulo'];

                        txtMods += '<div class=" checkbox">\n' +
                            '                     <label class="checkbox-inline">\n' +
                            '                     <input onclick="setModule('+mod+', '+id+')" type="checkbox" id="mod'+mod+'"  >\n' +
                            '                     '+nombreMod+'\n' +
                            '                     </label>\n' +
                            '                     </div>';

                    }

                    $('.showModulos').html( txtMods )

                    console.log(msg['modSel']);
                    var modSel = "";
                    for(var i = 0; i < msg['modSel'].length; i++) {

                        modSel = $('#mod'+msg['modSel'][i]['modulos_Id'])
                        modSel.prop('checked', true);

                    }

                    var txtWid = "";

                    for(var i = 0; i < msg['widgets'].length; i++) {

                        var mod = msg['widgets'][i]['Id'];
                        var nombreMod= msg['widgets'][i]['widget'];

                        txtWid += '<div class=" checkbox">\n' +
                            '                     <label class="checkbox-inline">\n' +
                            '                     <input onclick="setWidget('+mod+', '+id+')" type="checkbox" id="wid'+mod+'"  >\n' +
                            '                     '+nombreMod+'\n' +
                            '                     </label>\n' +
                            '                     </div>';

                    }

                    $('.showWidgets').html( txtWid );
                    var modWid = "";
                    for(var i = 0; i < msg['modWid'].length; i++) {

                        modWid = $('#wid'+msg['modWid'][i]['widgets_Id'])
                        modWid.prop('checked', true);

                    }



                   /* var txtSucs = "";
                    for(var i = 0; i < msg['tiendas'].length; i++) {

                        var suc = msg['tiendas'][i]['Id'];
                        var nombreMod= msg['tiendas'][i]['nombre'];

                        txtSucs += '<div class=" checkbox">\n' +
                            '                     <label class="checkbox-inline">\n' +
                            '                     <input onclick="setTienda('+suc+', '+id+')"  type="checkbox" id="suc'+suc+'"  >\n' +
                            '                     '+nombreMod+'\n' +
                            '                     </label>\n' +
                            '                     </div>';

                    }

                    $('.showSucursales').html( txtSucs );*/
                   /* var modSuc = "";
                    for(var i = 0; i < msg['modSuc'].length; i++) {

                        modSuc = $('#suc'+msg['modSuc'][i]['tiendas_Id'])
                        modSuc.prop('checked', true);
                    }*/

                  /*  var txtEmps = "";
                    for(var i = 0; i < msg['empleados'].length; i++) {

                        var emp = msg['empleados'][i]['Id'];
                        var nombreEmp = msg['empleados'][i]['nombre']+" "+msg['empleados'][i]['apepat']+" "+msg['empleados'][i]['apemat'];

                        if(msg['user']['empleados_Id'] != emp)
                        {
                            txtEmps += '<div class=" checkbox">\n' +
                                '                     <label class="checkbox-inline">\n' +
                                '                     <input onclick="setEmpleado('+emp+', '+id+')"  type="checkbox" id="emp'+emp+'"  >\n' +
                                '                     '+nombreEmp+'\n' +
                                '                     </label>\n' +
                                '                     </div>';
                        }


                    }

                    $('.showEmpleados').html( txtEmps );*/
                  /**Empleadis y Razones**/
                      var textPto = "";
                      textPto += '<b>Puesto</b> <select class="form-control select2" id="puesto" name="puesto" style="width: 100%;">\n' +
                          '<option value="0" selected><--Todos los puestos--> </option>' ;


                        for(var i = 0; i < msg['puestos'].length; i++)
                        {
                            textPto += ' <option value="'+msg['puestos'][i]['Id']+'">'+msg['puestos'][i]['puesto']+'</option>\n';
                        }

                        textPto +=    '                           </select>';
                        $('.puestos').html( textPto );

                        var razon = "";
                        razon += '<b>Razón</b> <select onchange="getPlazas()" class="form-control select2" id="razon" name="razon" style="width: 100%;">' +
                            '<option value="0" selected><--Todas las razones--> </option>' ;
                        for(var i = 0; i < msg['razones'].length; i++)
                        {
                            razon += ' <option value="'+msg['razones'][i]['Id']+'" >'+msg['razones'][i]['nombre']+'</option>\n';
                        }
                        razon +=    '                           </select>';
                        $('.razones').html( razon )

                    var modEmp = "";
                        modEmp += '<div class="box box-primary" style="position: relative; left: 0px; top: 0px;">\n' +
                            '                                <div class="box-header ui-sortable-handle" style="cursor: move;">\n' +
                            '                                    <i class="ion ion-clipboard"></i>\n' +
                            '\n' +
                            '                                    <h3 class="box-title">Empleados Asignados</h3>\n' +
                            '\n' +
                            '                                </div>\n' +
                            '                                <!-- /.box-header -->\n' +
                            '                                <div class="box-body">\n' +
                            '                                    <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->\n' +
                            '                                    <ul class="todo-list ui-sortable">';
                    for(var i = 0; i < msg['modEmp'].length; i++) {

                       modEmp += '<li>\n' +
                           '                                            <!-- drag handle -->\n' +
                           '                                            <!-- todo text -->\n' +
                           '                                            <span class="text">'+msg['modEmp'][i]['nombre']+' '+msg['modEmp'][i]['apepat']+' '+msg['modEmp'][i]['apemat']+'</span>\n' +
                           '                                            <!-- Emphasis label -->\n' +

                           '\n' +
                           '                                            <!-- General tools such as edit or delete-->\n' +
                           '                                            <div class="tools">\n' +
                           '                                                <i onclick="delEmpleado('+msg['modEmp'][i]['Id']+','+id+')" class="fa fa-trash-o"></i>\n' +
                           '                                            </div>\n' +
                           '                                        </li>';
                    }

                    modEmp += '</ul>\n' +
                        '                                </div>\n' +
                        '\n' +
                        '                            </div>';
                    $('.empAsignados').html( modEmp )
                  /**fin empleados**/
                    /**Sucursales y Razones**/
                    /*var modEmp = "";
                    for(var i = 0; i < msg['modEmp'].length; i++) {

                        modEmp = $('#emp'+msg['modEmp'][i]['empleados_Id'])
                        modEmp.prop('checked', true);
                    }*/


                    var txtSucs = "";
                    txtSucs += '<div class="box box-primary" style="position: relative; left: 0px; top: 0px;">\n' +
                        '                                <div class="box-header ui-sortable-handle" style="cursor: move;">\n' +
                        '                                    <i class="ion ion-clipboard"></i>\n' +
                        '\n' +
                        '                                    <h3 class="box-title">Sucursales Asignadas</h3>\n' +
                        '\n' +
                        '                                </div>\n' +
                        '                                <!-- /.box-header -->\n' +
                        '                                <div class="box-body">\n' +
                        '                                    <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->\n' +
                        '                                    <ul class="todo-list ui-sortable">';
                    for(var i = 0; i < msg['modSuc'].length; i++) {

                        var suc = msg['modSuc'][i]['Id'];
                        var nombreMod= msg['modSuc'][i]['nombre'];

                        /*txtSucs += '<div class=" checkbox">\n' +
                            '                     <label class="checkbox-inline">\n' +
                            '                     <input onclick="setTienda('+suc+', '+id+')"  type="checkbox" id="suc'+suc+'"  >\n' +
                            '                     '+nombreMod+'\n' +
                            '                     </label>\n' +
                            '                     </div>';*/

                        txtSucs += '<li>\n' +
                            '                                            <!-- drag handle -->\n' +
                            '                                            <!-- todo text -->\n' +
                            '                                            <span class="text">'+nombreMod+'</span>\n' +
                            '                                            <!-- Emphasis label -->\n' +

                            '\n' +
                            '                                            <!-- General tools such as edit or delete-->\n' +
                            '                                            <div class="tools">\n' +
                            '                                                <i onclick="delTienda('+msg['modSuc'][i]['Id']+','+id+')" class="fa fa-trash-o"></i>\n' +
                            '                                            </div>\n' +
                            '                                        </li>';

                    }

                    txtSucs += '</ul>\n' +
                        '                                </div>\n' +
                        '\n' +
                        '                            </div>';

                    $('.sucAsignados').html( txtSucs );

                    var razonsuc = "";
                    razonsuc += '<b>Razón</b> <select onchange="getPlazasSuc()" class="form-control select2" id="razonsuc" name="razonsuc" style="width: 100%;">' +
                        '<option value="0" selected><--Todas las razones--> </option>' ;
                    for(var i = 0; i < msg['razones'].length; i++)
                    {
                        razonsuc += ' <option value="'+msg['razones'][i]['Id']+'" >'+msg['razones'][i]['nombre']+'</option>\n';
                    }
                    razonsuc +=    '                           </select>';
                    $('.razonessuc').html( razonsuc )
                    /**fin sucursales**/
                    var txtCol = "";
                    for(var i = 0; i < msg['colores'].length; i++) {

                        var col = msg['colores'][i]['Id'];
                        var nombreCol = msg['colores'][i]['ejemplo'];

                        txtCol += '<div class=" checkbox">\n' +
                            '                     <label style="background-color: '+msg['colores'][i]['logo']+'"  class="checkbox-inline btn-block">\n' +
                            '                     <input  onclick="setColor('+col+', '+id+')"  type="radio" name="color" id="col'+col+'"  >\n' +
                            '                     '+nombreCol+'\n' +
                            '                     </label>\n' +
                            '                     </div>';

                    }
                    $('.showColores').html( txtCol );
                    var modCol = "";
                    for(var i = 0; i < msg['modCol'].length; i++) {

                        modCol = $('#col'+msg['modCol'][i]['colores_Id'])
                        modCol.prop('checked', true);
                    }




                });


        }

        function getPlazas()
        {
            // alert(document.getElementById('razon').value) plazas capturadas
            //vamos a obtener el listado de cosas para asignar al empleado
            var url = '{{route('cargaPlazas')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  document.getElementById('razon').value  }
            })
                .done(function(msg){

                    console.log(msg['puestos']);
                    var text = "";

                    text += '<b>Plazas</b> <select  class="form-control select2" id="plaza" name="plaza" style="width: 100%;">\n' +
                        '<option value="0" selected><--Todas las plazas--> </option>' ;


                    for(var i = 0; i < msg['plazas'].length; i++)
                    {
                        text += ' <option value="'+msg['plazas'][i]['Id']+'">'+msg['plazas'][i]['plaza']+'</option>\n';
                    }

                    text +=    '                           </select>';
                    $('.plazas').html( text )


                });

        }

        function getPlazasSuc()
        {
            // alert(document.getElementById('razon').value) plazas capturadas
            //vamos a obtener el listado de cosas para asignar al empleado
            var url = '{{route('cargaPlazas')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  document.getElementById('razonsuc').value  }
            })
                .done(function(msg){

                    console.log(msg['puestos']);
                    var text = "";

                    text += '<b>Plazas</b> <select  class="form-control select2" id="plazasuc" name="plazasuc" style="width: 100%;">\n' +
                        '<option value="0" selected><--Todas las plazas--> </option>' ;


                    for(var i = 0; i < msg['plazas'].length; i++)
                    {
                        text += ' <option value="'+msg['plazas'][i]['Id']+'">'+msg['plazas'][i]['plaza']+'</option>\n';
                    }

                    text +=    '                           </select>';
                    $('.plazasuc').html( text )


                });

        }

        function getTiendas()
        {
            // alert(document.getElementById('razon').value) plazas capturadas
            //vamos a obtener el listado de cosas para asignar al empleado
            var url = '{{route('cargaTiendas')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  document.getElementById('plaza').value  }
            })
                .done(function(msg){

                    console.log(msg['tiendas']);
                    var text = "";

                    text += '<b>Sucursales</b> <select  class="form-control select2" id="suc" name="suc" style="width: 100%;">\n' +
                        '<option value="0" selected><--Todas las tiendas--> </option>' ;


                    for(var i = 0; i < msg['tiendas'].length; i++)
                    {
                        text += ' <option value="'+msg['tiendas'][i]['Id']+'">'+msg['tiendas'][i]['nombre']+'</option>\n';
                    }

                    text +=    '                           </select>';
                    $('.tiendas').html( text )


                });

        }

        function bajaUsuario(id)
        {
            //alert(id);
            var box = bootbox.confirm({
                title: "Baja de Usuario",
                message: "¿Deseas dar de baja al usuario?, Todas sus asignaciones serán borradas y no podrán recuperarse.",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> No'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Si'
                    }
                },
                callback: function (result) {
                    console.log(result);

                    if(result == true)
                    {
                        // alert('realizar cuestionario')
                        box.modal('hide');

                        url = '{{route('bajaUser')}}';
                        // alert('Muestra despues')

                        $.ajax({
                            method: 'POST',
                            url: url,
                            data: {user_id:id}
                        })
                            .done(function(msg){

                                console.log(msg['message']);
                                url_lista = route('usuarios.lista');
                                window.location.href = url_lista;

                            });


                    }
                }
            });

        }


        function setModule(id, user_id)
        {
            var mod = $("#mod"+id);
            url = '{{route('addModEmpleado')}}';
            // alert('Muestra despues')

            $.ajax({
                method: 'POST',
                url: url,
                data: {user_id:user_id, modulos_id: id, type:mod.is( ":checked" )}
            })
                .done(function(msg){

                    console.log(msg['message']);

                });
        }


        function setWidget(id, user_id)
        {
            var mod = $("#wid"+id);
            url = '{{route('addWidEmpleado')}}';
            // alert('Muestra despues')

            $.ajax({
                method: 'POST',
                url: url,
                data: {user_id:user_id, widgets_id: id, type:mod.is( ":checked" )}
            })
                .done(function(msg){

                    console.log(msg['message']);

                });
        }

        function setTienda(id, user_id)
        {
            var mod = $("#suc"+id);
            url = '{{route('addSucEmpleado')}}';
            // alert('Muestra despues')

            $.ajax({
                method: 'POST',
                url: url,
                data: {user_id:user_id, tiendas_id: id, type:mod.is( ":checked" )}
            })
                .done(function(msg){

                    console.log(msg['message']);

                });
        }

        function setEmpleado(id, user_id)
        {
            var mod = $("#emp"+id);
            url = '{{route('addEmpEmpleado')}}';
            // alert('Muestra despues')

            $.ajax({
                method: 'POST',
                url: url,
                data: {user_id:user_id, empleados_id: id, type:mod.is( ":checked" )}
            })
                .done(function(msg){

                    console.log(msg['message']);
                   // editar(user_id);

                });
        }


        function delEmpleado(id, user_id)
        {
            var mod = $("#emp"+id);
            url = '{{route('addEmpEmpleado')}}';
            // alert('Muestra despues')

            $.ajax({
                method: 'POST',
                url: url,
                data: {user_id:user_id, empleados_id: id, type:false}
            })
                .done(function(msg){

                    console.log(msg['message']);
                    editar(user_id);

                });
        }

        function delTienda(id, user_id)
        {

            url = '{{route('addSucEmpleado')}}';
            // alert('Muestra despues')

            $.ajax({
                method: 'POST',
                url: url,
                data: {user_id:user_id, tiendas_id: id, type:false}
            })
                .done(function(msg){

                    console.log(msg['message']);
                    editar(user_id);

                });
        }

        function setColor(id, user_id)
        {

            url = '{{route('addColEmpleado')}}';
            // alert('Muestra despues')

            $.ajax({
                method: 'POST',
                url: url,
                data: {user_id:user_id, colores_id: id}
            })
                .done(function(msg){

                    console.log(msg['message']);

                });
        }

    </script>

@stop