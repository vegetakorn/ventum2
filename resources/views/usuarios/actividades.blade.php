@extends('adminlte::page')
@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>Mis Actividades</h1>
@stop

@section('content')

    <!-- /.box -->
    <div class="row" id="perfilUsuario">

            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li  class="active" ><a href="#actividades" data-toggle="tab">Actividades</a></li>
                </ul>
                <div class="tab-content">

                    <div class="active tab-pane" id="actividades">
                        <div class="listaActividades">

                        </div>
                    </div>

                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->

        <!-- /.col -->
    </div>
    <div class="foto_perfil" >
        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
        <input type="hidden" id="foto_perfil" value="empleado.jpg" >
    </div>
    <div id="editTodo">
        <!-- Widget: user widget style 1 -->
        <div class="box box-widget widget-user-2 infoTodo">

        </div>
        <!-- /.widget-user -->
    </div>

    <!--ZONA DE EDICION DE ACTIVIDAD-->
    <div class="edit" >

    </div>




@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

@stop

@section('js')
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script>


    </script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()


            //Date picker
            $('#datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            })


        })


        $(document).ready(function() {

            // $("#agregarEmpleado").hide();
            //$("#agregaInfo").hide();
            url_mail = '{{route('todosPerfil')}}';

            //validamos si existe el correo electrónico
            getTodos()

        } );


        function getTodos()
        {


            $.ajax({
                method: 'POST',
                url: url_mail,
                data: {id:0 }
            })
                .done(function(msg){

                    console.log(msg['actividades']);
                    var text = "";
                    var textAct = "";

                    for(var i = 0; i < msg['todos'].length; i++)
                    {

                        var foto = '{{ asset('uploads/FotoTodo/')}}';
                        foto += "/"+msg['todos'][i]['Imagen'];
                        text += ' <div class="box-body">\n' +
                            '                            <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">\n' +
                            '                               '+msg['todos'][i]['Categoria']+' ('+msg['todos'][i]['Checklist']+') ' +'\n' +
                            '                            </h4>\n' +
                            '                            <div class="media">\n' +
                            '                                <div class="media-left">\n' +
                            '                                    <a  class="ad-click-event">\n' +
                            '                                        <img src="'+foto+'" alt="Imagen de To-Do" class="media-object" style="width: 150px;height: auto;border-radius: 4px;box-shadow: 0 1px 3px rgba(0,0,0,.15);">\n' +
                            '                                    </a>\n' +
                            '                                </div>\n' +
                            '                                <div class="media-body">\n' +
                            '                                    <div class="clearfix">\n' +
                            '                                        <p class="pull-right">\n' +
                            '                                            <a onclick="showInfoToDo('+msg['todos'][i]['Id']+', '+msg['todos'][i]['campos_Id']+','+msg['todos'][i]['visitas_Id']+')"  class="btn btn-success btn-sm ad-click-event">\n' +
                            '                                                Atender\n' +
                            '                                            </a>\n' +
                            '                                        </p>\n' +
                            '\n' +
                            '                                        <h4 style="margin-top: 0">'+msg['todos'][i]['Campo']+'</h4>\n' +
                            '\n' +
                            '                                        <p>'+msg['todos'][i]['Descripcion']+'</p>\n' +
                            '                                        <p>Sucursal: '+msg['todos'][i]['Tienda']+'</p>\n' +
                            '                                        <p>Tipo: '+msg['todos'][i]['Tipo']+'</p>\n' +
                            '                                        <p style="margin-bottom: 0">\n' +
                            '                                            <i class="fa fa-clock-o margin-r5"></i> '+msg['todos'][i]['Dias']+' días de antigüedad\n ('+ msg['todos'][i]['Fecha'] + ')' +
                            '                                        </p>\n' +
                            '                                        <hr>\n' +
                            '                                        <p style="margin-bottom: 0">\n' +
                            '                                            <button type="button" class="btn btn-block btn-'+msg['todos'][i]['Status'][1]+' btn-xs">'+msg['todos'][i]['Status'][0]+'</button>\n' +
                            '                                        </p>\n' +
                            '                                    </div>\n' +
                            '                                </div>\n' +
                            '                            </div>\n' +
                            '                        </div>';
                    }
                    $('.listaTodos').html (text);
                    for(var i = 0; i < msg['actividades'].length; i++)
                    {
                        var fotoA = '{{ asset('uploads/Actividades/')}}';
                        fotoA += "/"+msg['actividades'][i]['foto'];
                        textAct += ' <div class="box-body">\n' +
                            '                            <h4 onclick="editItem('+msg['actividades'][i]['Id']+')"  style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">\n' +
                            '                               '+msg['actividades'][i]['actividad']+'\n' +
                            '                            </h4>\n' +
                            '                            <div class="media">\n' +
                            '                                <div class="media-left">\n' +
                            '                                    <a  class="ad-click-event">\n' +
                            '                                        <img onclick="editItem('+msg['actividades'][i]['Id']+')" src="'+fotoA+'" alt="Imagen de To-Do" class="media-object" style="width: 150px;height: auto;border-radius: 4px;box-shadow: 0 1px 3px rgba(0,0,0,.15);">\n' +
                            '                                    </a>\n' +
                            '                                </div>\n' +
                            '                                <div class="media-body">\n' +
                            '                                    <div class="clearfix">\n' +
                            '                                        <p class="pull-right">\n' +
                            '                                            <a onclick="editItem('+msg['actividades'][i]['Id']+')"  class="btn btn-success btn-sm ad-click-event">\n' +
                            '                                                Atender\n' +
                            '                                            </a>\n' +
                            '                                        </p>\n' +
                            '\n' +
                            '                                        <h4 style="margin-top: 0">'+msg['actividades'][i]['descripcion']+'</h4>\n' +
                            '\n' +

                            '                                        <p>Creador: '+msg['actividades'][i]['creador']+'</p>\n' +
                            '                                        <p>Avance: '+msg['actividades'][i]['Avance']+'%</p>\n' +
                            '                                        <p style="margin-bottom: 0">\n' +
                            '                                            <i class="fa fa-clock-o margin-r5"></i> '+ msg['actividades'][i]['Dias'] +' días de antigüedad   \n ('+ msg['actividades'][i]['fhinicio'] + ')' +
                            '                                        </p>\n' +
                            '                                        <hr>\n' +
                            '                                        <p style="margin-bottom: 0">\n' +

                            '                                        </p>\n' +
                            '                                    </div>\n' +
                            '                                </div>\n' +
                            '                            </div>\n' +
                            '                        </div>';
                    }
                    $('.listaActividades').html (textAct);




                });
        }


        function setFoto()
        {
            $avatarImage = $('#avatarImage');
            $avatarInput = $('#avatarInput');
            $avatarForm = $('#avatarForm');
            $rutaFoto = $('.rutaFoto');
            $avatarInput.click();
            $avatarInput.on('change', function () {
                var formData = new FormData();
                formData.append('photo', $avatarInput[0].files[0]);

                $('#modal-info').modal('toggle');

                $.ajax({
                    url: $avatarForm.attr('action') + '?' + $avatarForm.serialize(),
                    method: $avatarForm.attr('method'),
                    data: formData,
                    processData: false,
                    contentType: false
                }).done(function (msg) {
                    // alert(cam);
                    /*if (data.success)
                        $avatarImage.attr('src', data.path);*/
                    var URLdomain = window.location.host;
                    var pathImg = 'http://'+URLdomain+'/'+msg['path'];

                    $avatarImage.attr('src', pathImg)
                    console.log(msg['message']);
                    $('#modal-info').modal('toggle');

                    $('.foto_perfil').html('<input type="hidden" id="foto_perfil" value="' + msg['message'] + '" >')


                });
            });
        }

        function save()
        {
            url_mail = '{{route('validaMail')}}';
            if(document.getElementById('inputEmail').value != "")
            {
                //validamos si existe el correo electrónico
                $.ajax({
                    method: 'POST',
                    url: url_mail,
                    data: {mail: document.getElementById('inputEmail').value  }
                })
                    .done(function(msg){

                        console.log(msg['message']);

                        if(msg['message'] != 0)
                        {
                            //bootbox.alert('El correo electrónico ya existe, favor de capturar otro')
                            validaEmpleado(1)//ya existe el email, no se actualiza
                        }else
                        {

                            validaEmpleado(0)//no existe el email, se actualizacon el nuevo email.
                        }

                    });

            }else
            {
                validaEmpleado()
            }

        }

        function validaEmpleado(type)
        {

            if(document.getElementById('inputEmail').value != "")
            {
                if(document.getElementById('inputName').value != "")
                {
                    url = '{{route('updateUser')}}';
                    // alert('Muestra despues')
                    var mail = '{{$empleado->mail}}';
                    if(type != 1)
                    {
                        mail = document.getElementById('inputEmail').value;
                    }
                    $.ajax({
                        method: 'POST',
                        url: url,
                        data: {id: document.getElementById('inputId').value,
                            nombre: document.getElementById('inputName').value,
                            apepat: document.getElementById('inputApePat').value,
                            apemat: document.getElementById('inputApeMat').value,
                            domicilio:document.getElementById('inputDom').value,
                            telefono: document.getElementById('inputTel').value,
                            curp:document.getElementById('inputCurp').value,
                            mail:mail,
                            nss:document.getElementById('inputNss').value,
                            foto: document.getElementById('foto_perfil').value
                        }
                    })
                        .done(function(msg){
                            if(type != 1)
                            {
                                bootbox.alert('Datos actualizados con éxito');
                            }else
                            {
                                bootbox.alert('Datos actualizados con éxito');
                            }
                        });
                }else
                {
                    bootbox.alert('Debes nombrar al empleado')
                }
            }else
            {
                bootbox.alert('Debes escribir un correo electrónico')
            }
        }

        function showInfoToDo(campo, campos_id, vis)
        {
            $("#perfilUsuario").hide();

            $("#editTodo").show();

            var text = "";
            var URLdomain = window.location.host;
            var pathImg = 'http://'+URLdomain+'/uploads/FotoTodo/';

            var url_cam = '{{ asset('images/buttons/camera.png')}}';

            url = '{{route('getInfoTodo')}}';

            $.ajax({
                method: 'POST',
                url: url,
                data: {campo_Id:campo}
            })
                .done(function(msg){
                    console.log(msg['message']);

                    var status_id = msg['message']['Status'];
                    var status_name = "";
                    switch(status_id)
                    {
                        case 151:
                            status_name = "Nuevo";
                            break;
                        case 152:
                            status_name = "En curso";
                            break;
                        case 153:
                            status_name = "Pendiente";
                            break;
                        case 154:
                            status_name = "Concluida";
                            break;
                    }
                    var url_fot = '{{route('regFotoCampo')}}';
                    var url_fot_com = '{{route('regFotoCampoCom')}}';

                    text += '<div class="widget-user-header bg-blue">\n' +
                        '    <form action="'+url_fot+'" method="post" style="display: none" id="avatarFormT'+campos_id+'">'+
                        '       <input type="file" id="avatarInputT'+campos_id+'" name="photo">'+
                        '    </form>'+
                        '    <div class="widget-user-image rutaFotoT">'+
                        '   <img onclick="setFotoCampoTodo('+campos_id+','+vis+')"  src="'+pathImg+msg['message']['ImagenIni']+'"id="avatarImageT'+campos_id+'">'+
                        '    </div>'+
                        // '                    <div class="widget-user-image"  >\n' +
                        //'                        <img  src="'+pathImg+msg['message']['ImagenIni']+'" alt="User Avatar">\n' +
                        //'                    </div>\n' +
                        '                    <!-- /.widget-user-image -->\n' +
                        '\n' +
                        '                    <h3  class="widget-user-username nombre_campo">'+msg['message']['nombre']+'</h3>\n' +
                        '                    <div style="padding-top: 20px">\n' +
                        '                        <strong><i class="fa fa-book margin-r-5"></i> Descripción</strong>\n' +
                        '                        <input onclick="clearComentTodo('+campo+')" id="comentario'+campo+'" type="text" value="'+msg['message']['descripcion']+'" class="form-control">\n' +
                        '                        <strong><i class="fa fa-flag-o margin-r-5"></i> Status</strong>\n' +
                        '                        <select class="form-control select2" id="stat'+campo+'" name="status" style="width: 100%;">\n' +
                        '                            <option value="'+status_id+'" selected="selected">'+status_name+'</option>\n' +
                        '                            <option  value="152">En Curso</option>\n' +
                        '                            <option value="153">Pendiente</option>\n' +
                        '                            <option value="154">Concluida</option>\n' +
                        '\n' +
                        '                        </select>\n' +
                        '                        <strong><i class="fa fa-commenting margin-r-5"></i> Comentarios</strong>\n' +
                        '                        <table width="100%">\n' +
                        '                            <tr>\n' +
                        '                                <td width="10%">\n' +
                        //'                                    <img  class="direct-chat-img" src="'+url_cam+'" >\n' +
                        '    <form action="'+url_fot_com+'" method="post" style="display: none" id="avatarFormTc'+campo+'">\n'+
                        '       <input type="file" class="direct-chat-img" id="avatarInputTc'+campo+'" name="photo">\n'+
                        '    </form>\n'+
                        '    <div class="widget-user-image rutaFotoTc">\n'+
                        '   <img onclick="setFotoCampoTodoCom('+campo+','+vis+')"  src="'+url_cam+'" id="avatarImageTc'+campo+'">\n'+
                        '    </div>\n'+
                        '                                </td>\n' +
                        '                                <td width="80%">\n' +
                        '                                    <input id="comvalue'+campo+'" type="text" placeholder="comentario" class="form-control">\n' +
                        '                                </td>\n' +
                        '                                <td width="10%">\n' +
                        '                                <input type="hidden" id="fotocomid'+campo+'" value="no_imagen.jpg">\n' +
                        '                                    <a onclick="setTodoCom('+campo+')"   class="btn btn-success btn-block pull-right">Agregar</a>\n' +
                        '                                </input>\n' +
                        '                            </tr>\n' +
                        '                        </table>\n' +
                        '                    </div>\n' +
                        '\n' +
                        '                </div>';
                    text += '<div class="box-footer no-padding">\n' +
                        '\n' +
                        '<div class="setListCom">'+

                        '</div >'+
                        '                    <a onclick="saveTodo('+campo+')"   class="btn btn-primary btn-block pull-right">Regresar</a>\n' +
                        ' </div>';

                    $('.infoTodo').html( text )


                    document.getElementById("editTodo").scrollIntoView();
                    var url_coms = '{{route('getFotoComs')}}'

                    $.ajax({
                        method: 'POST',
                        url: url_coms,
                        data: {campo_Id:campo}
                    })
                        .done(function(msg){

                            console.log(msg['message']);
                            var text = "";
                            var comentarios = 'comentario'+campo;
                            var url_cam = '{{ asset('images/buttons/camera.png')}}';
                            var URLdomain = window.location.host;
                            var pathImg = 'http://'+URLdomain+'/uploads/FotoTodoCom/';

                            text += '<ul class="nav nav-stacked " style="padding-bottom: 20px">\n';
                            for(var i = 0; i < msg['message'].length; i++)
                            {
                                text +=  '<li><a href="#">'+msg['message'][i]['descripcion']+' <span class="pull-right "><img  width="20px" height="20px" src="'+pathImg+msg['message'][i]['foto']+'" ></span></a></li>';
                            }
                            text += '</ul>\n';
                            $('.setListCom').html( text );
                        });
                });
        }


        function setFotoCampoTodo(cam, vis)
        {

            var $avatarImage, $avatarInput, $avatarForm, $rutaFoto;


            $avatarImage = $('#avatarImageT'+cam);
            $avatarInput = $('#avatarInputT'+cam);
            $avatarForm = $('#avatarFormT'+cam);
            $rutaFoto = $('.rutaFotoT'+cam);
            $avatarInput.click();
            $avatarInput.on('change', function () {
                var formData = new FormData();
                formData.append('photo', $avatarInput[0].files[0]);

                $('#modal-info').modal('toggle');

                $.ajax({
                    url: $avatarForm.attr('action') + '?' + $avatarForm.serialize(),
                    method: $avatarForm.attr('method'),
                    data: formData,
                    processData: false,
                    contentType: false
                }).done(function (msg) {
                    // alert(cam);
                    /*if (data.success)
                        $avatarImage.attr('src', data.path);*/
                    var URLdomain = window.location.host;
                    var pathImg = 'http://'+URLdomain+'/'+msg['path'];

                    $avatarImage.attr('src', pathImg)
                    console.log(msg['message']);
                    $('#modal-info').modal('toggle');
                    //console.log(msg['path']);
                    //console.log(pathImg);
                    var url = '{{route('putFotoCampo')}}'
                    $.ajax({
                        method: 'POST',
                        url: url,
                        data: {campo_Id:cam, visita_Id: vis, foto: msg['message']}
                    })
                        .done(function(msg){
                            console.log(msg['message']);
                        });

                });
            });

        }


        function clearComentTodo(cam)
        {
            var comentario_Id = "comentario"+cam;
            document.getElementById(comentario_Id).value = "";
        }

        function setFotoCampoTodoCom(cam, vis)
        {

            var $avatarImage, $avatarInput, $avatarForm, $rutaFoto;


            $avatarImage = $('#avatarImageTc'+cam);
            $avatarInput = $('#avatarInputTc'+cam);
            $avatarForm = $('#avatarFormTc'+cam);
            $rutaFoto = $('.rutaFotoTc'+cam);
            var foto_com_id = "fotocomid"+cam;

            $avatarInput.click();
            $avatarInput.on('change', function () {
                var formData = new FormData();
                formData.append('photo', $avatarInput[0].files[0]);

                //$('#modal-info').modal('toggle');

                $.ajax({
                    url: $avatarForm.attr('action') + '?' + $avatarForm.serialize(),
                    method: $avatarForm.attr('method'),
                    data: formData,
                    processData: false,
                    contentType: false
                }).done(function (msg) {
                    // $('#modal-info').modal('toggle');
                    console.log(msg['message']);

                    document.getElementById(foto_com_id).value = msg['message'];


                });
            });

        }


        function saveTodo(campo)
        {

            var coment = "comentario"+campo;
            var select = "stat"+campo;;
            var input_coment = document.getElementById(coment).value;
            var input_select = document.getElementById(select).value;
            var url = '{{route('updateTodo')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {campo_Id:campo, comentario: input_coment, status: input_select}
            })
                .done(function(msg){
                    console.log(msg['message']);

                    showDespues();
                });
        }

        function showDespues()
        {
            $("#perfilUsuario").show();
            $("#editTodo").hide();
            getTodos();
        }

        function setTodoCom(cam)
        {
            var foto_com_id = "fotocomid"+cam;
            var com_value = "comvalue"+cam;
            var com  = document.getElementById(com_value).value;
            var foto  = document.getElementById(foto_com_id).value;
            var url = '{{route('putFotoCom')}}';
            if(com == "")
            {
                bootbox.alert("Debes escribir un comentario");
            }else
            {
                //bootbox.alert(foto);


                $.ajax({
                    method: 'POST',
                    url: url,
                    data: {campo_Id:cam, foto: foto, comentario: com}
                })
                    .done(function(msg){


                        console.log(msg['message']);
                        var text = "";
                        var comentarios = 'comentario'+cam;
                        var url_cam = '{{ asset('images/buttons/camera.png')}}';
                        var URLdomain = window.location.host;
                        var pathImg = 'http://'+URLdomain+'/uploads/FotoTodoCom/';

                        text += '<ul class="nav nav-stacked " style="padding-bottom: 20px">\n';
                        for(var i = 0; i < msg['message'].length; i++)
                        {
                            text +=  '<li><a href="#">'+msg['message'][i]['descripcion']+' <span class="pull-right "><img  width="20px" height="20px" src="'+pathImg+msg['message'][i]['foto']+'" ></span></a></li>';
                        }
                        text += '</ul>\n';

                        //text += '<li><a href="#">Comentario 1 <span class="pull-right "><img  width="20px" height="20px" src="'+url_cam+'" ></span></a></li>';
                        $('.setListCom').html( text );

                        document.getElementById(com_value).value = "";
                        document.getElementById(foto_com_id).value = "no_imagen.jpg";
                    });
            }
        }


        /**AREA DE EDICION DE CADA ACTIVIDAD**/
        function editItem(id)
        {

            $("#perfilUsuario").hide();
            var url = '{{route('editarActividad')}}';

            // alert('Muestra fotos')

            $.ajax({
                method: 'POST',
                url: url,
                data: {  id: id}
            })
                .done(function(msg){

                    console.log(msg['avance']);

                    var text = "";

                    var imgUser = '{{ asset('uploads/Empleados/')}}';;
                    var imgActividad = '{{ asset('uploads/Actividades/')}}';

                    var status_id = msg['message']['status'];
                    var status_name = "";
                    var status_class = "";
                    switch(status_id)
                    {

                        case 152:
                        case '152':
                            status_name = "En curso";
                            status_class = "label-warning";
                            break;
                        case 153:
                        case '153':
                            status_name = "Pendiente";
                            status_class = "label-info";
                            break;
                        case 154:
                        case '154':
                            status_name = "Concluida";
                            status_class = "label-success";
                            break;
                    }


                    text += '<div class="box box-widget">\n' +
                        '        <div class="box-header with-border">\n' +
                        '            <div class="user-block">\n' +
                        '                <img class="img-circle" src="'+imgUser+'/'+msg['message']['fotoUser']+'"  alt="User Image">\n' +
                        '                <span class="username"><a href="#">'+msg['message']['name']+'</a></span>\n' +
                        '                <span class="description">'+msg['message']['actividad']+' - '+msg['message']['fhinicio']+'</span>\n' +
                        '            </div>\n' +
                        '            <!-- /.user-block -->\n' +
                        '            <div class="box-tools">\n' +

                        '                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>\n' +
                        '                </button>\n' +

                        '            </div>\n' +
                        '            <!-- /.box-tools -->\n' +
                        '        </div>\n' +
                        '        <!-- /.box-header -->\n' +
                        '        <div class="box-body">\n' +
                        '            <img class="img-responsive pad" src="'+imgActividad+'/'+msg['message']['foto']+'" alt="Photo">\n' +
                        '\n' +

                        '            <p>'+msg['message']['descripcion']+'</p>\n' +
                        '            <p></p>\n' +
                        '<p style="font-size: 30;"><label  for="customRange1" class="avanceLabel"> Avance: 0% </label></p>\n' +
                        ' <input onchange="setAvance()" type="range" min="0" max="100" value="0" id="rangoAvance" />'+
                        '            <p></p>\n' +
                        '            <input type="text" class="form-control" id="inputComentario" placeholder="Comentar actividad">\n' +
                        '            <button onclick="setComentario('+id+')" type="button" class="btn btn-default btn-xs pull-right"><i class="fa fa-share"></i> Comentar</button>\n' +
                        '\n' +
                        '        </div>\n' +
                        '        <!-- /.box-body -->\n' +
                        '        <div class="box-footer box-comments comentarios">\n' +
                        '\n' +
                        '            <!-- /.box-comment -->\n' +
                        '        </div>\n' +
                        '\n' +
                        '        <!-- /.box-footer -->\n' +
                        ' <div class="box-footer">\n' +
                        ' <button onclick="updateStatus('+id+')" type="button" class="btn  btn-primary pull-right">Terminar</button>'+

                        '    </div>';
                    $('.edit').html(text);
                    getComentarios(id);
                    document.getElementById('rangoAvance').value = msg['avance']
                    $(".avanceLabel").html('Avance: ' + msg['avance']+ " %");
                });
        }

        function setAvance()
        {
            //alert(document.getElementById('rangoAvance').value);
            $(".avanceLabel").html('Avance: ' + document.getElementById('rangoAvance').value+ " %");

        }

        function updateStatus(id)
        {
            $("#perfilUsuario").show();
            document.getElementById("perfilUsuario").scrollIntoView();
            $(".edit").html('');

            url = '{{route('updComentario')}}';

            $.ajax({
                method: 'POST',
                url: url,
                data: {
                    status: 152,
                    id: id
                }
            })
                .done(function(msg){
                    console.log(msg['message']);

                    getTodos()

                });
        }

        function setComentario(id)
        {

            var avance = document.getElementById('rangoAvance').value;

            if(avance != 0)
            {
                if(document.getElementById('inputComentario').value != "")
                {
                    bootbox.alert("Agregando comentario");
                    url = '{{route('setComentario')}}';

                    $.ajax({
                        method: 'POST',
                        url: url,
                        data: {
                            comentario: document.getElementById('inputComentario').value,
                            id: id,
                            avance: avance
                        }
                    })
                        .done(function(msg){
                            console.log(msg['message']);
                            bootbox.hideAll()
                            getComentarios(id);
                            document.getElementById('inputComentario').value = "";

                        });
                }else
                {
                    bootbox.alert("No puedes agregar un comentario vacio")
                }
            }else
            {
                bootbox.alert("Debes definir un porcentaje de avance")
            }


        }

        function getComentarios(id)
        {


            url = '{{route('getComentario')}}';

            $.ajax({
                method: 'POST',
                url: url,
                data: {
                    id: id
                }
            })
                .done(function(msg){
                    console.log(msg['message']);
                    var text = "";
                    var imgUser = '{{ asset('uploads/Empleados/')}}';;
                    for(var i = 0; i < msg['message'].length; i++)
                    {

                        text += '<div class="box-comment">\n' +
                            '                <!-- User image -->\n' +
                            '                <img class="img-circle img-sm" src="'+imgUser+'/'+msg['message'][i]['fotoUser']+'"  alt="User Image">\n' +
                            '\n' +
                            '                <div class="comment-text">\n' +
                            '                      <span class="username">\n' +
                            '                        '+msg['message'][i]['name']+'\n' +
                            '                        <span class="text-muted pull-right">'+msg['message'][i]['fhcomentario']+'</span>\n' +
                            '                      </span><!-- /.username -->\n' +

                            '      '+msg['message'][i]['comentario']+'          </div>\n' +
                            '                <!-- /.comment-text -->\n' +
                            '            </div>\n';
                    }


                    $('.comentarios').html(text);

                });
        }



        /**fin**/


    </script>


@stop