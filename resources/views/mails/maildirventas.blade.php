

<h2>Por este medio se le informa el status de sus ventas en sus sucursales asignadas</h2>
<hr>
<h3>Director <strong>{{$data['Supervisor']}}</strong></h3>
<hr>
<table style="border: solid; border-color: #001F3F; background-color: #9cc2cb">
    <thead>
    <tr >
        <th style="border-right:solid; border-color: #1b6d85; font-weight: bolder; text-align: center" >Razón</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Plaza</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">#</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Sucursal</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Presupuesto</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Ventas</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Última Venta Cargada</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Avance Presupuesto</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Tendencia de Cierre</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Ticket Prom. mes actual</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Trans. Diarias mes actual</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Art. por Ticket mes actual</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Ticket Prom. mes anterior</th>
        <th style="font-weight: bolder; border-color: #1b6d85;text-align: center">Trans. Diarias mes anterior</th>
        <th style="border-left: solid; border-color: #1b6d85;font-weight: bolder" >Art. por Ticket mes anterior</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data['TablaVentas'] as $ventas)
        <tr>
            <td style="background-color: #CCCCCC; border-right:solid; border-color: #707572;  text-align: center" >{{$ventas['Razon']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center">{{$ventas['Plaza']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center">{{$ventas['NumSuc']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center" >{{$ventas['Nombre']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center" width="10%"  >{{$ventas['Presupuesto']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center" width="10%" >{{$ventas['Ventas']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center" width="10%" >{{date('d/m/Y', strtotime($ventas['LastVenta']))}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center">{{$ventas['Avance']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center" width="10%">{{$ventas['Tendencia']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center">{{$ventas['TckPromAct']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572;text-align: center">{{$ventas['TicketsAct']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center">{{$ventas['NoTckAct']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center">{{$ventas['TckPromAnt']}}</td>
            <td style="background-color: #CCCCCC;border-color: #707572;text-align: center">{{$ventas['TicketsAnt']}}</td>
            <td style="background-color: #CCCCCC;border-left: solid; border-color: #707572;text-align: center" >{{$ventas['NoTckAnt']}}</td>
        </tr>
    @endforeach

    </tbody>
</table>
<hr>
<h4>*250 ticket promedio, 36 Transacciones diarias, 2.5 No. de Art x Ticket</h4>
<hr>
