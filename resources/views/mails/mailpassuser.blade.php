
<h3>Por este medio se informa que tu contraseña ha sido restaurada </h3>
<p></p>
<hr>
<h2>La contraseña es: {{$data['pass']}}</h2>
<hr>
<h2>Puedes cambiar tu contraseña desde tu perfil personal, puedes pedir ayuda a soporte técnico si tienes dudas de como hacerlo.</h2>
<hr>
<a href="https://ventumsupervision.com/usuarios/perfil" ><h5>Entrar a Ventum</h5></a>
