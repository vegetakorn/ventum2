

<h2>Por este medio se le informa las sucursales que tienen To-Do's con 30 días de antigüedad</h2>
<hr>
<h3>Supervisor <strong>{{$data['Supervisor']}}</strong></h3>
<hr>
<hr>
<h2>Visitas realizadas a las tiendas</h2>
<table style="border: solid; border-color: #001F3F; background-color: #9cc2cb" width="100%">
    <thead>
    <tr >
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Plaza</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">#</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Sucursal</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">To-Do's</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Fecha mas antigüa</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data['TablaTodo'] as $visitas)
        <tr>
            <td style="background-color: #CCCCCC; border-right:solid; border-color: #707572;  text-align: center" >{{$visitas['Plaza']}}</td>
            <td style="background-color: #CCCCCC; border-right:solid; border-color: #707572;  text-align: center" >{{$visitas['NumSuc']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center">{{$visitas['Sucursal']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center">{{$visitas['NoTodo']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center" >{{$visitas['Fecha']}}</td>
        </tr>
    @endforeach

    </tbody>
</table>
<hr>
