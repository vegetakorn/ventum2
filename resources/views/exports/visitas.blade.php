<!DOCTYPE html>
<html lang="en">
<head>
    <title>Reporte de Visitas</title>
    <meta charset="UTF-8">

</head>
<body>
<table border="1">
    <thead>
    <tr>
        <th colspan="3">
            Reporte de Visitas
        </th>
    </tr>

    <tr>
        <th colspan="3">
            Periodo:  {{$info['FechaIni']}} al {{$info['FechaFin']}}
        </th>
    </tr>
    <tr>
        <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Clave</th>

        <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Sucursal</th>

        <th style="background-color: #7bb8d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Supervisor</th>
        <th style="background-color: #7bb8d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Fecha</th>
        <th style="background-color: #d4d51b;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Duración</th>
        <th style="background-color: #7bb8d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Checklist</th>
        <th style="background-color: #7bb8d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Evaluación</th>
    </tr>
    </thead>
    <tbody>
    @foreach($info['visitas'] as $visita )
        <tr>
            <td style="text-align: center;color: #0b3e6f" >{{$visita->numsuc}}</td>
            <td style="text-align: center;color: #0b3e6f" >{{$visita->nombre}}</td>
            <td style="text-align: center;color: #0b3e6f" >{{$visita->name}}</td>
            <td style="text-align: center;color: #707572" >{{date('d/m/Y', strtotime($visita->HoraInicio))}}</td>
            <td style="text-align: center;color: #0b3e6f" >{{$visita->Duracion}}</td>
            @if($visita->tipo_visita == 2)
                <td style="text-align: center;color: #707572" >Seguimiento</td>
            @else
                <td style="text-align: center;color: #707572" >{{$visita->Checklist}}</td>
            @endif


            <td style="text-align: center;color: #0b3e6f" >{{$visita->Calif}}% </td>
        </tr>
    @endforeach
    </tbody>
</table>


</body>
</html>