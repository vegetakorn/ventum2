<!DOCTYPE html>
<html lang="en">
<head>
    <title>Reporte de Ventas</title>
    <meta charset="UTF-8">

</head>
<body>
<table border="1">
    <thead>
    <tr>
        <th colspan="3">
            Reporte de Ventas Mensual
        </th>
    </tr>

    <tr>
        <th colspan="3">
            Periodo:  {{$info['mes']}}  {{$info['anio']}}
        </th>
    </tr>
    <tr>
        <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Clave</th>

        <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Sucursal</th>

        <th style="background-color: #7bb8d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Presupuesto</th>
        <th style="background-color: #7bb8d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Venta</th>
        <th style="background-color: #d4d51b;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Avance</th>

    </tr>
    </thead>
    <tbody>
    @foreach($info['ventas'] as $ventas )
        <tr>
            <td style="text-align: center;color: #0b3e6f" >{{$ventas->numsuc}}</td>
            <td style="text-align: center;color: #0b3e6f" >{{$ventas->Tienda}}</td>
            <td style="text-align: center;color: #0b3e6f" >${{number_format($ventas->presupuesto, 2, '.', ',')}}</td>
            <td style="text-align: center;color: #707572" >${{number_format($ventas->ventas, 2, '.', ',')}}</td>

            <?php
                if($ventas->presupuesto != 0)
                {
                    $avance = round($ventas->ventas / $ventas->presupuesto * 100, 2);
                }else
                {
                    $avance = 0;
                }
                echo  '<td style="text-align: center;color: #0b3e6f" >'.$avance.'%</td>';
            ?>

        </tr>
    @endforeach
    </tbody>
</table>


</body>
</html>