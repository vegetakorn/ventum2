<!DOCTYPE html>
<html lang="en">
<head>
    <title>Actividades</title>
    <meta charset="UTF-8">
</head>
<body>

<table border="1">
    <thead>
    <tr>
        <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Actividad</th>

        <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Antigüedad</th>
        <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Responsables</th>

    </tr>
    </thead>
    <tbody>
    @if($actividades != 0)
        @foreach($actividades as $act)
            <tr>
                <td style="background-color: #ffffff;border-style: solid; border-bottom-color: #6f150f "  width="25%">{{$act['actividad']}} ({{$act['avance']}}%)  {{$act['status_name']}}</td>

                <td style="background-color: #ffffff " width="25%" >{{$act['Dias']}} días</td>
                <td style="background-color: #ffffff " width="25%" >
                    <table >
                        <thead>
                        <tr>
                            <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Responsable</th>
                            <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Avance</th>


                        </tr>
                        </thead>
                        <tbody>

                        @foreach($act['supervisores'] as $sup)
                            <tr>
                                <td style="background-color: #ffffff;border-style: solid; border-bottom-color: #6f150f "  width="25%">{{$sup['Supervisor']}}</td>
                                <td style="background-color: #eaeaea; " width="25%"   >{{$sup['Avance']}}%</td>

                            </tr>
                        @endforeach



                        </tbody>
                    </table>

                </td>

            </tr>
        @endforeach
    @endif


    </tbody>
</table>


</body>
</html>