<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sucursales</title>
    <meta charset="UTF-8">

    <style type="text/css">
        table.table-style-three {
            font-family: verdana, arial, sans-serif;
            font-size: 11px;
            color: #333333;
            border-width: 1px;
            border-color: #3A3A3A;
            border-collapse: collapse;
        }
        table.table-style-three th {
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #FFA6A6;
            background-color: #D56A6A;
            color: #ffffff;
        }
        table.table-style-three tr:hover td {
            cursor: pointer;
        }
        table.table-style-three tr:nth-child(even) td{
            background-color: #F7CFCF;
        }
        table.table-style-three td {
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #FFA6A6;
            background-color: #ffffff;
        }
    </style>

</head>
<body>

<table border="1">
    <thead>
    <tr>
        <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Clave</th>
        <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Nombre</th>
        <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Razón</th>
        <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Plaza</th>
        <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Correo Electrónico</th>
    </tr>
    </thead>
    <tbody>
    @foreach($tiendas as $tienda)
        <tr>
            <td style="background-color: #ffffff;border-style: solid; border-bottom-color: #6f150f " >{{$tienda->numsuc}}</td>
            <td style="background-color: #eaeaea " >{{$tienda->nombre}}</td>
            <td style="background-color: #eaeaea " >{{$tienda->razon}}</td>
            <td style="background-color: #eaeaea " >{{$tienda->plaza}}</td>
            <td style="background-color: #ffffff " >{{$tienda->mail}}</td>
        </tr>
    @endforeach


    </tbody>
</table>


</body>
</html>