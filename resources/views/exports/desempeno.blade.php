<!DOCTYPE html>
<html lang="en">
<head>
    <title>Evaluación del Desempeño</title>
    <meta charset="UTF-8">

</head>
<body>
<table border="1">
    <thead>
    <tr>
        <th colspan="14">
            Evaluación del Desempeño
        </th>
    </tr>

    <tr>
        <th colspan="14">
            Periodo:  {{date('d/m/Y', strtotime($info['fecha_ini']))}} al {{date('d/m/Y', strtotime($info['fecha_fin']))}}

        </th>
    </tr>
                           <tr>
                                <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Id</th>
                                    <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Nombre</th>
                                    <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Puesto</th>
                                    <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Razón</th>
                                    <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Plaza</th>
                                    <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Sucursales</th>
                                    <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Checklist</th>
                                    <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Seguimiento</th>
                                    <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Anteriores</th>
                                    <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Creados</th>
                                    <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Nuevos</th>
                                    <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">En curso</th>
                                    <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Pendientes</th>
                                    <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Concluidos</th>
                                    <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Efectividad</th>

                                </tr>
    </thead>
    <tbody>
    @foreach($info['supervisores'] as $sup)
        <tr>
            <td style="background-color: #ffffff;border-style: solid; border-bottom-color: #6f150f "  width="25%">{{$sup['clave']}}</td>
            <td style="background-color: #eaeaea; " width="25%"   >{{$sup['nombre']}}</td>
            <td style="background-color: #eaeaea; " width="25%"   >{{$sup['puesto']}}</td>
            <td style="background-color: #eaeaea; " width="25%"   >{{$sup['razon']}}</td>
            <td style="background-color: #eaeaea; " width="25%"   >{{$sup['plaza']}}</td>
            <td style="background-color: #eaeaea; " width="25%"   >{{$sup['tiendas']}}</td>
            <td style="background-color: #eaeaea; " width="25%"   >{{$sup['checklist']}}</td>
            <td style="background-color: #eaeaea; " width="25%"   >{{$sup['seguimientos']}}</td>
            <td style="background-color: #eaeaea; " width="25%"   >{{$sup['anteriores']}}</td>
            <td style="background-color: #eaeaea; " width="25%"   >{{$sup['creados']}}</td>
            <td style="background-color: #eaeaea; " width="25%"   >{{$sup['nuevos']}}</td>
            <td style="background-color: #eaeaea; " width="25%"   >{{$sup['curso']}}</td>
            <td style="background-color: #eaeaea; " width="25%"   >{{$sup['pendientes']}}</td>
            <td style="background-color: #eaeaea; " width="25%"   >{{$sup['concluidos']}}</td>
            <td style="background-color: #eaeaea; " width="25%"   >{{$sup['efectividad']}}%</td>
        </tr>
    @endforeach
    </tbody>
</table>


</body>
</html>