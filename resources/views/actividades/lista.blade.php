@extends('adminlte::page')
@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>Actividades para supervisores</h1>
@stop

@section('content')

    <div id="filtro">
        <div class="box-header">
            <!--<a  class="btn btn-social-icon btn-success pull-right"><i class="fa fa-file-excel-o"></i></a>-->
            <div class="btnExcel" >

            </div>

            <button type="button" onclick="add()" class="btn  btn-primary pull-right">Agregar Actividad</button>

            <a href="{{route('usuarios.actividades')}}"  class="btn  btn-warning pull-right">Mis Actividades</a>

        </div>
        <div  class="box box-success box-solid">

            <div class="box-header with-border">
                <h3 class="box-title">Filtro de Actividades</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <strong><i class="fa fa-tachometer margin-r-5"></i> Busca supervisor</strong>
                <select class="form-control select2" id="sup" name="supervisor" style="width: 100%;">
                    <option value="0" selected="selected">Selecciona Supervisor</option>
                    @foreach($puestos as $puesto)
                        @if(Auth::user()->empleados_Id != $puesto->Id)
                            <option value="{{$puesto->Id}}" >{{$puesto->nombre}} {{$puesto->apepat}}  {{$puesto->apemat}} </option>
                        @endif

                    @endforeach
                </select>
                <hr>
                <strong><i class="fa fa-tachometer margin-r-5"></i> Por Estatus</strong>
                <div class="form-group">
                    <label class="">
                        <input type="checkbox" id="boxOpen" class="minimal" checked="" style="position: absolute; opacity: 0;">
                        Abiertos
                    </label>
                    <label class="">

                    </label>
                    <label class="">
                        <input type="checkbox" id="boxClose"  class="minimal"  style="position: absolute; opacity: 0;">
                       Cerrados
                    </label>

                </div>

                <hr>
                <strong><i class="fa fa-tachometer margin-r-5"></i> Por Fecha</strong>

                <div class="form-group">


                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" name="rango" id="reservation">
                    </div>
                    <!-- /.input group -->
                </div>

                <hr>
                <button onclick="filtra()"  class="btn btn-success btn-block margin-bottom">Buscar</button>
            </div>
            <!-- /.box-body -->
        </div>

    </div>

    <!-- PRODUCT LIST -->
    <div class="box box-primary" id="listaItems">
        <div class="box-header with-border">
            <h3 class="box-title">Actividades para Supervisores</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>

            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <ul class="products-list product-list-in-box actividades">


            </ul>
        </div>
        <!-- /.box-body -->
        <div class="box-footer text-center">

        </div>
        <!-- /.box-footer -->
    </div>
    <!-- Profile Image -->
    <div class="box box-primary" id="agregaItem">
        <div class="box-body box-profile">

            <form action="{{route('regFotoActividad')}}" method="post" style="display: none" id="avatarForm">
                <input type="file" id="avatarInput" name="photo">
            </form>
            <div class="rutaFoto">
                <img onclick="setFoto()" class="profile-user-img img-responsive img-circle" id="avatarImage" src="{{ asset('uploads/Actividades/no_imagen.jpg')}}" alt="User profile picture">

            </div>

            <h3 class="profile-username text-center">Foto de Actividad</h3>

            <!--<p class="text-muted text-center">Software Engineer</p>-->

            <ul class="list-group list-group-unbordered">
                <li class="list-group-item ">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Actividad:</label>
                        <input maxlength="50" type="text" class="form-control" id="inputActividad" placeholder="Nombra tu actividad de forma breve">
                    </div>
                </li>
                <li class="list-group-item ">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Descripción</label>
                        <input maxlength="250" type="text" class="form-control" id="inputDescripcion" placeholder="Describe tu Actividad">
                    </div>
                </li>

                <!--<li class="list-group-item razones">
                    <label for="exampleInputEmail1">Supervisores por razón</label>
                    <select class="form-control select2" id="raz" name="razon" style="width: 100%;">
                        <option value="0" selected="selected">--Todas las razones--</option>

                        @foreach($razones as $razon)
                            <option value="{{$razon->Id}}"  > {{$razon->nombre}}</option>
                        @endforeach
                    </select>
                </li>-->
            </ul>
            <a href="{{route('actividades.lista')}}"  type="button" class="btn  btn-danger pull-right">Cancelar</a>
            <button onclick="insert()" type="button" class="btn  btn-primary pull-right">Agregar</button>

        </div>
        <!-- /.box-body -->
    </div>

    <div class="box box-primary" id="asignaSup">
        <div class="box-body box-profile">
                <li class="list-group-item ">
                    <label for="exampleInputEmail1">Asigna supervisores a actividad</label>
                    @foreach($puestos as $puesto)
                        @if(Auth::user()->empleados_Id != $puesto->Id)
                            <div class="input-group">
                            <span class="input-group-addon">
                              <input onclick="asignaSup({{$puesto->Id}})" type="checkbox" id="chkSup{{$puesto->Id}}" >
                            </span>
                                <label  class="form-control"> {{$puesto->nombre}} {{$puesto->apepat}}  {{$puesto->apemat}}  </label>
                            </div>
                         @endif

                    @endforeach

                </li>

            </ul>

            <a href="{{route('actividades.lista')}}" type="button" id="btnAsigna" class="btn  btn-success pull-right">Terminar</a>

        </div>
        <!-- /.box-body -->
    </div>
    <div class="foto_actividad" >
        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
        <input type="hidden" id="foto_actividad" value="no_imagen.jpg" >
    </div>

    <div class="id_actividad" >
        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
        <input type="hidden" id="id_actividad" value="0" >
    </div>

    <!--ZONA DE EDICION DE ACTIVIDAD-->
    <div class="edit" >

    </div>








@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link href="{{ asset('dist/dropzone.css') }}" rel="stylesheet">

    <style>
        .badge-important {

        }
        .image-upload > input
        {
            display: none;
        }

    </style>

    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }

        #vr{
            width : 1px ;
            height : 100px ;
        }

    </style>

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

@stop

@section('js')

    <script src="{{ asset('bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('dist/timer.jquery.min.js')}}"></script>
    @routes
    <script>


        $(document).ready(function() {
            $("#listaItems").show();
            $("#agregaItem").hide();
            $("#asignaSup").hide();
            $("#filtro").show();


            //Initialize Select2 Elements
            $('.select2').select2()

            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            })

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            });
            $('[data-toggle="tooltip"]').tooltip();


            //Date range picker
            $('#reservation').daterangepicker({
                "startDate": moment().add(-730, 'day'),
                "endDate": moment().add(1, 'day'),
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Aplicar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "De",
                    "toLabel": "a",
                    "customRangeLabel": "Custom",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Septiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                }
            })
        } );




        function add()
        {
            $("#listaItems").hide();
            $("#agregaItem").show();
            $("#asignaSup").hide();
            $("#filtro").hide();
        }

        function insert()
        {

            if(document.getElementById('inputActividad').value != "")
            {
                url = '{{route('agregaActividad')}}';

                $.ajax({
                    method: 'POST',
                    url: url,
                    data: {
                        actividad: document.getElementById('inputActividad').value,
                        descripcion: document.getElementById('inputDescripcion').value,
                        foto: document.getElementById('foto_actividad').value
                    }
                })
                    .done(function(msg){
                        console.log(msg['message']);

                        $("#agregaItem").hide();
                        $("#asignaSup").show();

                        $('.id_actividad').html( '<input type="hidden" id="id_actividad" value="'+msg['idactividad']+'" >' )



                    });
            }else
            {
                bootbox.alert('Debes definir una actividad');
            }


        }

        function asignaSup(id)
        {
            var chk = $("#chkSup"+id);

            url = '{{route('asignaSupervisor')}}';

            $.ajax({
                method: 'POST',
                url: url,
                data: {
                    actividad_Id: document.getElementById('id_actividad').value,
                    empleados_Id: id,
                    type:chk.is( ":checked" )
                }
            })
                .done(function(msg){
                    console.log(msg['message']);



                });

        }



        function setFoto()
        {
            $avatarImage = $('#avatarImage');
            $avatarInput = $('#avatarInput');
            $avatarForm = $('#avatarForm');
            $rutaFoto = $('.rutaFoto');
            $avatarInput.click();
            $avatarInput.on('change', function () {
                var formData = new FormData();
                formData.append('photo', $avatarInput[0].files[0]);

                $('#modal-info').modal('toggle');

                $.ajax({
                    url: $avatarForm.attr('action') + '?' + $avatarForm.serialize(),
                    method: $avatarForm.attr('method'),
                    data: formData,
                    processData: false,
                    contentType: false
                }).done(function (msg) {
                    // alert(cam);
                    /*if (data.success)
                        $avatarImage.attr('src', data.path);*/
                    var URLdomain = window.location.host;
                    var pathImg = 'http://'+URLdomain+'/'+msg['path'];

                    $avatarImage.attr('src', pathImg)
                    console.log(msg['message']);
                    $('#modal-info').modal('toggle');

                    $('.foto_actividad').html('<input type="hidden" id="foto_actividad" value="' + msg['message'] + '" >')


                });
            });
        }



        function filtra()
        {

            if(document.getElementsByName('supervisor')[0].value != 0)
            {
                $("#listaItems").show();
                $("#agregaItem").hide();
                $("#asignaSup").hide();
                $("#filtro").show();
                $('.edit').html('');



                //supervisor seleccionado
                var supervisor = document.getElementsByName('supervisor')[0].value;
                //por status
                var nuevo = 0;
                var curso = 0;
                var pendiente = 0;

                var abierto = 0;
                var cerrado = 0;

                //tipo de todo
                if(document.getElementById('boxOpen').checked) {
                     abierto = 1;
                }
                if(document.getElementById('boxClose').checked) {
                    cerrado = 1;
                }



                //por fecha
                var fecha = document.getElementsByName('rango')[0].value;
                var fechas = setFechaPdf(document.getElementsByName('rango')[0].value);
                var fecha_ini = setFechaFormat(fechas[0]);
                var fecha_fin = setFechaFormat(fechas[1]);

                var href_route = route('exportAct', [fecha_ini, fecha_fin, supervisor, abierto, cerrado]);

                $('.btnExcel').html('<a href="'+href_route+'"  class="btn btn-social-icon btn-success pull-right "><i class="fa fa-file-excel-o"></i></a>');
                var url = '{{route('getActividadFiltro')}}';

                // alert('Muestra fotos')
                bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Cargando actividades...</div>' })

                $.ajax({
                    method: 'POST',
                    url: url,
                    data: {  abierto: abierto,cerrado:cerrado, fecha:fecha, supervisor: supervisor}
                })
                    .done(function(msg){
                        bootbox.hideAll();
                        console.log(msg['message']);
                        var text = "";
                        for(var i = 0; i < msg['message'].length; i++)
                        {
                            var status_id = msg['message'][i]['status'];
                            var status_name = "";
                            var status_class = "";
                            var btn_cerrar = "";

                            switch(status_id)
                            {
                                case 151:
                                case '151':
                                case 152:
                                case '152':
                                case 153:
                                case '153':
                                    status_name = "Abierta";
                                    status_class = "btn-warning";
                                    btn_cerrar =  '                                    <p class="pull-right">\n' +
                                        '                                        <a onclick="cerrarAct('+msg['message'][i]['Id']+')" class="btn btn-danger btn-success btn-sm ">\n' +
                                        '                                   Cerrar     ' +
                                        '                                        </a>\n' +
                                        '                                    </p>\n' ;
                                    break;
                                case 154:
                                case '154':
                                    status_name = "Cerrada";
                                    status_class = "btn-success";
                                    btn_cerrar = ""
                                    break;
                            }

                            var img = '{{ asset('uploads/Actividades/')}}';

                            text += '      <div class="box-body">\n' +
                                '                        <h4 onclick="editItem('+msg['message'][i]['Id']+')"  style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">\n' +
                                '                            '+msg['message'][i]['actividad']+' ('+msg['message'][i]['avance']+'% ) '+status_name+'\n' +
                                '                        </h4>\n' +
                                '                        <div class="media">\n' +
                                '                            <div class="media-left">\n' +
                                '                                <a  class="ad-click-event">\n' +
                                '                                    <img onclick="editItem('+msg['message'][i]['Id']+')"  src="'+img+'/'+msg['message'][i]['foto']+'" alt="Product Image" class="media-object" style="width: 150px;height: auto;border-radius: 4px;box-shadow: 0 1px 3px rgba(0,0,0,.15);">\n' +
                                '                                </a>\n' +
                                '                            </div>\n' +
                                '                            <div class="media-body">\n' +
                                '                                <div class="clearfix">\n' + btn_cerrar +


                                '\n' +
                                '                                    <h4 style="margin-top: 0">'+msg['message'][i]['descripcion']+'</h4>\n' +
                                '\n' +
                                '                                        <p style="margin-bottom: 0">\n' +
                                '                                            <i class="fa fa-clock-o margin-r5"></i> '+ msg['message'][i]['Dias'] +' días de antigüedad   \n ('+ msg['message'][i]['fhinicio'] + ')' +
                                '                                        </p><p></p>\n' +
                                '                                    <p>Responsables:</p>\n';

                            for(var j = 0; j < msg['message'][i]['supervisores'].length; j++)
                            {
                                text +=  '                                    <p style="margin-bottom: 0">\n' +
                                    '                                        <i class="fa fa-user margin-r5"></i> <label>'+msg['message'][i]['supervisores'][j]['Supervisor']+'\n' +
                                    '                               </label> ( Avance '+msg['message'][i]['supervisores'][j]['Avance']+'% )   </p>\n';
                            }



                            text +=   '                                </div>\n' +
                                '                            </div>\n' +
                                '                        </div>\n' +
                                '                    </div>';


                            $('.actividades').html(text);


                        }



                        $('#filtro').boxWidget('collapse');


                    });

                // alert(fecha)
            }else
            {
                bootbox.alert('Debes seleccionar un supervisor');
            }

        }

        function setFechaPdf(fecha) {
            var str = fecha;
            var res = str.split("-");
            return res;
        }

        function setFechaFormat(fecha) {
            var str = fecha;
            var res = str.split("/");
            return res[0]+'-'+res[1]+'-'+res[2];
        }

        function cerrarAct(id)
        {
            //validamos que sea director
            url = '{{route('valDirector')}}';

            $.ajax({
                method: 'POST',
                url: url,
                data: {
                    status: 0,

                }
            })
                .done(function(msg){
                    console.log(msg['message']);

                    if(msg['message'] == 'Director')
                    {
                        var box = bootbox.confirm({
                            title: "Cierre de Actividad",
                            message: "¿Deseas cerrar la actividad?, este cambio no es reversible y los usuarios asignados ya no podrán verla.",
                            buttons: {
                                cancel: {
                                    label: '<i class="fa fa-times"></i> No'
                                },
                                confirm: {
                                    label: '<i class="fa fa-check"></i> Si'
                                }
                            },
                            callback: function (result) {
                                console.log(result);

                                if(result == true)
                                {
                                    url = '{{route('updComentario')}}';

                                    $.ajax({
                                        method: 'POST',
                                        url: url,
                                        data: {
                                            status: 154,
                                            id: id
                                        }
                                    })
                                        .done(function(msg){
                                            console.log(msg['message']);

                                            filtra()

                                        });

                                }
                            }
                        });
                    }else
                    {
                        bootbox.alert('Tu usuario no tiene permisos de Director, favor de consultarlo con el área de sistemas');
                    }


            });

          /* */


        }

        function editItem(id)
        {
            $("#listaItems").hide();
            $("#agregaItem").hide();
            $("#asignaSup").hide();
            $("#filtro").hide();

            var url = '{{route('editarActividad')}}';

            // alert('Muestra fotos')

            $.ajax({
                method: 'POST',
                url: url,
                data: {  id: id}
            })
                .done(function(msg){

                    console.log(msg['message']);

                    var text = "";

                    var imgUser = '{{ asset('uploads/Empleados/')}}';;
                    var imgActividad = '{{ asset('uploads/Actividades/')}}';

                    var status_id = msg['message']['status'];
                    var status_name = "";
                    var status_class = "";
                    switch(status_id)
                    {

                        case 152:
                        case '152':
                            status_name = "En curso";
                            status_class = "label-warning";
                            break;
                        case 153:
                        case '153':
                            status_name = "Pendiente";
                            status_class = "label-info";
                            break;
                        case 154:
                        case '154':
                            status_name = "Concluida";
                            status_class = "label-success";
                            break;
                    }



                    text += '<div class="box box-widget">\n' +
                        '        <div class="box-header with-border">\n' +
                        '            <div class="user-block">\n' +
                        '                <img class="img-circle" src="'+imgUser+'/'+msg['message']['fotoUser']+'"  alt="User Image">\n' +
                        '                <span class="username"><a href="#">'+msg['message']['name']+'</a></span>\n' +
                        '                <span class="description">'+msg['message']['actividad']+' - '+msg['message']['fhinicio']+'</span>\n' +
                        '            </div>\n' +
                        '            <!-- /.user-block -->\n' +
                        '            <div class="box-tools">\n' +

                        '                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>\n' +
                        '                </button>\n' +

                        '            </div>\n' +
                        '            <!-- /.box-tools -->\n' +
                        '        </div>\n' +
                        '        <!-- /.box-header -->\n' +
                        '        <div class="box-body">\n' +
                        '            <img class="img-responsive pad" src="'+imgActividad+'/'+msg['message']['foto']+'" alt="Photo">\n' +
                        '\n' +
                        '            <p>'+msg['message']['descripcion']+'</p>\n' +
                        '            <p></p>\n' +

                        '            <input type="text" class="form-control" id="inputComentario" placeholder="Comentar actividad">\n' +
                        '            <button onclick="setComentario('+id+')" type="button" class="btn btn-default btn-xs pull-right"><i class="fa fa-share"></i> Comentar</button>\n' +
                        '\n' +
                        '        </div>\n' +
                        '        <!-- /.box-body -->\n' +
                        '        <div class="box-footer box-comments comentarios">\n' +
                        '\n' +
                        '            <!-- /.box-comment -->\n' +
                        '        </div>\n' +
                        '\n' +
                        '        <!-- /.box-footer -->\n' +
                            ' <div class="box-footer">\n' +
                        ' <button onclick="updateStatus('+id+')" type="button" class="btn  btn-primary pull-right">Terminar</button>'+

                        '    </div>';


                    $('.edit').html(text);


                    getComentarios(id);


                });
        }

        function updateStatus(id)
        {


            url = '{{route('updComentario')}}';

            $.ajax({
                method: 'POST',
                url: url,
                data: {
                    status: 152,
                    id: id
                }
            })
                .done(function(msg){
                    console.log(msg['message']);

                    filtra()

                });
        }

        function setComentario(id)
        {

            if(document.getElementById('inputComentario').value != "")
            {
                bootbox.alert("Agregando comentario");
                url = '{{route('setComentario')}}';

                $.ajax({
                    method: 'POST',
                    url: url,
                    data: {
                        comentario: document.getElementById('inputComentario').value,
                        id: id,
                        avance: 0
                    }
                })
                    .done(function(msg){
                        console.log(msg['message']);
                        bootbox.hideAll()
                        getComentarios(id);
                        document.getElementById('inputComentario').value = "";

                    });
            }else
            {
                bootbox.alert("No puedes agregar un comentario vacio")
            }
        }

        function getComentarios(id)
        {


            url = '{{route('getComentario')}}';

            $.ajax({
                method: 'POST',
                url: url,
                data: {
                    id: id
                }
            })
                .done(function(msg){
                    console.log(msg['message']);
                    var text = "";
                    var imgUser = '{{ asset('uploads/Empleados/')}}';;
                    for(var i = 0; i < msg['message'].length; i++)
                    {

                        text += '<div class="box-comment">\n' +
                            '                <!-- User image -->\n' +
                            '                <img class="img-circle img-sm" src="'+imgUser+'/'+msg['message'][i]['fotoUser']+'"  alt="User Image">\n' +
                            '\n' +
                            '                <div class="comment-text">\n' +
                            '                      <span class="username">\n' +
                            '                        '+msg['message'][i]['name']+'\n' +
                            '                        <span class="text-muted pull-right">'+msg['message'][i]['fhcomentario']+'</span>\n' +
                            '                      </span><!-- /.username -->\n' +

                            '      '+msg['message'][i]['comentario']+'          </div>\n' +
                            '                <!-- /.comment-text -->\n' +
                            '            </div>\n';
                    }


                    $('.comentarios').html(text);

                });
        }


    </script>

@stop