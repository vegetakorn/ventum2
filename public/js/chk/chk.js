$('#example1').DataTable( {
    "pagingType": "full_numbers",
    "scrollY":        '60vh',
    "scrollCollapse": true,
    "language": {
        "lengthMenu": "Mostrando _MENU_ registros por página ",
        "zeroRecords": "Sin registros encontrados",
        "info": "Mostrando página  _PAGE_ de _PAGES_ páginas",
        "infoEmpty": "Sin registros encontrados",
        "infoFiltered": "(filtrados de _MAX_ registros totales)",
        "search": "Buscar:",
        "paginate": {
            "first":      "Primero",
            "last":       "Último",
            "next":       "Siguiente",
            "previous":   "Anterior"
        }
    }
} );
$(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : true,
        'ordering'    : true,
        'info'        : false,
        'autoWidth'   : false
    })
});

$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
        checkboxClass: 'icheckbox_minimal-red',
        radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass   : 'iradio_flat-green'
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass   : 'iradio_minimal-blue'
    })



});

var token = '{{csrf_token()}}';
var url   = "{{route('getcat')}}";


$('#chk').on('change' , function () {
    // alert(document.getElementsByName('checklist')[0].value);
    $.ajax({
        method: 'POST',
        url: url,
        data: {body:document.getElementsByName('checklist')[0].value, postId: '', _token: token}
    })
        .done(function(msg){
            console.log(msg['message'])
            var text = "";
            for(var i = 0; i < msg['message'].length; i++)
            {

                // text += ' <li><a href="#"><i class="fa fa-circle-o text-red"></i>'+msg['message'][i]['nombre']+'</a></li>'
                text += '<li> <label>  <input  type="radio" name="r1" class="iradio_flat-green"> '+msg['message'][i]['nombre']+ '</label> </li>'
            }
            $('.ajax-content').html( text )
        } );
})